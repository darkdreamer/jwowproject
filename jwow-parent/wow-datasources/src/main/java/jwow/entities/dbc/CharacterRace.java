/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.entities.dbc;

import jwow.dbc.support.DbcField;
import jwow.dbc.support.DbcRecord;

/**
 *
 * @author dreamer
 */
@DbcRecord(value = "nxixiixixxxxixssssssssssssssssxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxi")
public final class CharacterRace {
    @DbcField(index = 0)
    public int RaceID; // 0
    @DbcField(index = 2)
    public int FactionID; // 2 facton template id
    @DbcField(index = 4)
    public int model_m; // 4
    @DbcField(index = 5)
    public int model_f; // 5
    @DbcField(index = 7)
    public int TeamID; // 7 (7-Alliance 1-Horde)                                                            // 8-11 unused
    @DbcField(index = 12)
    public int CinematicSequence; // 12 id from CinematicSequences.dbc
    @DbcField(index = 14, count = 15)
    public String[] name; // 14-29 used for DBC language detection/selection
    @DbcField(index = 68)
    public int expansion;
    
}
