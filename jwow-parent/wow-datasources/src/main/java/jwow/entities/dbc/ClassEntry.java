/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.entities.dbc;

import jwow.dbc.support.DbcField;
import jwow.dbc.support.DbcRecord;

/**
 *
 * @author dreamer
 */
@DbcRecord("nxixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxixii")
public class ClassEntry {

    @DbcField(index = 0)
    public int ClassID;                                        // 0
    // 1, unused
    @DbcField(index = 2)
    public int powerType;                                      // 2
    // 3-4, unused
    //char*       name[16];                                 // 5-20 unused
    // 21 string flag, unused
    //char*       nameFemale[16];                           // 21-36 unused, if different from base (male) case
    // 37 string flag, unused
    //char*       nameNeutralGender[16];                    // 38-53 unused, if different from base (male) case
    // 54 string flag, unused
    // 55, unused
    @DbcField(index = 56)
    public int spellfamily;                                    // 56
    // 57, unused
    @DbcField(index = 58)
    public int CinematicSequence;                              // 58 id from CinematicSequences.dbc
    @DbcField(index = 59)
    public int expansion;                                       // 59 (0 - original race, 1 - tbc addon, ...)
}
