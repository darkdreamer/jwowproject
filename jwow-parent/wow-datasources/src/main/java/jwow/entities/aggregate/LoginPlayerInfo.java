/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.aggregate;

import java.util.Date;
import java.util.List;
import jwow.entities.characters.AccountInstanceTimes;
import jwow.entities.characters.ArenaTeamMember;
import jwow.entities.characters.CharacterAccountData;
import jwow.entities.characters.CharacterAchievement;
import jwow.entities.characters.CharacterAchievementProgress;
import jwow.entities.characters.CharacterAction;
import jwow.entities.characters.CharacterAura;
import jwow.entities.characters.CharacterBanned;
import jwow.entities.characters.CharacterBattlegroundData;
import jwow.entities.characters.CharacterBattlegroundRandom;
import jwow.entities.characters.CharacterDeclinedname;
import jwow.entities.characters.CharacterEquipmentsets;
import jwow.entities.characters.CharacterGlyphs;
import jwow.entities.characters.CharacterHomebind;
import jwow.entities.characters.CharacterInstance;
import jwow.entities.characters.CharacterInventory;
import jwow.entities.characters.CharacterQueststatus;
import jwow.entities.characters.CharacterQueststatusDaily;
import jwow.entities.characters.CharacterQueststatusMonthly;
import jwow.entities.characters.CharacterQueststatusRewarded;
import jwow.entities.characters.CharacterQueststatusSeasonal;
import jwow.entities.characters.CharacterQueststatusWeekly;
import jwow.entities.characters.CharacterReputation;
import jwow.entities.characters.CharacterSocial;
import jwow.entities.characters.CharacterSpell;
import jwow.entities.characters.CharacterSpellCooldown;
import jwow.entities.characters.CharacterTalent;
import jwow.entities.characters.Characters;

/**
 *
 * @author dreamer
 */
public class LoginPlayerInfo {
    public Characters character;
    public Integer groupGuid;
    public List<CharacterInstance> characterInstance;
    public List<CharacterAura> characterAuras;
    public List<CharacterSpell> characterSpells;
    public List<CharacterQueststatus> characterQuestStatuses;
    public List<CharacterQueststatusDaily> characterQuestStatusDailies;
    public List<CharacterQueststatusWeekly> characterQuestStatusWeeklies;
    public List<CharacterQueststatusMonthly> characterQuestStatusMonthlies;
    public List<CharacterQueststatusSeasonal> characterQueststatusSeasonals;
    public List<CharacterReputation> characterReputations;
    public List<CharacterInventory> characterInventory;
    public List<CharacterAction> characterActions;
    public int mailCount;
    public Date mailDate;
    public List<CharacterSocial> characterSocialList;
    public CharacterHomebind characterHomebind;
    public List<CharacterSpellCooldown> characterSpellCooldowns;
    public List<CharacterDeclinedname> characterDeclinednames;
//    public GuildMember guildMember;   nopk yet
    public List<ArenaTeamMember> arenaTeamMembership;
    public List<CharacterAchievement> characterAchievements;
    public List<CharacterAchievementProgress> achievementProgresses;
    public List<CharacterEquipmentsets> equipmentSets;
    public CharacterBattlegroundData battlegroundData;
    public List<CharacterGlyphs> characterGlyphs;
    public List<CharacterTalent> characterTalents;
    public List<CharacterAccountData> characterAccountData;
    public CharacterBattlegroundRandom battlegroundRandom;
    public CharacterBanned characterBanned;
    public List<CharacterQueststatusRewarded> rewardedQuests;
    public List<AccountInstanceTimes> instanceLockTimes;
    
}
