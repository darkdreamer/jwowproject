/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.entities.aggregate;

import java.util.Arrays;

/**
 *
 * @author prog_lf
 */
public class PlayerLevelInfo {

    public short[] stats;

    public PlayerLevelInfo() {
        stats = new short[5];
        Arrays.fill(stats, (short)0);
    }
}
