/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.entities.aggregate;

import java.util.List;

/**
 *
 * @author prog_lf
 */
public class PlayerInfo {
    // existence checked by displayId != 0             // existence checked by displayId != 0

    public PlayerInfo() 
    {
        displayId_m=0;
        displayId_f=0;
    }

    public int mapId;
    public int areaId;
    public float positionX;
    public float positionY;
    public float positionZ;
    public float orientation;
    public short phaseMap;
    public short displayId_m;
    public short displayId_f;
    
    public List<PlayerCreateInfoItem> item;
    public List<Integer> spell;
    public List<PlayerCreateInfoAction> action;
    public PlayerLevelInfo  []levelInfo ;                             //[level-1] 0..MaxPlayerLevel-1
}
