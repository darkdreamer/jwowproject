/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.entities.aggregate;

/**
 *
 * @author prog_lf
 */
public class PlayerCreateInfoAction {

    public PlayerCreateInfoAction() {
        button = 0;
        type = 0;
        action = 0;
    }

    public PlayerCreateInfoAction(byte button, byte type, int action) {
        this.button = button;
        this.type = type;
        this.action = action;
    }
    
    public byte button;
    public byte type;
    public int action;
}
