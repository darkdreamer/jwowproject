/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.entities.aggregate;

/**
 *
 * @author prog_lf
 */
public class PlayerCreateInfoItem {

    public PlayerCreateInfoItem(int item_id, int item_amount) {
        this.item_id = item_id;
        this.item_amount = item_amount;
    }
    

    public int item_id;
    public int item_amount;
}
