/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_queststatus_monthly")
@NamedQueries({
    @NamedQuery(name = "CharacterQueststatusMonthly.findAll", query = "SELECT c FROM CharacterQueststatusMonthly c"),
    @NamedQuery(name = "CharacterQueststatusMonthly.findByGuid", query = "SELECT c FROM CharacterQueststatusMonthly c WHERE c.characterQueststatusMonthlyPK.guid = :guid"),
    @NamedQuery(name = "CharacterQueststatusMonthly.findByQuest", query = "SELECT c FROM CharacterQueststatusMonthly c WHERE c.characterQueststatusMonthlyPK.quest = :quest")})
public class CharacterQueststatusMonthly implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterQueststatusMonthlyPK characterQueststatusMonthlyPK;

    public CharacterQueststatusMonthly() {
    }

    public CharacterQueststatusMonthly(CharacterQueststatusMonthlyPK characterQueststatusMonthlyPK) {
        this.characterQueststatusMonthlyPK = characterQueststatusMonthlyPK;
    }

    public CharacterQueststatusMonthly(int guid, int quest) {
        this.characterQueststatusMonthlyPK = new CharacterQueststatusMonthlyPK(guid, quest);
    }

    public CharacterQueststatusMonthlyPK getCharacterQueststatusMonthlyPK() {
        return characterQueststatusMonthlyPK;
    }

    public void setCharacterQueststatusMonthlyPK(CharacterQueststatusMonthlyPK characterQueststatusMonthlyPK) {
        this.characterQueststatusMonthlyPK = characterQueststatusMonthlyPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterQueststatusMonthlyPK != null ? characterQueststatusMonthlyPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterQueststatusMonthly)) {
            return false;
        }
        CharacterQueststatusMonthly other = (CharacterQueststatusMonthly) object;
        if ((this.characterQueststatusMonthlyPK == null && other.characterQueststatusMonthlyPK != null) || (this.characterQueststatusMonthlyPK != null && !this.characterQueststatusMonthlyPK.equals(other.characterQueststatusMonthlyPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterQueststatusMonthly[ characterQueststatusMonthlyPK=" + characterQueststatusMonthlyPK + " ]";
    }
    
}
