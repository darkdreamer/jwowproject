/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_arena_stats")
@NamedQueries({
    @NamedQuery(name = "CharacterArenaStats.findAll", query = "SELECT c FROM CharacterArenaStats c"),
    @NamedQuery(name = "CharacterArenaStats.findByGuid", query = "SELECT c FROM CharacterArenaStats c WHERE c.characterArenaStatsPK.guid = :guid"),
    @NamedQuery(name = "CharacterArenaStats.findBySlot", query = "SELECT c FROM CharacterArenaStats c WHERE c.characterArenaStatsPK.slot = :slot"),
    @NamedQuery(name = "CharacterArenaStats.findByMatchMakerRating", query = "SELECT c FROM CharacterArenaStats c WHERE c.matchMakerRating = :matchMakerRating")})
public class CharacterArenaStats implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterArenaStatsPK characterArenaStatsPK;
    @Basic(optional = false)
    @Column(name = "matchMakerRating")
    private short matchMakerRating;

    public CharacterArenaStats() {
    }

    public CharacterArenaStats(CharacterArenaStatsPK characterArenaStatsPK) {
        this.characterArenaStatsPK = characterArenaStatsPK;
    }

    public CharacterArenaStats(CharacterArenaStatsPK characterArenaStatsPK, short matchMakerRating) {
        this.characterArenaStatsPK = characterArenaStatsPK;
        this.matchMakerRating = matchMakerRating;
    }

    public CharacterArenaStats(int guid, short slot) {
        this.characterArenaStatsPK = new CharacterArenaStatsPK(guid, slot);
    }

    public CharacterArenaStatsPK getCharacterArenaStatsPK() {
        return characterArenaStatsPK;
    }

    public void setCharacterArenaStatsPK(CharacterArenaStatsPK characterArenaStatsPK) {
        this.characterArenaStatsPK = characterArenaStatsPK;
    }

    public short getMatchMakerRating() {
        return matchMakerRating;
    }

    public void setMatchMakerRating(short matchMakerRating) {
        this.matchMakerRating = matchMakerRating;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterArenaStatsPK != null ? characterArenaStatsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterArenaStats)) {
            return false;
        }
        CharacterArenaStats other = (CharacterArenaStats) object;
        if ((this.characterArenaStatsPK == null && other.characterArenaStatsPK != null) || (this.characterArenaStatsPK != null && !this.characterArenaStatsPK.equals(other.characterArenaStatsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterArenaStats[ characterArenaStatsPK=" + characterArenaStatsPK + " ]";
    }
    
}
