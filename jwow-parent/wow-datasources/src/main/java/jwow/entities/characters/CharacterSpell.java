/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_spell")
@NamedQueries({
    @NamedQuery(name = "CharacterSpell.findAll", query = "SELECT c FROM CharacterSpell c"),
    @NamedQuery(name = "CharacterSpell.findByGuid", query = "SELECT c FROM CharacterSpell c WHERE c.characterSpellPK.guid = :guid"),
    @NamedQuery(name = "CharacterSpell.findBySpell", query = "SELECT c FROM CharacterSpell c WHERE c.characterSpellPK.spell = :spell"),
    @NamedQuery(name = "CharacterSpell.findByActive", query = "SELECT c FROM CharacterSpell c WHERE c.active = :active"),
    @NamedQuery(name = "CharacterSpell.findByDisabled", query = "SELECT c FROM CharacterSpell c WHERE c.disabled = :disabled")})
public class CharacterSpell implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterSpellPK characterSpellPK;
    @Basic(optional = false)
    @Column(name = "active")
    private short active;
    @Basic(optional = false)
    @Column(name = "disabled")
    private short disabled;

    public CharacterSpell() {
    }

    public CharacterSpell(CharacterSpellPK characterSpellPK) {
        this.characterSpellPK = characterSpellPK;
    }

    public CharacterSpell(CharacterSpellPK characterSpellPK, short active, short disabled) {
        this.characterSpellPK = characterSpellPK;
        this.active = active;
        this.disabled = disabled;
    }

    public CharacterSpell(int guid, int spell) {
        this.characterSpellPK = new CharacterSpellPK(guid, spell);
    }

    public CharacterSpellPK getCharacterSpellPK() {
        return characterSpellPK;
    }

    public void setCharacterSpellPK(CharacterSpellPK characterSpellPK) {
        this.characterSpellPK = characterSpellPK;
    }

    public short getActive() {
        return active;
    }

    public void setActive(short active) {
        this.active = active;
    }

    public short getDisabled() {
        return disabled;
    }

    public void setDisabled(short disabled) {
        this.disabled = disabled;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterSpellPK != null ? characterSpellPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterSpell)) {
            return false;
        }
        CharacterSpell other = (CharacterSpell) object;
        if ((this.characterSpellPK == null && other.characterSpellPK != null) || (this.characterSpellPK != null && !this.characterSpellPK.equals(other.characterSpellPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterSpell[ characterSpellPK=" + characterSpellPK + " ]";
    }
    
}
