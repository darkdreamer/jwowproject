/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "lag_reports")
@NamedQueries({
    @NamedQuery(name = "LagReports.findAll", query = "SELECT l FROM LagReports l"),
    @NamedQuery(name = "LagReports.findByReportId", query = "SELECT l FROM LagReports l WHERE l.reportId = :reportId"),
    @NamedQuery(name = "LagReports.findByGuid", query = "SELECT l FROM LagReports l WHERE l.guid = :guid"),
    @NamedQuery(name = "LagReports.findByLagType", query = "SELECT l FROM LagReports l WHERE l.lagType = :lagType"),
    @NamedQuery(name = "LagReports.findByMapId", query = "SELECT l FROM LagReports l WHERE l.mapId = :mapId"),
    @NamedQuery(name = "LagReports.findByPosX", query = "SELECT l FROM LagReports l WHERE l.posX = :posX"),
    @NamedQuery(name = "LagReports.findByPosY", query = "SELECT l FROM LagReports l WHERE l.posY = :posY"),
    @NamedQuery(name = "LagReports.findByPosZ", query = "SELECT l FROM LagReports l WHERE l.posZ = :posZ"),
    @NamedQuery(name = "LagReports.findByLatency", query = "SELECT l FROM LagReports l WHERE l.latency = :latency"),
    @NamedQuery(name = "LagReports.findByCreateTime", query = "SELECT l FROM LagReports l WHERE l.createTime = :createTime")})
public class LagReports implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "reportId")
    private Integer reportId;
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "lagType")
    private short lagType;
    @Basic(optional = false)
    @Column(name = "mapId")
    private short mapId;
    @Basic(optional = false)
    @Column(name = "posX")
    private float posX;
    @Basic(optional = false)
    @Column(name = "posY")
    private float posY;
    @Basic(optional = false)
    @Column(name = "posZ")
    private float posZ;
    @Basic(optional = false)
    @Column(name = "latency")
    private int latency;
    @Basic(optional = false)
    @Column(name = "createTime")
    private int createTime;

    public LagReports() {
    }

    public LagReports(Integer reportId) {
        this.reportId = reportId;
    }

    public LagReports(Integer reportId, int guid, short lagType, short mapId, float posX, float posY, float posZ, int latency, int createTime) {
        this.reportId = reportId;
        this.guid = guid;
        this.lagType = lagType;
        this.mapId = mapId;
        this.posX = posX;
        this.posY = posY;
        this.posZ = posZ;
        this.latency = latency;
        this.createTime = createTime;
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public short getLagType() {
        return lagType;
    }

    public void setLagType(short lagType) {
        this.lagType = lagType;
    }

    public short getMapId() {
        return mapId;
    }

    public void setMapId(short mapId) {
        this.mapId = mapId;
    }

    public float getPosX() {
        return posX;
    }

    public void setPosX(float posX) {
        this.posX = posX;
    }

    public float getPosY() {
        return posY;
    }

    public void setPosY(float posY) {
        this.posY = posY;
    }

    public float getPosZ() {
        return posZ;
    }

    public void setPosZ(float posZ) {
        this.posZ = posZ;
    }

    public int getLatency() {
        return latency;
    }

    public void setLatency(int latency) {
        this.latency = latency;
    }

    public int getCreateTime() {
        return createTime;
    }

    public void setCreateTime(int createTime) {
        this.createTime = createTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reportId != null ? reportId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LagReports)) {
            return false;
        }
        LagReports other = (LagReports) object;
        if ((this.reportId == null && other.reportId != null) || (this.reportId != null && !this.reportId.equals(other.reportId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.LagReports[ reportId=" + reportId + " ]";
    }
    
}
