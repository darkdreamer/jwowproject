/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "guild_bank_tab")
@NamedQueries({
    @NamedQuery(name = "GuildBankTab.findAll", query = "SELECT g FROM GuildBankTab g"),
    @NamedQuery(name = "GuildBankTab.findByGuildid", query = "SELECT g FROM GuildBankTab g WHERE g.guildBankTabPK.guildid = :guildid"),
    @NamedQuery(name = "GuildBankTab.findByTabId", query = "SELECT g FROM GuildBankTab g WHERE g.guildBankTabPK.tabId = :tabId"),
    @NamedQuery(name = "GuildBankTab.findByTabName", query = "SELECT g FROM GuildBankTab g WHERE g.tabName = :tabName"),
    @NamedQuery(name = "GuildBankTab.findByTabIcon", query = "SELECT g FROM GuildBankTab g WHERE g.tabIcon = :tabIcon"),
    @NamedQuery(name = "GuildBankTab.findByTabText", query = "SELECT g FROM GuildBankTab g WHERE g.tabText = :tabText")})
public class GuildBankTab implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GuildBankTabPK guildBankTabPK;
    @Basic(optional = false)
    @Column(name = "TabName")
    private String tabName;
    @Basic(optional = false)
    @Column(name = "TabIcon")
    private String tabIcon;
    @Column(name = "TabText")
    private String tabText;

    public GuildBankTab() {
    }

    public GuildBankTab(GuildBankTabPK guildBankTabPK) {
        this.guildBankTabPK = guildBankTabPK;
    }

    public GuildBankTab(GuildBankTabPK guildBankTabPK, String tabName, String tabIcon) {
        this.guildBankTabPK = guildBankTabPK;
        this.tabName = tabName;
        this.tabIcon = tabIcon;
    }

    public GuildBankTab(int guildid, short tabId) {
        this.guildBankTabPK = new GuildBankTabPK(guildid, tabId);
    }

    public GuildBankTabPK getGuildBankTabPK() {
        return guildBankTabPK;
    }

    public void setGuildBankTabPK(GuildBankTabPK guildBankTabPK) {
        this.guildBankTabPK = guildBankTabPK;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public String getTabIcon() {
        return tabIcon;
    }

    public void setTabIcon(String tabIcon) {
        this.tabIcon = tabIcon;
    }

    public String getTabText() {
        return tabText;
    }

    public void setTabText(String tabText) {
        this.tabText = tabText;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guildBankTabPK != null ? guildBankTabPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuildBankTab)) {
            return false;
        }
        GuildBankTab other = (GuildBankTab) object;
        if ((this.guildBankTabPK == null && other.guildBankTabPK != null) || (this.guildBankTabPK != null && !this.guildBankTabPK.equals(other.guildBankTabPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GuildBankTab[ guildBankTabPK=" + guildBankTabPK + " ]";
    }
    
}
