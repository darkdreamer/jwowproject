/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "account_data")
@NamedQueries({
    @NamedQuery(name = "AccountData.findAll", query = "SELECT a FROM AccountData a"),
    @NamedQuery(name = "AccountData.findByAccountId", query = "SELECT a FROM AccountData a WHERE a.accountDataPK.accountId = :accountId"),
    @NamedQuery(name = "AccountData.findByType", query = "SELECT a FROM AccountData a WHERE a.accountDataPK.type = :type"),
    @NamedQuery(name = "AccountData.findByTime", query = "SELECT a FROM AccountData a WHERE a.time = :time")})
public class AccountData implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AccountDataPK accountDataPK;
    @Basic(optional = false)
    @Column(name = "time")
    private int time;
    @Basic(optional = false)
    @Lob
    @Column(name = "data")
    private byte[] data;

    public AccountData() {
    }

    public AccountData(AccountDataPK accountDataPK) {
        this.accountDataPK = accountDataPK;
    }

    public AccountData(AccountDataPK accountDataPK, int time, byte[] data) {
        this.accountDataPK = accountDataPK;
        this.time = time;
        this.data = data;
    }

    public AccountData(int accountId, short type) {
        this.accountDataPK = new AccountDataPK(accountId, type);
    }

    public AccountDataPK getAccountDataPK() {
        return accountDataPK;
    }

    public void setAccountDataPK(AccountDataPK accountDataPK) {
        this.accountDataPK = accountDataPK;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountDataPK != null ? accountDataPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountData)) {
            return false;
        }
        AccountData other = (AccountData) object;
        if ((this.accountDataPK == null && other.accountDataPK != null) || (this.accountDataPK != null && !this.accountDataPK.equals(other.accountDataPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.AccountData[ accountDataPK=" + accountDataPK + " ]";
    }
    
}
