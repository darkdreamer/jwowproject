/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterArenaStatsPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "slot")
    private short slot;

    public CharacterArenaStatsPK() {
    }

    public CharacterArenaStatsPK(int guid, short slot) {
        this.guid = guid;
        this.slot = slot;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public short getSlot() {
        return slot;
    }

    public void setSlot(short slot) {
        this.slot = slot;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) slot;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterArenaStatsPK)) {
            return false;
        }
        CharacterArenaStatsPK other = (CharacterArenaStatsPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.slot != other.slot) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterArenaStatsPK[ guid=" + guid + ", slot=" + slot + " ]";
    }
    
}
