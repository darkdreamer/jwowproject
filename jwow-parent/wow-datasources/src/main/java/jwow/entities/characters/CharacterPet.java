/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_pet")
@NamedQueries({
    @NamedQuery(name = "CharacterPet.findAll", query = "SELECT c FROM CharacterPet c"),
    @NamedQuery(name = "CharacterPet.findById", query = "SELECT c FROM CharacterPet c WHERE c.id = :id"),
    @NamedQuery(name = "CharacterPet.findByEntry", query = "SELECT c FROM CharacterPet c WHERE c.entry = :entry"),
    @NamedQuery(name = "CharacterPet.findByOwner", query = "SELECT c FROM CharacterPet c WHERE c.owner = :owner"),
    @NamedQuery(name = "CharacterPet.findByModelid", query = "SELECT c FROM CharacterPet c WHERE c.modelid = :modelid"),
    @NamedQuery(name = "CharacterPet.findByCreatedBySpell", query = "SELECT c FROM CharacterPet c WHERE c.createdBySpell = :createdBySpell"),
    @NamedQuery(name = "CharacterPet.findByPetType", query = "SELECT c FROM CharacterPet c WHERE c.petType = :petType"),
    @NamedQuery(name = "CharacterPet.findByLevel", query = "SELECT c FROM CharacterPet c WHERE c.level = :level"),
    @NamedQuery(name = "CharacterPet.findByExp", query = "SELECT c FROM CharacterPet c WHERE c.exp = :exp"),
    @NamedQuery(name = "CharacterPet.findByReactstate", query = "SELECT c FROM CharacterPet c WHERE c.reactstate = :reactstate"),
    @NamedQuery(name = "CharacterPet.findByName", query = "SELECT c FROM CharacterPet c WHERE c.name = :name"),
    @NamedQuery(name = "CharacterPet.findByRenamed", query = "SELECT c FROM CharacterPet c WHERE c.renamed = :renamed"),
    @NamedQuery(name = "CharacterPet.findBySlot", query = "SELECT c FROM CharacterPet c WHERE c.slot = :slot"),
    @NamedQuery(name = "CharacterPet.findByCurhealth", query = "SELECT c FROM CharacterPet c WHERE c.curhealth = :curhealth"),
    @NamedQuery(name = "CharacterPet.findByCurmana", query = "SELECT c FROM CharacterPet c WHERE c.curmana = :curmana"),
    @NamedQuery(name = "CharacterPet.findByCurhappiness", query = "SELECT c FROM CharacterPet c WHERE c.curhappiness = :curhappiness"),
    @NamedQuery(name = "CharacterPet.findBySavetime", query = "SELECT c FROM CharacterPet c WHERE c.savetime = :savetime")})
public class CharacterPet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "entry")
    private int entry;
    @Basic(optional = false)
    @Column(name = "owner")
    private int owner;
    @Column(name = "modelid")
    private Integer modelid;
    @Basic(optional = false)
    @Column(name = "CreatedBySpell")
    private int createdBySpell;
    @Basic(optional = false)
    @Column(name = "PetType")
    private short petType;
    @Basic(optional = false)
    @Column(name = "level")
    private short level;
    @Basic(optional = false)
    @Column(name = "exp")
    private int exp;
    @Basic(optional = false)
    @Column(name = "Reactstate")
    private short reactstate;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "renamed")
    private short renamed;
    @Basic(optional = false)
    @Column(name = "slot")
    private short slot;
    @Basic(optional = false)
    @Column(name = "curhealth")
    private int curhealth;
    @Basic(optional = false)
    @Column(name = "curmana")
    private int curmana;
    @Basic(optional = false)
    @Column(name = "curhappiness")
    private int curhappiness;
    @Basic(optional = false)
    @Column(name = "savetime")
    private int savetime;
    @Lob
    @Column(name = "abdata")
    private String abdata;

    public CharacterPet() {
    }

    public CharacterPet(Integer id) {
        this.id = id;
    }

    public CharacterPet(Integer id, int entry, int owner, int createdBySpell, short petType, short level, int exp, short reactstate, String name, short renamed, short slot, int curhealth, int curmana, int curhappiness, int savetime) {
        this.id = id;
        this.entry = entry;
        this.owner = owner;
        this.createdBySpell = createdBySpell;
        this.petType = petType;
        this.level = level;
        this.exp = exp;
        this.reactstate = reactstate;
        this.name = name;
        this.renamed = renamed;
        this.slot = slot;
        this.curhealth = curhealth;
        this.curmana = curmana;
        this.curhappiness = curhappiness;
        this.savetime = savetime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getEntry() {
        return entry;
    }

    public void setEntry(int entry) {
        this.entry = entry;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public Integer getModelid() {
        return modelid;
    }

    public void setModelid(Integer modelid) {
        this.modelid = modelid;
    }

    public int getCreatedBySpell() {
        return createdBySpell;
    }

    public void setCreatedBySpell(int createdBySpell) {
        this.createdBySpell = createdBySpell;
    }

    public short getPetType() {
        return petType;
    }

    public void setPetType(short petType) {
        this.petType = petType;
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public short getReactstate() {
        return reactstate;
    }

    public void setReactstate(short reactstate) {
        this.reactstate = reactstate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getRenamed() {
        return renamed;
    }

    public void setRenamed(short renamed) {
        this.renamed = renamed;
    }

    public short getSlot() {
        return slot;
    }

    public void setSlot(short slot) {
        this.slot = slot;
    }

    public int getCurhealth() {
        return curhealth;
    }

    public void setCurhealth(int curhealth) {
        this.curhealth = curhealth;
    }

    public int getCurmana() {
        return curmana;
    }

    public void setCurmana(int curmana) {
        this.curmana = curmana;
    }

    public int getCurhappiness() {
        return curhappiness;
    }

    public void setCurhappiness(int curhappiness) {
        this.curhappiness = curhappiness;
    }

    public int getSavetime() {
        return savetime;
    }

    public void setSavetime(int savetime) {
        this.savetime = savetime;
    }

    public String getAbdata() {
        return abdata;
    }

    public void setAbdata(String abdata) {
        this.abdata = abdata;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterPet)) {
            return false;
        }
        CharacterPet other = (CharacterPet) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterPet[ id=" + id + " ]";
    }
    
}
