/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_queststatus")
@NamedQueries({
    @NamedQuery(name = "CharacterQueststatus.findAll", query = "SELECT c FROM CharacterQueststatus c"),
    @NamedQuery(name = "CharacterQueststatus.findByGuid", query = "SELECT c FROM CharacterQueststatus c WHERE c.characterQueststatusPK.guid = :guid"),
    @NamedQuery(name = "CharacterQueststatus.findByQuest", query = "SELECT c FROM CharacterQueststatus c WHERE c.characterQueststatusPK.quest = :quest"),
    @NamedQuery(name = "CharacterQueststatus.findByStatus", query = "SELECT c FROM CharacterQueststatus c WHERE c.status = :status"),
    @NamedQuery(name = "CharacterQueststatus.findByExplored", query = "SELECT c FROM CharacterQueststatus c WHERE c.explored = :explored"),
    @NamedQuery(name = "CharacterQueststatus.findByTimer", query = "SELECT c FROM CharacterQueststatus c WHERE c.timer = :timer"),
    @NamedQuery(name = "CharacterQueststatus.findByMobcount1", query = "SELECT c FROM CharacterQueststatus c WHERE c.mobcount1 = :mobcount1"),
    @NamedQuery(name = "CharacterQueststatus.findByMobcount2", query = "SELECT c FROM CharacterQueststatus c WHERE c.mobcount2 = :mobcount2"),
    @NamedQuery(name = "CharacterQueststatus.findByMobcount3", query = "SELECT c FROM CharacterQueststatus c WHERE c.mobcount3 = :mobcount3"),
    @NamedQuery(name = "CharacterQueststatus.findByMobcount4", query = "SELECT c FROM CharacterQueststatus c WHERE c.mobcount4 = :mobcount4"),
    @NamedQuery(name = "CharacterQueststatus.findByItemcount1", query = "SELECT c FROM CharacterQueststatus c WHERE c.itemcount1 = :itemcount1"),
    @NamedQuery(name = "CharacterQueststatus.findByItemcount2", query = "SELECT c FROM CharacterQueststatus c WHERE c.itemcount2 = :itemcount2"),
    @NamedQuery(name = "CharacterQueststatus.findByItemcount3", query = "SELECT c FROM CharacterQueststatus c WHERE c.itemcount3 = :itemcount3"),
    @NamedQuery(name = "CharacterQueststatus.findByItemcount4", query = "SELECT c FROM CharacterQueststatus c WHERE c.itemcount4 = :itemcount4"),
    @NamedQuery(name = "CharacterQueststatus.findByPlayercount", query = "SELECT c FROM CharacterQueststatus c WHERE c.playercount = :playercount")})
public class CharacterQueststatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterQueststatusPK characterQueststatusPK;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @Basic(optional = false)
    @Column(name = "explored")
    private short explored;
    @Basic(optional = false)
    @Column(name = "timer")
    private int timer;
    @Basic(optional = false)
    @Column(name = "mobcount1")
    private short mobcount1;
    @Basic(optional = false)
    @Column(name = "mobcount2")
    private short mobcount2;
    @Basic(optional = false)
    @Column(name = "mobcount3")
    private short mobcount3;
    @Basic(optional = false)
    @Column(name = "mobcount4")
    private short mobcount4;
    @Basic(optional = false)
    @Column(name = "itemcount1")
    private short itemcount1;
    @Basic(optional = false)
    @Column(name = "itemcount2")
    private short itemcount2;
    @Basic(optional = false)
    @Column(name = "itemcount3")
    private short itemcount3;
    @Basic(optional = false)
    @Column(name = "itemcount4")
    private short itemcount4;
    @Basic(optional = false)
    @Column(name = "playercount")
    private short playercount;

    public CharacterQueststatus() {
    }

    public CharacterQueststatus(CharacterQueststatusPK characterQueststatusPK) {
        this.characterQueststatusPK = characterQueststatusPK;
    }

    public CharacterQueststatus(CharacterQueststatusPK characterQueststatusPK, short status, short explored, int timer, short mobcount1, short mobcount2, short mobcount3, short mobcount4, short itemcount1, short itemcount2, short itemcount3, short itemcount4, short playercount) {
        this.characterQueststatusPK = characterQueststatusPK;
        this.status = status;
        this.explored = explored;
        this.timer = timer;
        this.mobcount1 = mobcount1;
        this.mobcount2 = mobcount2;
        this.mobcount3 = mobcount3;
        this.mobcount4 = mobcount4;
        this.itemcount1 = itemcount1;
        this.itemcount2 = itemcount2;
        this.itemcount3 = itemcount3;
        this.itemcount4 = itemcount4;
        this.playercount = playercount;
    }

    public CharacterQueststatus(int guid, int quest) {
        this.characterQueststatusPK = new CharacterQueststatusPK(guid, quest);
    }

    public CharacterQueststatusPK getCharacterQueststatusPK() {
        return characterQueststatusPK;
    }

    public void setCharacterQueststatusPK(CharacterQueststatusPK characterQueststatusPK) {
        this.characterQueststatusPK = characterQueststatusPK;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public short getExplored() {
        return explored;
    }

    public void setExplored(short explored) {
        this.explored = explored;
    }

    public int getTimer() {
        return timer;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    public short getMobcount1() {
        return mobcount1;
    }

    public void setMobcount1(short mobcount1) {
        this.mobcount1 = mobcount1;
    }

    public short getMobcount2() {
        return mobcount2;
    }

    public void setMobcount2(short mobcount2) {
        this.mobcount2 = mobcount2;
    }

    public short getMobcount3() {
        return mobcount3;
    }

    public void setMobcount3(short mobcount3) {
        this.mobcount3 = mobcount3;
    }

    public short getMobcount4() {
        return mobcount4;
    }

    public void setMobcount4(short mobcount4) {
        this.mobcount4 = mobcount4;
    }

    public short getItemcount1() {
        return itemcount1;
    }

    public void setItemcount1(short itemcount1) {
        this.itemcount1 = itemcount1;
    }

    public short getItemcount2() {
        return itemcount2;
    }

    public void setItemcount2(short itemcount2) {
        this.itemcount2 = itemcount2;
    }

    public short getItemcount3() {
        return itemcount3;
    }

    public void setItemcount3(short itemcount3) {
        this.itemcount3 = itemcount3;
    }

    public short getItemcount4() {
        return itemcount4;
    }

    public void setItemcount4(short itemcount4) {
        this.itemcount4 = itemcount4;
    }

    public short getPlayercount() {
        return playercount;
    }

    public void setPlayercount(short playercount) {
        this.playercount = playercount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterQueststatusPK != null ? characterQueststatusPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterQueststatus)) {
            return false;
        }
        CharacterQueststatus other = (CharacterQueststatus) object;
        if ((this.characterQueststatusPK == null && other.characterQueststatusPK != null) || (this.characterQueststatusPK != null && !this.characterQueststatusPK.equals(other.characterQueststatusPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterQueststatus[ characterQueststatusPK=" + characterQueststatusPK + " ]";
    }
    
}
