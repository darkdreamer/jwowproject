/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_battleground_random")
@NamedQueries({
    @NamedQuery(name = "CharacterBattlegroundRandom.findAll", query = "SELECT c FROM CharacterBattlegroundRandom c"),
    @NamedQuery(name = "CharacterBattlegroundRandom.findByGuid", query = "SELECT c FROM CharacterBattlegroundRandom c WHERE c.guid = :guid")})
public class CharacterBattlegroundRandom implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "guid")
    private Integer guid;

    public CharacterBattlegroundRandom() {
    }

    public CharacterBattlegroundRandom(Integer guid) {
        this.guid = guid;
    }

    public Integer getGuid() {
        return guid;
    }

    public void setGuid(Integer guid) {
        this.guid = guid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guid != null ? guid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterBattlegroundRandom)) {
            return false;
        }
        CharacterBattlegroundRandom other = (CharacterBattlegroundRandom) object;
        if ((this.guid == null && other.guid != null) || (this.guid != null && !this.guid.equals(other.guid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterBattlegroundRandom[ guid=" + guid + " ]";
    }
    
}
