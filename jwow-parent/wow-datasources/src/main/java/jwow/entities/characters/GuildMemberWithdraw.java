/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "guild_member_withdraw")
@NamedQueries({
    @NamedQuery(name = "GuildMemberWithdraw.findAll", query = "SELECT g FROM GuildMemberWithdraw g"),
    @NamedQuery(name = "GuildMemberWithdraw.findByGuid", query = "SELECT g FROM GuildMemberWithdraw g WHERE g.guid = :guid"),
    @NamedQuery(name = "GuildMemberWithdraw.findByTab0", query = "SELECT g FROM GuildMemberWithdraw g WHERE g.tab0 = :tab0"),
    @NamedQuery(name = "GuildMemberWithdraw.findByTab1", query = "SELECT g FROM GuildMemberWithdraw g WHERE g.tab1 = :tab1"),
    @NamedQuery(name = "GuildMemberWithdraw.findByTab2", query = "SELECT g FROM GuildMemberWithdraw g WHERE g.tab2 = :tab2"),
    @NamedQuery(name = "GuildMemberWithdraw.findByTab3", query = "SELECT g FROM GuildMemberWithdraw g WHERE g.tab3 = :tab3"),
    @NamedQuery(name = "GuildMemberWithdraw.findByTab4", query = "SELECT g FROM GuildMemberWithdraw g WHERE g.tab4 = :tab4"),
    @NamedQuery(name = "GuildMemberWithdraw.findByTab5", query = "SELECT g FROM GuildMemberWithdraw g WHERE g.tab5 = :tab5"),
    @NamedQuery(name = "GuildMemberWithdraw.findByMoney", query = "SELECT g FROM GuildMemberWithdraw g WHERE g.money = :money")})
public class GuildMemberWithdraw implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "guid")
    private Integer guid;
    @Basic(optional = false)
    @Column(name = "tab0")
    private int tab0;
    @Basic(optional = false)
    @Column(name = "tab1")
    private int tab1;
    @Basic(optional = false)
    @Column(name = "tab2")
    private int tab2;
    @Basic(optional = false)
    @Column(name = "tab3")
    private int tab3;
    @Basic(optional = false)
    @Column(name = "tab4")
    private int tab4;
    @Basic(optional = false)
    @Column(name = "tab5")
    private int tab5;
    @Basic(optional = false)
    @Column(name = "money")
    private int money;

    public GuildMemberWithdraw() {
    }

    public GuildMemberWithdraw(Integer guid) {
        this.guid = guid;
    }

    public GuildMemberWithdraw(Integer guid, int tab0, int tab1, int tab2, int tab3, int tab4, int tab5, int money) {
        this.guid = guid;
        this.tab0 = tab0;
        this.tab1 = tab1;
        this.tab2 = tab2;
        this.tab3 = tab3;
        this.tab4 = tab4;
        this.tab5 = tab5;
        this.money = money;
    }

    public Integer getGuid() {
        return guid;
    }

    public void setGuid(Integer guid) {
        this.guid = guid;
    }

    public int getTab0() {
        return tab0;
    }

    public void setTab0(int tab0) {
        this.tab0 = tab0;
    }

    public int getTab1() {
        return tab1;
    }

    public void setTab1(int tab1) {
        this.tab1 = tab1;
    }

    public int getTab2() {
        return tab2;
    }

    public void setTab2(int tab2) {
        this.tab2 = tab2;
    }

    public int getTab3() {
        return tab3;
    }

    public void setTab3(int tab3) {
        this.tab3 = tab3;
    }

    public int getTab4() {
        return tab4;
    }

    public void setTab4(int tab4) {
        this.tab4 = tab4;
    }

    public int getTab5() {
        return tab5;
    }

    public void setTab5(int tab5) {
        this.tab5 = tab5;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guid != null ? guid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuildMemberWithdraw)) {
            return false;
        }
        GuildMemberWithdraw other = (GuildMemberWithdraw) object;
        if ((this.guid == null && other.guid != null) || (this.guid != null && !this.guid.equals(other.guid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GuildMemberWithdraw[ guid=" + guid + " ]";
    }
    
}
