/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "game_event_condition_save")
@NamedQueries({
    @NamedQuery(name = "GameEventConditionSave.findAll", query = "SELECT g FROM GameEventConditionSave g"),
    @NamedQuery(name = "GameEventConditionSave.findByEventEntry", query = "SELECT g FROM GameEventConditionSave g WHERE g.gameEventConditionSavePK.eventEntry = :eventEntry"),
    @NamedQuery(name = "GameEventConditionSave.findByConditionId", query = "SELECT g FROM GameEventConditionSave g WHERE g.gameEventConditionSavePK.conditionId = :conditionId"),
    @NamedQuery(name = "GameEventConditionSave.findByDone", query = "SELECT g FROM GameEventConditionSave g WHERE g.done = :done")})
public class GameEventConditionSave implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GameEventConditionSavePK gameEventConditionSavePK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "done")
    private Float done;

    public GameEventConditionSave() {
    }

    public GameEventConditionSave(GameEventConditionSavePK gameEventConditionSavePK) {
        this.gameEventConditionSavePK = gameEventConditionSavePK;
    }

    public GameEventConditionSave(short eventEntry, int conditionId) {
        this.gameEventConditionSavePK = new GameEventConditionSavePK(eventEntry, conditionId);
    }

    public GameEventConditionSavePK getGameEventConditionSavePK() {
        return gameEventConditionSavePK;
    }

    public void setGameEventConditionSavePK(GameEventConditionSavePK gameEventConditionSavePK) {
        this.gameEventConditionSavePK = gameEventConditionSavePK;
    }

    public Float getDone() {
        return done;
    }

    public void setDone(Float done) {
        this.done = done;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gameEventConditionSavePK != null ? gameEventConditionSavePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GameEventConditionSave)) {
            return false;
        }
        GameEventConditionSave other = (GameEventConditionSave) object;
        if ((this.gameEventConditionSavePK == null && other.gameEventConditionSavePK != null) || (this.gameEventConditionSavePK != null && !this.gameEventConditionSavePK.equals(other.gameEventConditionSavePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GameEventConditionSave[ gameEventConditionSavePK=" + gameEventConditionSavePK + " ]";
    }
    
}
