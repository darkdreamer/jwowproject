/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "worldstates")
@NamedQueries({
    @NamedQuery(name = "Worldstates.findAll", query = "SELECT w FROM Worldstates w"),
    @NamedQuery(name = "Worldstates.findByEntry", query = "SELECT w FROM Worldstates w WHERE w.entry = :entry"),
    @NamedQuery(name = "Worldstates.findByValue", query = "SELECT w FROM Worldstates w WHERE w.value = :value"),
    @NamedQuery(name = "Worldstates.findByComment", query = "SELECT w FROM Worldstates w WHERE w.comment = :comment")})
public class Worldstates implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "entry")
    private Integer entry;
    @Basic(optional = false)
    @Column(name = "value")
    private int value;
    @Column(name = "comment")
    private String comment;

    public Worldstates() {
    }

    public Worldstates(Integer entry) {
        this.entry = entry;
    }

    public Worldstates(Integer entry, int value) {
        this.entry = entry;
        this.value = value;
    }

    public Integer getEntry() {
        return entry;
    }

    public void setEntry(Integer entry) {
        this.entry = entry;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (entry != null ? entry.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Worldstates)) {
            return false;
        }
        Worldstates other = (Worldstates) object;
        if ((this.entry == null && other.entry != null) || (this.entry != null && !this.entry.equals(other.entry))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.Worldstates[ entry=" + entry + " ]";
    }
    
}
