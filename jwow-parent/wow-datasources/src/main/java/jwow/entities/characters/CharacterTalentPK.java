/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterTalentPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "spell")
    private int spell;
    @Basic(optional = false)
    @Column(name = "spec")
    private short spec;

    public CharacterTalentPK() {
    }

    public CharacterTalentPK(int guid, int spell, short spec) {
        this.guid = guid;
        this.spell = spell;
        this.spec = spec;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public int getSpell() {
        return spell;
    }

    public void setSpell(int spell) {
        this.spell = spell;
    }

    public short getSpec() {
        return spec;
    }

    public void setSpec(short spec) {
        this.spec = spec;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) spell;
        hash += (int) spec;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterTalentPK)) {
            return false;
        }
        CharacterTalentPK other = (CharacterTalentPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.spell != other.spell) {
            return false;
        }
        if (this.spec != other.spec) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterTalentPK[ guid=" + guid + ", spell=" + spell + ", spec=" + spec + " ]";
    }
    
}
