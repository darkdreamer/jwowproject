/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "gm_subsurveys")
@NamedQueries({
    @NamedQuery(name = "GmSubsurveys.findAll", query = "SELECT g FROM GmSubsurveys g"),
    @NamedQuery(name = "GmSubsurveys.findBySurveyId", query = "SELECT g FROM GmSubsurveys g WHERE g.gmSubsurveysPK.surveyId = :surveyId"),
    @NamedQuery(name = "GmSubsurveys.findBySubsurveyId", query = "SELECT g FROM GmSubsurveys g WHERE g.gmSubsurveysPK.subsurveyId = :subsurveyId"),
    @NamedQuery(name = "GmSubsurveys.findByRank", query = "SELECT g FROM GmSubsurveys g WHERE g.rank = :rank")})
public class GmSubsurveys implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GmSubsurveysPK gmSubsurveysPK;
    @Basic(optional = false)
    @Column(name = "rank")
    private int rank;
    @Basic(optional = false)
    @Lob
    @Column(name = "comment")
    private String comment;

    public GmSubsurveys() {
    }

    public GmSubsurveys(GmSubsurveysPK gmSubsurveysPK) {
        this.gmSubsurveysPK = gmSubsurveysPK;
    }

    public GmSubsurveys(GmSubsurveysPK gmSubsurveysPK, int rank, String comment) {
        this.gmSubsurveysPK = gmSubsurveysPK;
        this.rank = rank;
        this.comment = comment;
    }

    public GmSubsurveys(int surveyId, int subsurveyId) {
        this.gmSubsurveysPK = new GmSubsurveysPK(surveyId, subsurveyId);
    }

    public GmSubsurveysPK getGmSubsurveysPK() {
        return gmSubsurveysPK;
    }

    public void setGmSubsurveysPK(GmSubsurveysPK gmSubsurveysPK) {
        this.gmSubsurveysPK = gmSubsurveysPK;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gmSubsurveysPK != null ? gmSubsurveysPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GmSubsurveys)) {
            return false;
        }
        GmSubsurveys other = (GmSubsurveys) object;
        if ((this.gmSubsurveysPK == null && other.gmSubsurveysPK != null) || (this.gmSubsurveysPK != null && !this.gmSubsurveysPK.equals(other.gmSubsurveysPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GmSubsurveys[ gmSubsurveysPK=" + gmSubsurveysPK + " ]";
    }
    
}
