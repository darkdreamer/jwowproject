/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterGlyphsPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "spec")
    private short spec;

    public CharacterGlyphsPK() {
    }

    public CharacterGlyphsPK(int guid, short spec) {
        this.guid = guid;
        this.spec = spec;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public short getSpec() {
        return spec;
    }

    public void setSpec(short spec) {
        this.spec = spec;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) spec;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterGlyphsPK)) {
            return false;
        }
        CharacterGlyphsPK other = (CharacterGlyphsPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.spec != other.spec) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterGlyphsPK[ guid=" + guid + ", spec=" + spec + " ]";
    }
    
}
