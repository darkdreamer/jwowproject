/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "pet_aura")
@NamedQueries({
    @NamedQuery(name = "PetAura.findAll", query = "SELECT p FROM PetAura p"),
    @NamedQuery(name = "PetAura.findByGuid", query = "SELECT p FROM PetAura p WHERE p.petAuraPK.guid = :guid"),
    @NamedQuery(name = "PetAura.findByCasterGuid", query = "SELECT p FROM PetAura p WHERE p.casterGuid = :casterGuid"),
    @NamedQuery(name = "PetAura.findBySpell", query = "SELECT p FROM PetAura p WHERE p.petAuraPK.spell = :spell"),
    @NamedQuery(name = "PetAura.findByEffectMask", query = "SELECT p FROM PetAura p WHERE p.petAuraPK.effectMask = :effectMask"),
    @NamedQuery(name = "PetAura.findByRecalculateMask", query = "SELECT p FROM PetAura p WHERE p.recalculateMask = :recalculateMask"),
    @NamedQuery(name = "PetAura.findByStackcount", query = "SELECT p FROM PetAura p WHERE p.stackcount = :stackcount"),
    @NamedQuery(name = "PetAura.findByAmount0", query = "SELECT p FROM PetAura p WHERE p.amount0 = :amount0"),
    @NamedQuery(name = "PetAura.findByAmount1", query = "SELECT p FROM PetAura p WHERE p.amount1 = :amount1"),
    @NamedQuery(name = "PetAura.findByAmount2", query = "SELECT p FROM PetAura p WHERE p.amount2 = :amount2"),
    @NamedQuery(name = "PetAura.findByBaseAmount0", query = "SELECT p FROM PetAura p WHERE p.baseAmount0 = :baseAmount0"),
    @NamedQuery(name = "PetAura.findByBaseAmount1", query = "SELECT p FROM PetAura p WHERE p.baseAmount1 = :baseAmount1"),
    @NamedQuery(name = "PetAura.findByBaseAmount2", query = "SELECT p FROM PetAura p WHERE p.baseAmount2 = :baseAmount2"),
    @NamedQuery(name = "PetAura.findByMaxduration", query = "SELECT p FROM PetAura p WHERE p.maxduration = :maxduration"),
    @NamedQuery(name = "PetAura.findByRemaintime", query = "SELECT p FROM PetAura p WHERE p.remaintime = :remaintime"),
    @NamedQuery(name = "PetAura.findByRemaincharges", query = "SELECT p FROM PetAura p WHERE p.remaincharges = :remaincharges")})
public class PetAura implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PetAuraPK petAuraPK;
    @Basic(optional = false)
    @Column(name = "caster_guid")
    private long casterGuid;
    @Basic(optional = false)
    @Column(name = "recalculate_mask")
    private short recalculateMask;
    @Basic(optional = false)
    @Column(name = "stackcount")
    private short stackcount;
    @Basic(optional = false)
    @Column(name = "amount0")
    private int amount0;
    @Basic(optional = false)
    @Column(name = "amount1")
    private int amount1;
    @Basic(optional = false)
    @Column(name = "amount2")
    private int amount2;
    @Basic(optional = false)
    @Column(name = "base_amount0")
    private int baseAmount0;
    @Basic(optional = false)
    @Column(name = "base_amount1")
    private int baseAmount1;
    @Basic(optional = false)
    @Column(name = "base_amount2")
    private int baseAmount2;
    @Basic(optional = false)
    @Column(name = "maxduration")
    private int maxduration;
    @Basic(optional = false)
    @Column(name = "remaintime")
    private int remaintime;
    @Basic(optional = false)
    @Column(name = "remaincharges")
    private short remaincharges;

    public PetAura() {
    }

    public PetAura(PetAuraPK petAuraPK) {
        this.petAuraPK = petAuraPK;
    }

    public PetAura(PetAuraPK petAuraPK, long casterGuid, short recalculateMask, short stackcount, int amount0, int amount1, int amount2, int baseAmount0, int baseAmount1, int baseAmount2, int maxduration, int remaintime, short remaincharges) {
        this.petAuraPK = petAuraPK;
        this.casterGuid = casterGuid;
        this.recalculateMask = recalculateMask;
        this.stackcount = stackcount;
        this.amount0 = amount0;
        this.amount1 = amount1;
        this.amount2 = amount2;
        this.baseAmount0 = baseAmount0;
        this.baseAmount1 = baseAmount1;
        this.baseAmount2 = baseAmount2;
        this.maxduration = maxduration;
        this.remaintime = remaintime;
        this.remaincharges = remaincharges;
    }

    public PetAura(int guid, int spell, short effectMask) {
        this.petAuraPK = new PetAuraPK(guid, spell, effectMask);
    }

    public PetAuraPK getPetAuraPK() {
        return petAuraPK;
    }

    public void setPetAuraPK(PetAuraPK petAuraPK) {
        this.petAuraPK = petAuraPK;
    }

    public long getCasterGuid() {
        return casterGuid;
    }

    public void setCasterGuid(long casterGuid) {
        this.casterGuid = casterGuid;
    }

    public short getRecalculateMask() {
        return recalculateMask;
    }

    public void setRecalculateMask(short recalculateMask) {
        this.recalculateMask = recalculateMask;
    }

    public short getStackcount() {
        return stackcount;
    }

    public void setStackcount(short stackcount) {
        this.stackcount = stackcount;
    }

    public int getAmount0() {
        return amount0;
    }

    public void setAmount0(int amount0) {
        this.amount0 = amount0;
    }

    public int getAmount1() {
        return amount1;
    }

    public void setAmount1(int amount1) {
        this.amount1 = amount1;
    }

    public int getAmount2() {
        return amount2;
    }

    public void setAmount2(int amount2) {
        this.amount2 = amount2;
    }

    public int getBaseAmount0() {
        return baseAmount0;
    }

    public void setBaseAmount0(int baseAmount0) {
        this.baseAmount0 = baseAmount0;
    }

    public int getBaseAmount1() {
        return baseAmount1;
    }

    public void setBaseAmount1(int baseAmount1) {
        this.baseAmount1 = baseAmount1;
    }

    public int getBaseAmount2() {
        return baseAmount2;
    }

    public void setBaseAmount2(int baseAmount2) {
        this.baseAmount2 = baseAmount2;
    }

    public int getMaxduration() {
        return maxduration;
    }

    public void setMaxduration(int maxduration) {
        this.maxduration = maxduration;
    }

    public int getRemaintime() {
        return remaintime;
    }

    public void setRemaintime(int remaintime) {
        this.remaintime = remaintime;
    }

    public short getRemaincharges() {
        return remaincharges;
    }

    public void setRemaincharges(short remaincharges) {
        this.remaincharges = remaincharges;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (petAuraPK != null ? petAuraPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PetAura)) {
            return false;
        }
        PetAura other = (PetAura) object;
        if ((this.petAuraPK == null && other.petAuraPK != null) || (this.petAuraPK != null && !this.petAuraPK.equals(other.petAuraPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.PetAura[ petAuraPK=" + petAuraPK + " ]";
    }
    
}
