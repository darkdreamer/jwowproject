/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "game_event_save")
@NamedQueries({
    @NamedQuery(name = "GameEventSave.findAll", query = "SELECT g FROM GameEventSave g"),
    @NamedQuery(name = "GameEventSave.findByEventEntry", query = "SELECT g FROM GameEventSave g WHERE g.eventEntry = :eventEntry"),
    @NamedQuery(name = "GameEventSave.findByState", query = "SELECT g FROM GameEventSave g WHERE g.state = :state"),
    @NamedQuery(name = "GameEventSave.findByNextStart", query = "SELECT g FROM GameEventSave g WHERE g.nextStart = :nextStart")})
public class GameEventSave implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "eventEntry")
    private Short eventEntry;
    @Basic(optional = false)
    @Column(name = "state")
    private short state;
    @Basic(optional = false)
    @Column(name = "next_start")
    private int nextStart;

    public GameEventSave() {
    }

    public GameEventSave(Short eventEntry) {
        this.eventEntry = eventEntry;
    }

    public GameEventSave(Short eventEntry, short state, int nextStart) {
        this.eventEntry = eventEntry;
        this.state = state;
        this.nextStart = nextStart;
    }

    public Short getEventEntry() {
        return eventEntry;
    }

    public void setEventEntry(Short eventEntry) {
        this.eventEntry = eventEntry;
    }

    public short getState() {
        return state;
    }

    public void setState(short state) {
        this.state = state;
    }

    public int getNextStart() {
        return nextStart;
    }

    public void setNextStart(int nextStart) {
        this.nextStart = nextStart;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventEntry != null ? eventEntry.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GameEventSave)) {
            return false;
        }
        GameEventSave other = (GameEventSave) object;
        if ((this.eventEntry == null && other.eventEntry != null) || (this.eventEntry != null && !this.eventEntry.equals(other.eventEntry))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GameEventSave[ eventEntry=" + eventEntry + " ]";
    }
    
}
