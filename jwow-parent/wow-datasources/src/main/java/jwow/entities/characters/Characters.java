/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "characters")
@NamedQueries({
    @NamedQuery(name = "Characters.findAll", query = "SELECT c FROM Characters c"),
    @NamedQuery(name = "Characters.findByGuid", query = "SELECT c FROM Characters c WHERE c.guid = :guid"),
    @NamedQuery(name = "Characters.findByAccount", query = "SELECT c FROM Characters c WHERE c.account = :account"),
    @NamedQuery(name = "Characters.findByName", query = "SELECT c FROM Characters c WHERE c.name = :name"),
    @NamedQuery(name = "Characters.findByRace", query = "SELECT c FROM Characters c WHERE c.race = :race"),
    @NamedQuery(name = "Characters.findByClass1", query = "SELECT c FROM Characters c WHERE c.class1 = :class1"),
    @NamedQuery(name = "Characters.findByGender", query = "SELECT c FROM Characters c WHERE c.gender = :gender"),
    @NamedQuery(name = "Characters.findByLevel", query = "SELECT c FROM Characters c WHERE c.level = :level"),
    @NamedQuery(name = "Characters.findByXp", query = "SELECT c FROM Characters c WHERE c.xp = :xp"),
    @NamedQuery(name = "Characters.findByMoney", query = "SELECT c FROM Characters c WHERE c.money = :money"),
    @NamedQuery(name = "Characters.findByPlayerBytes", query = "SELECT c FROM Characters c WHERE c.playerBytes = :playerBytes"),
    @NamedQuery(name = "Characters.findByPlayerBytes2", query = "SELECT c FROM Characters c WHERE c.playerBytes2 = :playerBytes2"),
    @NamedQuery(name = "Characters.findByPlayerFlags", query = "SELECT c FROM Characters c WHERE c.playerFlags = :playerFlags"),
    @NamedQuery(name = "Characters.findByPositionX", query = "SELECT c FROM Characters c WHERE c.positionX = :positionX"),
    @NamedQuery(name = "Characters.findByPositionY", query = "SELECT c FROM Characters c WHERE c.positionY = :positionY"),
    @NamedQuery(name = "Characters.findByPositionZ", query = "SELECT c FROM Characters c WHERE c.positionZ = :positionZ"),
    @NamedQuery(name = "Characters.findByMap", query = "SELECT c FROM Characters c WHERE c.map = :map"),
    @NamedQuery(name = "Characters.findByInstanceId", query = "SELECT c FROM Characters c WHERE c.instanceId = :instanceId"),
    @NamedQuery(name = "Characters.findByInstanceModeMask", query = "SELECT c FROM Characters c WHERE c.instanceModeMask = :instanceModeMask"),
    @NamedQuery(name = "Characters.findByOrientation", query = "SELECT c FROM Characters c WHERE c.orientation = :orientation"),
    @NamedQuery(name = "Characters.findByOnline", query = "SELECT c FROM Characters c WHERE c.online = :online"),
    @NamedQuery(name = "Characters.findByCinematic", query = "SELECT c FROM Characters c WHERE c.cinematic = :cinematic"),
    @NamedQuery(name = "Characters.findByTotaltime", query = "SELECT c FROM Characters c WHERE c.totaltime = :totaltime"),
    @NamedQuery(name = "Characters.findByLeveltime", query = "SELECT c FROM Characters c WHERE c.leveltime = :leveltime"),
    @NamedQuery(name = "Characters.findByLogoutTime", query = "SELECT c FROM Characters c WHERE c.logoutTime = :logoutTime"),
    @NamedQuery(name = "Characters.findByIsLogoutResting", query = "SELECT c FROM Characters c WHERE c.isLogoutResting = :isLogoutResting"),
    @NamedQuery(name = "Characters.findByRestBonus", query = "SELECT c FROM Characters c WHERE c.restBonus = :restBonus"),
    @NamedQuery(name = "Characters.findByResettalentsCost", query = "SELECT c FROM Characters c WHERE c.resettalentsCost = :resettalentsCost"),
    @NamedQuery(name = "Characters.findByResettalentsTime", query = "SELECT c FROM Characters c WHERE c.resettalentsTime = :resettalentsTime"),
    @NamedQuery(name = "Characters.findByTransX", query = "SELECT c FROM Characters c WHERE c.transX = :transX"),
    @NamedQuery(name = "Characters.findByTransY", query = "SELECT c FROM Characters c WHERE c.transY = :transY"),
    @NamedQuery(name = "Characters.findByTransZ", query = "SELECT c FROM Characters c WHERE c.transZ = :transZ"),
    @NamedQuery(name = "Characters.findByTransO", query = "SELECT c FROM Characters c WHERE c.transO = :transO"),
    @NamedQuery(name = "Characters.findByTransguid", query = "SELECT c FROM Characters c WHERE c.transguid = :transguid"),
    @NamedQuery(name = "Characters.findByExtraFlags", query = "SELECT c FROM Characters c WHERE c.extraFlags = :extraFlags"),
    @NamedQuery(name = "Characters.findByStableSlots", query = "SELECT c FROM Characters c WHERE c.stableSlots = :stableSlots"),
    @NamedQuery(name = "Characters.findByAtLogin", query = "SELECT c FROM Characters c WHERE c.atLogin = :atLogin"),
    @NamedQuery(name = "Characters.findByZone", query = "SELECT c FROM Characters c WHERE c.zone = :zone"),
    @NamedQuery(name = "Characters.findByDeathExpireTime", query = "SELECT c FROM Characters c WHERE c.deathExpireTime = :deathExpireTime"),
    @NamedQuery(name = "Characters.findByArenaPoints", query = "SELECT c FROM Characters c WHERE c.arenaPoints = :arenaPoints"),
    @NamedQuery(name = "Characters.findByTotalHonorPoints", query = "SELECT c FROM Characters c WHERE c.totalHonorPoints = :totalHonorPoints"),
    @NamedQuery(name = "Characters.findByTodayHonorPoints", query = "SELECT c FROM Characters c WHERE c.todayHonorPoints = :todayHonorPoints"),
    @NamedQuery(name = "Characters.findByYesterdayHonorPoints", query = "SELECT c FROM Characters c WHERE c.yesterdayHonorPoints = :yesterdayHonorPoints"),
    @NamedQuery(name = "Characters.findByTotalKills", query = "SELECT c FROM Characters c WHERE c.totalKills = :totalKills"),
    @NamedQuery(name = "Characters.findByTodayKills", query = "SELECT c FROM Characters c WHERE c.todayKills = :todayKills"),
    @NamedQuery(name = "Characters.findByYesterdayKills", query = "SELECT c FROM Characters c WHERE c.yesterdayKills = :yesterdayKills"),
    @NamedQuery(name = "Characters.findByChosenTitle", query = "SELECT c FROM Characters c WHERE c.chosenTitle = :chosenTitle"),
    @NamedQuery(name = "Characters.findByKnownCurrencies", query = "SELECT c FROM Characters c WHERE c.knownCurrencies = :knownCurrencies"),
    @NamedQuery(name = "Characters.findByWatchedFaction", query = "SELECT c FROM Characters c WHERE c.watchedFaction = :watchedFaction"),
    @NamedQuery(name = "Characters.findByDrunk", query = "SELECT c FROM Characters c WHERE c.drunk = :drunk"),
    @NamedQuery(name = "Characters.findByHealth", query = "SELECT c FROM Characters c WHERE c.health = :health"),
    @NamedQuery(name = "Characters.findByPower1", query = "SELECT c FROM Characters c WHERE c.power1 = :power1"),
    @NamedQuery(name = "Characters.findByPower2", query = "SELECT c FROM Characters c WHERE c.power2 = :power2"),
    @NamedQuery(name = "Characters.findByPower3", query = "SELECT c FROM Characters c WHERE c.power3 = :power3"),
    @NamedQuery(name = "Characters.findByPower4", query = "SELECT c FROM Characters c WHERE c.power4 = :power4"),
    @NamedQuery(name = "Characters.findByPower5", query = "SELECT c FROM Characters c WHERE c.power5 = :power5"),
    @NamedQuery(name = "Characters.findByPower6", query = "SELECT c FROM Characters c WHERE c.power6 = :power6"),
    @NamedQuery(name = "Characters.findByPower7", query = "SELECT c FROM Characters c WHERE c.power7 = :power7"),
    @NamedQuery(name = "Characters.findByLatency", query = "SELECT c FROM Characters c WHERE c.latency = :latency"),
    @NamedQuery(name = "Characters.findBySpeccount", query = "SELECT c FROM Characters c WHERE c.speccount = :speccount"),
    @NamedQuery(name = "Characters.findByActivespec", query = "SELECT c FROM Characters c WHERE c.activespec = :activespec"),
    @NamedQuery(name = "Characters.findByAmmoId", query = "SELECT c FROM Characters c WHERE c.ammoId = :ammoId"),
    @NamedQuery(name = "Characters.findByActionBars", query = "SELECT c FROM Characters c WHERE c.actionBars = :actionBars"),
    @NamedQuery(name = "Characters.findByGrantableLevels", query = "SELECT c FROM Characters c WHERE c.grantableLevels = :grantableLevels"),
    @NamedQuery(name = "Characters.findByDeleteInfosAccount", query = "SELECT c FROM Characters c WHERE c.deleteInfosAccount = :deleteInfosAccount"),
    @NamedQuery(name = "Characters.findByDeleteInfosName", query = "SELECT c FROM Characters c WHERE c.deleteInfosName = :deleteInfosName"),
    @NamedQuery(name = "Characters.findByDeleteDate", query = "SELECT c FROM Characters c WHERE c.deleteDate = :deleteDate")})
public class Characters implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "guid")
    private Integer guid;
    @Basic(optional = false)
    @Column(name = "account")
    private int account;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "race")
    private short race;
    @Basic(optional = false)
    @Column(name = "class")
    private short class1;
    @Basic(optional = false)
    @Column(name = "gender")
    private short gender;
    @Basic(optional = false)
    @Column(name = "level")
    private short level;
    @Basic(optional = false)
    @Column(name = "xp")
    private int xp;
    @Basic(optional = false)
    @Column(name = "money")
    private int money;
    @Basic(optional = false)
    @Column(name = "playerBytes")
    private int playerBytes;
    @Basic(optional = false)
    @Column(name = "playerBytes2")
    private int playerBytes2;
    @Basic(optional = false)
    @Column(name = "playerFlags")
    private int playerFlags;
    @Basic(optional = false)
    @Column(name = "position_x")
    private float positionX;
    @Basic(optional = false)
    @Column(name = "position_y")
    private float positionY;
    @Basic(optional = false)
    @Column(name = "position_z")
    private float positionZ;
    @Basic(optional = false)
    @Column(name = "map")
    private short map;
    @Basic(optional = false)
    @Column(name = "instance_id")
    private int instanceId;
    @Basic(optional = false)
    @Column(name = "instance_mode_mask")
    private short instanceModeMask;
    @Basic(optional = false)
    @Column(name = "orientation")
    private float orientation;
    @Basic(optional = false)
    @Lob
    @Column(name = "taximask")
    private String taximask;
    @Basic(optional = false)
    @Column(name = "online")
    private short online;
    @Basic(optional = false)
    @Column(name = "cinematic")
    private short cinematic;
    @Basic(optional = false)
    @Column(name = "totaltime")
    private int totaltime;
    @Basic(optional = false)
    @Column(name = "leveltime")
    private int leveltime;
    @Basic(optional = false)
    @Column(name = "logout_time")
    private int logoutTime;
    @Basic(optional = false)
    @Column(name = "is_logout_resting")
    private short isLogoutResting;
    @Basic(optional = false)
    @Column(name = "rest_bonus")
    private float restBonus;
    @Basic(optional = false)
    @Column(name = "resettalents_cost")
    private int resettalentsCost;
    @Basic(optional = false)
    @Column(name = "resettalents_time")
    private int resettalentsTime;
    @Basic(optional = false)
    @Column(name = "trans_x")
    private float transX;
    @Basic(optional = false)
    @Column(name = "trans_y")
    private float transY;
    @Basic(optional = false)
    @Column(name = "trans_z")
    private float transZ;
    @Basic(optional = false)
    @Column(name = "trans_o")
    private float transO;
    @Basic(optional = false)
    @Column(name = "transguid")
    private int transguid;
    @Basic(optional = false)
    @Column(name = "extra_flags")
    private short extraFlags;
    @Basic(optional = false)
    @Column(name = "stable_slots")
    private short stableSlots;
    @Basic(optional = false)
    @Column(name = "at_login")
    private short atLogin;
    @Basic(optional = false)
    @Column(name = "zone")
    private short zone;
    @Basic(optional = false)
    @Column(name = "death_expire_time")
    private int deathExpireTime;
    @Lob
    @Column(name = "taxi_path")
    private String taxiPath;
    @Basic(optional = false)
    @Column(name = "arenaPoints")
    private int arenaPoints;
    @Basic(optional = false)
    @Column(name = "totalHonorPoints")
    private int totalHonorPoints;
    @Basic(optional = false)
    @Column(name = "todayHonorPoints")
    private int todayHonorPoints;
    @Basic(optional = false)
    @Column(name = "yesterdayHonorPoints")
    private int yesterdayHonorPoints;
    @Basic(optional = false)
    @Column(name = "totalKills")
    private int totalKills;
    @Basic(optional = false)
    @Column(name = "todayKills")
    private short todayKills;
    @Basic(optional = false)
    @Column(name = "yesterdayKills")
    private short yesterdayKills;
    @Basic(optional = false)
    @Column(name = "chosenTitle")
    private int chosenTitle;
    @Basic(optional = false)
    @Column(name = "knownCurrencies")
    private long knownCurrencies;
    @Basic(optional = false)
    @Column(name = "watchedFaction")
    private int watchedFaction;
    @Basic(optional = false)
    @Column(name = "drunk")
    private short drunk;
    @Basic(optional = false)
    @Column(name = "health")
    private int health;
    @Basic(optional = false)
    @Column(name = "power1")
    private int power1;
    @Basic(optional = false)
    @Column(name = "power2")
    private int power2;
    @Basic(optional = false)
    @Column(name = "power3")
    private int power3;
    @Basic(optional = false)
    @Column(name = "power4")
    private int power4;
    @Basic(optional = false)
    @Column(name = "power5")
    private int power5;
    @Basic(optional = false)
    @Column(name = "power6")
    private int power6;
    @Basic(optional = false)
    @Column(name = "power7")
    private int power7;
    @Basic(optional = false)
    @Column(name = "latency")
    private int latency;
    @Basic(optional = false)
    @Column(name = "speccount")
    private short speccount;
    @Basic(optional = false)
    @Column(name = "activespec")
    private short activespec;
    @Lob
    @Column(name = "exploredZones")
    private String exploredZones;
    @Lob
    @Column(name = "equipmentCache")
    private String equipmentCache;
    @Basic(optional = false)
    @Column(name = "ammoId")
    private int ammoId;
    @Lob
    @Column(name = "knownTitles")
    private String knownTitles;
    @Basic(optional = false)
    @Column(name = "actionBars")
    private short actionBars;
    @Basic(optional = false)
    @Column(name = "grantableLevels")
    private short grantableLevels;
    @Column(name = "deleteInfos_Account")
    private Integer deleteInfosAccount;
    @Column(name = "deleteInfos_Name")
    private String deleteInfosName;
    @Column(name = "deleteDate")
    private Integer deleteDate;

    public Characters() {
    }

    public Characters(Integer guid) {
        this.guid = guid;
    }

    public Characters(Integer guid, int account, String name, short race, short class1, short gender, short level, int xp, int money, int playerBytes, int playerBytes2, int playerFlags, float positionX, float positionY, float positionZ, short map, int instanceId, short instanceModeMask, float orientation, String taximask, short online, short cinematic, int totaltime, int leveltime, int logoutTime, short isLogoutResting, float restBonus, int resettalentsCost, int resettalentsTime, float transX, float transY, float transZ, float transO, int transguid, short extraFlags, short stableSlots, short atLogin, short zone, int deathExpireTime, int arenaPoints, int totalHonorPoints, int todayHonorPoints, int yesterdayHonorPoints, int totalKills, short todayKills, short yesterdayKills, int chosenTitle, long knownCurrencies, int watchedFaction, short drunk, int health, int power1, int power2, int power3, int power4, int power5, int power6, int power7, int latency, short speccount, short activespec, int ammoId, short actionBars, short grantableLevels) {
        this.guid = guid;
        this.account = account;
        this.name = name;
        this.race = race;
        this.class1 = class1;
        this.gender = gender;
        this.level = level;
        this.xp = xp;
        this.money = money;
        this.playerBytes = playerBytes;
        this.playerBytes2 = playerBytes2;
        this.playerFlags = playerFlags;
        this.positionX = positionX;
        this.positionY = positionY;
        this.positionZ = positionZ;
        this.map = map;
        this.instanceId = instanceId;
        this.instanceModeMask = instanceModeMask;
        this.orientation = orientation;
        this.taximask = taximask;
        this.online = online;
        this.cinematic = cinematic;
        this.totaltime = totaltime;
        this.leveltime = leveltime;
        this.logoutTime = logoutTime;
        this.isLogoutResting = isLogoutResting;
        this.restBonus = restBonus;
        this.resettalentsCost = resettalentsCost;
        this.resettalentsTime = resettalentsTime;
        this.transX = transX;
        this.transY = transY;
        this.transZ = transZ;
        this.transO = transO;
        this.transguid = transguid;
        this.extraFlags = extraFlags;
        this.stableSlots = stableSlots;
        this.atLogin = atLogin;
        this.zone = zone;
        this.deathExpireTime = deathExpireTime;
        this.arenaPoints = arenaPoints;
        this.totalHonorPoints = totalHonorPoints;
        this.todayHonorPoints = todayHonorPoints;
        this.yesterdayHonorPoints = yesterdayHonorPoints;
        this.totalKills = totalKills;
        this.todayKills = todayKills;
        this.yesterdayKills = yesterdayKills;
        this.chosenTitle = chosenTitle;
        this.knownCurrencies = knownCurrencies;
        this.watchedFaction = watchedFaction;
        this.drunk = drunk;
        this.health = health;
        this.power1 = power1;
        this.power2 = power2;
        this.power3 = power3;
        this.power4 = power4;
        this.power5 = power5;
        this.power6 = power6;
        this.power7 = power7;
        this.latency = latency;
        this.speccount = speccount;
        this.activespec = activespec;
        this.ammoId = ammoId;
        this.actionBars = actionBars;
        this.grantableLevels = grantableLevels;
    }

    public Integer getGuid() {
        return guid;
    }

    public void setGuid(Integer guid) {
        this.guid = guid;
    }

    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getRace() {
        return race;
    }

    public void setRace(short race) {
        this.race = race;
    }

    public short getClass1() {
        return class1;
    }

    public void setClass1(short class1) {
        this.class1 = class1;
    }

    public short getGender() {
        return gender;
    }

    public void setGender(short gender) {
        this.gender = gender;
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getPlayerBytes() {
        return playerBytes;
    }

    public void setPlayerBytes(int playerBytes) {
        this.playerBytes = playerBytes;
    }

    public int getPlayerBytes2() {
        return playerBytes2;
    }

    public void setPlayerBytes2(int playerBytes2) {
        this.playerBytes2 = playerBytes2;
    }

    public int getPlayerFlags() {
        return playerFlags;
    }

    public void setPlayerFlags(int playerFlags) {
        this.playerFlags = playerFlags;
    }

    public float getPositionX() {
        return positionX;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public float getPositionZ() {
        return positionZ;
    }

    public void setPositionZ(float positionZ) {
        this.positionZ = positionZ;
    }

    public short getMap() {
        return map;
    }

    public void setMap(short map) {
        this.map = map;
    }

    public int getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(int instanceId) {
        this.instanceId = instanceId;
    }

    public short getInstanceModeMask() {
        return instanceModeMask;
    }

    public void setInstanceModeMask(short instanceModeMask) {
        this.instanceModeMask = instanceModeMask;
    }

    public float getOrientation() {
        return orientation;
    }

    public void setOrientation(float orientation) {
        this.orientation = orientation;
    }

    public String getTaximask() {
        return taximask;
    }

    public void setTaximask(String taximask) {
        this.taximask = taximask;
    }

    public short getOnline() {
        return online;
    }

    public void setOnline(short online) {
        this.online = online;
    }

    public short getCinematic() {
        return cinematic;
    }

    public void setCinematic(short cinematic) {
        this.cinematic = cinematic;
    }

    public int getTotaltime() {
        return totaltime;
    }

    public void setTotaltime(int totaltime) {
        this.totaltime = totaltime;
    }

    public int getLeveltime() {
        return leveltime;
    }

    public void setLeveltime(int leveltime) {
        this.leveltime = leveltime;
    }

    public int getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(int logoutTime) {
        this.logoutTime = logoutTime;
    }

    public short getIsLogoutResting() {
        return isLogoutResting;
    }

    public void setIsLogoutResting(short isLogoutResting) {
        this.isLogoutResting = isLogoutResting;
    }

    public float getRestBonus() {
        return restBonus;
    }

    public void setRestBonus(float restBonus) {
        this.restBonus = restBonus;
    }

    public int getResettalentsCost() {
        return resettalentsCost;
    }

    public void setResettalentsCost(int resettalentsCost) {
        this.resettalentsCost = resettalentsCost;
    }

    public int getResettalentsTime() {
        return resettalentsTime;
    }

    public void setResettalentsTime(int resettalentsTime) {
        this.resettalentsTime = resettalentsTime;
    }

    public float getTransX() {
        return transX;
    }

    public void setTransX(float transX) {
        this.transX = transX;
    }

    public float getTransY() {
        return transY;
    }

    public void setTransY(float transY) {
        this.transY = transY;
    }

    public float getTransZ() {
        return transZ;
    }

    public void setTransZ(float transZ) {
        this.transZ = transZ;
    }

    public float getTransO() {
        return transO;
    }

    public void setTransO(float transO) {
        this.transO = transO;
    }

    public int getTransguid() {
        return transguid;
    }

    public void setTransguid(int transguid) {
        this.transguid = transguid;
    }

    public short getExtraFlags() {
        return extraFlags;
    }

    public void setExtraFlags(short extraFlags) {
        this.extraFlags = extraFlags;
    }

    public short getStableSlots() {
        return stableSlots;
    }

    public void setStableSlots(short stableSlots) {
        this.stableSlots = stableSlots;
    }

    public short getAtLogin() {
        return atLogin;
    }

    public void setAtLogin(short atLogin) {
        this.atLogin = atLogin;
    }

    public short getZone() {
        return zone;
    }

    public void setZone(short zone) {
        this.zone = zone;
    }

    public int getDeathExpireTime() {
        return deathExpireTime;
    }

    public void setDeathExpireTime(int deathExpireTime) {
        this.deathExpireTime = deathExpireTime;
    }

    public String getTaxiPath() {
        return taxiPath;
    }

    public void setTaxiPath(String taxiPath) {
        this.taxiPath = taxiPath;
    }

    public int getArenaPoints() {
        return arenaPoints;
    }

    public void setArenaPoints(int arenaPoints) {
        this.arenaPoints = arenaPoints;
    }

    public int getTotalHonorPoints() {
        return totalHonorPoints;
    }

    public void setTotalHonorPoints(int totalHonorPoints) {
        this.totalHonorPoints = totalHonorPoints;
    }

    public int getTodayHonorPoints() {
        return todayHonorPoints;
    }

    public void setTodayHonorPoints(int todayHonorPoints) {
        this.todayHonorPoints = todayHonorPoints;
    }

    public int getYesterdayHonorPoints() {
        return yesterdayHonorPoints;
    }

    public void setYesterdayHonorPoints(int yesterdayHonorPoints) {
        this.yesterdayHonorPoints = yesterdayHonorPoints;
    }

    public int getTotalKills() {
        return totalKills;
    }

    public void setTotalKills(int totalKills) {
        this.totalKills = totalKills;
    }

    public short getTodayKills() {
        return todayKills;
    }

    public void setTodayKills(short todayKills) {
        this.todayKills = todayKills;
    }

    public short getYesterdayKills() {
        return yesterdayKills;
    }

    public void setYesterdayKills(short yesterdayKills) {
        this.yesterdayKills = yesterdayKills;
    }

    public int getChosenTitle() {
        return chosenTitle;
    }

    public void setChosenTitle(int chosenTitle) {
        this.chosenTitle = chosenTitle;
    }

    public long getKnownCurrencies() {
        return knownCurrencies;
    }

    public void setKnownCurrencies(long knownCurrencies) {
        this.knownCurrencies = knownCurrencies;
    }

    public int getWatchedFaction() {
        return watchedFaction;
    }

    public void setWatchedFaction(int watchedFaction) {
        this.watchedFaction = watchedFaction;
    }

    public short getDrunk() {
        return drunk;
    }

    public void setDrunk(short drunk) {
        this.drunk = drunk;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getPower1() {
        return power1;
    }

    public void setPower1(int power1) {
        this.power1 = power1;
    }

    public int getPower2() {
        return power2;
    }

    public void setPower2(int power2) {
        this.power2 = power2;
    }

    public int getPower3() {
        return power3;
    }

    public void setPower3(int power3) {
        this.power3 = power3;
    }

    public int getPower4() {
        return power4;
    }

    public void setPower4(int power4) {
        this.power4 = power4;
    }

    public int getPower5() {
        return power5;
    }

    public void setPower5(int power5) {
        this.power5 = power5;
    }

    public int getPower6() {
        return power6;
    }

    public void setPower6(int power6) {
        this.power6 = power6;
    }

    public int getPower7() {
        return power7;
    }

    public void setPower7(int power7) {
        this.power7 = power7;
    }

    public int getLatency() {
        return latency;
    }

    public void setLatency(int latency) {
        this.latency = latency;
    }

    public short getSpeccount() {
        return speccount;
    }

    public void setSpeccount(short speccount) {
        this.speccount = speccount;
    }

    public short getActivespec() {
        return activespec;
    }

    public void setActivespec(short activespec) {
        this.activespec = activespec;
    }

    public String getExploredZones() {
        return exploredZones;
    }

    public void setExploredZones(String exploredZones) {
        this.exploredZones = exploredZones;
    }

    public String getEquipmentCache() {
        return equipmentCache;
    }

    public void setEquipmentCache(String equipmentCache) {
        this.equipmentCache = equipmentCache;
    }

    public int getAmmoId() {
        return ammoId;
    }

    public void setAmmoId(int ammoId) {
        this.ammoId = ammoId;
    }

    public String getKnownTitles() {
        return knownTitles;
    }

    public void setKnownTitles(String knownTitles) {
        this.knownTitles = knownTitles;
    }

    public short getActionBars() {
        return actionBars;
    }

    public void setActionBars(short actionBars) {
        this.actionBars = actionBars;
    }

    public short getGrantableLevels() {
        return grantableLevels;
    }

    public void setGrantableLevels(short grantableLevels) {
        this.grantableLevels = grantableLevels;
    }

    public Integer getDeleteInfosAccount() {
        return deleteInfosAccount;
    }

    public void setDeleteInfosAccount(Integer deleteInfosAccount) {
        this.deleteInfosAccount = deleteInfosAccount;
    }

    public String getDeleteInfosName() {
        return deleteInfosName;
    }

    public void setDeleteInfosName(String deleteInfosName) {
        this.deleteInfosName = deleteInfosName;
    }

    public Integer getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Integer deleteDate) {
        this.deleteDate = deleteDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guid != null ? guid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Characters)) {
            return false;
        }
        Characters other = (Characters) object;
        if ((this.guid == null && other.guid != null) || (this.guid != null && !this.guid.equals(other.guid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.Characters[ guid=" + guid + " ]";
    }
    
}
