/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class PetitionSignPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "petitionguid")
    private int petitionguid;
    @Basic(optional = false)
    @Column(name = "playerguid")
    private int playerguid;

    public PetitionSignPK() {
    }

    public PetitionSignPK(int petitionguid, int playerguid) {
        this.petitionguid = petitionguid;
        this.playerguid = playerguid;
    }

    public int getPetitionguid() {
        return petitionguid;
    }

    public void setPetitionguid(int petitionguid) {
        this.petitionguid = petitionguid;
    }

    public int getPlayerguid() {
        return playerguid;
    }

    public void setPlayerguid(int playerguid) {
        this.playerguid = playerguid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) petitionguid;
        hash += (int) playerguid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PetitionSignPK)) {
            return false;
        }
        PetitionSignPK other = (PetitionSignPK) object;
        if (this.petitionguid != other.petitionguid) {
            return false;
        }
        if (this.playerguid != other.playerguid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.PetitionSignPK[ petitionguid=" + petitionguid + ", playerguid=" + playerguid + " ]";
    }
    
}
