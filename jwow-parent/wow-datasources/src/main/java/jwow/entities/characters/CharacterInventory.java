/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_inventory")
@NamedQueries({
    @NamedQuery(name = "CharacterInventory.findAll", query = "SELECT c FROM CharacterInventory c"),
    @NamedQuery(name = "CharacterInventory.findByGuid", query = "SELECT c FROM CharacterInventory c WHERE c.guid = :guid"),
    @NamedQuery(name = "CharacterInventory.findByBag", query = "SELECT c FROM CharacterInventory c WHERE c.bag = :bag"),
    @NamedQuery(name = "CharacterInventory.findBySlot", query = "SELECT c FROM CharacterInventory c WHERE c.slot = :slot"),
    @NamedQuery(name = "CharacterInventory.findByItem", query = "SELECT c FROM CharacterInventory c WHERE c.item = :item")})
public class CharacterInventory implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "bag")
    private int bag;
    @Basic(optional = false)
    @Column(name = "slot")
    private short slot;
    @Id
    @Basic(optional = false)
    @Column(name = "item")
    private Integer item;

    public CharacterInventory() {
    }

    public CharacterInventory(Integer item) {
        this.item = item;
    }

    public CharacterInventory(Integer item, int guid, int bag, short slot) {
        this.item = item;
        this.guid = guid;
        this.bag = bag;
        this.slot = slot;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public int getBag() {
        return bag;
    }

    public void setBag(int bag) {
        this.bag = bag;
    }

    public short getSlot() {
        return slot;
    }

    public void setSlot(short slot) {
        this.slot = slot;
    }

    public Integer getItem() {
        return item;
    }

    public void setItem(Integer item) {
        this.item = item;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (item != null ? item.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterInventory)) {
            return false;
        }
        CharacterInventory other = (CharacterInventory) object;
        if ((this.item == null && other.item != null) || (this.item != null && !this.item.equals(other.item))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterInventory[ item=" + item + " ]";
    }
    
}
