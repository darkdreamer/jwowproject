/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "item_refund_instance")
@NamedQueries({
    @NamedQuery(name = "ItemRefundInstance.findAll", query = "SELECT i FROM ItemRefundInstance i"),
    @NamedQuery(name = "ItemRefundInstance.findByItemGuid", query = "SELECT i FROM ItemRefundInstance i WHERE i.itemRefundInstancePK.itemGuid = :itemGuid"),
    @NamedQuery(name = "ItemRefundInstance.findByPlayerGuid", query = "SELECT i FROM ItemRefundInstance i WHERE i.itemRefundInstancePK.playerGuid = :playerGuid"),
    @NamedQuery(name = "ItemRefundInstance.findByPaidMoney", query = "SELECT i FROM ItemRefundInstance i WHERE i.paidMoney = :paidMoney"),
    @NamedQuery(name = "ItemRefundInstance.findByPaidExtendedCost", query = "SELECT i FROM ItemRefundInstance i WHERE i.paidExtendedCost = :paidExtendedCost")})
public class ItemRefundInstance implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ItemRefundInstancePK itemRefundInstancePK;
    @Basic(optional = false)
    @Column(name = "paidMoney")
    private int paidMoney;
    @Basic(optional = false)
    @Column(name = "paidExtendedCost")
    private short paidExtendedCost;

    public ItemRefundInstance() {
    }

    public ItemRefundInstance(ItemRefundInstancePK itemRefundInstancePK) {
        this.itemRefundInstancePK = itemRefundInstancePK;
    }

    public ItemRefundInstance(ItemRefundInstancePK itemRefundInstancePK, int paidMoney, short paidExtendedCost) {
        this.itemRefundInstancePK = itemRefundInstancePK;
        this.paidMoney = paidMoney;
        this.paidExtendedCost = paidExtendedCost;
    }

    public ItemRefundInstance(int itemGuid, int playerGuid) {
        this.itemRefundInstancePK = new ItemRefundInstancePK(itemGuid, playerGuid);
    }

    public ItemRefundInstancePK getItemRefundInstancePK() {
        return itemRefundInstancePK;
    }

    public void setItemRefundInstancePK(ItemRefundInstancePK itemRefundInstancePK) {
        this.itemRefundInstancePK = itemRefundInstancePK;
    }

    public int getPaidMoney() {
        return paidMoney;
    }

    public void setPaidMoney(int paidMoney) {
        this.paidMoney = paidMoney;
    }

    public short getPaidExtendedCost() {
        return paidExtendedCost;
    }

    public void setPaidExtendedCost(short paidExtendedCost) {
        this.paidExtendedCost = paidExtendedCost;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemRefundInstancePK != null ? itemRefundInstancePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemRefundInstance)) {
            return false;
        }
        ItemRefundInstance other = (ItemRefundInstance) object;
        if ((this.itemRefundInstancePK == null && other.itemRefundInstancePK != null) || (this.itemRefundInstancePK != null && !this.itemRefundInstancePK.equals(other.itemRefundInstancePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.ItemRefundInstance[ itemRefundInstancePK=" + itemRefundInstancePK + " ]";
    }
    
}
