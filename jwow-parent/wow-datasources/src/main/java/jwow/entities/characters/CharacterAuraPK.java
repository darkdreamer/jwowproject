/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterAuraPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "caster_guid")
    private long casterGuid;
    @Basic(optional = false)
    @Column(name = "item_guid")
    private long itemGuid;
    @Basic(optional = false)
    @Column(name = "spell")
    private int spell;
    @Basic(optional = false)
    @Column(name = "effect_mask")
    private short effectMask;

    public CharacterAuraPK() {
    }

    public CharacterAuraPK(int guid, long casterGuid, long itemGuid, int spell, short effectMask) {
        this.guid = guid;
        this.casterGuid = casterGuid;
        this.itemGuid = itemGuid;
        this.spell = spell;
        this.effectMask = effectMask;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public long getCasterGuid() {
        return casterGuid;
    }

    public void setCasterGuid(long casterGuid) {
        this.casterGuid = casterGuid;
    }

    public long getItemGuid() {
        return itemGuid;
    }

    public void setItemGuid(long itemGuid) {
        this.itemGuid = itemGuid;
    }

    public int getSpell() {
        return spell;
    }

    public void setSpell(int spell) {
        this.spell = spell;
    }

    public short getEffectMask() {
        return effectMask;
    }

    public void setEffectMask(short effectMask) {
        this.effectMask = effectMask;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) casterGuid;
        hash += (int) itemGuid;
        hash += (int) spell;
        hash += (int) effectMask;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterAuraPK)) {
            return false;
        }
        CharacterAuraPK other = (CharacterAuraPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.casterGuid != other.casterGuid) {
            return false;
        }
        if (this.itemGuid != other.itemGuid) {
            return false;
        }
        if (this.spell != other.spell) {
            return false;
        }
        if (this.effectMask != other.effectMask) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterAuraPK[ guid=" + guid + ", casterGuid=" + casterGuid + ", itemGuid=" + itemGuid + ", spell=" + spell + ", effectMask=" + effectMask + " ]";
    }
    
}
