/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class AccountInstanceTimesPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "accountId")
    private int accountId;
    @Basic(optional = false)
    @Column(name = "instanceId")
    private int instanceId;

    public AccountInstanceTimesPK() {
    }

    public AccountInstanceTimesPK(int accountId, int instanceId) {
        this.accountId = accountId;
        this.instanceId = instanceId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(int instanceId) {
        this.instanceId = instanceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) accountId;
        hash += (int) instanceId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountInstanceTimesPK)) {
            return false;
        }
        AccountInstanceTimesPK other = (AccountInstanceTimesPK) object;
        if (this.accountId != other.accountId) {
            return false;
        }
        if (this.instanceId != other.instanceId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.AccountInstanceTimesPK[ accountId=" + accountId + ", instanceId=" + instanceId + " ]";
    }
    
}
