/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "group_member")
@NamedQueries({
    @NamedQuery(name = "GroupMember.findAll", query = "SELECT g FROM GroupMember g"),
    @NamedQuery(name = "GroupMember.findByGuid", query = "SELECT g FROM GroupMember g WHERE g.guid = :guid"),
    @NamedQuery(name = "GroupMember.findByMemberGuid", query = "SELECT g FROM GroupMember g WHERE g.memberGuid = :memberGuid"),
    @NamedQuery(name = "GroupMember.findByMemberFlags", query = "SELECT g FROM GroupMember g WHERE g.memberFlags = :memberFlags"),
    @NamedQuery(name = "GroupMember.findBySubgroup", query = "SELECT g FROM GroupMember g WHERE g.subgroup = :subgroup"),
    @NamedQuery(name = "GroupMember.findByRoles", query = "SELECT g FROM GroupMember g WHERE g.roles = :roles")})
public class GroupMember implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Id
    @Basic(optional = false)
    @Column(name = "memberGuid")
    private Integer memberGuid;
    @Basic(optional = false)
    @Column(name = "memberFlags")
    private short memberFlags;
    @Basic(optional = false)
    @Column(name = "subgroup")
    private short subgroup;
    @Basic(optional = false)
    @Column(name = "roles")
    private short roles;

    public GroupMember() {
    }

    public GroupMember(Integer memberGuid) {
        this.memberGuid = memberGuid;
    }

    public GroupMember(Integer memberGuid, int guid, short memberFlags, short subgroup, short roles) {
        this.memberGuid = memberGuid;
        this.guid = guid;
        this.memberFlags = memberFlags;
        this.subgroup = subgroup;
        this.roles = roles;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public Integer getMemberGuid() {
        return memberGuid;
    }

    public void setMemberGuid(Integer memberGuid) {
        this.memberGuid = memberGuid;
    }

    public short getMemberFlags() {
        return memberFlags;
    }

    public void setMemberFlags(short memberFlags) {
        this.memberFlags = memberFlags;
    }

    public short getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(short subgroup) {
        this.subgroup = subgroup;
    }

    public short getRoles() {
        return roles;
    }

    public void setRoles(short roles) {
        this.roles = roles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (memberGuid != null ? memberGuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupMember)) {
            return false;
        }
        GroupMember other = (GroupMember) object;
        if ((this.memberGuid == null && other.memberGuid != null) || (this.memberGuid != null && !this.memberGuid.equals(other.memberGuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GroupMember[ memberGuid=" + memberGuid + " ]";
    }
    
}
