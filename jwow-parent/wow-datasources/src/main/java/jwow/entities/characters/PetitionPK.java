/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class PetitionPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ownerguid")
    private int ownerguid;
    @Basic(optional = false)
    @Column(name = "type")
    private short type;

    public PetitionPK() {
    }

    public PetitionPK(int ownerguid, short type) {
        this.ownerguid = ownerguid;
        this.type = type;
    }

    public int getOwnerguid() {
        return ownerguid;
    }

    public void setOwnerguid(int ownerguid) {
        this.ownerguid = ownerguid;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) ownerguid;
        hash += (int) type;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PetitionPK)) {
            return false;
        }
        PetitionPK other = (PetitionPK) object;
        if (this.ownerguid != other.ownerguid) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.PetitionPK[ ownerguid=" + ownerguid + ", type=" + type + " ]";
    }
    
}
