/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "petition")
@NamedQueries({
    @NamedQuery(name = "Petition.findAll", query = "SELECT p FROM Petition p"),
    @NamedQuery(name = "Petition.findByOwnerguid", query = "SELECT p FROM Petition p WHERE p.petitionPK.ownerguid = :ownerguid"),
    @NamedQuery(name = "Petition.findByPetitionguid", query = "SELECT p FROM Petition p WHERE p.petitionguid = :petitionguid"),
    @NamedQuery(name = "Petition.findByName", query = "SELECT p FROM Petition p WHERE p.name = :name"),
    @NamedQuery(name = "Petition.findByType", query = "SELECT p FROM Petition p WHERE p.petitionPK.type = :type")})
public class Petition implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PetitionPK petitionPK;
    @Column(name = "petitionguid")
    private Integer petitionguid;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    public Petition() {
    }

    public Petition(PetitionPK petitionPK) {
        this.petitionPK = petitionPK;
    }

    public Petition(PetitionPK petitionPK, String name) {
        this.petitionPK = petitionPK;
        this.name = name;
    }

    public Petition(int ownerguid, short type) {
        this.petitionPK = new PetitionPK(ownerguid, type);
    }

    public PetitionPK getPetitionPK() {
        return petitionPK;
    }

    public void setPetitionPK(PetitionPK petitionPK) {
        this.petitionPK = petitionPK;
    }

    public Integer getPetitionguid() {
        return petitionguid;
    }

    public void setPetitionguid(Integer petitionguid) {
        this.petitionguid = petitionguid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (petitionPK != null ? petitionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Petition)) {
            return false;
        }
        Petition other = (Petition) object;
        if ((this.petitionPK == null && other.petitionPK != null) || (this.petitionPK != null && !this.petitionPK.equals(other.petitionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.Petition[ petitionPK=" + petitionPK + " ]";
    }
    
}
