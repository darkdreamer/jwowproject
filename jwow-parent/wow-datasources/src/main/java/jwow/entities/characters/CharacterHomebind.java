/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_homebind")
@NamedQueries({
    @NamedQuery(name = "CharacterHomebind.findAll", query = "SELECT c FROM CharacterHomebind c"),
    @NamedQuery(name = "CharacterHomebind.findByGuid", query = "SELECT c FROM CharacterHomebind c WHERE c.guid = :guid"),
    @NamedQuery(name = "CharacterHomebind.findByMapId", query = "SELECT c FROM CharacterHomebind c WHERE c.mapId = :mapId"),
    @NamedQuery(name = "CharacterHomebind.findByZoneId", query = "SELECT c FROM CharacterHomebind c WHERE c.zoneId = :zoneId"),
    @NamedQuery(name = "CharacterHomebind.findByPosX", query = "SELECT c FROM CharacterHomebind c WHERE c.posX = :posX"),
    @NamedQuery(name = "CharacterHomebind.findByPosY", query = "SELECT c FROM CharacterHomebind c WHERE c.posY = :posY"),
    @NamedQuery(name = "CharacterHomebind.findByPosZ", query = "SELECT c FROM CharacterHomebind c WHERE c.posZ = :posZ")})
public class CharacterHomebind implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "guid")
    private Integer guid;
    @Basic(optional = false)
    @Column(name = "mapId")
    private short mapId;
    @Basic(optional = false)
    @Column(name = "zoneId")
    private short zoneId;
    @Basic(optional = false)
    @Column(name = "posX")
    private float posX;
    @Basic(optional = false)
    @Column(name = "posY")
    private float posY;
    @Basic(optional = false)
    @Column(name = "posZ")
    private float posZ;

    public CharacterHomebind() {
    }

    public CharacterHomebind(Integer guid) {
        this.guid = guid;
    }

    public CharacterHomebind(Integer guid, short mapId, short zoneId, float posX, float posY, float posZ) {
        this.guid = guid;
        this.mapId = mapId;
        this.zoneId = zoneId;
        this.posX = posX;
        this.posY = posY;
        this.posZ = posZ;
    }

    public Integer getGuid() {
        return guid;
    }

    public void setGuid(Integer guid) {
        this.guid = guid;
    }

    public short getMapId() {
        return mapId;
    }

    public void setMapId(short mapId) {
        this.mapId = mapId;
    }

    public short getZoneId() {
        return zoneId;
    }

    public void setZoneId(short zoneId) {
        this.zoneId = zoneId;
    }

    public float getPosX() {
        return posX;
    }

    public void setPosX(float posX) {
        this.posX = posX;
    }

    public float getPosY() {
        return posY;
    }

    public void setPosY(float posY) {
        this.posY = posY;
    }

    public float getPosZ() {
        return posZ;
    }

    public void setPosZ(float posZ) {
        this.posZ = posZ;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guid != null ? guid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterHomebind)) {
            return false;
        }
        CharacterHomebind other = (CharacterHomebind) object;
        if ((this.guid == null && other.guid != null) || (this.guid != null && !this.guid.equals(other.guid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterHomebind[ guid=" + guid + " ]";
    }
    
}
