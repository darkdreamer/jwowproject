/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class GuildEventlogPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guildid")
    private int guildid;
    @Basic(optional = false)
    @Column(name = "LogGuid")
    private int logGuid;

    public GuildEventlogPK() {
    }

    public GuildEventlogPK(int guildid, int logGuid) {
        this.guildid = guildid;
        this.logGuid = logGuid;
    }

    public int getGuildid() {
        return guildid;
    }

    public void setGuildid(int guildid) {
        this.guildid = guildid;
    }

    public int getLogGuid() {
        return logGuid;
    }

    public void setLogGuid(int logGuid) {
        this.logGuid = logGuid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guildid;
        hash += (int) logGuid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuildEventlogPK)) {
            return false;
        }
        GuildEventlogPK other = (GuildEventlogPK) object;
        if (this.guildid != other.guildid) {
            return false;
        }
        if (this.logGuid != other.logGuid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GuildEventlogPK[ guildid=" + guildid + ", logGuid=" + logGuid + " ]";
    }
    
}
