/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "mail")
@NamedQueries({
    @NamedQuery(name = "Mail.findAll", query = "SELECT m FROM Mail m"),
    @NamedQuery(name = "Mail.findById", query = "SELECT m FROM Mail m WHERE m.id = :id"),
    @NamedQuery(name = "Mail.findByMessageType", query = "SELECT m FROM Mail m WHERE m.messageType = :messageType"),
    @NamedQuery(name = "Mail.findByStationery", query = "SELECT m FROM Mail m WHERE m.stationery = :stationery"),
    @NamedQuery(name = "Mail.findByMailTemplateId", query = "SELECT m FROM Mail m WHERE m.mailTemplateId = :mailTemplateId"),
    @NamedQuery(name = "Mail.findBySender", query = "SELECT m FROM Mail m WHERE m.sender = :sender"),
    @NamedQuery(name = "Mail.findByReceiver", query = "SELECT m FROM Mail m WHERE m.receiver = :receiver"),
    @NamedQuery(name = "Mail.findByHasItems", query = "SELECT m FROM Mail m WHERE m.hasItems = :hasItems"),
    @NamedQuery(name = "Mail.findByExpireTime", query = "SELECT m FROM Mail m WHERE m.expireTime = :expireTime"),
    @NamedQuery(name = "Mail.findByDeliverTime", query = "SELECT m FROM Mail m WHERE m.deliverTime = :deliverTime"),
    @NamedQuery(name = "Mail.findByMoney", query = "SELECT m FROM Mail m WHERE m.money = :money"),
    @NamedQuery(name = "Mail.findByCod", query = "SELECT m FROM Mail m WHERE m.cod = :cod"),
    @NamedQuery(name = "Mail.findByChecked", query = "SELECT m FROM Mail m WHERE m.checked = :checked")})
public class Mail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "messageType")
    private short messageType;
    @Basic(optional = false)
    @Column(name = "stationery")
    private short stationery;
    @Basic(optional = false)
    @Column(name = "mailTemplateId")
    private short mailTemplateId;
    @Basic(optional = false)
    @Column(name = "sender")
    private int sender;
    @Basic(optional = false)
    @Column(name = "receiver")
    private int receiver;
    @Lob
    @Column(name = "subject")
    private String subject;
    @Lob
    @Column(name = "body")
    private String body;
    @Basic(optional = false)
    @Column(name = "has_items")
    private short hasItems;
    @Basic(optional = false)
    @Column(name = "expire_time")
    private int expireTime;
    @Basic(optional = false)
    @Column(name = "deliver_time")
    private int deliverTime;
    @Basic(optional = false)
    @Column(name = "money")
    private int money;
    @Basic(optional = false)
    @Column(name = "cod")
    private int cod;
    @Basic(optional = false)
    @Column(name = "checked")
    private short checked;

    public Mail() {
    }

    public Mail(Integer id) {
        this.id = id;
    }

    public Mail(Integer id, short messageType, short stationery, short mailTemplateId, int sender, int receiver, short hasItems, int expireTime, int deliverTime, int money, int cod, short checked) {
        this.id = id;
        this.messageType = messageType;
        this.stationery = stationery;
        this.mailTemplateId = mailTemplateId;
        this.sender = sender;
        this.receiver = receiver;
        this.hasItems = hasItems;
        this.expireTime = expireTime;
        this.deliverTime = deliverTime;
        this.money = money;
        this.cod = cod;
        this.checked = checked;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public short getMessageType() {
        return messageType;
    }

    public void setMessageType(short messageType) {
        this.messageType = messageType;
    }

    public short getStationery() {
        return stationery;
    }

    public void setStationery(short stationery) {
        this.stationery = stationery;
    }

    public short getMailTemplateId() {
        return mailTemplateId;
    }

    public void setMailTemplateId(short mailTemplateId) {
        this.mailTemplateId = mailTemplateId;
    }

    public int getSender() {
        return sender;
    }

    public void setSender(int sender) {
        this.sender = sender;
    }

    public int getReceiver() {
        return receiver;
    }

    public void setReceiver(int receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public short getHasItems() {
        return hasItems;
    }

    public void setHasItems(short hasItems) {
        this.hasItems = hasItems;
    }

    public int getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(int expireTime) {
        this.expireTime = expireTime;
    }

    public int getDeliverTime() {
        return deliverTime;
    }

    public void setDeliverTime(int deliverTime) {
        this.deliverTime = deliverTime;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public short getChecked() {
        return checked;
    }

    public void setChecked(short checked) {
        this.checked = checked;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mail)) {
            return false;
        }
        Mail other = (Mail) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.Mail[ id=" + id + " ]";
    }
    
}
