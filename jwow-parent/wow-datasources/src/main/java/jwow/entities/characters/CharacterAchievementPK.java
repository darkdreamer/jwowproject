/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterAchievementPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "achievement")
    private short achievement;

    public CharacterAchievementPK() {
    }

    public CharacterAchievementPK(int guid, short achievement) {
        this.guid = guid;
        this.achievement = achievement;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public short getAchievement() {
        return achievement;
    }

    public void setAchievement(short achievement) {
        this.achievement = achievement;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) achievement;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterAchievementPK)) {
            return false;
        }
        CharacterAchievementPK other = (CharacterAchievementPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.achievement != other.achievement) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterAchievementPK[ guid=" + guid + ", achievement=" + achievement + " ]";
    }
    
}
