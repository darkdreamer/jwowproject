/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "creature_respawn")
@NamedQueries({
    @NamedQuery(name = "CreatureRespawn.findAll", query = "SELECT c FROM CreatureRespawn c"),
    @NamedQuery(name = "CreatureRespawn.findByGuid", query = "SELECT c FROM CreatureRespawn c WHERE c.creatureRespawnPK.guid = :guid"),
    @NamedQuery(name = "CreatureRespawn.findByRespawnTime", query = "SELECT c FROM CreatureRespawn c WHERE c.respawnTime = :respawnTime"),
    @NamedQuery(name = "CreatureRespawn.findByMapId", query = "SELECT c FROM CreatureRespawn c WHERE c.mapId = :mapId"),
    @NamedQuery(name = "CreatureRespawn.findByInstanceId", query = "SELECT c FROM CreatureRespawn c WHERE c.creatureRespawnPK.instanceId = :instanceId")})
public class CreatureRespawn implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CreatureRespawnPK creatureRespawnPK;
    @Basic(optional = false)
    @Column(name = "respawnTime")
    private int respawnTime;
    @Basic(optional = false)
    @Column(name = "mapId")
    private short mapId;

    public CreatureRespawn() {
    }

    public CreatureRespawn(CreatureRespawnPK creatureRespawnPK) {
        this.creatureRespawnPK = creatureRespawnPK;
    }

    public CreatureRespawn(CreatureRespawnPK creatureRespawnPK, int respawnTime, short mapId) {
        this.creatureRespawnPK = creatureRespawnPK;
        this.respawnTime = respawnTime;
        this.mapId = mapId;
    }

    public CreatureRespawn(int guid, int instanceId) {
        this.creatureRespawnPK = new CreatureRespawnPK(guid, instanceId);
    }

    public CreatureRespawnPK getCreatureRespawnPK() {
        return creatureRespawnPK;
    }

    public void setCreatureRespawnPK(CreatureRespawnPK creatureRespawnPK) {
        this.creatureRespawnPK = creatureRespawnPK;
    }

    public int getRespawnTime() {
        return respawnTime;
    }

    public void setRespawnTime(int respawnTime) {
        this.respawnTime = respawnTime;
    }

    public short getMapId() {
        return mapId;
    }

    public void setMapId(short mapId) {
        this.mapId = mapId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (creatureRespawnPK != null ? creatureRespawnPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CreatureRespawn)) {
            return false;
        }
        CreatureRespawn other = (CreatureRespawn) object;
        if ((this.creatureRespawnPK == null && other.creatureRespawnPK != null) || (this.creatureRespawnPK != null && !this.creatureRespawnPK.equals(other.creatureRespawnPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CreatureRespawn[ creatureRespawnPK=" + creatureRespawnPK + " ]";
    }
    
}
