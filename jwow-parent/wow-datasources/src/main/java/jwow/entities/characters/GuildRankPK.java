/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class GuildRankPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guildid")
    private int guildid;
    @Basic(optional = false)
    @Column(name = "rid")
    private short rid;

    public GuildRankPK() {
    }

    public GuildRankPK(int guildid, short rid) {
        this.guildid = guildid;
        this.rid = rid;
    }

    public int getGuildid() {
        return guildid;
    }

    public void setGuildid(int guildid) {
        this.guildid = guildid;
    }

    public short getRid() {
        return rid;
    }

    public void setRid(short rid) {
        this.rid = rid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guildid;
        hash += (int) rid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuildRankPK)) {
            return false;
        }
        GuildRankPK other = (GuildRankPK) object;
        if (this.guildid != other.guildid) {
            return false;
        }
        if (this.rid != other.rid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GuildRankPK[ guildid=" + guildid + ", rid=" + rid + " ]";
    }
    
}
