/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "calendar_invites")
@NamedQueries({
    @NamedQuery(name = "CalendarInvites.findAll", query = "SELECT c FROM CalendarInvites c"),
    @NamedQuery(name = "CalendarInvites.findById", query = "SELECT c FROM CalendarInvites c WHERE c.id = :id"),
    @NamedQuery(name = "CalendarInvites.findByEvent", query = "SELECT c FROM CalendarInvites c WHERE c.event = :event"),
    @NamedQuery(name = "CalendarInvites.findByInvitee", query = "SELECT c FROM CalendarInvites c WHERE c.invitee = :invitee"),
    @NamedQuery(name = "CalendarInvites.findBySender", query = "SELECT c FROM CalendarInvites c WHERE c.sender = :sender"),
    @NamedQuery(name = "CalendarInvites.findByStatus", query = "SELECT c FROM CalendarInvites c WHERE c.status = :status"),
    @NamedQuery(name = "CalendarInvites.findByStatustime", query = "SELECT c FROM CalendarInvites c WHERE c.statustime = :statustime"),
    @NamedQuery(name = "CalendarInvites.findByRank", query = "SELECT c FROM CalendarInvites c WHERE c.rank = :rank"),
    @NamedQuery(name = "CalendarInvites.findByText", query = "SELECT c FROM CalendarInvites c WHERE c.text = :text")})
public class CalendarInvites implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "event")
    private long event;
    @Basic(optional = false)
    @Column(name = "invitee")
    private int invitee;
    @Basic(optional = false)
    @Column(name = "sender")
    private int sender;
    @Basic(optional = false)
    @Column(name = "status")
    private boolean status;
    @Basic(optional = false)
    @Column(name = "statustime")
    private int statustime;
    @Basic(optional = false)
    @Column(name = "rank")
    private boolean rank;
    @Basic(optional = false)
    @Column(name = "text")
    private String text;

    public CalendarInvites() {
    }

    public CalendarInvites(Long id) {
        this.id = id;
    }

    public CalendarInvites(Long id, long event, int invitee, int sender, boolean status, int statustime, boolean rank, String text) {
        this.id = id;
        this.event = event;
        this.invitee = invitee;
        this.sender = sender;
        this.status = status;
        this.statustime = statustime;
        this.rank = rank;
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getEvent() {
        return event;
    }

    public void setEvent(long event) {
        this.event = event;
    }

    public int getInvitee() {
        return invitee;
    }

    public void setInvitee(int invitee) {
        this.invitee = invitee;
    }

    public int getSender() {
        return sender;
    }

    public void setSender(int sender) {
        this.sender = sender;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getStatustime() {
        return statustime;
    }

    public void setStatustime(int statustime) {
        this.statustime = statustime;
    }

    public boolean getRank() {
        return rank;
    }

    public void setRank(boolean rank) {
        this.rank = rank;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalendarInvites)) {
            return false;
        }
        CalendarInvites other = (CalendarInvites) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CalendarInvites[ id=" + id + " ]";
    }
    
}
