/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "item_instance")
@NamedQueries({
    @NamedQuery(name = "ItemInstance.findAll", query = "SELECT i FROM ItemInstance i"),
    @NamedQuery(name = "ItemInstance.findByGuid", query = "SELECT i FROM ItemInstance i WHERE i.guid = :guid"),
    @NamedQuery(name = "ItemInstance.findByItemEntry", query = "SELECT i FROM ItemInstance i WHERE i.itemEntry = :itemEntry"),
    @NamedQuery(name = "ItemInstance.findByOwnerGuid", query = "SELECT i FROM ItemInstance i WHERE i.ownerGuid = :ownerGuid"),
    @NamedQuery(name = "ItemInstance.findByCreatorGuid", query = "SELECT i FROM ItemInstance i WHERE i.creatorGuid = :creatorGuid"),
    @NamedQuery(name = "ItemInstance.findByGiftCreatorGuid", query = "SELECT i FROM ItemInstance i WHERE i.giftCreatorGuid = :giftCreatorGuid"),
    @NamedQuery(name = "ItemInstance.findByCount", query = "SELECT i FROM ItemInstance i WHERE i.count = :count"),
    @NamedQuery(name = "ItemInstance.findByDuration", query = "SELECT i FROM ItemInstance i WHERE i.duration = :duration"),
    @NamedQuery(name = "ItemInstance.findByCharges", query = "SELECT i FROM ItemInstance i WHERE i.charges = :charges"),
    @NamedQuery(name = "ItemInstance.findByFlags", query = "SELECT i FROM ItemInstance i WHERE i.flags = :flags"),
    @NamedQuery(name = "ItemInstance.findByRandomPropertyId", query = "SELECT i FROM ItemInstance i WHERE i.randomPropertyId = :randomPropertyId"),
    @NamedQuery(name = "ItemInstance.findByDurability", query = "SELECT i FROM ItemInstance i WHERE i.durability = :durability"),
    @NamedQuery(name = "ItemInstance.findByPlayedTime", query = "SELECT i FROM ItemInstance i WHERE i.playedTime = :playedTime")})
public class ItemInstance implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "guid")
    private Integer guid;
    @Basic(optional = false)
    @Column(name = "itemEntry")
    private int itemEntry;
    @Basic(optional = false)
    @Column(name = "owner_guid")
    private int ownerGuid;
    @Basic(optional = false)
    @Column(name = "creatorGuid")
    private int creatorGuid;
    @Basic(optional = false)
    @Column(name = "giftCreatorGuid")
    private int giftCreatorGuid;
    @Basic(optional = false)
    @Column(name = "count")
    private int count;
    @Basic(optional = false)
    @Column(name = "duration")
    private int duration;
    @Column(name = "charges")
    private String charges;
    @Basic(optional = false)
    @Column(name = "flags")
    private int flags;
    @Basic(optional = false)
    @Lob
    @Column(name = "enchantments")
    private String enchantments;
    @Basic(optional = false)
    @Column(name = "randomPropertyId")
    private short randomPropertyId;
    @Basic(optional = false)
    @Column(name = "durability")
    private short durability;
    @Basic(optional = false)
    @Column(name = "playedTime")
    private int playedTime;
    @Lob
    @Column(name = "text")
    private String text;

    public ItemInstance() {
    }

    public ItemInstance(Integer guid) {
        this.guid = guid;
    }

    public ItemInstance(Integer guid, int itemEntry, int ownerGuid, int creatorGuid, int giftCreatorGuid, int count, int duration, int flags, String enchantments, short randomPropertyId, short durability, int playedTime) {
        this.guid = guid;
        this.itemEntry = itemEntry;
        this.ownerGuid = ownerGuid;
        this.creatorGuid = creatorGuid;
        this.giftCreatorGuid = giftCreatorGuid;
        this.count = count;
        this.duration = duration;
        this.flags = flags;
        this.enchantments = enchantments;
        this.randomPropertyId = randomPropertyId;
        this.durability = durability;
        this.playedTime = playedTime;
    }

    public Integer getGuid() {
        return guid;
    }

    public void setGuid(Integer guid) {
        this.guid = guid;
    }

    public int getItemEntry() {
        return itemEntry;
    }

    public void setItemEntry(int itemEntry) {
        this.itemEntry = itemEntry;
    }

    public int getOwnerGuid() {
        return ownerGuid;
    }

    public void setOwnerGuid(int ownerGuid) {
        this.ownerGuid = ownerGuid;
    }

    public int getCreatorGuid() {
        return creatorGuid;
    }

    public void setCreatorGuid(int creatorGuid) {
        this.creatorGuid = creatorGuid;
    }

    public int getGiftCreatorGuid() {
        return giftCreatorGuid;
    }

    public void setGiftCreatorGuid(int giftCreatorGuid) {
        this.giftCreatorGuid = giftCreatorGuid;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public String getEnchantments() {
        return enchantments;
    }

    public void setEnchantments(String enchantments) {
        this.enchantments = enchantments;
    }

    public short getRandomPropertyId() {
        return randomPropertyId;
    }

    public void setRandomPropertyId(short randomPropertyId) {
        this.randomPropertyId = randomPropertyId;
    }

    public short getDurability() {
        return durability;
    }

    public void setDurability(short durability) {
        this.durability = durability;
    }

    public int getPlayedTime() {
        return playedTime;
    }

    public void setPlayedTime(int playedTime) {
        this.playedTime = playedTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guid != null ? guid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemInstance)) {
            return false;
        }
        ItemInstance other = (ItemInstance) object;
        if ((this.guid == null && other.guid != null) || (this.guid != null && !this.guid.equals(other.guid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.ItemInstance[ guid=" + guid + " ]";
    }
    
}
