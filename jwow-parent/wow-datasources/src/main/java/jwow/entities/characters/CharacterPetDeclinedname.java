/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_pet_declinedname")
@NamedQueries({
    @NamedQuery(name = "CharacterPetDeclinedname.findAll", query = "SELECT c FROM CharacterPetDeclinedname c"),
    @NamedQuery(name = "CharacterPetDeclinedname.findById", query = "SELECT c FROM CharacterPetDeclinedname c WHERE c.id = :id"),
    @NamedQuery(name = "CharacterPetDeclinedname.findByOwner", query = "SELECT c FROM CharacterPetDeclinedname c WHERE c.owner = :owner"),
    @NamedQuery(name = "CharacterPetDeclinedname.findByGenitive", query = "SELECT c FROM CharacterPetDeclinedname c WHERE c.genitive = :genitive"),
    @NamedQuery(name = "CharacterPetDeclinedname.findByDative", query = "SELECT c FROM CharacterPetDeclinedname c WHERE c.dative = :dative"),
    @NamedQuery(name = "CharacterPetDeclinedname.findByAccusative", query = "SELECT c FROM CharacterPetDeclinedname c WHERE c.accusative = :accusative"),
    @NamedQuery(name = "CharacterPetDeclinedname.findByInstrumental", query = "SELECT c FROM CharacterPetDeclinedname c WHERE c.instrumental = :instrumental"),
    @NamedQuery(name = "CharacterPetDeclinedname.findByPrepositional", query = "SELECT c FROM CharacterPetDeclinedname c WHERE c.prepositional = :prepositional")})
public class CharacterPetDeclinedname implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "owner")
    private int owner;
    @Basic(optional = false)
    @Column(name = "genitive")
    private String genitive;
    @Basic(optional = false)
    @Column(name = "dative")
    private String dative;
    @Basic(optional = false)
    @Column(name = "accusative")
    private String accusative;
    @Basic(optional = false)
    @Column(name = "instrumental")
    private String instrumental;
    @Basic(optional = false)
    @Column(name = "prepositional")
    private String prepositional;

    public CharacterPetDeclinedname() {
    }

    public CharacterPetDeclinedname(Integer id) {
        this.id = id;
    }

    public CharacterPetDeclinedname(Integer id, int owner, String genitive, String dative, String accusative, String instrumental, String prepositional) {
        this.id = id;
        this.owner = owner;
        this.genitive = genitive;
        this.dative = dative;
        this.accusative = accusative;
        this.instrumental = instrumental;
        this.prepositional = prepositional;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public String getGenitive() {
        return genitive;
    }

    public void setGenitive(String genitive) {
        this.genitive = genitive;
    }

    public String getDative() {
        return dative;
    }

    public void setDative(String dative) {
        this.dative = dative;
    }

    public String getAccusative() {
        return accusative;
    }

    public void setAccusative(String accusative) {
        this.accusative = accusative;
    }

    public String getInstrumental() {
        return instrumental;
    }

    public void setInstrumental(String instrumental) {
        this.instrumental = instrumental;
    }

    public String getPrepositional() {
        return prepositional;
    }

    public void setPrepositional(String prepositional) {
        this.prepositional = prepositional;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterPetDeclinedname)) {
            return false;
        }
        CharacterPetDeclinedname other = (CharacterPetDeclinedname) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterPetDeclinedname[ id=" + id + " ]";
    }
    
}
