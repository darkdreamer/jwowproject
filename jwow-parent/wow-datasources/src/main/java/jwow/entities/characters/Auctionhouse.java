/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "auctionhouse")
@NamedQueries({
    @NamedQuery(name = "Auctionhouse.findAll", query = "SELECT a FROM Auctionhouse a"),
    @NamedQuery(name = "Auctionhouse.findById", query = "SELECT a FROM Auctionhouse a WHERE a.id = :id"),
    @NamedQuery(name = "Auctionhouse.findByAuctioneerguid", query = "SELECT a FROM Auctionhouse a WHERE a.auctioneerguid = :auctioneerguid"),
    @NamedQuery(name = "Auctionhouse.findByItemguid", query = "SELECT a FROM Auctionhouse a WHERE a.itemguid = :itemguid"),
    @NamedQuery(name = "Auctionhouse.findByItemowner", query = "SELECT a FROM Auctionhouse a WHERE a.itemowner = :itemowner"),
    @NamedQuery(name = "Auctionhouse.findByBuyoutprice", query = "SELECT a FROM Auctionhouse a WHERE a.buyoutprice = :buyoutprice"),
    @NamedQuery(name = "Auctionhouse.findByTime", query = "SELECT a FROM Auctionhouse a WHERE a.time = :time"),
    @NamedQuery(name = "Auctionhouse.findByBuyguid", query = "SELECT a FROM Auctionhouse a WHERE a.buyguid = :buyguid"),
    @NamedQuery(name = "Auctionhouse.findByLastbid", query = "SELECT a FROM Auctionhouse a WHERE a.lastbid = :lastbid"),
    @NamedQuery(name = "Auctionhouse.findByStartbid", query = "SELECT a FROM Auctionhouse a WHERE a.startbid = :startbid"),
    @NamedQuery(name = "Auctionhouse.findByDeposit", query = "SELECT a FROM Auctionhouse a WHERE a.deposit = :deposit")})
public class Auctionhouse implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "auctioneerguid")
    private int auctioneerguid;
    @Basic(optional = false)
    @Column(name = "itemguid")
    private int itemguid;
    @Basic(optional = false)
    @Column(name = "itemowner")
    private int itemowner;
    @Basic(optional = false)
    @Column(name = "buyoutprice")
    private int buyoutprice;
    @Basic(optional = false)
    @Column(name = "time")
    private int time;
    @Basic(optional = false)
    @Column(name = "buyguid")
    private int buyguid;
    @Basic(optional = false)
    @Column(name = "lastbid")
    private int lastbid;
    @Basic(optional = false)
    @Column(name = "startbid")
    private int startbid;
    @Basic(optional = false)
    @Column(name = "deposit")
    private int deposit;

    public Auctionhouse() {
    }

    public Auctionhouse(Integer id) {
        this.id = id;
    }

    public Auctionhouse(Integer id, int auctioneerguid, int itemguid, int itemowner, int buyoutprice, int time, int buyguid, int lastbid, int startbid, int deposit) {
        this.id = id;
        this.auctioneerguid = auctioneerguid;
        this.itemguid = itemguid;
        this.itemowner = itemowner;
        this.buyoutprice = buyoutprice;
        this.time = time;
        this.buyguid = buyguid;
        this.lastbid = lastbid;
        this.startbid = startbid;
        this.deposit = deposit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAuctioneerguid() {
        return auctioneerguid;
    }

    public void setAuctioneerguid(int auctioneerguid) {
        this.auctioneerguid = auctioneerguid;
    }

    public int getItemguid() {
        return itemguid;
    }

    public void setItemguid(int itemguid) {
        this.itemguid = itemguid;
    }

    public int getItemowner() {
        return itemowner;
    }

    public void setItemowner(int itemowner) {
        this.itemowner = itemowner;
    }

    public int getBuyoutprice() {
        return buyoutprice;
    }

    public void setBuyoutprice(int buyoutprice) {
        this.buyoutprice = buyoutprice;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getBuyguid() {
        return buyguid;
    }

    public void setBuyguid(int buyguid) {
        this.buyguid = buyguid;
    }

    public int getLastbid() {
        return lastbid;
    }

    public void setLastbid(int lastbid) {
        this.lastbid = lastbid;
    }

    public int getStartbid() {
        return startbid;
    }

    public void setStartbid(int startbid) {
        this.startbid = startbid;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Auctionhouse)) {
            return false;
        }
        Auctionhouse other = (Auctionhouse) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.Auctionhouse[ id=" + id + " ]";
    }
    
}
