/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "arena_team")
@NamedQueries({
    @NamedQuery(name = "ArenaTeam.findAll", query = "SELECT a FROM ArenaTeam a"),
    @NamedQuery(name = "ArenaTeam.findByArenaTeamId", query = "SELECT a FROM ArenaTeam a WHERE a.arenaTeamId = :arenaTeamId"),
    @NamedQuery(name = "ArenaTeam.findByName", query = "SELECT a FROM ArenaTeam a WHERE a.name = :name"),
    @NamedQuery(name = "ArenaTeam.findByCaptainGuid", query = "SELECT a FROM ArenaTeam a WHERE a.captainGuid = :captainGuid"),
    @NamedQuery(name = "ArenaTeam.findByType", query = "SELECT a FROM ArenaTeam a WHERE a.type = :type"),
    @NamedQuery(name = "ArenaTeam.findByRating", query = "SELECT a FROM ArenaTeam a WHERE a.rating = :rating"),
    @NamedQuery(name = "ArenaTeam.findBySeasonGames", query = "SELECT a FROM ArenaTeam a WHERE a.seasonGames = :seasonGames"),
    @NamedQuery(name = "ArenaTeam.findBySeasonWins", query = "SELECT a FROM ArenaTeam a WHERE a.seasonWins = :seasonWins"),
    @NamedQuery(name = "ArenaTeam.findByWeekGames", query = "SELECT a FROM ArenaTeam a WHERE a.weekGames = :weekGames"),
    @NamedQuery(name = "ArenaTeam.findByWeekWins", query = "SELECT a FROM ArenaTeam a WHERE a.weekWins = :weekWins"),
    @NamedQuery(name = "ArenaTeam.findByRank", query = "SELECT a FROM ArenaTeam a WHERE a.rank = :rank"),
    @NamedQuery(name = "ArenaTeam.findByBackgroundColor", query = "SELECT a FROM ArenaTeam a WHERE a.backgroundColor = :backgroundColor"),
    @NamedQuery(name = "ArenaTeam.findByEmblemStyle", query = "SELECT a FROM ArenaTeam a WHERE a.emblemStyle = :emblemStyle"),
    @NamedQuery(name = "ArenaTeam.findByEmblemColor", query = "SELECT a FROM ArenaTeam a WHERE a.emblemColor = :emblemColor"),
    @NamedQuery(name = "ArenaTeam.findByBorderStyle", query = "SELECT a FROM ArenaTeam a WHERE a.borderStyle = :borderStyle"),
    @NamedQuery(name = "ArenaTeam.findByBorderColor", query = "SELECT a FROM ArenaTeam a WHERE a.borderColor = :borderColor")})
public class ArenaTeam implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "arenaTeamId")
    private Integer arenaTeamId;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "captainGuid")
    private int captainGuid;
    @Basic(optional = false)
    @Column(name = "type")
    private short type;
    @Basic(optional = false)
    @Column(name = "rating")
    private short rating;
    @Basic(optional = false)
    @Column(name = "seasonGames")
    private short seasonGames;
    @Basic(optional = false)
    @Column(name = "seasonWins")
    private short seasonWins;
    @Basic(optional = false)
    @Column(name = "weekGames")
    private short weekGames;
    @Basic(optional = false)
    @Column(name = "weekWins")
    private short weekWins;
    @Basic(optional = false)
    @Column(name = "rank")
    private int rank;
    @Basic(optional = false)
    @Column(name = "backgroundColor")
    private int backgroundColor;
    @Basic(optional = false)
    @Column(name = "emblemStyle")
    private short emblemStyle;
    @Basic(optional = false)
    @Column(name = "emblemColor")
    private int emblemColor;
    @Basic(optional = false)
    @Column(name = "borderStyle")
    private short borderStyle;
    @Basic(optional = false)
    @Column(name = "borderColor")
    private int borderColor;

    public ArenaTeam() {
    }

    public ArenaTeam(Integer arenaTeamId) {
        this.arenaTeamId = arenaTeamId;
    }

    public ArenaTeam(Integer arenaTeamId, String name, int captainGuid, short type, short rating, short seasonGames, short seasonWins, short weekGames, short weekWins, int rank, int backgroundColor, short emblemStyle, int emblemColor, short borderStyle, int borderColor) {
        this.arenaTeamId = arenaTeamId;
        this.name = name;
        this.captainGuid = captainGuid;
        this.type = type;
        this.rating = rating;
        this.seasonGames = seasonGames;
        this.seasonWins = seasonWins;
        this.weekGames = weekGames;
        this.weekWins = weekWins;
        this.rank = rank;
        this.backgroundColor = backgroundColor;
        this.emblemStyle = emblemStyle;
        this.emblemColor = emblemColor;
        this.borderStyle = borderStyle;
        this.borderColor = borderColor;
    }

    public Integer getArenaTeamId() {
        return arenaTeamId;
    }

    public void setArenaTeamId(Integer arenaTeamId) {
        this.arenaTeamId = arenaTeamId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCaptainGuid() {
        return captainGuid;
    }

    public void setCaptainGuid(int captainGuid) {
        this.captainGuid = captainGuid;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public short getRating() {
        return rating;
    }

    public void setRating(short rating) {
        this.rating = rating;
    }

    public short getSeasonGames() {
        return seasonGames;
    }

    public void setSeasonGames(short seasonGames) {
        this.seasonGames = seasonGames;
    }

    public short getSeasonWins() {
        return seasonWins;
    }

    public void setSeasonWins(short seasonWins) {
        this.seasonWins = seasonWins;
    }

    public short getWeekGames() {
        return weekGames;
    }

    public void setWeekGames(short weekGames) {
        this.weekGames = weekGames;
    }

    public short getWeekWins() {
        return weekWins;
    }

    public void setWeekWins(short weekWins) {
        this.weekWins = weekWins;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public short getEmblemStyle() {
        return emblemStyle;
    }

    public void setEmblemStyle(short emblemStyle) {
        this.emblemStyle = emblemStyle;
    }

    public int getEmblemColor() {
        return emblemColor;
    }

    public void setEmblemColor(int emblemColor) {
        this.emblemColor = emblemColor;
    }

    public short getBorderStyle() {
        return borderStyle;
    }

    public void setBorderStyle(short borderStyle) {
        this.borderStyle = borderStyle;
    }

    public int getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (arenaTeamId != null ? arenaTeamId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArenaTeam)) {
            return false;
        }
        ArenaTeam other = (ArenaTeam) object;
        if ((this.arenaTeamId == null && other.arenaTeamId != null) || (this.arenaTeamId != null && !this.arenaTeamId.equals(other.arenaTeamId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.ArenaTeam[ arenaTeamId=" + arenaTeamId + " ]";
    }
    
}
