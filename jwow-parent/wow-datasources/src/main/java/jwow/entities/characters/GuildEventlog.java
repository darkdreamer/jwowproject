/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "guild_eventlog")
@NamedQueries({
    @NamedQuery(name = "GuildEventlog.findAll", query = "SELECT g FROM GuildEventlog g"),
    @NamedQuery(name = "GuildEventlog.findByGuildid", query = "SELECT g FROM GuildEventlog g WHERE g.guildEventlogPK.guildid = :guildid"),
    @NamedQuery(name = "GuildEventlog.findByLogGuid", query = "SELECT g FROM GuildEventlog g WHERE g.guildEventlogPK.logGuid = :logGuid"),
    @NamedQuery(name = "GuildEventlog.findByEventType", query = "SELECT g FROM GuildEventlog g WHERE g.eventType = :eventType"),
    @NamedQuery(name = "GuildEventlog.findByPlayerGuid1", query = "SELECT g FROM GuildEventlog g WHERE g.playerGuid1 = :playerGuid1"),
    @NamedQuery(name = "GuildEventlog.findByPlayerGuid2", query = "SELECT g FROM GuildEventlog g WHERE g.playerGuid2 = :playerGuid2"),
    @NamedQuery(name = "GuildEventlog.findByNewRank", query = "SELECT g FROM GuildEventlog g WHERE g.newRank = :newRank"),
    @NamedQuery(name = "GuildEventlog.findByTimeStamp", query = "SELECT g FROM GuildEventlog g WHERE g.timeStamp = :timeStamp")})
public class GuildEventlog implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GuildEventlogPK guildEventlogPK;
    @Basic(optional = false)
    @Column(name = "EventType")
    private short eventType;
    @Basic(optional = false)
    @Column(name = "PlayerGuid1")
    private int playerGuid1;
    @Basic(optional = false)
    @Column(name = "PlayerGuid2")
    private int playerGuid2;
    @Basic(optional = false)
    @Column(name = "NewRank")
    private short newRank;
    @Basic(optional = false)
    @Column(name = "TimeStamp")
    private int timeStamp;

    public GuildEventlog() {
    }

    public GuildEventlog(GuildEventlogPK guildEventlogPK) {
        this.guildEventlogPK = guildEventlogPK;
    }

    public GuildEventlog(GuildEventlogPK guildEventlogPK, short eventType, int playerGuid1, int playerGuid2, short newRank, int timeStamp) {
        this.guildEventlogPK = guildEventlogPK;
        this.eventType = eventType;
        this.playerGuid1 = playerGuid1;
        this.playerGuid2 = playerGuid2;
        this.newRank = newRank;
        this.timeStamp = timeStamp;
    }

    public GuildEventlog(int guildid, int logGuid) {
        this.guildEventlogPK = new GuildEventlogPK(guildid, logGuid);
    }

    public GuildEventlogPK getGuildEventlogPK() {
        return guildEventlogPK;
    }

    public void setGuildEventlogPK(GuildEventlogPK guildEventlogPK) {
        this.guildEventlogPK = guildEventlogPK;
    }

    public short getEventType() {
        return eventType;
    }

    public void setEventType(short eventType) {
        this.eventType = eventType;
    }

    public int getPlayerGuid1() {
        return playerGuid1;
    }

    public void setPlayerGuid1(int playerGuid1) {
        this.playerGuid1 = playerGuid1;
    }

    public int getPlayerGuid2() {
        return playerGuid2;
    }

    public void setPlayerGuid2(int playerGuid2) {
        this.playerGuid2 = playerGuid2;
    }

    public short getNewRank() {
        return newRank;
    }

    public void setNewRank(short newRank) {
        this.newRank = newRank;
    }

    public int getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(int timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guildEventlogPK != null ? guildEventlogPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuildEventlog)) {
            return false;
        }
        GuildEventlog other = (GuildEventlog) object;
        if ((this.guildEventlogPK == null && other.guildEventlogPK != null) || (this.guildEventlogPK != null && !this.guildEventlogPK.equals(other.guildEventlogPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GuildEventlog[ guildEventlogPK=" + guildEventlogPK + " ]";
    }
    
}
