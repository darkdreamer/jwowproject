/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "instance_reset")
@NamedQueries({
    @NamedQuery(name = "InstanceReset.findAll", query = "SELECT i FROM InstanceReset i"),
    @NamedQuery(name = "InstanceReset.findByMapid", query = "SELECT i FROM InstanceReset i WHERE i.instanceResetPK.mapid = :mapid"),
    @NamedQuery(name = "InstanceReset.findByDifficulty", query = "SELECT i FROM InstanceReset i WHERE i.instanceResetPK.difficulty = :difficulty"),
    @NamedQuery(name = "InstanceReset.findByResettime", query = "SELECT i FROM InstanceReset i WHERE i.resettime = :resettime")})
public class InstanceReset implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InstanceResetPK instanceResetPK;
    @Basic(optional = false)
    @Column(name = "resettime")
    private int resettime;

    public InstanceReset() {
    }

    public InstanceReset(InstanceResetPK instanceResetPK) {
        this.instanceResetPK = instanceResetPK;
    }

    public InstanceReset(InstanceResetPK instanceResetPK, int resettime) {
        this.instanceResetPK = instanceResetPK;
        this.resettime = resettime;
    }

    public InstanceReset(short mapid, short difficulty) {
        this.instanceResetPK = new InstanceResetPK(mapid, difficulty);
    }

    public InstanceResetPK getInstanceResetPK() {
        return instanceResetPK;
    }

    public void setInstanceResetPK(InstanceResetPK instanceResetPK) {
        this.instanceResetPK = instanceResetPK;
    }

    public int getResettime() {
        return resettime;
    }

    public void setResettime(int resettime) {
        this.resettime = resettime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instanceResetPK != null ? instanceResetPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstanceReset)) {
            return false;
        }
        InstanceReset other = (InstanceReset) object;
        if ((this.instanceResetPK == null && other.instanceResetPK != null) || (this.instanceResetPK != null && !this.instanceResetPK.equals(other.instanceResetPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.InstanceReset[ instanceResetPK=" + instanceResetPK + " ]";
    }
    
}
