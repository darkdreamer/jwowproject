/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_glyphs")
@NamedQueries({
    @NamedQuery(name = "CharacterGlyphs.findAll", query = "SELECT c FROM CharacterGlyphs c"),
    @NamedQuery(name = "CharacterGlyphs.findByGuid", query = "SELECT c FROM CharacterGlyphs c WHERE c.characterGlyphsPK.guid = :guid"),
    @NamedQuery(name = "CharacterGlyphs.findBySpec", query = "SELECT c FROM CharacterGlyphs c WHERE c.characterGlyphsPK.spec = :spec"),
    @NamedQuery(name = "CharacterGlyphs.findByGlyph1", query = "SELECT c FROM CharacterGlyphs c WHERE c.glyph1 = :glyph1"),
    @NamedQuery(name = "CharacterGlyphs.findByGlyph2", query = "SELECT c FROM CharacterGlyphs c WHERE c.glyph2 = :glyph2"),
    @NamedQuery(name = "CharacterGlyphs.findByGlyph3", query = "SELECT c FROM CharacterGlyphs c WHERE c.glyph3 = :glyph3"),
    @NamedQuery(name = "CharacterGlyphs.findByGlyph4", query = "SELECT c FROM CharacterGlyphs c WHERE c.glyph4 = :glyph4"),
    @NamedQuery(name = "CharacterGlyphs.findByGlyph5", query = "SELECT c FROM CharacterGlyphs c WHERE c.glyph5 = :glyph5"),
    @NamedQuery(name = "CharacterGlyphs.findByGlyph6", query = "SELECT c FROM CharacterGlyphs c WHERE c.glyph6 = :glyph6")})
public class CharacterGlyphs implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterGlyphsPK characterGlyphsPK;
    @Column(name = "glyph1")
    private Short glyph1;
    @Column(name = "glyph2")
    private Short glyph2;
    @Column(name = "glyph3")
    private Short glyph3;
    @Column(name = "glyph4")
    private Short glyph4;
    @Column(name = "glyph5")
    private Short glyph5;
    @Column(name = "glyph6")
    private Short glyph6;

    public CharacterGlyphs() {
    }

    public CharacterGlyphs(CharacterGlyphsPK characterGlyphsPK) {
        this.characterGlyphsPK = characterGlyphsPK;
    }

    public CharacterGlyphs(int guid, short spec) {
        this.characterGlyphsPK = new CharacterGlyphsPK(guid, spec);
    }

    public CharacterGlyphsPK getCharacterGlyphsPK() {
        return characterGlyphsPK;
    }

    public void setCharacterGlyphsPK(CharacterGlyphsPK characterGlyphsPK) {
        this.characterGlyphsPK = characterGlyphsPK;
    }

    public Short getGlyph1() {
        return glyph1;
    }

    public void setGlyph1(Short glyph1) {
        this.glyph1 = glyph1;
    }

    public Short getGlyph2() {
        return glyph2;
    }

    public void setGlyph2(Short glyph2) {
        this.glyph2 = glyph2;
    }

    public Short getGlyph3() {
        return glyph3;
    }

    public void setGlyph3(Short glyph3) {
        this.glyph3 = glyph3;
    }

    public Short getGlyph4() {
        return glyph4;
    }

    public void setGlyph4(Short glyph4) {
        this.glyph4 = glyph4;
    }

    public Short getGlyph5() {
        return glyph5;
    }

    public void setGlyph5(Short glyph5) {
        this.glyph5 = glyph5;
    }

    public Short getGlyph6() {
        return glyph6;
    }

    public void setGlyph6(Short glyph6) {
        this.glyph6 = glyph6;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterGlyphsPK != null ? characterGlyphsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterGlyphs)) {
            return false;
        }
        CharacterGlyphs other = (CharacterGlyphs) object;
        if ((this.characterGlyphsPK == null && other.characterGlyphsPK != null) || (this.characterGlyphsPK != null && !this.characterGlyphsPK.equals(other.characterGlyphsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterGlyphs[ characterGlyphsPK=" + characterGlyphsPK + " ]";
    }
    
}
