/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class PoolQuestSavePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "pool_id")
    private int poolId;
    @Basic(optional = false)
    @Column(name = "quest_id")
    private int questId;

    public PoolQuestSavePK() {
    }

    public PoolQuestSavePK(int poolId, int questId) {
        this.poolId = poolId;
        this.questId = questId;
    }

    public int getPoolId() {
        return poolId;
    }

    public void setPoolId(int poolId) {
        this.poolId = poolId;
    }

    public int getQuestId() {
        return questId;
    }

    public void setQuestId(int questId) {
        this.questId = questId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) poolId;
        hash += (int) questId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PoolQuestSavePK)) {
            return false;
        }
        PoolQuestSavePK other = (PoolQuestSavePK) object;
        if (this.poolId != other.poolId) {
            return false;
        }
        if (this.questId != other.questId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.PoolQuestSavePK[ poolId=" + poolId + ", questId=" + questId + " ]";
    }
    
}
