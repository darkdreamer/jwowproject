/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterQueststatusWeeklyPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "quest")
    private int quest;

    public CharacterQueststatusWeeklyPK() {
    }

    public CharacterQueststatusWeeklyPK(int guid, int quest) {
        this.guid = guid;
        this.quest = quest;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public int getQuest() {
        return quest;
    }

    public void setQuest(int quest) {
        this.quest = quest;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) quest;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterQueststatusWeeklyPK)) {
            return false;
        }
        CharacterQueststatusWeeklyPK other = (CharacterQueststatusWeeklyPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.quest != other.quest) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterQueststatusWeeklyPK[ guid=" + guid + ", quest=" + quest + " ]";
    }
    
}
