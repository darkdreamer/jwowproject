/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class GameEventConditionSavePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "eventEntry")
    private short eventEntry;
    @Basic(optional = false)
    @Column(name = "condition_id")
    private int conditionId;

    public GameEventConditionSavePK() {
    }

    public GameEventConditionSavePK(short eventEntry, int conditionId) {
        this.eventEntry = eventEntry;
        this.conditionId = conditionId;
    }

    public short getEventEntry() {
        return eventEntry;
    }

    public void setEventEntry(short eventEntry) {
        this.eventEntry = eventEntry;
    }

    public int getConditionId() {
        return conditionId;
    }

    public void setConditionId(int conditionId) {
        this.conditionId = conditionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) eventEntry;
        hash += (int) conditionId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GameEventConditionSavePK)) {
            return false;
        }
        GameEventConditionSavePK other = (GameEventConditionSavePK) object;
        if (this.eventEntry != other.eventEntry) {
            return false;
        }
        if (this.conditionId != other.conditionId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GameEventConditionSavePK[ eventEntry=" + eventEntry + ", conditionId=" + conditionId + " ]";
    }
    
}
