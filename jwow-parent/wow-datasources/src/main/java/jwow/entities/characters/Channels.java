/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "channels")
@NamedQueries({
    @NamedQuery(name = "Channels.findAll", query = "SELECT c FROM Channels c"),
    @NamedQuery(name = "Channels.findByName", query = "SELECT c FROM Channels c WHERE c.channelsPK.name = :name"),
    @NamedQuery(name = "Channels.findByTeam", query = "SELECT c FROM Channels c WHERE c.channelsPK.team = :team"),
    @NamedQuery(name = "Channels.findByAnnounce", query = "SELECT c FROM Channels c WHERE c.announce = :announce"),
    @NamedQuery(name = "Channels.findByOwnership", query = "SELECT c FROM Channels c WHERE c.ownership = :ownership"),
    @NamedQuery(name = "Channels.findByPassword", query = "SELECT c FROM Channels c WHERE c.password = :password"),
    @NamedQuery(name = "Channels.findByLastUsed", query = "SELECT c FROM Channels c WHERE c.lastUsed = :lastUsed")})
public class Channels implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ChannelsPK channelsPK;
    @Basic(optional = false)
    @Column(name = "announce")
    private short announce;
    @Basic(optional = false)
    @Column(name = "ownership")
    private short ownership;
    @Column(name = "password")
    private String password;
    @Lob
    @Column(name = "bannedList")
    private String bannedList;
    @Basic(optional = false)
    @Column(name = "lastUsed")
    private int lastUsed;

    public Channels() {
    }

    public Channels(ChannelsPK channelsPK) {
        this.channelsPK = channelsPK;
    }

    public Channels(ChannelsPK channelsPK, short announce, short ownership, int lastUsed) {
        this.channelsPK = channelsPK;
        this.announce = announce;
        this.ownership = ownership;
        this.lastUsed = lastUsed;
    }

    public Channels(String name, int team) {
        this.channelsPK = new ChannelsPK(name, team);
    }

    public ChannelsPK getChannelsPK() {
        return channelsPK;
    }

    public void setChannelsPK(ChannelsPK channelsPK) {
        this.channelsPK = channelsPK;
    }

    public short getAnnounce() {
        return announce;
    }

    public void setAnnounce(short announce) {
        this.announce = announce;
    }

    public short getOwnership() {
        return ownership;
    }

    public void setOwnership(short ownership) {
        this.ownership = ownership;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBannedList() {
        return bannedList;
    }

    public void setBannedList(String bannedList) {
        this.bannedList = bannedList;
    }

    public int getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(int lastUsed) {
        this.lastUsed = lastUsed;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (channelsPK != null ? channelsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Channels)) {
            return false;
        }
        Channels other = (Channels) object;
        if ((this.channelsPK == null && other.channelsPK != null) || (this.channelsPK != null && !this.channelsPK.equals(other.channelsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.Channels[ channelsPK=" + channelsPK + " ]";
    }
    
}
