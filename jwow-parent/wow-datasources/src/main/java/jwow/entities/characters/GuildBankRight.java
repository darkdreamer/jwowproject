/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "guild_bank_right")
@NamedQueries({
    @NamedQuery(name = "GuildBankRight.findAll", query = "SELECT g FROM GuildBankRight g"),
    @NamedQuery(name = "GuildBankRight.findByGuildid", query = "SELECT g FROM GuildBankRight g WHERE g.guildBankRightPK.guildid = :guildid"),
    @NamedQuery(name = "GuildBankRight.findByTabId", query = "SELECT g FROM GuildBankRight g WHERE g.guildBankRightPK.tabId = :tabId"),
    @NamedQuery(name = "GuildBankRight.findByRid", query = "SELECT g FROM GuildBankRight g WHERE g.guildBankRightPK.rid = :rid"),
    @NamedQuery(name = "GuildBankRight.findByGbright", query = "SELECT g FROM GuildBankRight g WHERE g.gbright = :gbright"),
    @NamedQuery(name = "GuildBankRight.findBySlotPerDay", query = "SELECT g FROM GuildBankRight g WHERE g.slotPerDay = :slotPerDay")})
public class GuildBankRight implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GuildBankRightPK guildBankRightPK;
    @Basic(optional = false)
    @Column(name = "gbright")
    private short gbright;
    @Basic(optional = false)
    @Column(name = "SlotPerDay")
    private int slotPerDay;

    public GuildBankRight() {
    }

    public GuildBankRight(GuildBankRightPK guildBankRightPK) {
        this.guildBankRightPK = guildBankRightPK;
    }

    public GuildBankRight(GuildBankRightPK guildBankRightPK, short gbright, int slotPerDay) {
        this.guildBankRightPK = guildBankRightPK;
        this.gbright = gbright;
        this.slotPerDay = slotPerDay;
    }

    public GuildBankRight(int guildid, short tabId, short rid) {
        this.guildBankRightPK = new GuildBankRightPK(guildid, tabId, rid);
    }

    public GuildBankRightPK getGuildBankRightPK() {
        return guildBankRightPK;
    }

    public void setGuildBankRightPK(GuildBankRightPK guildBankRightPK) {
        this.guildBankRightPK = guildBankRightPK;
    }

    public short getGbright() {
        return gbright;
    }

    public void setGbright(short gbright) {
        this.gbright = gbright;
    }

    public int getSlotPerDay() {
        return slotPerDay;
    }

    public void setSlotPerDay(int slotPerDay) {
        this.slotPerDay = slotPerDay;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guildBankRightPK != null ? guildBankRightPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuildBankRight)) {
            return false;
        }
        GuildBankRight other = (GuildBankRight) object;
        if ((this.guildBankRightPK == null && other.guildBankRightPK != null) || (this.guildBankRightPK != null && !this.guildBankRightPK.equals(other.guildBankRightPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GuildBankRight[ guildBankRightPK=" + guildBankRightPK + " ]";
    }
    
}
