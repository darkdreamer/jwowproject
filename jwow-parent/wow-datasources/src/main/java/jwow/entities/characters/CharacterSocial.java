/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_social")
@NamedQueries({
    @NamedQuery(name = "CharacterSocial.findAll", query = "SELECT c FROM CharacterSocial c"),
    @NamedQuery(name = "CharacterSocial.findByGuid", query = "SELECT c FROM CharacterSocial c WHERE c.characterSocialPK.guid = :guid"),
    @NamedQuery(name = "CharacterSocial.findByFriend", query = "SELECT c FROM CharacterSocial c WHERE c.characterSocialPK.friend = :friend"),
    @NamedQuery(name = "CharacterSocial.findByFlags", query = "SELECT c FROM CharacterSocial c WHERE c.characterSocialPK.flags = :flags"),
    @NamedQuery(name = "CharacterSocial.findByNote", query = "SELECT c FROM CharacterSocial c WHERE c.note = :note")})
public class CharacterSocial implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterSocialPK characterSocialPK;
    @Basic(optional = false)
    @Column(name = "note")
    private String note;

    public CharacterSocial() {
    }

    public CharacterSocial(CharacterSocialPK characterSocialPK) {
        this.characterSocialPK = characterSocialPK;
    }

    public CharacterSocial(CharacterSocialPK characterSocialPK, String note) {
        this.characterSocialPK = characterSocialPK;
        this.note = note;
    }

    public CharacterSocial(int guid, int friend, short flags) {
        this.characterSocialPK = new CharacterSocialPK(guid, friend, flags);
    }

    public CharacterSocialPK getCharacterSocialPK() {
        return characterSocialPK;
    }

    public void setCharacterSocialPK(CharacterSocialPK characterSocialPK) {
        this.characterSocialPK = characterSocialPK;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterSocialPK != null ? characterSocialPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterSocial)) {
            return false;
        }
        CharacterSocial other = (CharacterSocial) object;
        if ((this.characterSocialPK == null && other.characterSocialPK != null) || (this.characterSocialPK != null && !this.characterSocialPK.equals(other.characterSocialPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterSocial[ characterSocialPK=" + characterSocialPK + " ]";
    }
    
}
