/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterSkillsPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "skill")
    private short skill;

    public CharacterSkillsPK() {
    }

    public CharacterSkillsPK(int guid, short skill) {
        this.guid = guid;
        this.skill = skill;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public short getSkill() {
        return skill;
    }

    public void setSkill(short skill) {
        this.skill = skill;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) skill;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterSkillsPK)) {
            return false;
        }
        CharacterSkillsPK other = (CharacterSkillsPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.skill != other.skill) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterSkillsPK[ guid=" + guid + ", skill=" + skill + " ]";
    }
    
}
