/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_reputation")
@NamedQueries({
    @NamedQuery(name = "CharacterReputation.findAll", query = "SELECT c FROM CharacterReputation c"),
    @NamedQuery(name = "CharacterReputation.findByGuid", query = "SELECT c FROM CharacterReputation c WHERE c.characterReputationPK.guid = :guid"),
    @NamedQuery(name = "CharacterReputation.findByFaction", query = "SELECT c FROM CharacterReputation c WHERE c.characterReputationPK.faction = :faction"),
    @NamedQuery(name = "CharacterReputation.findByStanding", query = "SELECT c FROM CharacterReputation c WHERE c.standing = :standing"),
    @NamedQuery(name = "CharacterReputation.findByFlags", query = "SELECT c FROM CharacterReputation c WHERE c.flags = :flags")})
public class CharacterReputation implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterReputationPK characterReputationPK;
    @Basic(optional = false)
    @Column(name = "standing")
    private int standing;
    @Basic(optional = false)
    @Column(name = "flags")
    private short flags;

    public CharacterReputation() {
    }

    public CharacterReputation(CharacterReputationPK characterReputationPK) {
        this.characterReputationPK = characterReputationPK;
    }

    public CharacterReputation(CharacterReputationPK characterReputationPK, int standing, short flags) {
        this.characterReputationPK = characterReputationPK;
        this.standing = standing;
        this.flags = flags;
    }

    public CharacterReputation(int guid, short faction) {
        this.characterReputationPK = new CharacterReputationPK(guid, faction);
    }

    public CharacterReputationPK getCharacterReputationPK() {
        return characterReputationPK;
    }

    public void setCharacterReputationPK(CharacterReputationPK characterReputationPK) {
        this.characterReputationPK = characterReputationPK;
    }

    public int getStanding() {
        return standing;
    }

    public void setStanding(int standing) {
        this.standing = standing;
    }

    public short getFlags() {
        return flags;
    }

    public void setFlags(short flags) {
        this.flags = flags;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterReputationPK != null ? characterReputationPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterReputation)) {
            return false;
        }
        CharacterReputation other = (CharacterReputation) object;
        if ((this.characterReputationPK == null && other.characterReputationPK != null) || (this.characterReputationPK != null && !this.characterReputationPK.equals(other.characterReputationPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterReputation[ characterReputationPK=" + characterReputationPK + " ]";
    }
    
}
