/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class InstanceResetPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "mapid")
    private short mapid;
    @Basic(optional = false)
    @Column(name = "difficulty")
    private short difficulty;

    public InstanceResetPK() {
    }

    public InstanceResetPK(short mapid, short difficulty) {
        this.mapid = mapid;
        this.difficulty = difficulty;
    }

    public short getMapid() {
        return mapid;
    }

    public void setMapid(short mapid) {
        this.mapid = mapid;
    }

    public short getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(short difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) mapid;
        hash += (int) difficulty;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstanceResetPK)) {
            return false;
        }
        InstanceResetPK other = (InstanceResetPK) object;
        if (this.mapid != other.mapid) {
            return false;
        }
        if (this.difficulty != other.difficulty) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.InstanceResetPK[ mapid=" + mapid + ", difficulty=" + difficulty + " ]";
    }
    
}
