/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_achievement_progress")
@NamedQueries({
    @NamedQuery(name = "CharacterAchievementProgress.findAll", query = "SELECT c FROM CharacterAchievementProgress c"),
    @NamedQuery(name = "CharacterAchievementProgress.findByGuid", query = "SELECT c FROM CharacterAchievementProgress c WHERE c.characterAchievementProgressPK.guid = :guid"),
    @NamedQuery(name = "CharacterAchievementProgress.findByCriteria", query = "SELECT c FROM CharacterAchievementProgress c WHERE c.characterAchievementProgressPK.criteria = :criteria"),
    @NamedQuery(name = "CharacterAchievementProgress.findByCounter", query = "SELECT c FROM CharacterAchievementProgress c WHERE c.counter = :counter"),
    @NamedQuery(name = "CharacterAchievementProgress.findByDate", query = "SELECT c FROM CharacterAchievementProgress c WHERE c.date = :date")})
public class CharacterAchievementProgress implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterAchievementProgressPK characterAchievementProgressPK;
    @Basic(optional = false)
    @Column(name = "counter")
    private int counter;
    @Basic(optional = false)
    @Column(name = "date")
    private int date;

    public CharacterAchievementProgress() {
    }

    public CharacterAchievementProgress(CharacterAchievementProgressPK characterAchievementProgressPK) {
        this.characterAchievementProgressPK = characterAchievementProgressPK;
    }

    public CharacterAchievementProgress(CharacterAchievementProgressPK characterAchievementProgressPK, int counter, int date) {
        this.characterAchievementProgressPK = characterAchievementProgressPK;
        this.counter = counter;
        this.date = date;
    }

    public CharacterAchievementProgress(int guid, short criteria) {
        this.characterAchievementProgressPK = new CharacterAchievementProgressPK(guid, criteria);
    }

    public CharacterAchievementProgressPK getCharacterAchievementProgressPK() {
        return characterAchievementProgressPK;
    }

    public void setCharacterAchievementProgressPK(CharacterAchievementProgressPK characterAchievementProgressPK) {
        this.characterAchievementProgressPK = characterAchievementProgressPK;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterAchievementProgressPK != null ? characterAchievementProgressPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterAchievementProgress)) {
            return false;
        }
        CharacterAchievementProgress other = (CharacterAchievementProgress) object;
        if ((this.characterAchievementProgressPK == null && other.characterAchievementProgressPK != null) || (this.characterAchievementProgressPK != null && !this.characterAchievementProgressPK.equals(other.characterAchievementProgressPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterAchievementProgress[ characterAchievementProgressPK=" + characterAchievementProgressPK + " ]";
    }
    
}
