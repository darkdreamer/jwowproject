/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "instance")
@NamedQueries({
    @NamedQuery(name = "Instance.findAll", query = "SELECT i FROM Instance i"),
    @NamedQuery(name = "Instance.findById", query = "SELECT i FROM Instance i WHERE i.id = :id"),
    @NamedQuery(name = "Instance.findByMap", query = "SELECT i FROM Instance i WHERE i.map = :map"),
    @NamedQuery(name = "Instance.findByResettime", query = "SELECT i FROM Instance i WHERE i.resettime = :resettime"),
    @NamedQuery(name = "Instance.findByDifficulty", query = "SELECT i FROM Instance i WHERE i.difficulty = :difficulty"),
    @NamedQuery(name = "Instance.findByCompletedEncounters", query = "SELECT i FROM Instance i WHERE i.completedEncounters = :completedEncounters"),
    @NamedQuery(name = "Instance.findByData", query = "SELECT i FROM Instance i WHERE i.data = :data")})
public class Instance implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "map")
    private short map;
    @Basic(optional = false)
    @Column(name = "resettime")
    private int resettime;
    @Basic(optional = false)
    @Column(name = "difficulty")
    private short difficulty;
    @Basic(optional = false)
    @Column(name = "completedEncounters")
    private int completedEncounters;
    @Basic(optional = false)
    @Column(name = "data")
    private String data;

    public Instance() {
    }

    public Instance(Integer id) {
        this.id = id;
    }

    public Instance(Integer id, short map, int resettime, short difficulty, int completedEncounters, String data) {
        this.id = id;
        this.map = map;
        this.resettime = resettime;
        this.difficulty = difficulty;
        this.completedEncounters = completedEncounters;
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public short getMap() {
        return map;
    }

    public void setMap(short map) {
        this.map = map;
    }

    public int getResettime() {
        return resettime;
    }

    public void setResettime(int resettime) {
        this.resettime = resettime;
    }

    public short getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(short difficulty) {
        this.difficulty = difficulty;
    }

    public int getCompletedEncounters() {
        return completedEncounters;
    }

    public void setCompletedEncounters(int completedEncounters) {
        this.completedEncounters = completedEncounters;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Instance)) {
            return false;
        }
        Instance other = (Instance) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.Instance[ id=" + id + " ]";
    }
    
}
