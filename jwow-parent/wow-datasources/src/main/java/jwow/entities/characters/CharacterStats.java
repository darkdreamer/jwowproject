/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_stats")
@NamedQueries({
    @NamedQuery(name = "CharacterStats.findAll", query = "SELECT c FROM CharacterStats c"),
    @NamedQuery(name = "CharacterStats.findByGuid", query = "SELECT c FROM CharacterStats c WHERE c.guid = :guid"),
    @NamedQuery(name = "CharacterStats.findByMaxhealth", query = "SELECT c FROM CharacterStats c WHERE c.maxhealth = :maxhealth"),
    @NamedQuery(name = "CharacterStats.findByMaxpower1", query = "SELECT c FROM CharacterStats c WHERE c.maxpower1 = :maxpower1"),
    @NamedQuery(name = "CharacterStats.findByMaxpower2", query = "SELECT c FROM CharacterStats c WHERE c.maxpower2 = :maxpower2"),
    @NamedQuery(name = "CharacterStats.findByMaxpower3", query = "SELECT c FROM CharacterStats c WHERE c.maxpower3 = :maxpower3"),
    @NamedQuery(name = "CharacterStats.findByMaxpower4", query = "SELECT c FROM CharacterStats c WHERE c.maxpower4 = :maxpower4"),
    @NamedQuery(name = "CharacterStats.findByMaxpower5", query = "SELECT c FROM CharacterStats c WHERE c.maxpower5 = :maxpower5"),
    @NamedQuery(name = "CharacterStats.findByMaxpower6", query = "SELECT c FROM CharacterStats c WHERE c.maxpower6 = :maxpower6"),
    @NamedQuery(name = "CharacterStats.findByMaxpower7", query = "SELECT c FROM CharacterStats c WHERE c.maxpower7 = :maxpower7"),
    @NamedQuery(name = "CharacterStats.findByStrength", query = "SELECT c FROM CharacterStats c WHERE c.strength = :strength"),
    @NamedQuery(name = "CharacterStats.findByAgility", query = "SELECT c FROM CharacterStats c WHERE c.agility = :agility"),
    @NamedQuery(name = "CharacterStats.findByStamina", query = "SELECT c FROM CharacterStats c WHERE c.stamina = :stamina"),
    @NamedQuery(name = "CharacterStats.findByIntellect", query = "SELECT c FROM CharacterStats c WHERE c.intellect = :intellect"),
    @NamedQuery(name = "CharacterStats.findBySpirit", query = "SELECT c FROM CharacterStats c WHERE c.spirit = :spirit"),
    @NamedQuery(name = "CharacterStats.findByArmor", query = "SELECT c FROM CharacterStats c WHERE c.armor = :armor"),
    @NamedQuery(name = "CharacterStats.findByResHoly", query = "SELECT c FROM CharacterStats c WHERE c.resHoly = :resHoly"),
    @NamedQuery(name = "CharacterStats.findByResFire", query = "SELECT c FROM CharacterStats c WHERE c.resFire = :resFire"),
    @NamedQuery(name = "CharacterStats.findByResNature", query = "SELECT c FROM CharacterStats c WHERE c.resNature = :resNature"),
    @NamedQuery(name = "CharacterStats.findByResFrost", query = "SELECT c FROM CharacterStats c WHERE c.resFrost = :resFrost"),
    @NamedQuery(name = "CharacterStats.findByResShadow", query = "SELECT c FROM CharacterStats c WHERE c.resShadow = :resShadow"),
    @NamedQuery(name = "CharacterStats.findByResArcane", query = "SELECT c FROM CharacterStats c WHERE c.resArcane = :resArcane"),
    @NamedQuery(name = "CharacterStats.findByBlockPct", query = "SELECT c FROM CharacterStats c WHERE c.blockPct = :blockPct"),
    @NamedQuery(name = "CharacterStats.findByDodgePct", query = "SELECT c FROM CharacterStats c WHERE c.dodgePct = :dodgePct"),
    @NamedQuery(name = "CharacterStats.findByParryPct", query = "SELECT c FROM CharacterStats c WHERE c.parryPct = :parryPct"),
    @NamedQuery(name = "CharacterStats.findByCritPct", query = "SELECT c FROM CharacterStats c WHERE c.critPct = :critPct"),
    @NamedQuery(name = "CharacterStats.findByRangedCritPct", query = "SELECT c FROM CharacterStats c WHERE c.rangedCritPct = :rangedCritPct"),
    @NamedQuery(name = "CharacterStats.findBySpellCritPct", query = "SELECT c FROM CharacterStats c WHERE c.spellCritPct = :spellCritPct"),
    @NamedQuery(name = "CharacterStats.findByAttackPower", query = "SELECT c FROM CharacterStats c WHERE c.attackPower = :attackPower"),
    @NamedQuery(name = "CharacterStats.findByRangedAttackPower", query = "SELECT c FROM CharacterStats c WHERE c.rangedAttackPower = :rangedAttackPower"),
    @NamedQuery(name = "CharacterStats.findBySpellPower", query = "SELECT c FROM CharacterStats c WHERE c.spellPower = :spellPower"),
    @NamedQuery(name = "CharacterStats.findByResilience", query = "SELECT c FROM CharacterStats c WHERE c.resilience = :resilience")})
public class CharacterStats implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "guid")
    private Integer guid;
    @Basic(optional = false)
    @Column(name = "maxhealth")
    private int maxhealth;
    @Basic(optional = false)
    @Column(name = "maxpower1")
    private int maxpower1;
    @Basic(optional = false)
    @Column(name = "maxpower2")
    private int maxpower2;
    @Basic(optional = false)
    @Column(name = "maxpower3")
    private int maxpower3;
    @Basic(optional = false)
    @Column(name = "maxpower4")
    private int maxpower4;
    @Basic(optional = false)
    @Column(name = "maxpower5")
    private int maxpower5;
    @Basic(optional = false)
    @Column(name = "maxpower6")
    private int maxpower6;
    @Basic(optional = false)
    @Column(name = "maxpower7")
    private int maxpower7;
    @Basic(optional = false)
    @Column(name = "strength")
    private int strength;
    @Basic(optional = false)
    @Column(name = "agility")
    private int agility;
    @Basic(optional = false)
    @Column(name = "stamina")
    private int stamina;
    @Basic(optional = false)
    @Column(name = "intellect")
    private int intellect;
    @Basic(optional = false)
    @Column(name = "spirit")
    private int spirit;
    @Basic(optional = false)
    @Column(name = "armor")
    private int armor;
    @Basic(optional = false)
    @Column(name = "resHoly")
    private int resHoly;
    @Basic(optional = false)
    @Column(name = "resFire")
    private int resFire;
    @Basic(optional = false)
    @Column(name = "resNature")
    private int resNature;
    @Basic(optional = false)
    @Column(name = "resFrost")
    private int resFrost;
    @Basic(optional = false)
    @Column(name = "resShadow")
    private int resShadow;
    @Basic(optional = false)
    @Column(name = "resArcane")
    private int resArcane;
    @Basic(optional = false)
    @Column(name = "blockPct")
    private float blockPct;
    @Basic(optional = false)
    @Column(name = "dodgePct")
    private float dodgePct;
    @Basic(optional = false)
    @Column(name = "parryPct")
    private float parryPct;
    @Basic(optional = false)
    @Column(name = "critPct")
    private float critPct;
    @Basic(optional = false)
    @Column(name = "rangedCritPct")
    private float rangedCritPct;
    @Basic(optional = false)
    @Column(name = "spellCritPct")
    private float spellCritPct;
    @Basic(optional = false)
    @Column(name = "attackPower")
    private int attackPower;
    @Basic(optional = false)
    @Column(name = "rangedAttackPower")
    private int rangedAttackPower;
    @Basic(optional = false)
    @Column(name = "spellPower")
    private int spellPower;
    @Basic(optional = false)
    @Column(name = "resilience")
    private int resilience;

    public CharacterStats() {
    }

    public CharacterStats(Integer guid) {
        this.guid = guid;
    }

    public CharacterStats(Integer guid, int maxhealth, int maxpower1, int maxpower2, int maxpower3, int maxpower4, int maxpower5, int maxpower6, int maxpower7, int strength, int agility, int stamina, int intellect, int spirit, int armor, int resHoly, int resFire, int resNature, int resFrost, int resShadow, int resArcane, float blockPct, float dodgePct, float parryPct, float critPct, float rangedCritPct, float spellCritPct, int attackPower, int rangedAttackPower, int spellPower, int resilience) {
        this.guid = guid;
        this.maxhealth = maxhealth;
        this.maxpower1 = maxpower1;
        this.maxpower2 = maxpower2;
        this.maxpower3 = maxpower3;
        this.maxpower4 = maxpower4;
        this.maxpower5 = maxpower5;
        this.maxpower6 = maxpower6;
        this.maxpower7 = maxpower7;
        this.strength = strength;
        this.agility = agility;
        this.stamina = stamina;
        this.intellect = intellect;
        this.spirit = spirit;
        this.armor = armor;
        this.resHoly = resHoly;
        this.resFire = resFire;
        this.resNature = resNature;
        this.resFrost = resFrost;
        this.resShadow = resShadow;
        this.resArcane = resArcane;
        this.blockPct = blockPct;
        this.dodgePct = dodgePct;
        this.parryPct = parryPct;
        this.critPct = critPct;
        this.rangedCritPct = rangedCritPct;
        this.spellCritPct = spellCritPct;
        this.attackPower = attackPower;
        this.rangedAttackPower = rangedAttackPower;
        this.spellPower = spellPower;
        this.resilience = resilience;
    }

    public Integer getGuid() {
        return guid;
    }

    public void setGuid(Integer guid) {
        this.guid = guid;
    }

    public int getMaxhealth() {
        return maxhealth;
    }

    public void setMaxhealth(int maxhealth) {
        this.maxhealth = maxhealth;
    }

    public int getMaxpower1() {
        return maxpower1;
    }

    public void setMaxpower1(int maxpower1) {
        this.maxpower1 = maxpower1;
    }

    public int getMaxpower2() {
        return maxpower2;
    }

    public void setMaxpower2(int maxpower2) {
        this.maxpower2 = maxpower2;
    }

    public int getMaxpower3() {
        return maxpower3;
    }

    public void setMaxpower3(int maxpower3) {
        this.maxpower3 = maxpower3;
    }

    public int getMaxpower4() {
        return maxpower4;
    }

    public void setMaxpower4(int maxpower4) {
        this.maxpower4 = maxpower4;
    }

    public int getMaxpower5() {
        return maxpower5;
    }

    public void setMaxpower5(int maxpower5) {
        this.maxpower5 = maxpower5;
    }

    public int getMaxpower6() {
        return maxpower6;
    }

    public void setMaxpower6(int maxpower6) {
        this.maxpower6 = maxpower6;
    }

    public int getMaxpower7() {
        return maxpower7;
    }

    public void setMaxpower7(int maxpower7) {
        this.maxpower7 = maxpower7;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getIntellect() {
        return intellect;
    }

    public void setIntellect(int intellect) {
        this.intellect = intellect;
    }

    public int getSpirit() {
        return spirit;
    }

    public void setSpirit(int spirit) {
        this.spirit = spirit;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getResHoly() {
        return resHoly;
    }

    public void setResHoly(int resHoly) {
        this.resHoly = resHoly;
    }

    public int getResFire() {
        return resFire;
    }

    public void setResFire(int resFire) {
        this.resFire = resFire;
    }

    public int getResNature() {
        return resNature;
    }

    public void setResNature(int resNature) {
        this.resNature = resNature;
    }

    public int getResFrost() {
        return resFrost;
    }

    public void setResFrost(int resFrost) {
        this.resFrost = resFrost;
    }

    public int getResShadow() {
        return resShadow;
    }

    public void setResShadow(int resShadow) {
        this.resShadow = resShadow;
    }

    public int getResArcane() {
        return resArcane;
    }

    public void setResArcane(int resArcane) {
        this.resArcane = resArcane;
    }

    public float getBlockPct() {
        return blockPct;
    }

    public void setBlockPct(float blockPct) {
        this.blockPct = blockPct;
    }

    public float getDodgePct() {
        return dodgePct;
    }

    public void setDodgePct(float dodgePct) {
        this.dodgePct = dodgePct;
    }

    public float getParryPct() {
        return parryPct;
    }

    public void setParryPct(float parryPct) {
        this.parryPct = parryPct;
    }

    public float getCritPct() {
        return critPct;
    }

    public void setCritPct(float critPct) {
        this.critPct = critPct;
    }

    public float getRangedCritPct() {
        return rangedCritPct;
    }

    public void setRangedCritPct(float rangedCritPct) {
        this.rangedCritPct = rangedCritPct;
    }

    public float getSpellCritPct() {
        return spellCritPct;
    }

    public void setSpellCritPct(float spellCritPct) {
        this.spellCritPct = spellCritPct;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }

    public int getRangedAttackPower() {
        return rangedAttackPower;
    }

    public void setRangedAttackPower(int rangedAttackPower) {
        this.rangedAttackPower = rangedAttackPower;
    }

    public int getSpellPower() {
        return spellPower;
    }

    public void setSpellPower(int spellPower) {
        this.spellPower = spellPower;
    }

    public int getResilience() {
        return resilience;
    }

    public void setResilience(int resilience) {
        this.resilience = resilience;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guid != null ? guid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterStats)) {
            return false;
        }
        CharacterStats other = (CharacterStats) object;
        if ((this.guid == null && other.guid != null) || (this.guid != null && !this.guid.equals(other.guid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterStats[ guid=" + guid + " ]";
    }
    
}
