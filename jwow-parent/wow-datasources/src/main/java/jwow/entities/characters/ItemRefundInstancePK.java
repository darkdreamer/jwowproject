/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class ItemRefundInstancePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "item_guid")
    private int itemGuid;
    @Basic(optional = false)
    @Column(name = "player_guid")
    private int playerGuid;

    public ItemRefundInstancePK() {
    }

    public ItemRefundInstancePK(int itemGuid, int playerGuid) {
        this.itemGuid = itemGuid;
        this.playerGuid = playerGuid;
    }

    public int getItemGuid() {
        return itemGuid;
    }

    public void setItemGuid(int itemGuid) {
        this.itemGuid = itemGuid;
    }

    public int getPlayerGuid() {
        return playerGuid;
    }

    public void setPlayerGuid(int playerGuid) {
        this.playerGuid = playerGuid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) itemGuid;
        hash += (int) playerGuid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemRefundInstancePK)) {
            return false;
        }
        ItemRefundInstancePK other = (ItemRefundInstancePK) object;
        if (this.itemGuid != other.itemGuid) {
            return false;
        }
        if (this.playerGuid != other.playerGuid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.ItemRefundInstancePK[ itemGuid=" + itemGuid + ", playerGuid=" + playerGuid + " ]";
    }
    
}
