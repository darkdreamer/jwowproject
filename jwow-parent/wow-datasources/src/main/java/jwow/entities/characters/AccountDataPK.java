/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class AccountDataPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "accountId")
    private int accountId;
    @Basic(optional = false)
    @Column(name = "type")
    private short type;

    public AccountDataPK() {
    }

    public AccountDataPK(int accountId, short type) {
        this.accountId = accountId;
        this.type = type;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) accountId;
        hash += (int) type;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountDataPK)) {
            return false;
        }
        AccountDataPK other = (AccountDataPK) object;
        if (this.accountId != other.accountId) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.AccountDataPK[ accountId=" + accountId + ", type=" + type + " ]";
    }
    
}
