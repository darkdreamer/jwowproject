/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "account_tutorial")
@NamedQueries({
    @NamedQuery(name = "AccountTutorial.findAll", query = "SELECT a FROM AccountTutorial a"),
    @NamedQuery(name = "AccountTutorial.findByAccountId", query = "SELECT a FROM AccountTutorial a WHERE a.accountId = :accountId"),
    @NamedQuery(name = "AccountTutorial.findByTut0", query = "SELECT a FROM AccountTutorial a WHERE a.tut0 = :tut0"),
    @NamedQuery(name = "AccountTutorial.findByTut1", query = "SELECT a FROM AccountTutorial a WHERE a.tut1 = :tut1"),
    @NamedQuery(name = "AccountTutorial.findByTut2", query = "SELECT a FROM AccountTutorial a WHERE a.tut2 = :tut2"),
    @NamedQuery(name = "AccountTutorial.findByTut3", query = "SELECT a FROM AccountTutorial a WHERE a.tut3 = :tut3"),
    @NamedQuery(name = "AccountTutorial.findByTut4", query = "SELECT a FROM AccountTutorial a WHERE a.tut4 = :tut4"),
    @NamedQuery(name = "AccountTutorial.findByTut5", query = "SELECT a FROM AccountTutorial a WHERE a.tut5 = :tut5"),
    @NamedQuery(name = "AccountTutorial.findByTut6", query = "SELECT a FROM AccountTutorial a WHERE a.tut6 = :tut6"),
    @NamedQuery(name = "AccountTutorial.findByTut7", query = "SELECT a FROM AccountTutorial a WHERE a.tut7 = :tut7")})
public class AccountTutorial implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "accountId")
    private Integer accountId;
    @Basic(optional = false)
    @Column(name = "tut0")
    private int tut0;
    @Basic(optional = false)
    @Column(name = "tut1")
    private int tut1;
    @Basic(optional = false)
    @Column(name = "tut2")
    private int tut2;
    @Basic(optional = false)
    @Column(name = "tut3")
    private int tut3;
    @Basic(optional = false)
    @Column(name = "tut4")
    private int tut4;
    @Basic(optional = false)
    @Column(name = "tut5")
    private int tut5;
    @Basic(optional = false)
    @Column(name = "tut6")
    private int tut6;
    @Basic(optional = false)
    @Column(name = "tut7")
    private int tut7;

    public AccountTutorial() {
    }

    public AccountTutorial(Integer accountId) {
        this.accountId = accountId;
    }

    public AccountTutorial(Integer accountId, int tut0, int tut1, int tut2, int tut3, int tut4, int tut5, int tut6, int tut7) {
        this.accountId = accountId;
        this.tut0 = tut0;
        this.tut1 = tut1;
        this.tut2 = tut2;
        this.tut3 = tut3;
        this.tut4 = tut4;
        this.tut5 = tut5;
        this.tut6 = tut6;
        this.tut7 = tut7;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public int getTut0() {
        return tut0;
    }

    public void setTut0(int tut0) {
        this.tut0 = tut0;
    }

    public int getTut1() {
        return tut1;
    }

    public void setTut1(int tut1) {
        this.tut1 = tut1;
    }

    public int getTut2() {
        return tut2;
    }

    public void setTut2(int tut2) {
        this.tut2 = tut2;
    }

    public int getTut3() {
        return tut3;
    }

    public void setTut3(int tut3) {
        this.tut3 = tut3;
    }

    public int getTut4() {
        return tut4;
    }

    public void setTut4(int tut4) {
        this.tut4 = tut4;
    }

    public int getTut5() {
        return tut5;
    }

    public void setTut5(int tut5) {
        this.tut5 = tut5;
    }

    public int getTut6() {
        return tut6;
    }

    public void setTut6(int tut6) {
        this.tut6 = tut6;
    }

    public int getTut7() {
        return tut7;
    }

    public void setTut7(int tut7) {
        this.tut7 = tut7;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountId != null ? accountId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountTutorial)) {
            return false;
        }
        AccountTutorial other = (AccountTutorial) object;
        if ((this.accountId == null && other.accountId != null) || (this.accountId != null && !this.accountId.equals(other.accountId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.AccountTutorial[ accountId=" + accountId + " ]";
    }
    
}
