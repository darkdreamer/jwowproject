/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "group_instance")
@NamedQueries({
    @NamedQuery(name = "GroupInstance.findAll", query = "SELECT g FROM GroupInstance g"),
    @NamedQuery(name = "GroupInstance.findByGuid", query = "SELECT g FROM GroupInstance g WHERE g.groupInstancePK.guid = :guid"),
    @NamedQuery(name = "GroupInstance.findByInstance", query = "SELECT g FROM GroupInstance g WHERE g.groupInstancePK.instance = :instance"),
    @NamedQuery(name = "GroupInstance.findByPermanent", query = "SELECT g FROM GroupInstance g WHERE g.permanent = :permanent")})
public class GroupInstance implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GroupInstancePK groupInstancePK;
    @Basic(optional = false)
    @Column(name = "permanent")
    private short permanent;

    public GroupInstance() {
    }

    public GroupInstance(GroupInstancePK groupInstancePK) {
        this.groupInstancePK = groupInstancePK;
    }

    public GroupInstance(GroupInstancePK groupInstancePK, short permanent) {
        this.groupInstancePK = groupInstancePK;
        this.permanent = permanent;
    }

    public GroupInstance(int guid, int instance) {
        this.groupInstancePK = new GroupInstancePK(guid, instance);
    }

    public GroupInstancePK getGroupInstancePK() {
        return groupInstancePK;
    }

    public void setGroupInstancePK(GroupInstancePK groupInstancePK) {
        this.groupInstancePK = groupInstancePK;
    }

    public short getPermanent() {
        return permanent;
    }

    public void setPermanent(short permanent) {
        this.permanent = permanent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupInstancePK != null ? groupInstancePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupInstance)) {
            return false;
        }
        GroupInstance other = (GroupInstance) object;
        if ((this.groupInstancePK == null && other.groupInstancePK != null) || (this.groupInstancePK != null && !this.groupInstancePK.equals(other.groupInstancePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GroupInstance[ groupInstancePK=" + groupInstancePK + " ]";
    }
    
}
