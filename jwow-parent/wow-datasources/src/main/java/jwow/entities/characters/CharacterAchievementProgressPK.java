/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterAchievementProgressPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "criteria")
    private short criteria;

    public CharacterAchievementProgressPK() {
    }

    public CharacterAchievementProgressPK(int guid, short criteria) {
        this.guid = guid;
        this.criteria = criteria;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public short getCriteria() {
        return criteria;
    }

    public void setCriteria(short criteria) {
        this.criteria = criteria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) criteria;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterAchievementProgressPK)) {
            return false;
        }
        CharacterAchievementProgressPK other = (CharacterAchievementProgressPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.criteria != other.criteria) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterAchievementProgressPK[ guid=" + guid + ", criteria=" + criteria + " ]";
    }
    
}
