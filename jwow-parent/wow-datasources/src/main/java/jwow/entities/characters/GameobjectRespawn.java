/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "gameobject_respawn")
@NamedQueries({
    @NamedQuery(name = "GameobjectRespawn.findAll", query = "SELECT g FROM GameobjectRespawn g"),
    @NamedQuery(name = "GameobjectRespawn.findByGuid", query = "SELECT g FROM GameobjectRespawn g WHERE g.gameobjectRespawnPK.guid = :guid"),
    @NamedQuery(name = "GameobjectRespawn.findByRespawnTime", query = "SELECT g FROM GameobjectRespawn g WHERE g.respawnTime = :respawnTime"),
    @NamedQuery(name = "GameobjectRespawn.findByMapId", query = "SELECT g FROM GameobjectRespawn g WHERE g.mapId = :mapId"),
    @NamedQuery(name = "GameobjectRespawn.findByInstanceId", query = "SELECT g FROM GameobjectRespawn g WHERE g.gameobjectRespawnPK.instanceId = :instanceId")})
public class GameobjectRespawn implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GameobjectRespawnPK gameobjectRespawnPK;
    @Basic(optional = false)
    @Column(name = "respawnTime")
    private int respawnTime;
    @Basic(optional = false)
    @Column(name = "mapId")
    private short mapId;

    public GameobjectRespawn() {
    }

    public GameobjectRespawn(GameobjectRespawnPK gameobjectRespawnPK) {
        this.gameobjectRespawnPK = gameobjectRespawnPK;
    }

    public GameobjectRespawn(GameobjectRespawnPK gameobjectRespawnPK, int respawnTime, short mapId) {
        this.gameobjectRespawnPK = gameobjectRespawnPK;
        this.respawnTime = respawnTime;
        this.mapId = mapId;
    }

    public GameobjectRespawn(int guid, int instanceId) {
        this.gameobjectRespawnPK = new GameobjectRespawnPK(guid, instanceId);
    }

    public GameobjectRespawnPK getGameobjectRespawnPK() {
        return gameobjectRespawnPK;
    }

    public void setGameobjectRespawnPK(GameobjectRespawnPK gameobjectRespawnPK) {
        this.gameobjectRespawnPK = gameobjectRespawnPK;
    }

    public int getRespawnTime() {
        return respawnTime;
    }

    public void setRespawnTime(int respawnTime) {
        this.respawnTime = respawnTime;
    }

    public short getMapId() {
        return mapId;
    }

    public void setMapId(short mapId) {
        this.mapId = mapId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gameobjectRespawnPK != null ? gameobjectRespawnPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GameobjectRespawn)) {
            return false;
        }
        GameobjectRespawn other = (GameobjectRespawn) object;
        if ((this.gameobjectRespawnPK == null && other.gameobjectRespawnPK != null) || (this.gameobjectRespawnPK != null && !this.gameobjectRespawnPK.equals(other.gameobjectRespawnPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GameobjectRespawn[ gameobjectRespawnPK=" + gameobjectRespawnPK + " ]";
    }
    
}
