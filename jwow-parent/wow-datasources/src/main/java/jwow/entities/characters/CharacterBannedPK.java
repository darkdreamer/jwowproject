/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterBannedPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "bandate")
    private int bandate;

    public CharacterBannedPK() {
    }

    public CharacterBannedPK(int guid, int bandate) {
        this.guid = guid;
        this.bandate = bandate;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public int getBandate() {
        return bandate;
    }

    public void setBandate(int bandate) {
        this.bandate = bandate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) bandate;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterBannedPK)) {
            return false;
        }
        CharacterBannedPK other = (CharacterBannedPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.bandate != other.bandate) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterBannedPK[ guid=" + guid + ", bandate=" + bandate + " ]";
    }
    
}
