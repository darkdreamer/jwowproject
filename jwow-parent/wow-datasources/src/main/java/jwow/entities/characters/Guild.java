/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "guild")
@NamedQueries({
    @NamedQuery(name = "Guild.findAll", query = "SELECT g FROM Guild g"),
    @NamedQuery(name = "Guild.findByGuildid", query = "SELECT g FROM Guild g WHERE g.guildid = :guildid"),
    @NamedQuery(name = "Guild.findByName", query = "SELECT g FROM Guild g WHERE g.name = :name"),
    @NamedQuery(name = "Guild.findByLeaderguid", query = "SELECT g FROM Guild g WHERE g.leaderguid = :leaderguid"),
    @NamedQuery(name = "Guild.findByEmblemStyle", query = "SELECT g FROM Guild g WHERE g.emblemStyle = :emblemStyle"),
    @NamedQuery(name = "Guild.findByEmblemColor", query = "SELECT g FROM Guild g WHERE g.emblemColor = :emblemColor"),
    @NamedQuery(name = "Guild.findByBorderStyle", query = "SELECT g FROM Guild g WHERE g.borderStyle = :borderStyle"),
    @NamedQuery(name = "Guild.findByBorderColor", query = "SELECT g FROM Guild g WHERE g.borderColor = :borderColor"),
    @NamedQuery(name = "Guild.findByBackgroundColor", query = "SELECT g FROM Guild g WHERE g.backgroundColor = :backgroundColor"),
    @NamedQuery(name = "Guild.findByMotd", query = "SELECT g FROM Guild g WHERE g.motd = :motd"),
    @NamedQuery(name = "Guild.findByCreatedate", query = "SELECT g FROM Guild g WHERE g.createdate = :createdate"),
    @NamedQuery(name = "Guild.findByBankMoney", query = "SELECT g FROM Guild g WHERE g.bankMoney = :bankMoney")})
public class Guild implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "guildid")
    private Integer guildid;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "leaderguid")
    private int leaderguid;
    @Basic(optional = false)
    @Column(name = "EmblemStyle")
    private short emblemStyle;
    @Basic(optional = false)
    @Column(name = "EmblemColor")
    private short emblemColor;
    @Basic(optional = false)
    @Column(name = "BorderStyle")
    private short borderStyle;
    @Basic(optional = false)
    @Column(name = "BorderColor")
    private short borderColor;
    @Basic(optional = false)
    @Column(name = "BackgroundColor")
    private short backgroundColor;
    @Basic(optional = false)
    @Lob
    @Column(name = "info")
    private String info;
    @Basic(optional = false)
    @Column(name = "motd")
    private String motd;
    @Basic(optional = false)
    @Column(name = "createdate")
    private int createdate;
    @Basic(optional = false)
    @Column(name = "BankMoney")
    private long bankMoney;

    public Guild() {
    }

    public Guild(Integer guildid) {
        this.guildid = guildid;
    }

    public Guild(Integer guildid, String name, int leaderguid, short emblemStyle, short emblemColor, short borderStyle, short borderColor, short backgroundColor, String info, String motd, int createdate, long bankMoney) {
        this.guildid = guildid;
        this.name = name;
        this.leaderguid = leaderguid;
        this.emblemStyle = emblemStyle;
        this.emblemColor = emblemColor;
        this.borderStyle = borderStyle;
        this.borderColor = borderColor;
        this.backgroundColor = backgroundColor;
        this.info = info;
        this.motd = motd;
        this.createdate = createdate;
        this.bankMoney = bankMoney;
    }

    public Integer getGuildid() {
        return guildid;
    }

    public void setGuildid(Integer guildid) {
        this.guildid = guildid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLeaderguid() {
        return leaderguid;
    }

    public void setLeaderguid(int leaderguid) {
        this.leaderguid = leaderguid;
    }

    public short getEmblemStyle() {
        return emblemStyle;
    }

    public void setEmblemStyle(short emblemStyle) {
        this.emblemStyle = emblemStyle;
    }

    public short getEmblemColor() {
        return emblemColor;
    }

    public void setEmblemColor(short emblemColor) {
        this.emblemColor = emblemColor;
    }

    public short getBorderStyle() {
        return borderStyle;
    }

    public void setBorderStyle(short borderStyle) {
        this.borderStyle = borderStyle;
    }

    public short getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(short borderColor) {
        this.borderColor = borderColor;
    }

    public short getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(short backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getMotd() {
        return motd;
    }

    public void setMotd(String motd) {
        this.motd = motd;
    }

    public int getCreatedate() {
        return createdate;
    }

    public void setCreatedate(int createdate) {
        this.createdate = createdate;
    }

    public long getBankMoney() {
        return bankMoney;
    }

    public void setBankMoney(long bankMoney) {
        this.bankMoney = bankMoney;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guildid != null ? guildid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Guild)) {
            return false;
        }
        Guild other = (Guild) object;
        if ((this.guildid == null && other.guildid != null) || (this.guildid != null && !this.guildid.equals(other.guildid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.Guild[ guildid=" + guildid + " ]";
    }
    
}
