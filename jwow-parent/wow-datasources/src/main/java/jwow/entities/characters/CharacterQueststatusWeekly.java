/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_queststatus_weekly")
@NamedQueries({
    @NamedQuery(name = "CharacterQueststatusWeekly.findAll", query = "SELECT c FROM CharacterQueststatusWeekly c"),
    @NamedQuery(name = "CharacterQueststatusWeekly.findByGuid", query = "SELECT c FROM CharacterQueststatusWeekly c WHERE c.characterQueststatusWeeklyPK.guid = :guid"),
    @NamedQuery(name = "CharacterQueststatusWeekly.findByQuest", query = "SELECT c FROM CharacterQueststatusWeekly c WHERE c.characterQueststatusWeeklyPK.quest = :quest")})
public class CharacterQueststatusWeekly implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterQueststatusWeeklyPK characterQueststatusWeeklyPK;

    public CharacterQueststatusWeekly() {
    }

    public CharacterQueststatusWeekly(CharacterQueststatusWeeklyPK characterQueststatusWeeklyPK) {
        this.characterQueststatusWeeklyPK = characterQueststatusWeeklyPK;
    }

    public CharacterQueststatusWeekly(int guid, int quest) {
        this.characterQueststatusWeeklyPK = new CharacterQueststatusWeeklyPK(guid, quest);
    }

    public CharacterQueststatusWeeklyPK getCharacterQueststatusWeeklyPK() {
        return characterQueststatusWeeklyPK;
    }

    public void setCharacterQueststatusWeeklyPK(CharacterQueststatusWeeklyPK characterQueststatusWeeklyPK) {
        this.characterQueststatusWeeklyPK = characterQueststatusWeeklyPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterQueststatusWeeklyPK != null ? characterQueststatusWeeklyPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterQueststatusWeekly)) {
            return false;
        }
        CharacterQueststatusWeekly other = (CharacterQueststatusWeekly) object;
        if ((this.characterQueststatusWeeklyPK == null && other.characterQueststatusWeeklyPK != null) || (this.characterQueststatusWeeklyPK != null && !this.characterQueststatusWeeklyPK.equals(other.characterQueststatusWeeklyPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterQueststatusWeekly[ characterQueststatusWeeklyPK=" + characterQueststatusWeeklyPK + " ]";
    }
    
}
