/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_equipmentsets")
@NamedQueries({
    @NamedQuery(name = "CharacterEquipmentsets.findAll", query = "SELECT c FROM CharacterEquipmentsets c"),
    @NamedQuery(name = "CharacterEquipmentsets.findByGuid", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.guid = :guid"),
    @NamedQuery(name = "CharacterEquipmentsets.findBySetguid", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.setguid = :setguid"),
    @NamedQuery(name = "CharacterEquipmentsets.findBySetindex", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.setindex = :setindex"),
    @NamedQuery(name = "CharacterEquipmentsets.findByName", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.name = :name"),
    @NamedQuery(name = "CharacterEquipmentsets.findByIconname", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.iconname = :iconname"),
    @NamedQuery(name = "CharacterEquipmentsets.findByIgnoreMask", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.ignoreMask = :ignoreMask"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem0", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item0 = :item0"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem1", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item1 = :item1"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem2", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item2 = :item2"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem3", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item3 = :item3"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem4", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item4 = :item4"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem5", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item5 = :item5"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem6", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item6 = :item6"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem7", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item7 = :item7"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem8", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item8 = :item8"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem9", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item9 = :item9"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem10", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item10 = :item10"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem11", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item11 = :item11"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem12", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item12 = :item12"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem13", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item13 = :item13"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem14", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item14 = :item14"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem15", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item15 = :item15"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem16", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item16 = :item16"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem17", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item17 = :item17"),
    @NamedQuery(name = "CharacterEquipmentsets.findByItem18", query = "SELECT c FROM CharacterEquipmentsets c WHERE c.item18 = :item18")})
public class CharacterEquipmentsets implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "setguid")
    private Long setguid;
    @Basic(optional = false)
    @Column(name = "setindex")
    private short setindex;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "iconname")
    private String iconname;
    @Basic(optional = false)
    @Column(name = "ignore_mask")
    private int ignoreMask;
    @Basic(optional = false)
    @Column(name = "item0")
    private int item0;
    @Basic(optional = false)
    @Column(name = "item1")
    private int item1;
    @Basic(optional = false)
    @Column(name = "item2")
    private int item2;
    @Basic(optional = false)
    @Column(name = "item3")
    private int item3;
    @Basic(optional = false)
    @Column(name = "item4")
    private int item4;
    @Basic(optional = false)
    @Column(name = "item5")
    private int item5;
    @Basic(optional = false)
    @Column(name = "item6")
    private int item6;
    @Basic(optional = false)
    @Column(name = "item7")
    private int item7;
    @Basic(optional = false)
    @Column(name = "item8")
    private int item8;
    @Basic(optional = false)
    @Column(name = "item9")
    private int item9;
    @Basic(optional = false)
    @Column(name = "item10")
    private int item10;
    @Basic(optional = false)
    @Column(name = "item11")
    private int item11;
    @Basic(optional = false)
    @Column(name = "item12")
    private int item12;
    @Basic(optional = false)
    @Column(name = "item13")
    private int item13;
    @Basic(optional = false)
    @Column(name = "item14")
    private int item14;
    @Basic(optional = false)
    @Column(name = "item15")
    private int item15;
    @Basic(optional = false)
    @Column(name = "item16")
    private int item16;
    @Basic(optional = false)
    @Column(name = "item17")
    private int item17;
    @Basic(optional = false)
    @Column(name = "item18")
    private int item18;

    public CharacterEquipmentsets() {
    }

    public CharacterEquipmentsets(Long setguid) {
        this.setguid = setguid;
    }

    public CharacterEquipmentsets(Long setguid, int guid, short setindex, String name, String iconname, int ignoreMask, int item0, int item1, int item2, int item3, int item4, int item5, int item6, int item7, int item8, int item9, int item10, int item11, int item12, int item13, int item14, int item15, int item16, int item17, int item18) {
        this.setguid = setguid;
        this.guid = guid;
        this.setindex = setindex;
        this.name = name;
        this.iconname = iconname;
        this.ignoreMask = ignoreMask;
        this.item0 = item0;
        this.item1 = item1;
        this.item2 = item2;
        this.item3 = item3;
        this.item4 = item4;
        this.item5 = item5;
        this.item6 = item6;
        this.item7 = item7;
        this.item8 = item8;
        this.item9 = item9;
        this.item10 = item10;
        this.item11 = item11;
        this.item12 = item12;
        this.item13 = item13;
        this.item14 = item14;
        this.item15 = item15;
        this.item16 = item16;
        this.item17 = item17;
        this.item18 = item18;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public Long getSetguid() {
        return setguid;
    }

    public void setSetguid(Long setguid) {
        this.setguid = setguid;
    }

    public short getSetindex() {
        return setindex;
    }

    public void setSetindex(short setindex) {
        this.setindex = setindex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconname() {
        return iconname;
    }

    public void setIconname(String iconname) {
        this.iconname = iconname;
    }

    public int getIgnoreMask() {
        return ignoreMask;
    }

    public void setIgnoreMask(int ignoreMask) {
        this.ignoreMask = ignoreMask;
    }

    public int getItem0() {
        return item0;
    }

    public void setItem0(int item0) {
        this.item0 = item0;
    }

    public int getItem1() {
        return item1;
    }

    public void setItem1(int item1) {
        this.item1 = item1;
    }

    public int getItem2() {
        return item2;
    }

    public void setItem2(int item2) {
        this.item2 = item2;
    }

    public int getItem3() {
        return item3;
    }

    public void setItem3(int item3) {
        this.item3 = item3;
    }

    public int getItem4() {
        return item4;
    }

    public void setItem4(int item4) {
        this.item4 = item4;
    }

    public int getItem5() {
        return item5;
    }

    public void setItem5(int item5) {
        this.item5 = item5;
    }

    public int getItem6() {
        return item6;
    }

    public void setItem6(int item6) {
        this.item6 = item6;
    }

    public int getItem7() {
        return item7;
    }

    public void setItem7(int item7) {
        this.item7 = item7;
    }

    public int getItem8() {
        return item8;
    }

    public void setItem8(int item8) {
        this.item8 = item8;
    }

    public int getItem9() {
        return item9;
    }

    public void setItem9(int item9) {
        this.item9 = item9;
    }

    public int getItem10() {
        return item10;
    }

    public void setItem10(int item10) {
        this.item10 = item10;
    }

    public int getItem11() {
        return item11;
    }

    public void setItem11(int item11) {
        this.item11 = item11;
    }

    public int getItem12() {
        return item12;
    }

    public void setItem12(int item12) {
        this.item12 = item12;
    }

    public int getItem13() {
        return item13;
    }

    public void setItem13(int item13) {
        this.item13 = item13;
    }

    public int getItem14() {
        return item14;
    }

    public void setItem14(int item14) {
        this.item14 = item14;
    }

    public int getItem15() {
        return item15;
    }

    public void setItem15(int item15) {
        this.item15 = item15;
    }

    public int getItem16() {
        return item16;
    }

    public void setItem16(int item16) {
        this.item16 = item16;
    }

    public int getItem17() {
        return item17;
    }

    public void setItem17(int item17) {
        this.item17 = item17;
    }

    public int getItem18() {
        return item18;
    }

    public void setItem18(int item18) {
        this.item18 = item18;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (setguid != null ? setguid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterEquipmentsets)) {
            return false;
        }
        CharacterEquipmentsets other = (CharacterEquipmentsets) object;
        if ((this.setguid == null && other.setguid != null) || (this.setguid != null && !this.setguid.equals(other.setguid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterEquipmentsets[ setguid=" + setguid + " ]";
    }
    
}
