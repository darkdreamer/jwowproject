/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterInstancePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "instance")
    private int instance;

    public CharacterInstancePK() {
    }

    public CharacterInstancePK(int guid, int instance) {
        this.guid = guid;
        this.instance = instance;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public int getInstance() {
        return instance;
    }

    public void setInstance(int instance) {
        this.instance = instance;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) instance;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterInstancePK)) {
            return false;
        }
        CharacterInstancePK other = (CharacterInstancePK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.instance != other.instance) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterInstancePK[ guid=" + guid + ", instance=" + instance + " ]";
    }
    
}
