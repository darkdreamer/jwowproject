/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class GuildBankItemPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guildid")
    private int guildid;
    @Basic(optional = false)
    @Column(name = "TabId")
    private short tabId;
    @Basic(optional = false)
    @Column(name = "SlotId")
    private short slotId;

    public GuildBankItemPK() {
    }

    public GuildBankItemPK(int guildid, short tabId, short slotId) {
        this.guildid = guildid;
        this.tabId = tabId;
        this.slotId = slotId;
    }

    public int getGuildid() {
        return guildid;
    }

    public void setGuildid(int guildid) {
        this.guildid = guildid;
    }

    public short getTabId() {
        return tabId;
    }

    public void setTabId(short tabId) {
        this.tabId = tabId;
    }

    public short getSlotId() {
        return slotId;
    }

    public void setSlotId(short slotId) {
        this.slotId = slotId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guildid;
        hash += (int) tabId;
        hash += (int) slotId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuildBankItemPK)) {
            return false;
        }
        GuildBankItemPK other = (GuildBankItemPK) object;
        if (this.guildid != other.guildid) {
            return false;
        }
        if (this.tabId != other.tabId) {
            return false;
        }
        if (this.slotId != other.slotId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GuildBankItemPK[ guildid=" + guildid + ", tabId=" + tabId + ", slotId=" + slotId + " ]";
    }
    
}
