/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "pool_quest_save")
@NamedQueries({
    @NamedQuery(name = "PoolQuestSave.findAll", query = "SELECT p FROM PoolQuestSave p"),
    @NamedQuery(name = "PoolQuestSave.findByPoolId", query = "SELECT p FROM PoolQuestSave p WHERE p.poolQuestSavePK.poolId = :poolId"),
    @NamedQuery(name = "PoolQuestSave.findByQuestId", query = "SELECT p FROM PoolQuestSave p WHERE p.poolQuestSavePK.questId = :questId")})
public class PoolQuestSave implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PoolQuestSavePK poolQuestSavePK;

    public PoolQuestSave() {
    }

    public PoolQuestSave(PoolQuestSavePK poolQuestSavePK) {
        this.poolQuestSavePK = poolQuestSavePK;
    }

    public PoolQuestSave(int poolId, int questId) {
        this.poolQuestSavePK = new PoolQuestSavePK(poolId, questId);
    }

    public PoolQuestSavePK getPoolQuestSavePK() {
        return poolQuestSavePK;
    }

    public void setPoolQuestSavePK(PoolQuestSavePK poolQuestSavePK) {
        this.poolQuestSavePK = poolQuestSavePK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (poolQuestSavePK != null ? poolQuestSavePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PoolQuestSave)) {
            return false;
        }
        PoolQuestSave other = (PoolQuestSave) object;
        if ((this.poolQuestSavePK == null && other.poolQuestSavePK != null) || (this.poolQuestSavePK != null && !this.poolQuestSavePK.equals(other.poolQuestSavePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.PoolQuestSave[ poolQuestSavePK=" + poolQuestSavePK + " ]";
    }
    
}
