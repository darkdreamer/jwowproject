/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_queststatus_daily")
@NamedQueries({
    @NamedQuery(name = "CharacterQueststatusDaily.findAll", query = "SELECT c FROM CharacterQueststatusDaily c"),
    @NamedQuery(name = "CharacterQueststatusDaily.findByGuid", query = "SELECT c FROM CharacterQueststatusDaily c WHERE c.characterQueststatusDailyPK.guid = :guid"),
    @NamedQuery(name = "CharacterQueststatusDaily.findByQuest", query = "SELECT c FROM CharacterQueststatusDaily c WHERE c.characterQueststatusDailyPK.quest = :quest"),
    @NamedQuery(name = "CharacterQueststatusDaily.findByTime", query = "SELECT c FROM CharacterQueststatusDaily c WHERE c.time = :time")})
public class CharacterQueststatusDaily implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterQueststatusDailyPK characterQueststatusDailyPK;
    @Basic(optional = false)
    @Column(name = "time")
    private int time;

    public CharacterQueststatusDaily() {
    }

    public CharacterQueststatusDaily(CharacterQueststatusDailyPK characterQueststatusDailyPK) {
        this.characterQueststatusDailyPK = characterQueststatusDailyPK;
    }

    public CharacterQueststatusDaily(CharacterQueststatusDailyPK characterQueststatusDailyPK, int time) {
        this.characterQueststatusDailyPK = characterQueststatusDailyPK;
        this.time = time;
    }

    public CharacterQueststatusDaily(int guid, int quest) {
        this.characterQueststatusDailyPK = new CharacterQueststatusDailyPK(guid, quest);
    }

    public CharacterQueststatusDailyPK getCharacterQueststatusDailyPK() {
        return characterQueststatusDailyPK;
    }

    public void setCharacterQueststatusDailyPK(CharacterQueststatusDailyPK characterQueststatusDailyPK) {
        this.characterQueststatusDailyPK = characterQueststatusDailyPK;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterQueststatusDailyPK != null ? characterQueststatusDailyPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterQueststatusDaily)) {
            return false;
        }
        CharacterQueststatusDaily other = (CharacterQueststatusDaily) object;
        if ((this.characterQueststatusDailyPK == null && other.characterQueststatusDailyPK != null) || (this.characterQueststatusDailyPK != null && !this.characterQueststatusDailyPK.equals(other.characterQueststatusDailyPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterQueststatusDaily[ characterQueststatusDailyPK=" + characterQueststatusDailyPK + " ]";
    }
    
}
