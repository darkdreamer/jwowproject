/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "banned_addons")
@NamedQueries({
    @NamedQuery(name = "BannedAddons.findAll", query = "SELECT b FROM BannedAddons b"),
    @NamedQuery(name = "BannedAddons.findById", query = "SELECT b FROM BannedAddons b WHERE b.id = :id"),
    @NamedQuery(name = "BannedAddons.findByName", query = "SELECT b FROM BannedAddons b WHERE b.name = :name"),
    @NamedQuery(name = "BannedAddons.findByVersion", query = "SELECT b FROM BannedAddons b WHERE b.version = :version"),
    @NamedQuery(name = "BannedAddons.findByTimestamp", query = "SELECT b FROM BannedAddons b WHERE b.timestamp = :timestamp")})
public class BannedAddons implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @Column(name = "Version")
    private String version;
    @Basic(optional = false)
    @Column(name = "Timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    public BannedAddons() {
    }

    public BannedAddons(Integer id) {
        this.id = id;
    }

    public BannedAddons(Integer id, String name, String version, Date timestamp) {
        this.id = id;
        this.name = name;
        this.version = version;
        this.timestamp = timestamp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BannedAddons)) {
            return false;
        }
        BannedAddons other = (BannedAddons) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.BannedAddons[ id=" + id + " ]";
    }
    
}
