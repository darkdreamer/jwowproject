/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "arena_team_member")
@NamedQueries({
    @NamedQuery(name = "ArenaTeamMember.findAll", query = "SELECT a FROM ArenaTeamMember a"),
    @NamedQuery(name = "ArenaTeamMember.findByArenaTeamId", query = "SELECT a FROM ArenaTeamMember a WHERE a.arenaTeamMemberPK.arenaTeamId = :arenaTeamId"),
    @NamedQuery(name = "ArenaTeamMember.findByGuid", query = "SELECT a FROM ArenaTeamMember a WHERE a.arenaTeamMemberPK.guid = :guid"),
    @NamedQuery(name = "ArenaTeamMember.findByWeekGames", query = "SELECT a FROM ArenaTeamMember a WHERE a.weekGames = :weekGames"),
    @NamedQuery(name = "ArenaTeamMember.findByWeekWins", query = "SELECT a FROM ArenaTeamMember a WHERE a.weekWins = :weekWins"),
    @NamedQuery(name = "ArenaTeamMember.findBySeasonGames", query = "SELECT a FROM ArenaTeamMember a WHERE a.seasonGames = :seasonGames"),
    @NamedQuery(name = "ArenaTeamMember.findBySeasonWins", query = "SELECT a FROM ArenaTeamMember a WHERE a.seasonWins = :seasonWins"),
    @NamedQuery(name = "ArenaTeamMember.findByPersonalRating", query = "SELECT a FROM ArenaTeamMember a WHERE a.personalRating = :personalRating")})
public class ArenaTeamMember implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ArenaTeamMemberPK arenaTeamMemberPK;
    @Basic(optional = false)
    @Column(name = "weekGames")
    private short weekGames;
    @Basic(optional = false)
    @Column(name = "weekWins")
    private short weekWins;
    @Basic(optional = false)
    @Column(name = "seasonGames")
    private short seasonGames;
    @Basic(optional = false)
    @Column(name = "seasonWins")
    private short seasonWins;
    @Basic(optional = false)
    @Column(name = "personalRating")
    private short personalRating;

    public ArenaTeamMember() {
    }

    public ArenaTeamMember(ArenaTeamMemberPK arenaTeamMemberPK) {
        this.arenaTeamMemberPK = arenaTeamMemberPK;
    }

    public ArenaTeamMember(ArenaTeamMemberPK arenaTeamMemberPK, short weekGames, short weekWins, short seasonGames, short seasonWins, short personalRating) {
        this.arenaTeamMemberPK = arenaTeamMemberPK;
        this.weekGames = weekGames;
        this.weekWins = weekWins;
        this.seasonGames = seasonGames;
        this.seasonWins = seasonWins;
        this.personalRating = personalRating;
    }

    public ArenaTeamMember(int arenaTeamId, int guid) {
        this.arenaTeamMemberPK = new ArenaTeamMemberPK(arenaTeamId, guid);
    }

    public ArenaTeamMemberPK getArenaTeamMemberPK() {
        return arenaTeamMemberPK;
    }

    public void setArenaTeamMemberPK(ArenaTeamMemberPK arenaTeamMemberPK) {
        this.arenaTeamMemberPK = arenaTeamMemberPK;
    }

    public short getWeekGames() {
        return weekGames;
    }

    public void setWeekGames(short weekGames) {
        this.weekGames = weekGames;
    }

    public short getWeekWins() {
        return weekWins;
    }

    public void setWeekWins(short weekWins) {
        this.weekWins = weekWins;
    }

    public short getSeasonGames() {
        return seasonGames;
    }

    public void setSeasonGames(short seasonGames) {
        this.seasonGames = seasonGames;
    }

    public short getSeasonWins() {
        return seasonWins;
    }

    public void setSeasonWins(short seasonWins) {
        this.seasonWins = seasonWins;
    }

    public short getPersonalRating() {
        return personalRating;
    }

    public void setPersonalRating(short personalRating) {
        this.personalRating = personalRating;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (arenaTeamMemberPK != null ? arenaTeamMemberPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArenaTeamMember)) {
            return false;
        }
        ArenaTeamMember other = (ArenaTeamMember) object;
        if ((this.arenaTeamMemberPK == null && other.arenaTeamMemberPK != null) || (this.arenaTeamMemberPK != null && !this.arenaTeamMemberPK.equals(other.arenaTeamMemberPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.ArenaTeamMember[ arenaTeamMemberPK=" + arenaTeamMemberPK + " ]";
    }
    
}
