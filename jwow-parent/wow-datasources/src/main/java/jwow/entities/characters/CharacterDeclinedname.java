/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_declinedname")
@NamedQueries({
    @NamedQuery(name = "CharacterDeclinedname.findAll", query = "SELECT c FROM CharacterDeclinedname c"),
    @NamedQuery(name = "CharacterDeclinedname.findByGuid", query = "SELECT c FROM CharacterDeclinedname c WHERE c.guid = :guid"),
    @NamedQuery(name = "CharacterDeclinedname.findByGenitive", query = "SELECT c FROM CharacterDeclinedname c WHERE c.genitive = :genitive"),
    @NamedQuery(name = "CharacterDeclinedname.findByDative", query = "SELECT c FROM CharacterDeclinedname c WHERE c.dative = :dative"),
    @NamedQuery(name = "CharacterDeclinedname.findByAccusative", query = "SELECT c FROM CharacterDeclinedname c WHERE c.accusative = :accusative"),
    @NamedQuery(name = "CharacterDeclinedname.findByInstrumental", query = "SELECT c FROM CharacterDeclinedname c WHERE c.instrumental = :instrumental"),
    @NamedQuery(name = "CharacterDeclinedname.findByPrepositional", query = "SELECT c FROM CharacterDeclinedname c WHERE c.prepositional = :prepositional")})
public class CharacterDeclinedname implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "guid")
    private Integer guid;
    @Basic(optional = false)
    @Column(name = "genitive")
    private String genitive;
    @Basic(optional = false)
    @Column(name = "dative")
    private String dative;
    @Basic(optional = false)
    @Column(name = "accusative")
    private String accusative;
    @Basic(optional = false)
    @Column(name = "instrumental")
    private String instrumental;
    @Basic(optional = false)
    @Column(name = "prepositional")
    private String prepositional;

    public CharacterDeclinedname() {
    }

    public CharacterDeclinedname(Integer guid) {
        this.guid = guid;
    }

    public CharacterDeclinedname(Integer guid, String genitive, String dative, String accusative, String instrumental, String prepositional) {
        this.guid = guid;
        this.genitive = genitive;
        this.dative = dative;
        this.accusative = accusative;
        this.instrumental = instrumental;
        this.prepositional = prepositional;
    }

    public Integer getGuid() {
        return guid;
    }

    public void setGuid(Integer guid) {
        this.guid = guid;
    }

    public String getGenitive() {
        return genitive;
    }

    public void setGenitive(String genitive) {
        this.genitive = genitive;
    }

    public String getDative() {
        return dative;
    }

    public void setDative(String dative) {
        this.dative = dative;
    }

    public String getAccusative() {
        return accusative;
    }

    public void setAccusative(String accusative) {
        this.accusative = accusative;
    }

    public String getInstrumental() {
        return instrumental;
    }

    public void setInstrumental(String instrumental) {
        this.instrumental = instrumental;
    }

    public String getPrepositional() {
        return prepositional;
    }

    public void setPrepositional(String prepositional) {
        this.prepositional = prepositional;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guid != null ? guid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterDeclinedname)) {
            return false;
        }
        CharacterDeclinedname other = (CharacterDeclinedname) object;
        if ((this.guid == null && other.guid != null) || (this.guid != null && !this.guid.equals(other.guid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterDeclinedname[ guid=" + guid + " ]";
    }
    
}
