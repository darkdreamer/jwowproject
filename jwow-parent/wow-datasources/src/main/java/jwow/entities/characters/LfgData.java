/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "lfg_data")
@NamedQueries({
    @NamedQuery(name = "LfgData.findAll", query = "SELECT l FROM LfgData l"),
    @NamedQuery(name = "LfgData.findByGuid", query = "SELECT l FROM LfgData l WHERE l.guid = :guid"),
    @NamedQuery(name = "LfgData.findByDungeon", query = "SELECT l FROM LfgData l WHERE l.dungeon = :dungeon"),
    @NamedQuery(name = "LfgData.findByState", query = "SELECT l FROM LfgData l WHERE l.state = :state")})
public class LfgData implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "guid")
    private Integer guid;
    @Basic(optional = false)
    @Column(name = "dungeon")
    private int dungeon;
    @Basic(optional = false)
    @Column(name = "state")
    private short state;

    public LfgData() {
    }

    public LfgData(Integer guid) {
        this.guid = guid;
    }

    public LfgData(Integer guid, int dungeon, short state) {
        this.guid = guid;
        this.dungeon = dungeon;
        this.state = state;
    }

    public Integer getGuid() {
        return guid;
    }

    public void setGuid(Integer guid) {
        this.guid = guid;
    }

    public int getDungeon() {
        return dungeon;
    }

    public void setDungeon(int dungeon) {
        this.dungeon = dungeon;
    }

    public short getState() {
        return state;
    }

    public void setState(short state) {
        this.state = state;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guid != null ? guid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LfgData)) {
            return false;
        }
        LfgData other = (LfgData) object;
        if ((this.guid == null && other.guid != null) || (this.guid != null && !this.guid.equals(other.guid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.LfgData[ guid=" + guid + " ]";
    }
    
}
