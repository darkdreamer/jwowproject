/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class GuildBankTabPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guildid")
    private int guildid;
    @Basic(optional = false)
    @Column(name = "TabId")
    private short tabId;

    public GuildBankTabPK() {
    }

    public GuildBankTabPK(int guildid, short tabId) {
        this.guildid = guildid;
        this.tabId = tabId;
    }

    public int getGuildid() {
        return guildid;
    }

    public void setGuildid(int guildid) {
        this.guildid = guildid;
    }

    public short getTabId() {
        return tabId;
    }

    public void setTabId(short tabId) {
        this.tabId = tabId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guildid;
        hash += (int) tabId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuildBankTabPK)) {
            return false;
        }
        GuildBankTabPK other = (GuildBankTabPK) object;
        if (this.guildid != other.guildid) {
            return false;
        }
        if (this.tabId != other.tabId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GuildBankTabPK[ guildid=" + guildid + ", tabId=" + tabId + " ]";
    }
    
}
