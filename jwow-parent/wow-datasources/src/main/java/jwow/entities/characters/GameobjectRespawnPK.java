/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class GameobjectRespawnPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "instanceId")
    private int instanceId;

    public GameobjectRespawnPK() {
    }

    public GameobjectRespawnPK(int guid, int instanceId) {
        this.guid = guid;
        this.instanceId = instanceId;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public int getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(int instanceId) {
        this.instanceId = instanceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) instanceId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GameobjectRespawnPK)) {
            return false;
        }
        GameobjectRespawnPK other = (GameobjectRespawnPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.instanceId != other.instanceId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GameobjectRespawnPK[ guid=" + guid + ", instanceId=" + instanceId + " ]";
    }
    
}
