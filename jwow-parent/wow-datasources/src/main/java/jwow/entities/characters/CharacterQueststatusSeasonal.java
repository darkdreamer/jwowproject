/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_queststatus_seasonal")
@NamedQueries({
    @NamedQuery(name = "CharacterQueststatusSeasonal.findAll", query = "SELECT c FROM CharacterQueststatusSeasonal c"),
    @NamedQuery(name = "CharacterQueststatusSeasonal.findByGuid", query = "SELECT c FROM CharacterQueststatusSeasonal c WHERE c.characterQueststatusSeasonalPK.guid = :guid"),
    @NamedQuery(name = "CharacterQueststatusSeasonal.findByQuest", query = "SELECT c FROM CharacterQueststatusSeasonal c WHERE c.characterQueststatusSeasonalPK.quest = :quest"),
    @NamedQuery(name = "CharacterQueststatusSeasonal.findByEvent", query = "SELECT c FROM CharacterQueststatusSeasonal c WHERE c.event = :event")})
public class CharacterQueststatusSeasonal implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterQueststatusSeasonalPK characterQueststatusSeasonalPK;
    @Basic(optional = false)
    @Column(name = "event")
    private int event;

    public CharacterQueststatusSeasonal() {
    }

    public CharacterQueststatusSeasonal(CharacterQueststatusSeasonalPK characterQueststatusSeasonalPK) {
        this.characterQueststatusSeasonalPK = characterQueststatusSeasonalPK;
    }

    public CharacterQueststatusSeasonal(CharacterQueststatusSeasonalPK characterQueststatusSeasonalPK, int event) {
        this.characterQueststatusSeasonalPK = characterQueststatusSeasonalPK;
        this.event = event;
    }

    public CharacterQueststatusSeasonal(int guid, int quest) {
        this.characterQueststatusSeasonalPK = new CharacterQueststatusSeasonalPK(guid, quest);
    }

    public CharacterQueststatusSeasonalPK getCharacterQueststatusSeasonalPK() {
        return characterQueststatusSeasonalPK;
    }

    public void setCharacterQueststatusSeasonalPK(CharacterQueststatusSeasonalPK characterQueststatusSeasonalPK) {
        this.characterQueststatusSeasonalPK = characterQueststatusSeasonalPK;
    }

    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterQueststatusSeasonalPK != null ? characterQueststatusSeasonalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterQueststatusSeasonal)) {
            return false;
        }
        CharacterQueststatusSeasonal other = (CharacterQueststatusSeasonal) object;
        if ((this.characterQueststatusSeasonalPK == null && other.characterQueststatusSeasonalPK != null) || (this.characterQueststatusSeasonalPK != null && !this.characterQueststatusSeasonalPK.equals(other.characterQueststatusSeasonalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterQueststatusSeasonal[ characterQueststatusSeasonalPK=" + characterQueststatusSeasonalPK + " ]";
    }
    
}
