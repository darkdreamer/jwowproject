/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "addons")
@NamedQueries({
    @NamedQuery(name = "Addons.findAll", query = "SELECT a FROM Addons a"),
    @NamedQuery(name = "Addons.findByName", query = "SELECT a FROM Addons a WHERE a.name = :name"),
    @NamedQuery(name = "Addons.findByCrc", query = "SELECT a FROM Addons a WHERE a.crc = :crc")})
public class Addons implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "crc")
    private int crc;

    public Addons() {
    }

    public Addons(String name) {
        this.name = name;
    }

    public Addons(String name, int crc) {
        this.name = name;
        this.crc = crc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCrc() {
        return crc;
    }

    public void setCrc(int crc) {
        this.crc = crc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Addons)) {
            return false;
        }
        Addons other = (Addons) object;
        if ((this.name == null && other.name != null) || (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.Addons[ name=" + name + " ]";
    }
    
}
