/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "corpse")
@NamedQueries({
    @NamedQuery(name = "Corpse.findAll", query = "SELECT c FROM Corpse c"),
    @NamedQuery(name = "Corpse.findByCorpseGuid", query = "SELECT c FROM Corpse c WHERE c.corpseGuid = :corpseGuid"),
    @NamedQuery(name = "Corpse.findByGuid", query = "SELECT c FROM Corpse c WHERE c.guid = :guid"),
    @NamedQuery(name = "Corpse.findByPosX", query = "SELECT c FROM Corpse c WHERE c.posX = :posX"),
    @NamedQuery(name = "Corpse.findByPosY", query = "SELECT c FROM Corpse c WHERE c.posY = :posY"),
    @NamedQuery(name = "Corpse.findByPosZ", query = "SELECT c FROM Corpse c WHERE c.posZ = :posZ"),
    @NamedQuery(name = "Corpse.findByOrientation", query = "SELECT c FROM Corpse c WHERE c.orientation = :orientation"),
    @NamedQuery(name = "Corpse.findByMapId", query = "SELECT c FROM Corpse c WHERE c.mapId = :mapId"),
    @NamedQuery(name = "Corpse.findByPhaseMask", query = "SELECT c FROM Corpse c WHERE c.phaseMask = :phaseMask"),
    @NamedQuery(name = "Corpse.findByDisplayId", query = "SELECT c FROM Corpse c WHERE c.displayId = :displayId"),
    @NamedQuery(name = "Corpse.findByBytes1", query = "SELECT c FROM Corpse c WHERE c.bytes1 = :bytes1"),
    @NamedQuery(name = "Corpse.findByBytes2", query = "SELECT c FROM Corpse c WHERE c.bytes2 = :bytes2"),
    @NamedQuery(name = "Corpse.findByGuildId", query = "SELECT c FROM Corpse c WHERE c.guildId = :guildId"),
    @NamedQuery(name = "Corpse.findByFlags", query = "SELECT c FROM Corpse c WHERE c.flags = :flags"),
    @NamedQuery(name = "Corpse.findByDynFlags", query = "SELECT c FROM Corpse c WHERE c.dynFlags = :dynFlags"),
    @NamedQuery(name = "Corpse.findByTime", query = "SELECT c FROM Corpse c WHERE c.time = :time"),
    @NamedQuery(name = "Corpse.findByCorpseType", query = "SELECT c FROM Corpse c WHERE c.corpseType = :corpseType"),
    @NamedQuery(name = "Corpse.findByInstanceId", query = "SELECT c FROM Corpse c WHERE c.instanceId = :instanceId")})
public class Corpse implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "corpseGuid")
    private Integer corpseGuid;
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "posX")
    private float posX;
    @Basic(optional = false)
    @Column(name = "posY")
    private float posY;
    @Basic(optional = false)
    @Column(name = "posZ")
    private float posZ;
    @Basic(optional = false)
    @Column(name = "orientation")
    private float orientation;
    @Basic(optional = false)
    @Column(name = "mapId")
    private short mapId;
    @Basic(optional = false)
    @Column(name = "phaseMask")
    private short phaseMask;
    @Basic(optional = false)
    @Column(name = "displayId")
    private int displayId;
    @Basic(optional = false)
    @Lob
    @Column(name = "itemCache")
    private String itemCache;
    @Basic(optional = false)
    @Column(name = "bytes1")
    private int bytes1;
    @Basic(optional = false)
    @Column(name = "bytes2")
    private int bytes2;
    @Basic(optional = false)
    @Column(name = "guildId")
    private int guildId;
    @Basic(optional = false)
    @Column(name = "flags")
    private short flags;
    @Basic(optional = false)
    @Column(name = "dynFlags")
    private short dynFlags;
    @Basic(optional = false)
    @Column(name = "time")
    private int time;
    @Basic(optional = false)
    @Column(name = "corpseType")
    private short corpseType;
    @Basic(optional = false)
    @Column(name = "instanceId")
    private int instanceId;

    public Corpse() {
    }

    public Corpse(Integer corpseGuid) {
        this.corpseGuid = corpseGuid;
    }

    public Corpse(Integer corpseGuid, int guid, float posX, float posY, float posZ, float orientation, short mapId, short phaseMask, int displayId, String itemCache, int bytes1, int bytes2, int guildId, short flags, short dynFlags, int time, short corpseType, int instanceId) {
        this.corpseGuid = corpseGuid;
        this.guid = guid;
        this.posX = posX;
        this.posY = posY;
        this.posZ = posZ;
        this.orientation = orientation;
        this.mapId = mapId;
        this.phaseMask = phaseMask;
        this.displayId = displayId;
        this.itemCache = itemCache;
        this.bytes1 = bytes1;
        this.bytes2 = bytes2;
        this.guildId = guildId;
        this.flags = flags;
        this.dynFlags = dynFlags;
        this.time = time;
        this.corpseType = corpseType;
        this.instanceId = instanceId;
    }

    public Integer getCorpseGuid() {
        return corpseGuid;
    }

    public void setCorpseGuid(Integer corpseGuid) {
        this.corpseGuid = corpseGuid;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public float getPosX() {
        return posX;
    }

    public void setPosX(float posX) {
        this.posX = posX;
    }

    public float getPosY() {
        return posY;
    }

    public void setPosY(float posY) {
        this.posY = posY;
    }

    public float getPosZ() {
        return posZ;
    }

    public void setPosZ(float posZ) {
        this.posZ = posZ;
    }

    public float getOrientation() {
        return orientation;
    }

    public void setOrientation(float orientation) {
        this.orientation = orientation;
    }

    public short getMapId() {
        return mapId;
    }

    public void setMapId(short mapId) {
        this.mapId = mapId;
    }

    public short getPhaseMask() {
        return phaseMask;
    }

    public void setPhaseMask(short phaseMask) {
        this.phaseMask = phaseMask;
    }

    public int getDisplayId() {
        return displayId;
    }

    public void setDisplayId(int displayId) {
        this.displayId = displayId;
    }

    public String getItemCache() {
        return itemCache;
    }

    public void setItemCache(String itemCache) {
        this.itemCache = itemCache;
    }

    public int getBytes1() {
        return bytes1;
    }

    public void setBytes1(int bytes1) {
        this.bytes1 = bytes1;
    }

    public int getBytes2() {
        return bytes2;
    }

    public void setBytes2(int bytes2) {
        this.bytes2 = bytes2;
    }

    public int getGuildId() {
        return guildId;
    }

    public void setGuildId(int guildId) {
        this.guildId = guildId;
    }

    public short getFlags() {
        return flags;
    }

    public void setFlags(short flags) {
        this.flags = flags;
    }

    public short getDynFlags() {
        return dynFlags;
    }

    public void setDynFlags(short dynFlags) {
        this.dynFlags = dynFlags;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public short getCorpseType() {
        return corpseType;
    }

    public void setCorpseType(short corpseType) {
        this.corpseType = corpseType;
    }

    public int getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(int instanceId) {
        this.instanceId = instanceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (corpseGuid != null ? corpseGuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Corpse)) {
            return false;
        }
        Corpse other = (Corpse) object;
        if ((this.corpseGuid == null && other.corpseGuid != null) || (this.corpseGuid != null && !this.corpseGuid.equals(other.corpseGuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.Corpse[ corpseGuid=" + corpseGuid + " ]";
    }
    
}
