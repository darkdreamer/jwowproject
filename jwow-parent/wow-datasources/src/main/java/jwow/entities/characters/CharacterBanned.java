/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_banned")
@NamedQueries({
    @NamedQuery(name = "CharacterBanned.findAll", query = "SELECT c FROM CharacterBanned c"),
    @NamedQuery(name = "CharacterBanned.findByGuid", query = "SELECT c FROM CharacterBanned c WHERE c.characterBannedPK.guid = :guid"),
    @NamedQuery(name = "CharacterBanned.findByBandate", query = "SELECT c FROM CharacterBanned c WHERE c.characterBannedPK.bandate = :bandate"),
    @NamedQuery(name = "CharacterBanned.findByUnbandate", query = "SELECT c FROM CharacterBanned c WHERE c.unbandate = :unbandate"),
    @NamedQuery(name = "CharacterBanned.findByBannedby", query = "SELECT c FROM CharacterBanned c WHERE c.bannedby = :bannedby"),
    @NamedQuery(name = "CharacterBanned.findByBanreason", query = "SELECT c FROM CharacterBanned c WHERE c.banreason = :banreason"),
    @NamedQuery(name = "CharacterBanned.findByActive", query = "SELECT c FROM CharacterBanned c WHERE c.active = :active")})
public class CharacterBanned implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterBannedPK characterBannedPK;
    @Basic(optional = false)
    @Column(name = "unbandate")
    private int unbandate;
    @Basic(optional = false)
    @Column(name = "bannedby")
    private String bannedby;
    @Basic(optional = false)
    @Column(name = "banreason")
    private String banreason;
    @Basic(optional = false)
    @Column(name = "active")
    private short active;

    public CharacterBanned() {
    }

    public CharacterBanned(CharacterBannedPK characterBannedPK) {
        this.characterBannedPK = characterBannedPK;
    }

    public CharacterBanned(CharacterBannedPK characterBannedPK, int unbandate, String bannedby, String banreason, short active) {
        this.characterBannedPK = characterBannedPK;
        this.unbandate = unbandate;
        this.bannedby = bannedby;
        this.banreason = banreason;
        this.active = active;
    }

    public CharacterBanned(int guid, int bandate) {
        this.characterBannedPK = new CharacterBannedPK(guid, bandate);
    }

    public CharacterBannedPK getCharacterBannedPK() {
        return characterBannedPK;
    }

    public void setCharacterBannedPK(CharacterBannedPK characterBannedPK) {
        this.characterBannedPK = characterBannedPK;
    }

    public int getUnbandate() {
        return unbandate;
    }

    public void setUnbandate(int unbandate) {
        this.unbandate = unbandate;
    }

    public String getBannedby() {
        return bannedby;
    }

    public void setBannedby(String bannedby) {
        this.bannedby = bannedby;
    }

    public String getBanreason() {
        return banreason;
    }

    public void setBanreason(String banreason) {
        this.banreason = banreason;
    }

    public short getActive() {
        return active;
    }

    public void setActive(short active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterBannedPK != null ? characterBannedPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterBanned)) {
            return false;
        }
        CharacterBanned other = (CharacterBanned) object;
        if ((this.characterBannedPK == null && other.characterBannedPK != null) || (this.characterBannedPK != null && !this.characterBannedPK.equals(other.characterBannedPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterBanned[ characterBannedPK=" + characterBannedPK + " ]";
    }
    
}
