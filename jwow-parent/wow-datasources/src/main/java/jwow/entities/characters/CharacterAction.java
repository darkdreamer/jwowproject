/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_action")
@NamedQueries({
    @NamedQuery(name = "CharacterAction.findAll", query = "SELECT c FROM CharacterAction c"),
    @NamedQuery(name = "CharacterAction.findByGuid", query = "SELECT c FROM CharacterAction c WHERE c.characterActionPK.guid = :guid"),
    @NamedQuery(name = "CharacterAction.findBySpec", query = "SELECT c FROM CharacterAction c WHERE c.characterActionPK.spec = :spec"),
    @NamedQuery(name = "CharacterAction.findByButton", query = "SELECT c FROM CharacterAction c WHERE c.characterActionPK.button = :button"),
    @NamedQuery(name = "CharacterAction.findByAction", query = "SELECT c FROM CharacterAction c WHERE c.action = :action"),
    @NamedQuery(name = "CharacterAction.findByType", query = "SELECT c FROM CharacterAction c WHERE c.type = :type")})
public class CharacterAction implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterActionPK characterActionPK;
    @Basic(optional = false)
    @Column(name = "action")
    private int action;
    @Basic(optional = false)
    @Column(name = "type")
    private short type;

    public CharacterAction() {
    }

    public CharacterAction(CharacterActionPK characterActionPK) {
        this.characterActionPK = characterActionPK;
    }

    public CharacterAction(CharacterActionPK characterActionPK, int action, short type) {
        this.characterActionPK = characterActionPK;
        this.action = action;
        this.type = type;
    }

    public CharacterAction(int guid, short spec, short button) {
        this.characterActionPK = new CharacterActionPK(guid, spec, button);
    }

    public CharacterActionPK getCharacterActionPK() {
        return characterActionPK;
    }

    public void setCharacterActionPK(CharacterActionPK characterActionPK) {
        this.characterActionPK = characterActionPK;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterActionPK != null ? characterActionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterAction)) {
            return false;
        }
        CharacterAction other = (CharacterAction) object;
        if ((this.characterActionPK == null && other.characterActionPK != null) || (this.characterActionPK != null && !this.characterActionPK.equals(other.characterActionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterAction[ characterActionPK=" + characterActionPK + " ]";
    }
    
}
