/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_queststatus_rewarded")
@NamedQueries({
    @NamedQuery(name = "CharacterQueststatusRewarded.findAll", query = "SELECT c FROM CharacterQueststatusRewarded c"),
    @NamedQuery(name = "CharacterQueststatusRewarded.findByGuid", query = "SELECT c FROM CharacterQueststatusRewarded c WHERE c.characterQueststatusRewardedPK.guid = :guid"),
    @NamedQuery(name = "CharacterQueststatusRewarded.findByQuest", query = "SELECT c FROM CharacterQueststatusRewarded c WHERE c.characterQueststatusRewardedPK.quest = :quest"),
    @NamedQuery(name = "CharacterQueststatusRewarded.findByActive", query = "SELECT c FROM CharacterQueststatusRewarded c WHERE c.active = :active")})
public class CharacterQueststatusRewarded implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterQueststatusRewardedPK characterQueststatusRewardedPK;
    @Basic(optional = false)
    @Column(name = "active")
    private short active;

    public CharacterQueststatusRewarded() {
    }

    public CharacterQueststatusRewarded(CharacterQueststatusRewardedPK characterQueststatusRewardedPK) {
        this.characterQueststatusRewardedPK = characterQueststatusRewardedPK;
    }

    public CharacterQueststatusRewarded(CharacterQueststatusRewardedPK characterQueststatusRewardedPK, short active) {
        this.characterQueststatusRewardedPK = characterQueststatusRewardedPK;
        this.active = active;
    }

    public CharacterQueststatusRewarded(int guid, int quest) {
        this.characterQueststatusRewardedPK = new CharacterQueststatusRewardedPK(guid, quest);
    }

    public CharacterQueststatusRewardedPK getCharacterQueststatusRewardedPK() {
        return characterQueststatusRewardedPK;
    }

    public void setCharacterQueststatusRewardedPK(CharacterQueststatusRewardedPK characterQueststatusRewardedPK) {
        this.characterQueststatusRewardedPK = characterQueststatusRewardedPK;
    }

    public short getActive() {
        return active;
    }

    public void setActive(short active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterQueststatusRewardedPK != null ? characterQueststatusRewardedPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterQueststatusRewarded)) {
            return false;
        }
        CharacterQueststatusRewarded other = (CharacterQueststatusRewarded) object;
        if ((this.characterQueststatusRewardedPK == null && other.characterQueststatusRewardedPK != null) || (this.characterQueststatusRewardedPK != null && !this.characterQueststatusRewardedPK.equals(other.characterQueststatusRewardedPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterQueststatusRewarded[ characterQueststatusRewardedPK=" + characterQueststatusRewardedPK + " ]";
    }
    
}
