/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "gm_surveys")
@NamedQueries({
    @NamedQuery(name = "GmSurveys.findAll", query = "SELECT g FROM GmSurveys g"),
    @NamedQuery(name = "GmSurveys.findBySurveyId", query = "SELECT g FROM GmSurveys g WHERE g.surveyId = :surveyId"),
    @NamedQuery(name = "GmSurveys.findByGuid", query = "SELECT g FROM GmSurveys g WHERE g.guid = :guid"),
    @NamedQuery(name = "GmSurveys.findByMainSurvey", query = "SELECT g FROM GmSurveys g WHERE g.mainSurvey = :mainSurvey"),
    @NamedQuery(name = "GmSurveys.findByCreateTime", query = "SELECT g FROM GmSurveys g WHERE g.createTime = :createTime")})
public class GmSurveys implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "surveyId")
    private Integer surveyId;
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "mainSurvey")
    private int mainSurvey;
    @Basic(optional = false)
    @Lob
    @Column(name = "overallComment")
    private String overallComment;
    @Basic(optional = false)
    @Column(name = "createTime")
    private int createTime;

    public GmSurveys() {
    }

    public GmSurveys(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public GmSurveys(Integer surveyId, int guid, int mainSurvey, String overallComment, int createTime) {
        this.surveyId = surveyId;
        this.guid = guid;
        this.mainSurvey = mainSurvey;
        this.overallComment = overallComment;
        this.createTime = createTime;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public int getMainSurvey() {
        return mainSurvey;
    }

    public void setMainSurvey(int mainSurvey) {
        this.mainSurvey = mainSurvey;
    }

    public String getOverallComment() {
        return overallComment;
    }

    public void setOverallComment(String overallComment) {
        this.overallComment = overallComment;
    }

    public int getCreateTime() {
        return createTime;
    }

    public void setCreateTime(int createTime) {
        this.createTime = createTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (surveyId != null ? surveyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GmSurveys)) {
            return false;
        }
        GmSurveys other = (GmSurveys) object;
        if ((this.surveyId == null && other.surveyId != null) || (this.surveyId != null && !this.surveyId.equals(other.surveyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GmSurveys[ surveyId=" + surveyId + " ]";
    }
    
}
