/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class GmSubsurveysPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "surveyId")
    private int surveyId;
    @Basic(optional = false)
    @Column(name = "subsurveyId")
    private int subsurveyId;

    public GmSubsurveysPK() {
    }

    public GmSubsurveysPK(int surveyId, int subsurveyId) {
        this.surveyId = surveyId;
        this.subsurveyId = subsurveyId;
    }

    public int getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(int surveyId) {
        this.surveyId = surveyId;
    }

    public int getSubsurveyId() {
        return subsurveyId;
    }

    public void setSubsurveyId(int subsurveyId) {
        this.subsurveyId = subsurveyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) surveyId;
        hash += (int) subsurveyId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GmSubsurveysPK)) {
            return false;
        }
        GmSubsurveysPK other = (GmSubsurveysPK) object;
        if (this.surveyId != other.surveyId) {
            return false;
        }
        if (this.subsurveyId != other.subsurveyId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GmSubsurveysPK[ surveyId=" + surveyId + ", subsurveyId=" + subsurveyId + " ]";
    }
    
}
