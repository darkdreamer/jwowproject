/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_achievement")
@NamedQueries({
    @NamedQuery(name = "CharacterAchievement.findAll", query = "SELECT c FROM CharacterAchievement c"),
    @NamedQuery(name = "CharacterAchievement.findByGuid", query = "SELECT c FROM CharacterAchievement c WHERE c.characterAchievementPK.guid = :guid"),
    @NamedQuery(name = "CharacterAchievement.findByAchievement", query = "SELECT c FROM CharacterAchievement c WHERE c.characterAchievementPK.achievement = :achievement"),
    @NamedQuery(name = "CharacterAchievement.findByDate", query = "SELECT c FROM CharacterAchievement c WHERE c.date = :date")})
public class CharacterAchievement implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterAchievementPK characterAchievementPK;
    @Basic(optional = false)
    @Column(name = "date")
    private int date;

    public CharacterAchievement() {
    }

    public CharacterAchievement(CharacterAchievementPK characterAchievementPK) {
        this.characterAchievementPK = characterAchievementPK;
    }

    public CharacterAchievement(CharacterAchievementPK characterAchievementPK, int date) {
        this.characterAchievementPK = characterAchievementPK;
        this.date = date;
    }

    public CharacterAchievement(int guid, short achievement) {
        this.characterAchievementPK = new CharacterAchievementPK(guid, achievement);
    }

    public CharacterAchievementPK getCharacterAchievementPK() {
        return characterAchievementPK;
    }

    public void setCharacterAchievementPK(CharacterAchievementPK characterAchievementPK) {
        this.characterAchievementPK = characterAchievementPK;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterAchievementPK != null ? characterAchievementPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterAchievement)) {
            return false;
        }
        CharacterAchievement other = (CharacterAchievement) object;
        if ((this.characterAchievementPK == null && other.characterAchievementPK != null) || (this.characterAchievementPK != null && !this.characterAchievementPK.equals(other.characterAchievementPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterAchievement[ characterAchievementPK=" + characterAchievementPK + " ]";
    }
    
}
