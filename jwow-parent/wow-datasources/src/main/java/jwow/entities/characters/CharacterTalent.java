/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_talent")
@NamedQueries({
    @NamedQuery(name = "CharacterTalent.findAll", query = "SELECT c FROM CharacterTalent c"),
    @NamedQuery(name = "CharacterTalent.findByGuid", query = "SELECT c FROM CharacterTalent c WHERE c.characterTalentPK.guid = :guid"),
    @NamedQuery(name = "CharacterTalent.findBySpell", query = "SELECT c FROM CharacterTalent c WHERE c.characterTalentPK.spell = :spell"),
    @NamedQuery(name = "CharacterTalent.findBySpec", query = "SELECT c FROM CharacterTalent c WHERE c.characterTalentPK.spec = :spec")})
public class CharacterTalent implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterTalentPK characterTalentPK;

    public CharacterTalent() {
    }

    public CharacterTalent(CharacterTalentPK characterTalentPK) {
        this.characterTalentPK = characterTalentPK;
    }

    public CharacterTalent(int guid, int spell, short spec) {
        this.characterTalentPK = new CharacterTalentPK(guid, spell, spec);
    }

    public CharacterTalentPK getCharacterTalentPK() {
        return characterTalentPK;
    }

    public void setCharacterTalentPK(CharacterTalentPK characterTalentPK) {
        this.characterTalentPK = characterTalentPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterTalentPK != null ? characterTalentPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterTalent)) {
            return false;
        }
        CharacterTalent other = (CharacterTalent) object;
        if ((this.characterTalentPK == null && other.characterTalentPK != null) || (this.characterTalentPK != null && !this.characterTalentPK.equals(other.characterTalentPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterTalent[ characterTalentPK=" + characterTalentPK + " ]";
    }
    
}
