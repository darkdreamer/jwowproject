/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "guild_bank_eventlog")
@NamedQueries({
    @NamedQuery(name = "GuildBankEventlog.findAll", query = "SELECT g FROM GuildBankEventlog g"),
    @NamedQuery(name = "GuildBankEventlog.findByGuildid", query = "SELECT g FROM GuildBankEventlog g WHERE g.guildBankEventlogPK.guildid = :guildid"),
    @NamedQuery(name = "GuildBankEventlog.findByLogGuid", query = "SELECT g FROM GuildBankEventlog g WHERE g.guildBankEventlogPK.logGuid = :logGuid"),
    @NamedQuery(name = "GuildBankEventlog.findByTabId", query = "SELECT g FROM GuildBankEventlog g WHERE g.guildBankEventlogPK.tabId = :tabId"),
    @NamedQuery(name = "GuildBankEventlog.findByEventType", query = "SELECT g FROM GuildBankEventlog g WHERE g.eventType = :eventType"),
    @NamedQuery(name = "GuildBankEventlog.findByPlayerGuid", query = "SELECT g FROM GuildBankEventlog g WHERE g.playerGuid = :playerGuid"),
    @NamedQuery(name = "GuildBankEventlog.findByItemOrMoney", query = "SELECT g FROM GuildBankEventlog g WHERE g.itemOrMoney = :itemOrMoney"),
    @NamedQuery(name = "GuildBankEventlog.findByItemStackCount", query = "SELECT g FROM GuildBankEventlog g WHERE g.itemStackCount = :itemStackCount"),
    @NamedQuery(name = "GuildBankEventlog.findByDestTabId", query = "SELECT g FROM GuildBankEventlog g WHERE g.destTabId = :destTabId"),
    @NamedQuery(name = "GuildBankEventlog.findByTimeStamp", query = "SELECT g FROM GuildBankEventlog g WHERE g.timeStamp = :timeStamp")})
public class GuildBankEventlog implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GuildBankEventlogPK guildBankEventlogPK;
    @Basic(optional = false)
    @Column(name = "EventType")
    private short eventType;
    @Basic(optional = false)
    @Column(name = "PlayerGuid")
    private int playerGuid;
    @Basic(optional = false)
    @Column(name = "ItemOrMoney")
    private int itemOrMoney;
    @Basic(optional = false)
    @Column(name = "ItemStackCount")
    private short itemStackCount;
    @Basic(optional = false)
    @Column(name = "DestTabId")
    private short destTabId;
    @Basic(optional = false)
    @Column(name = "TimeStamp")
    private int timeStamp;

    public GuildBankEventlog() {
    }

    public GuildBankEventlog(GuildBankEventlogPK guildBankEventlogPK) {
        this.guildBankEventlogPK = guildBankEventlogPK;
    }

    public GuildBankEventlog(GuildBankEventlogPK guildBankEventlogPK, short eventType, int playerGuid, int itemOrMoney, short itemStackCount, short destTabId, int timeStamp) {
        this.guildBankEventlogPK = guildBankEventlogPK;
        this.eventType = eventType;
        this.playerGuid = playerGuid;
        this.itemOrMoney = itemOrMoney;
        this.itemStackCount = itemStackCount;
        this.destTabId = destTabId;
        this.timeStamp = timeStamp;
    }

    public GuildBankEventlog(int guildid, int logGuid, short tabId) {
        this.guildBankEventlogPK = new GuildBankEventlogPK(guildid, logGuid, tabId);
    }

    public GuildBankEventlogPK getGuildBankEventlogPK() {
        return guildBankEventlogPK;
    }

    public void setGuildBankEventlogPK(GuildBankEventlogPK guildBankEventlogPK) {
        this.guildBankEventlogPK = guildBankEventlogPK;
    }

    public short getEventType() {
        return eventType;
    }

    public void setEventType(short eventType) {
        this.eventType = eventType;
    }

    public int getPlayerGuid() {
        return playerGuid;
    }

    public void setPlayerGuid(int playerGuid) {
        this.playerGuid = playerGuid;
    }

    public int getItemOrMoney() {
        return itemOrMoney;
    }

    public void setItemOrMoney(int itemOrMoney) {
        this.itemOrMoney = itemOrMoney;
    }

    public short getItemStackCount() {
        return itemStackCount;
    }

    public void setItemStackCount(short itemStackCount) {
        this.itemStackCount = itemStackCount;
    }

    public short getDestTabId() {
        return destTabId;
    }

    public void setDestTabId(short destTabId) {
        this.destTabId = destTabId;
    }

    public int getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(int timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guildBankEventlogPK != null ? guildBankEventlogPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuildBankEventlog)) {
            return false;
        }
        GuildBankEventlog other = (GuildBankEventlog) object;
        if ((this.guildBankEventlogPK == null && other.guildBankEventlogPK != null) || (this.guildBankEventlogPK != null && !this.guildBankEventlogPK.equals(other.guildBankEventlogPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GuildBankEventlog[ guildBankEventlogPK=" + guildBankEventlogPK + " ]";
    }
    
}
