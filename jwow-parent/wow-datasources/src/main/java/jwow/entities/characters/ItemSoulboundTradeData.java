/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "item_soulbound_trade_data")
@NamedQueries({
    @NamedQuery(name = "ItemSoulboundTradeData.findAll", query = "SELECT i FROM ItemSoulboundTradeData i"),
    @NamedQuery(name = "ItemSoulboundTradeData.findByItemGuid", query = "SELECT i FROM ItemSoulboundTradeData i WHERE i.itemGuid = :itemGuid")})
public class ItemSoulboundTradeData implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "itemGuid")
    private Integer itemGuid;
    @Basic(optional = false)
    @Lob
    @Column(name = "allowedPlayers")
    private String allowedPlayers;

    public ItemSoulboundTradeData() {
    }

    public ItemSoulboundTradeData(Integer itemGuid) {
        this.itemGuid = itemGuid;
    }

    public ItemSoulboundTradeData(Integer itemGuid, String allowedPlayers) {
        this.itemGuid = itemGuid;
        this.allowedPlayers = allowedPlayers;
    }

    public Integer getItemGuid() {
        return itemGuid;
    }

    public void setItemGuid(Integer itemGuid) {
        this.itemGuid = itemGuid;
    }

    public String getAllowedPlayers() {
        return allowedPlayers;
    }

    public void setAllowedPlayers(String allowedPlayers) {
        this.allowedPlayers = allowedPlayers;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemGuid != null ? itemGuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemSoulboundTradeData)) {
            return false;
        }
        ItemSoulboundTradeData other = (ItemSoulboundTradeData) object;
        if ((this.itemGuid == null && other.itemGuid != null) || (this.itemGuid != null && !this.itemGuid.equals(other.itemGuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.ItemSoulboundTradeData[ itemGuid=" + itemGuid + " ]";
    }
    
}
