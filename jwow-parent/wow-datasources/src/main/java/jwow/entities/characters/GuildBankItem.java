/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "guild_bank_item")
@NamedQueries({
    @NamedQuery(name = "GuildBankItem.findAll", query = "SELECT g FROM GuildBankItem g"),
    @NamedQuery(name = "GuildBankItem.findByGuildid", query = "SELECT g FROM GuildBankItem g WHERE g.guildBankItemPK.guildid = :guildid"),
    @NamedQuery(name = "GuildBankItem.findByTabId", query = "SELECT g FROM GuildBankItem g WHERE g.guildBankItemPK.tabId = :tabId"),
    @NamedQuery(name = "GuildBankItem.findBySlotId", query = "SELECT g FROM GuildBankItem g WHERE g.guildBankItemPK.slotId = :slotId"),
    @NamedQuery(name = "GuildBankItem.findByItemGuid", query = "SELECT g FROM GuildBankItem g WHERE g.itemGuid = :itemGuid")})
public class GuildBankItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GuildBankItemPK guildBankItemPK;
    @Basic(optional = false)
    @Column(name = "item_guid")
    private int itemGuid;

    public GuildBankItem() {
    }

    public GuildBankItem(GuildBankItemPK guildBankItemPK) {
        this.guildBankItemPK = guildBankItemPK;
    }

    public GuildBankItem(GuildBankItemPK guildBankItemPK, int itemGuid) {
        this.guildBankItemPK = guildBankItemPK;
        this.itemGuid = itemGuid;
    }

    public GuildBankItem(int guildid, short tabId, short slotId) {
        this.guildBankItemPK = new GuildBankItemPK(guildid, tabId, slotId);
    }

    public GuildBankItemPK getGuildBankItemPK() {
        return guildBankItemPK;
    }

    public void setGuildBankItemPK(GuildBankItemPK guildBankItemPK) {
        this.guildBankItemPK = guildBankItemPK;
    }

    public int getItemGuid() {
        return itemGuid;
    }

    public void setItemGuid(int itemGuid) {
        this.itemGuid = itemGuid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guildBankItemPK != null ? guildBankItemPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuildBankItem)) {
            return false;
        }
        GuildBankItem other = (GuildBankItem) object;
        if ((this.guildBankItemPK == null && other.guildBankItemPK != null) || (this.guildBankItemPK != null && !this.guildBankItemPK.equals(other.guildBankItemPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GuildBankItem[ guildBankItemPK=" + guildBankItemPK + " ]";
    }
    
}
