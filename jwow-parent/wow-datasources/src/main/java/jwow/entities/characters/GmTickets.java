/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "gm_tickets")
@NamedQueries({
    @NamedQuery(name = "GmTickets.findAll", query = "SELECT g FROM GmTickets g"),
    @NamedQuery(name = "GmTickets.findByTicketId", query = "SELECT g FROM GmTickets g WHERE g.ticketId = :ticketId"),
    @NamedQuery(name = "GmTickets.findByGuid", query = "SELECT g FROM GmTickets g WHERE g.guid = :guid"),
    @NamedQuery(name = "GmTickets.findByName", query = "SELECT g FROM GmTickets g WHERE g.name = :name"),
    @NamedQuery(name = "GmTickets.findByCreateTime", query = "SELECT g FROM GmTickets g WHERE g.createTime = :createTime"),
    @NamedQuery(name = "GmTickets.findByMapId", query = "SELECT g FROM GmTickets g WHERE g.mapId = :mapId"),
    @NamedQuery(name = "GmTickets.findByPosX", query = "SELECT g FROM GmTickets g WHERE g.posX = :posX"),
    @NamedQuery(name = "GmTickets.findByPosY", query = "SELECT g FROM GmTickets g WHERE g.posY = :posY"),
    @NamedQuery(name = "GmTickets.findByPosZ", query = "SELECT g FROM GmTickets g WHERE g.posZ = :posZ"),
    @NamedQuery(name = "GmTickets.findByLastModifiedTime", query = "SELECT g FROM GmTickets g WHERE g.lastModifiedTime = :lastModifiedTime"),
    @NamedQuery(name = "GmTickets.findByClosedBy", query = "SELECT g FROM GmTickets g WHERE g.closedBy = :closedBy"),
    @NamedQuery(name = "GmTickets.findByAssignedTo", query = "SELECT g FROM GmTickets g WHERE g.assignedTo = :assignedTo"),
    @NamedQuery(name = "GmTickets.findByCompleted", query = "SELECT g FROM GmTickets g WHERE g.completed = :completed"),
    @NamedQuery(name = "GmTickets.findByEscalated", query = "SELECT g FROM GmTickets g WHERE g.escalated = :escalated"),
    @NamedQuery(name = "GmTickets.findByViewed", query = "SELECT g FROM GmTickets g WHERE g.viewed = :viewed"),
    @NamedQuery(name = "GmTickets.findByHaveTicket", query = "SELECT g FROM GmTickets g WHERE g.haveTicket = :haveTicket")})
public class GmTickets implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ticketId")
    private Integer ticketId;
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Lob
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @Column(name = "createTime")
    private int createTime;
    @Basic(optional = false)
    @Column(name = "mapId")
    private short mapId;
    @Basic(optional = false)
    @Column(name = "posX")
    private float posX;
    @Basic(optional = false)
    @Column(name = "posY")
    private float posY;
    @Basic(optional = false)
    @Column(name = "posZ")
    private float posZ;
    @Basic(optional = false)
    @Column(name = "lastModifiedTime")
    private int lastModifiedTime;
    @Basic(optional = false)
    @Column(name = "closedBy")
    private int closedBy;
    @Basic(optional = false)
    @Column(name = "assignedTo")
    private int assignedTo;
    @Basic(optional = false)
    @Lob
    @Column(name = "comment")
    private String comment;
    @Basic(optional = false)
    @Lob
    @Column(name = "response")
    private String response;
    @Basic(optional = false)
    @Column(name = "completed")
    private short completed;
    @Basic(optional = false)
    @Column(name = "escalated")
    private short escalated;
    @Basic(optional = false)
    @Column(name = "viewed")
    private short viewed;
    @Basic(optional = false)
    @Column(name = "haveTicket")
    private short haveTicket;

    public GmTickets() {
    }

    public GmTickets(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public GmTickets(Integer ticketId, int guid, String name, String message, int createTime, short mapId, float posX, float posY, float posZ, int lastModifiedTime, int closedBy, int assignedTo, String comment, String response, short completed, short escalated, short viewed, short haveTicket) {
        this.ticketId = ticketId;
        this.guid = guid;
        this.name = name;
        this.message = message;
        this.createTime = createTime;
        this.mapId = mapId;
        this.posX = posX;
        this.posY = posY;
        this.posZ = posZ;
        this.lastModifiedTime = lastModifiedTime;
        this.closedBy = closedBy;
        this.assignedTo = assignedTo;
        this.comment = comment;
        this.response = response;
        this.completed = completed;
        this.escalated = escalated;
        this.viewed = viewed;
        this.haveTicket = haveTicket;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCreateTime() {
        return createTime;
    }

    public void setCreateTime(int createTime) {
        this.createTime = createTime;
    }

    public short getMapId() {
        return mapId;
    }

    public void setMapId(short mapId) {
        this.mapId = mapId;
    }

    public float getPosX() {
        return posX;
    }

    public void setPosX(float posX) {
        this.posX = posX;
    }

    public float getPosY() {
        return posY;
    }

    public void setPosY(float posY) {
        this.posY = posY;
    }

    public float getPosZ() {
        return posZ;
    }

    public void setPosZ(float posZ) {
        this.posZ = posZ;
    }

    public int getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(int lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    public int getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(int closedBy) {
        this.closedBy = closedBy;
    }

    public int getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(int assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public short getCompleted() {
        return completed;
    }

    public void setCompleted(short completed) {
        this.completed = completed;
    }

    public short getEscalated() {
        return escalated;
    }

    public void setEscalated(short escalated) {
        this.escalated = escalated;
    }

    public short getViewed() {
        return viewed;
    }

    public void setViewed(short viewed) {
        this.viewed = viewed;
    }

    public short getHaveTicket() {
        return haveTicket;
    }

    public void setHaveTicket(short haveTicket) {
        this.haveTicket = haveTicket;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ticketId != null ? ticketId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GmTickets)) {
            return false;
        }
        GmTickets other = (GmTickets) object;
        if ((this.ticketId == null && other.ticketId != null) || (this.ticketId != null && !this.ticketId.equals(other.ticketId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GmTickets[ ticketId=" + ticketId + " ]";
    }
    
}
