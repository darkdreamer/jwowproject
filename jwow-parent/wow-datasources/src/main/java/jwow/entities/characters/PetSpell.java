/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "pet_spell")
@NamedQueries({
    @NamedQuery(name = "PetSpell.findAll", query = "SELECT p FROM PetSpell p"),
    @NamedQuery(name = "PetSpell.findByGuid", query = "SELECT p FROM PetSpell p WHERE p.petSpellPK.guid = :guid"),
    @NamedQuery(name = "PetSpell.findBySpell", query = "SELECT p FROM PetSpell p WHERE p.petSpellPK.spell = :spell"),
    @NamedQuery(name = "PetSpell.findByActive", query = "SELECT p FROM PetSpell p WHERE p.active = :active")})
public class PetSpell implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PetSpellPK petSpellPK;
    @Basic(optional = false)
    @Column(name = "active")
    private short active;

    public PetSpell() {
    }

    public PetSpell(PetSpellPK petSpellPK) {
        this.petSpellPK = petSpellPK;
    }

    public PetSpell(PetSpellPK petSpellPK, short active) {
        this.petSpellPK = petSpellPK;
        this.active = active;
    }

    public PetSpell(int guid, int spell) {
        this.petSpellPK = new PetSpellPK(guid, spell);
    }

    public PetSpellPK getPetSpellPK() {
        return petSpellPK;
    }

    public void setPetSpellPK(PetSpellPK petSpellPK) {
        this.petSpellPK = petSpellPK;
    }

    public short getActive() {
        return active;
    }

    public void setActive(short active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (petSpellPK != null ? petSpellPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PetSpell)) {
            return false;
        }
        PetSpell other = (PetSpell) object;
        if ((this.petSpellPK == null && other.petSpellPK != null) || (this.petSpellPK != null && !this.petSpellPK.equals(other.petSpellPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.PetSpell[ petSpellPK=" + petSpellPK + " ]";
    }
    
}
