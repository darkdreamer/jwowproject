/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "mail_items")
@NamedQueries({
    @NamedQuery(name = "MailItems.findAll", query = "SELECT m FROM MailItems m"),
    @NamedQuery(name = "MailItems.findByMailId", query = "SELECT m FROM MailItems m WHERE m.mailId = :mailId"),
    @NamedQuery(name = "MailItems.findByItemGuid", query = "SELECT m FROM MailItems m WHERE m.itemGuid = :itemGuid"),
    @NamedQuery(name = "MailItems.findByReceiver", query = "SELECT m FROM MailItems m WHERE m.receiver = :receiver")})
public class MailItems implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "mail_id")
    private int mailId;
    @Id
    @Basic(optional = false)
    @Column(name = "item_guid")
    private Integer itemGuid;
    @Basic(optional = false)
    @Column(name = "receiver")
    private int receiver;

    public MailItems() {
    }

    public MailItems(Integer itemGuid) {
        this.itemGuid = itemGuid;
    }

    public MailItems(Integer itemGuid, int mailId, int receiver) {
        this.itemGuid = itemGuid;
        this.mailId = mailId;
        this.receiver = receiver;
    }

    public int getMailId() {
        return mailId;
    }

    public void setMailId(int mailId) {
        this.mailId = mailId;
    }

    public Integer getItemGuid() {
        return itemGuid;
    }

    public void setItemGuid(Integer itemGuid) {
        this.itemGuid = itemGuid;
    }

    public int getReceiver() {
        return receiver;
    }

    public void setReceiver(int receiver) {
        this.receiver = receiver;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemGuid != null ? itemGuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MailItems)) {
            return false;
        }
        MailItems other = (MailItems) object;
        if ((this.itemGuid == null && other.itemGuid != null) || (this.itemGuid != null && !this.itemGuid.equals(other.itemGuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.MailItems[ itemGuid=" + itemGuid + " ]";
    }
    
}
