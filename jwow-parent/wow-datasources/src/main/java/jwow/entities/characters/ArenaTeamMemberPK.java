/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class ArenaTeamMemberPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "arenaTeamId")
    private int arenaTeamId;
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;

    public ArenaTeamMemberPK() {
    }

    public ArenaTeamMemberPK(int arenaTeamId, int guid) {
        this.arenaTeamId = arenaTeamId;
        this.guid = guid;
    }

    public int getArenaTeamId() {
        return arenaTeamId;
    }

    public void setArenaTeamId(int arenaTeamId) {
        this.arenaTeamId = arenaTeamId;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) arenaTeamId;
        hash += (int) guid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArenaTeamMemberPK)) {
            return false;
        }
        ArenaTeamMemberPK other = (ArenaTeamMemberPK) object;
        if (this.arenaTeamId != other.arenaTeamId) {
            return false;
        }
        if (this.guid != other.guid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.ArenaTeamMemberPK[ arenaTeamId=" + arenaTeamId + ", guid=" + guid + " ]";
    }
    
}
