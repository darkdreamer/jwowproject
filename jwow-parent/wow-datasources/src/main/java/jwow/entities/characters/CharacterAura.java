/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_aura")
@NamedQueries({
    @NamedQuery(name = "CharacterAura.findAll", query = "SELECT c FROM CharacterAura c"),
    @NamedQuery(name = "CharacterAura.findByGuid", query = "SELECT c FROM CharacterAura c WHERE c.characterAuraPK.guid = :guid"),
    @NamedQuery(name = "CharacterAura.findByCasterGuid", query = "SELECT c FROM CharacterAura c WHERE c.characterAuraPK.casterGuid = :casterGuid"),
    @NamedQuery(name = "CharacterAura.findByItemGuid", query = "SELECT c FROM CharacterAura c WHERE c.characterAuraPK.itemGuid = :itemGuid"),
    @NamedQuery(name = "CharacterAura.findBySpell", query = "SELECT c FROM CharacterAura c WHERE c.characterAuraPK.spell = :spell"),
    @NamedQuery(name = "CharacterAura.findByEffectMask", query = "SELECT c FROM CharacterAura c WHERE c.characterAuraPK.effectMask = :effectMask"),
    @NamedQuery(name = "CharacterAura.findByRecalculateMask", query = "SELECT c FROM CharacterAura c WHERE c.recalculateMask = :recalculateMask"),
    @NamedQuery(name = "CharacterAura.findByStackcount", query = "SELECT c FROM CharacterAura c WHERE c.stackcount = :stackcount"),
    @NamedQuery(name = "CharacterAura.findByAmount0", query = "SELECT c FROM CharacterAura c WHERE c.amount0 = :amount0"),
    @NamedQuery(name = "CharacterAura.findByAmount1", query = "SELECT c FROM CharacterAura c WHERE c.amount1 = :amount1"),
    @NamedQuery(name = "CharacterAura.findByAmount2", query = "SELECT c FROM CharacterAura c WHERE c.amount2 = :amount2"),
    @NamedQuery(name = "CharacterAura.findByBaseAmount0", query = "SELECT c FROM CharacterAura c WHERE c.baseAmount0 = :baseAmount0"),
    @NamedQuery(name = "CharacterAura.findByBaseAmount1", query = "SELECT c FROM CharacterAura c WHERE c.baseAmount1 = :baseAmount1"),
    @NamedQuery(name = "CharacterAura.findByBaseAmount2", query = "SELECT c FROM CharacterAura c WHERE c.baseAmount2 = :baseAmount2"),
    @NamedQuery(name = "CharacterAura.findByMaxduration", query = "SELECT c FROM CharacterAura c WHERE c.maxduration = :maxduration"),
    @NamedQuery(name = "CharacterAura.findByRemaintime", query = "SELECT c FROM CharacterAura c WHERE c.remaintime = :remaintime"),
    @NamedQuery(name = "CharacterAura.findByRemaincharges", query = "SELECT c FROM CharacterAura c WHERE c.remaincharges = :remaincharges")})
public class CharacterAura implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterAuraPK characterAuraPK;
    @Basic(optional = false)
    @Column(name = "recalculate_mask")
    private short recalculateMask;
    @Basic(optional = false)
    @Column(name = "stackcount")
    private short stackcount;
    @Basic(optional = false)
    @Column(name = "amount0")
    private int amount0;
    @Basic(optional = false)
    @Column(name = "amount1")
    private int amount1;
    @Basic(optional = false)
    @Column(name = "amount2")
    private int amount2;
    @Basic(optional = false)
    @Column(name = "base_amount0")
    private int baseAmount0;
    @Basic(optional = false)
    @Column(name = "base_amount1")
    private int baseAmount1;
    @Basic(optional = false)
    @Column(name = "base_amount2")
    private int baseAmount2;
    @Basic(optional = false)
    @Column(name = "maxduration")
    private int maxduration;
    @Basic(optional = false)
    @Column(name = "remaintime")
    private int remaintime;
    @Basic(optional = false)
    @Column(name = "remaincharges")
    private short remaincharges;

    public CharacterAura() {
    }

    public CharacterAura(CharacterAuraPK characterAuraPK) {
        this.characterAuraPK = characterAuraPK;
    }

    public CharacterAura(CharacterAuraPK characterAuraPK, short recalculateMask, short stackcount, int amount0, int amount1, int amount2, int baseAmount0, int baseAmount1, int baseAmount2, int maxduration, int remaintime, short remaincharges) {
        this.characterAuraPK = characterAuraPK;
        this.recalculateMask = recalculateMask;
        this.stackcount = stackcount;
        this.amount0 = amount0;
        this.amount1 = amount1;
        this.amount2 = amount2;
        this.baseAmount0 = baseAmount0;
        this.baseAmount1 = baseAmount1;
        this.baseAmount2 = baseAmount2;
        this.maxduration = maxduration;
        this.remaintime = remaintime;
        this.remaincharges = remaincharges;
    }

    public CharacterAura(int guid, long casterGuid, long itemGuid, int spell, short effectMask) {
        this.characterAuraPK = new CharacterAuraPK(guid, casterGuid, itemGuid, spell, effectMask);
    }

    public CharacterAuraPK getCharacterAuraPK() {
        return characterAuraPK;
    }

    public void setCharacterAuraPK(CharacterAuraPK characterAuraPK) {
        this.characterAuraPK = characterAuraPK;
    }

    public short getRecalculateMask() {
        return recalculateMask;
    }

    public void setRecalculateMask(short recalculateMask) {
        this.recalculateMask = recalculateMask;
    }

    public short getStackcount() {
        return stackcount;
    }

    public void setStackcount(short stackcount) {
        this.stackcount = stackcount;
    }

    public int getAmount0() {
        return amount0;
    }

    public void setAmount0(int amount0) {
        this.amount0 = amount0;
    }

    public int getAmount1() {
        return amount1;
    }

    public void setAmount1(int amount1) {
        this.amount1 = amount1;
    }

    public int getAmount2() {
        return amount2;
    }

    public void setAmount2(int amount2) {
        this.amount2 = amount2;
    }

    public int getBaseAmount0() {
        return baseAmount0;
    }

    public void setBaseAmount0(int baseAmount0) {
        this.baseAmount0 = baseAmount0;
    }

    public int getBaseAmount1() {
        return baseAmount1;
    }

    public void setBaseAmount1(int baseAmount1) {
        this.baseAmount1 = baseAmount1;
    }

    public int getBaseAmount2() {
        return baseAmount2;
    }

    public void setBaseAmount2(int baseAmount2) {
        this.baseAmount2 = baseAmount2;
    }

    public int getMaxduration() {
        return maxduration;
    }

    public void setMaxduration(int maxduration) {
        this.maxduration = maxduration;
    }

    public int getRemaintime() {
        return remaintime;
    }

    public void setRemaintime(int remaintime) {
        this.remaintime = remaintime;
    }

    public short getRemaincharges() {
        return remaincharges;
    }

    public void setRemaincharges(short remaincharges) {
        this.remaincharges = remaincharges;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterAuraPK != null ? characterAuraPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterAura)) {
            return false;
        }
        CharacterAura other = (CharacterAura) object;
        if ((this.characterAuraPK == null && other.characterAuraPK != null) || (this.characterAuraPK != null && !this.characterAuraPK.equals(other.characterAuraPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterAura[ characterAuraPK=" + characterAuraPK + " ]";
    }
    
}
