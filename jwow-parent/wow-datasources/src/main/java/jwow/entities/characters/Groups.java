/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "groups")
@NamedQueries({
    @NamedQuery(name = "Groups.findAll", query = "SELECT g FROM Groups g"),
    @NamedQuery(name = "Groups.findByGuid", query = "SELECT g FROM Groups g WHERE g.guid = :guid"),
    @NamedQuery(name = "Groups.findByLeaderGuid", query = "SELECT g FROM Groups g WHERE g.leaderGuid = :leaderGuid"),
    @NamedQuery(name = "Groups.findByLootMethod", query = "SELECT g FROM Groups g WHERE g.lootMethod = :lootMethod"),
    @NamedQuery(name = "Groups.findByLooterGuid", query = "SELECT g FROM Groups g WHERE g.looterGuid = :looterGuid"),
    @NamedQuery(name = "Groups.findByLootThreshold", query = "SELECT g FROM Groups g WHERE g.lootThreshold = :lootThreshold"),
    @NamedQuery(name = "Groups.findByIcon1", query = "SELECT g FROM Groups g WHERE g.icon1 = :icon1"),
    @NamedQuery(name = "Groups.findByIcon2", query = "SELECT g FROM Groups g WHERE g.icon2 = :icon2"),
    @NamedQuery(name = "Groups.findByIcon3", query = "SELECT g FROM Groups g WHERE g.icon3 = :icon3"),
    @NamedQuery(name = "Groups.findByIcon4", query = "SELECT g FROM Groups g WHERE g.icon4 = :icon4"),
    @NamedQuery(name = "Groups.findByIcon5", query = "SELECT g FROM Groups g WHERE g.icon5 = :icon5"),
    @NamedQuery(name = "Groups.findByIcon6", query = "SELECT g FROM Groups g WHERE g.icon6 = :icon6"),
    @NamedQuery(name = "Groups.findByIcon7", query = "SELECT g FROM Groups g WHERE g.icon7 = :icon7"),
    @NamedQuery(name = "Groups.findByIcon8", query = "SELECT g FROM Groups g WHERE g.icon8 = :icon8"),
    @NamedQuery(name = "Groups.findByGroupType", query = "SELECT g FROM Groups g WHERE g.groupType = :groupType"),
    @NamedQuery(name = "Groups.findByDifficulty", query = "SELECT g FROM Groups g WHERE g.difficulty = :difficulty"),
    @NamedQuery(name = "Groups.findByRaiddifficulty", query = "SELECT g FROM Groups g WHERE g.raiddifficulty = :raiddifficulty")})
public class Groups implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "guid")
    private Integer guid;
    @Basic(optional = false)
    @Column(name = "leaderGuid")
    private int leaderGuid;
    @Basic(optional = false)
    @Column(name = "lootMethod")
    private short lootMethod;
    @Basic(optional = false)
    @Column(name = "looterGuid")
    private int looterGuid;
    @Basic(optional = false)
    @Column(name = "lootThreshold")
    private short lootThreshold;
    @Basic(optional = false)
    @Column(name = "icon1")
    private int icon1;
    @Basic(optional = false)
    @Column(name = "icon2")
    private int icon2;
    @Basic(optional = false)
    @Column(name = "icon3")
    private int icon3;
    @Basic(optional = false)
    @Column(name = "icon4")
    private int icon4;
    @Basic(optional = false)
    @Column(name = "icon5")
    private int icon5;
    @Basic(optional = false)
    @Column(name = "icon6")
    private int icon6;
    @Basic(optional = false)
    @Column(name = "icon7")
    private int icon7;
    @Basic(optional = false)
    @Column(name = "icon8")
    private int icon8;
    @Basic(optional = false)
    @Column(name = "groupType")
    private short groupType;
    @Basic(optional = false)
    @Column(name = "difficulty")
    private short difficulty;
    @Basic(optional = false)
    @Column(name = "raiddifficulty")
    private short raiddifficulty;

    public Groups() {
    }

    public Groups(Integer guid) {
        this.guid = guid;
    }

    public Groups(Integer guid, int leaderGuid, short lootMethod, int looterGuid, short lootThreshold, int icon1, int icon2, int icon3, int icon4, int icon5, int icon6, int icon7, int icon8, short groupType, short difficulty, short raiddifficulty) {
        this.guid = guid;
        this.leaderGuid = leaderGuid;
        this.lootMethod = lootMethod;
        this.looterGuid = looterGuid;
        this.lootThreshold = lootThreshold;
        this.icon1 = icon1;
        this.icon2 = icon2;
        this.icon3 = icon3;
        this.icon4 = icon4;
        this.icon5 = icon5;
        this.icon6 = icon6;
        this.icon7 = icon7;
        this.icon8 = icon8;
        this.groupType = groupType;
        this.difficulty = difficulty;
        this.raiddifficulty = raiddifficulty;
    }

    public Integer getGuid() {
        return guid;
    }

    public void setGuid(Integer guid) {
        this.guid = guid;
    }

    public int getLeaderGuid() {
        return leaderGuid;
    }

    public void setLeaderGuid(int leaderGuid) {
        this.leaderGuid = leaderGuid;
    }

    public short getLootMethod() {
        return lootMethod;
    }

    public void setLootMethod(short lootMethod) {
        this.lootMethod = lootMethod;
    }

    public int getLooterGuid() {
        return looterGuid;
    }

    public void setLooterGuid(int looterGuid) {
        this.looterGuid = looterGuid;
    }

    public short getLootThreshold() {
        return lootThreshold;
    }

    public void setLootThreshold(short lootThreshold) {
        this.lootThreshold = lootThreshold;
    }

    public int getIcon1() {
        return icon1;
    }

    public void setIcon1(int icon1) {
        this.icon1 = icon1;
    }

    public int getIcon2() {
        return icon2;
    }

    public void setIcon2(int icon2) {
        this.icon2 = icon2;
    }

    public int getIcon3() {
        return icon3;
    }

    public void setIcon3(int icon3) {
        this.icon3 = icon3;
    }

    public int getIcon4() {
        return icon4;
    }

    public void setIcon4(int icon4) {
        this.icon4 = icon4;
    }

    public int getIcon5() {
        return icon5;
    }

    public void setIcon5(int icon5) {
        this.icon5 = icon5;
    }

    public int getIcon6() {
        return icon6;
    }

    public void setIcon6(int icon6) {
        this.icon6 = icon6;
    }

    public int getIcon7() {
        return icon7;
    }

    public void setIcon7(int icon7) {
        this.icon7 = icon7;
    }

    public int getIcon8() {
        return icon8;
    }

    public void setIcon8(int icon8) {
        this.icon8 = icon8;
    }

    public short getGroupType() {
        return groupType;
    }

    public void setGroupType(short groupType) {
        this.groupType = groupType;
    }

    public short getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(short difficulty) {
        this.difficulty = difficulty;
    }

    public short getRaiddifficulty() {
        return raiddifficulty;
    }

    public void setRaiddifficulty(short raiddifficulty) {
        this.raiddifficulty = raiddifficulty;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guid != null ? guid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Groups)) {
            return false;
        }
        Groups other = (Groups) object;
        if ((this.guid == null && other.guid != null) || (this.guid != null && !this.guid.equals(other.guid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.Groups[ guid=" + guid + " ]";
    }
    
}
