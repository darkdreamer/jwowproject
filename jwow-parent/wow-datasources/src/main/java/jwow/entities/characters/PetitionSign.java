/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "petition_sign")
@NamedQueries({
    @NamedQuery(name = "PetitionSign.findAll", query = "SELECT p FROM PetitionSign p"),
    @NamedQuery(name = "PetitionSign.findByOwnerguid", query = "SELECT p FROM PetitionSign p WHERE p.ownerguid = :ownerguid"),
    @NamedQuery(name = "PetitionSign.findByPetitionguid", query = "SELECT p FROM PetitionSign p WHERE p.petitionSignPK.petitionguid = :petitionguid"),
    @NamedQuery(name = "PetitionSign.findByPlayerguid", query = "SELECT p FROM PetitionSign p WHERE p.petitionSignPK.playerguid = :playerguid"),
    @NamedQuery(name = "PetitionSign.findByPlayerAccount", query = "SELECT p FROM PetitionSign p WHERE p.playerAccount = :playerAccount"),
    @NamedQuery(name = "PetitionSign.findByType", query = "SELECT p FROM PetitionSign p WHERE p.type = :type")})
public class PetitionSign implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PetitionSignPK petitionSignPK;
    @Basic(optional = false)
    @Column(name = "ownerguid")
    private int ownerguid;
    @Basic(optional = false)
    @Column(name = "player_account")
    private int playerAccount;
    @Basic(optional = false)
    @Column(name = "type")
    private short type;

    public PetitionSign() {
    }

    public PetitionSign(PetitionSignPK petitionSignPK) {
        this.petitionSignPK = petitionSignPK;
    }

    public PetitionSign(PetitionSignPK petitionSignPK, int ownerguid, int playerAccount, short type) {
        this.petitionSignPK = petitionSignPK;
        this.ownerguid = ownerguid;
        this.playerAccount = playerAccount;
        this.type = type;
    }

    public PetitionSign(int petitionguid, int playerguid) {
        this.petitionSignPK = new PetitionSignPK(petitionguid, playerguid);
    }

    public PetitionSignPK getPetitionSignPK() {
        return petitionSignPK;
    }

    public void setPetitionSignPK(PetitionSignPK petitionSignPK) {
        this.petitionSignPK = petitionSignPK;
    }

    public int getOwnerguid() {
        return ownerguid;
    }

    public void setOwnerguid(int ownerguid) {
        this.ownerguid = ownerguid;
    }

    public int getPlayerAccount() {
        return playerAccount;
    }

    public void setPlayerAccount(int playerAccount) {
        this.playerAccount = playerAccount;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (petitionSignPK != null ? petitionSignPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PetitionSign)) {
            return false;
        }
        PetitionSign other = (PetitionSign) object;
        if ((this.petitionSignPK == null && other.petitionSignPK != null) || (this.petitionSignPK != null && !this.petitionSignPK.equals(other.petitionSignPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.PetitionSign[ petitionSignPK=" + petitionSignPK + " ]";
    }
    
}
