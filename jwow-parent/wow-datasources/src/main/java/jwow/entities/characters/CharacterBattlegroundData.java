/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_battleground_data")
@NamedQueries({
    @NamedQuery(name = "CharacterBattlegroundData.findAll", query = "SELECT c FROM CharacterBattlegroundData c"),
    @NamedQuery(name = "CharacterBattlegroundData.findByGuid", query = "SELECT c FROM CharacterBattlegroundData c WHERE c.guid = :guid"),
    @NamedQuery(name = "CharacterBattlegroundData.findByInstanceId", query = "SELECT c FROM CharacterBattlegroundData c WHERE c.instanceId = :instanceId"),
    @NamedQuery(name = "CharacterBattlegroundData.findByTeam", query = "SELECT c FROM CharacterBattlegroundData c WHERE c.team = :team"),
    @NamedQuery(name = "CharacterBattlegroundData.findByJoinX", query = "SELECT c FROM CharacterBattlegroundData c WHERE c.joinX = :joinX"),
    @NamedQuery(name = "CharacterBattlegroundData.findByJoinY", query = "SELECT c FROM CharacterBattlegroundData c WHERE c.joinY = :joinY"),
    @NamedQuery(name = "CharacterBattlegroundData.findByJoinZ", query = "SELECT c FROM CharacterBattlegroundData c WHERE c.joinZ = :joinZ"),
    @NamedQuery(name = "CharacterBattlegroundData.findByJoinO", query = "SELECT c FROM CharacterBattlegroundData c WHERE c.joinO = :joinO"),
    @NamedQuery(name = "CharacterBattlegroundData.findByJoinMapId", query = "SELECT c FROM CharacterBattlegroundData c WHERE c.joinMapId = :joinMapId"),
    @NamedQuery(name = "CharacterBattlegroundData.findByTaxiStart", query = "SELECT c FROM CharacterBattlegroundData c WHERE c.taxiStart = :taxiStart"),
    @NamedQuery(name = "CharacterBattlegroundData.findByTaxiEnd", query = "SELECT c FROM CharacterBattlegroundData c WHERE c.taxiEnd = :taxiEnd"),
    @NamedQuery(name = "CharacterBattlegroundData.findByMountSpell", query = "SELECT c FROM CharacterBattlegroundData c WHERE c.mountSpell = :mountSpell")})
public class CharacterBattlegroundData implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "guid")
    private Integer guid;
    @Basic(optional = false)
    @Column(name = "instanceId")
    private int instanceId;
    @Basic(optional = false)
    @Column(name = "team")
    private short team;
    @Basic(optional = false)
    @Column(name = "joinX")
    private float joinX;
    @Basic(optional = false)
    @Column(name = "joinY")
    private float joinY;
    @Basic(optional = false)
    @Column(name = "joinZ")
    private float joinZ;
    @Basic(optional = false)
    @Column(name = "joinO")
    private float joinO;
    @Basic(optional = false)
    @Column(name = "joinMapId")
    private short joinMapId;
    @Basic(optional = false)
    @Column(name = "taxiStart")
    private int taxiStart;
    @Basic(optional = false)
    @Column(name = "taxiEnd")
    private int taxiEnd;
    @Basic(optional = false)
    @Column(name = "mountSpell")
    private int mountSpell;

    public CharacterBattlegroundData() {
    }

    public CharacterBattlegroundData(Integer guid) {
        this.guid = guid;
    }

    public CharacterBattlegroundData(Integer guid, int instanceId, short team, float joinX, float joinY, float joinZ, float joinO, short joinMapId, int taxiStart, int taxiEnd, int mountSpell) {
        this.guid = guid;
        this.instanceId = instanceId;
        this.team = team;
        this.joinX = joinX;
        this.joinY = joinY;
        this.joinZ = joinZ;
        this.joinO = joinO;
        this.joinMapId = joinMapId;
        this.taxiStart = taxiStart;
        this.taxiEnd = taxiEnd;
        this.mountSpell = mountSpell;
    }

    public Integer getGuid() {
        return guid;
    }

    public void setGuid(Integer guid) {
        this.guid = guid;
    }

    public int getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(int instanceId) {
        this.instanceId = instanceId;
    }

    public short getTeam() {
        return team;
    }

    public void setTeam(short team) {
        this.team = team;
    }

    public float getJoinX() {
        return joinX;
    }

    public void setJoinX(float joinX) {
        this.joinX = joinX;
    }

    public float getJoinY() {
        return joinY;
    }

    public void setJoinY(float joinY) {
        this.joinY = joinY;
    }

    public float getJoinZ() {
        return joinZ;
    }

    public void setJoinZ(float joinZ) {
        this.joinZ = joinZ;
    }

    public float getJoinO() {
        return joinO;
    }

    public void setJoinO(float joinO) {
        this.joinO = joinO;
    }

    public short getJoinMapId() {
        return joinMapId;
    }

    public void setJoinMapId(short joinMapId) {
        this.joinMapId = joinMapId;
    }

    public int getTaxiStart() {
        return taxiStart;
    }

    public void setTaxiStart(int taxiStart) {
        this.taxiStart = taxiStart;
    }

    public int getTaxiEnd() {
        return taxiEnd;
    }

    public void setTaxiEnd(int taxiEnd) {
        this.taxiEnd = taxiEnd;
    }

    public int getMountSpell() {
        return mountSpell;
    }

    public void setMountSpell(int mountSpell) {
        this.mountSpell = mountSpell;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guid != null ? guid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterBattlegroundData)) {
            return false;
        }
        CharacterBattlegroundData other = (CharacterBattlegroundData) object;
        if ((this.guid == null && other.guid != null) || (this.guid != null && !this.guid.equals(other.guid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterBattlegroundData[ guid=" + guid + " ]";
    }
    
}
