/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "calendar_events")
@NamedQueries({
    @NamedQuery(name = "CalendarEvents.findAll", query = "SELECT c FROM CalendarEvents c"),
    @NamedQuery(name = "CalendarEvents.findById", query = "SELECT c FROM CalendarEvents c WHERE c.id = :id"),
    @NamedQuery(name = "CalendarEvents.findByCreator", query = "SELECT c FROM CalendarEvents c WHERE c.creator = :creator"),
    @NamedQuery(name = "CalendarEvents.findByTitle", query = "SELECT c FROM CalendarEvents c WHERE c.title = :title"),
    @NamedQuery(name = "CalendarEvents.findByDescription", query = "SELECT c FROM CalendarEvents c WHERE c.description = :description"),
    @NamedQuery(name = "CalendarEvents.findByType", query = "SELECT c FROM CalendarEvents c WHERE c.type = :type"),
    @NamedQuery(name = "CalendarEvents.findByDungeon", query = "SELECT c FROM CalendarEvents c WHERE c.dungeon = :dungeon"),
    @NamedQuery(name = "CalendarEvents.findByEventtime", query = "SELECT c FROM CalendarEvents c WHERE c.eventtime = :eventtime"),
    @NamedQuery(name = "CalendarEvents.findByFlags", query = "SELECT c FROM CalendarEvents c WHERE c.flags = :flags"),
    @NamedQuery(name = "CalendarEvents.findByTime2", query = "SELECT c FROM CalendarEvents c WHERE c.time2 = :time2")})
public class CalendarEvents implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "creator")
    private int creator;
    @Basic(optional = false)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "type")
    private boolean type;
    @Basic(optional = false)
    @Column(name = "dungeon")
    private int dungeon;
    @Basic(optional = false)
    @Column(name = "eventtime")
    private int eventtime;
    @Basic(optional = false)
    @Column(name = "flags")
    private int flags;
    @Basic(optional = false)
    @Column(name = "time2")
    private int time2;

    public CalendarEvents() {
    }

    public CalendarEvents(Long id) {
        this.id = id;
    }

    public CalendarEvents(Long id, int creator, String title, String description, boolean type, int dungeon, int eventtime, int flags, int time2) {
        this.id = id;
        this.creator = creator;
        this.title = title;
        this.description = description;
        this.type = type;
        this.dungeon = dungeon;
        this.eventtime = eventtime;
        this.flags = flags;
        this.time2 = time2;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public int getDungeon() {
        return dungeon;
    }

    public void setDungeon(int dungeon) {
        this.dungeon = dungeon;
    }

    public int getEventtime() {
        return eventtime;
    }

    public void setEventtime(int eventtime) {
        this.eventtime = eventtime;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public int getTime2() {
        return time2;
    }

    public void setTime2(int time2) {
        this.time2 = time2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalendarEvents)) {
            return false;
        }
        CalendarEvents other = (CalendarEvents) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CalendarEvents[ id=" + id + " ]";
    }
    
}
