/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "pet_spell_cooldown")
@NamedQueries({
    @NamedQuery(name = "PetSpellCooldown.findAll", query = "SELECT p FROM PetSpellCooldown p"),
    @NamedQuery(name = "PetSpellCooldown.findByGuid", query = "SELECT p FROM PetSpellCooldown p WHERE p.petSpellCooldownPK.guid = :guid"),
    @NamedQuery(name = "PetSpellCooldown.findBySpell", query = "SELECT p FROM PetSpellCooldown p WHERE p.petSpellCooldownPK.spell = :spell"),
    @NamedQuery(name = "PetSpellCooldown.findByTime", query = "SELECT p FROM PetSpellCooldown p WHERE p.time = :time")})
public class PetSpellCooldown implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PetSpellCooldownPK petSpellCooldownPK;
    @Basic(optional = false)
    @Column(name = "time")
    private int time;

    public PetSpellCooldown() {
    }

    public PetSpellCooldown(PetSpellCooldownPK petSpellCooldownPK) {
        this.petSpellCooldownPK = petSpellCooldownPK;
    }

    public PetSpellCooldown(PetSpellCooldownPK petSpellCooldownPK, int time) {
        this.petSpellCooldownPK = petSpellCooldownPK;
        this.time = time;
    }

    public PetSpellCooldown(int guid, int spell) {
        this.petSpellCooldownPK = new PetSpellCooldownPK(guid, spell);
    }

    public PetSpellCooldownPK getPetSpellCooldownPK() {
        return petSpellCooldownPK;
    }

    public void setPetSpellCooldownPK(PetSpellCooldownPK petSpellCooldownPK) {
        this.petSpellCooldownPK = petSpellCooldownPK;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (petSpellCooldownPK != null ? petSpellCooldownPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PetSpellCooldown)) {
            return false;
        }
        PetSpellCooldown other = (PetSpellCooldown) object;
        if ((this.petSpellCooldownPK == null && other.petSpellCooldownPK != null) || (this.petSpellCooldownPK != null && !this.petSpellCooldownPK.equals(other.petSpellCooldownPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.PetSpellCooldown[ petSpellCooldownPK=" + petSpellCooldownPK + " ]";
    }
    
}
