/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "guild_rank")
@NamedQueries({
    @NamedQuery(name = "GuildRank.findAll", query = "SELECT g FROM GuildRank g"),
    @NamedQuery(name = "GuildRank.findByGuildid", query = "SELECT g FROM GuildRank g WHERE g.guildRankPK.guildid = :guildid"),
    @NamedQuery(name = "GuildRank.findByRid", query = "SELECT g FROM GuildRank g WHERE g.guildRankPK.rid = :rid"),
    @NamedQuery(name = "GuildRank.findByRname", query = "SELECT g FROM GuildRank g WHERE g.rname = :rname"),
    @NamedQuery(name = "GuildRank.findByRights", query = "SELECT g FROM GuildRank g WHERE g.rights = :rights"),
    @NamedQuery(name = "GuildRank.findByBankMoneyPerDay", query = "SELECT g FROM GuildRank g WHERE g.bankMoneyPerDay = :bankMoneyPerDay")})
public class GuildRank implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GuildRankPK guildRankPK;
    @Basic(optional = false)
    @Column(name = "rname")
    private String rname;
    @Basic(optional = false)
    @Column(name = "rights")
    private int rights;
    @Basic(optional = false)
    @Column(name = "BankMoneyPerDay")
    private int bankMoneyPerDay;

    public GuildRank() {
    }

    public GuildRank(GuildRankPK guildRankPK) {
        this.guildRankPK = guildRankPK;
    }

    public GuildRank(GuildRankPK guildRankPK, String rname, int rights, int bankMoneyPerDay) {
        this.guildRankPK = guildRankPK;
        this.rname = rname;
        this.rights = rights;
        this.bankMoneyPerDay = bankMoneyPerDay;
    }

    public GuildRank(int guildid, short rid) {
        this.guildRankPK = new GuildRankPK(guildid, rid);
    }

    public GuildRankPK getGuildRankPK() {
        return guildRankPK;
    }

    public void setGuildRankPK(GuildRankPK guildRankPK) {
        this.guildRankPK = guildRankPK;
    }

    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }

    public int getRights() {
        return rights;
    }

    public void setRights(int rights) {
        this.rights = rights;
    }

    public int getBankMoneyPerDay() {
        return bankMoneyPerDay;
    }

    public void setBankMoneyPerDay(int bankMoneyPerDay) {
        this.bankMoneyPerDay = bankMoneyPerDay;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (guildRankPK != null ? guildRankPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GuildRank)) {
            return false;
        }
        GuildRank other = (GuildRank) object;
        if ((this.guildRankPK == null && other.guildRankPK != null) || (this.guildRankPK != null && !this.guildRankPK.equals(other.guildRankPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.GuildRank[ guildRankPK=" + guildRankPK + " ]";
    }
    
}
