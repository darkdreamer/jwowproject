/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "warden_action")
@NamedQueries({
    @NamedQuery(name = "WardenAction.findAll", query = "SELECT w FROM WardenAction w"),
    @NamedQuery(name = "WardenAction.findByWardenId", query = "SELECT w FROM WardenAction w WHERE w.wardenId = :wardenId"),
    @NamedQuery(name = "WardenAction.findByAction", query = "SELECT w FROM WardenAction w WHERE w.action = :action")})
public class WardenAction implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "wardenId")
    private Short wardenId;
    @Column(name = "action")
    private Short action;

    public WardenAction() {
    }

    public WardenAction(Short wardenId) {
        this.wardenId = wardenId;
    }

    public Short getWardenId() {
        return wardenId;
    }

    public void setWardenId(Short wardenId) {
        this.wardenId = wardenId;
    }

    public Short getAction() {
        return action;
    }

    public void setAction(Short action) {
        this.action = action;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wardenId != null ? wardenId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WardenAction)) {
            return false;
        }
        WardenAction other = (WardenAction) object;
        if ((this.wardenId == null && other.wardenId != null) || (this.wardenId != null && !this.wardenId.equals(other.wardenId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.WardenAction[ wardenId=" + wardenId + " ]";
    }
    
}
