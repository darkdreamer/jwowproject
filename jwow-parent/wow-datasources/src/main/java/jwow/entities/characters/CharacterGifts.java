/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_gifts")
@NamedQueries({
    @NamedQuery(name = "CharacterGifts.findAll", query = "SELECT c FROM CharacterGifts c"),
    @NamedQuery(name = "CharacterGifts.findByGuid", query = "SELECT c FROM CharacterGifts c WHERE c.guid = :guid"),
    @NamedQuery(name = "CharacterGifts.findByItemGuid", query = "SELECT c FROM CharacterGifts c WHERE c.itemGuid = :itemGuid"),
    @NamedQuery(name = "CharacterGifts.findByEntry", query = "SELECT c FROM CharacterGifts c WHERE c.entry = :entry"),
    @NamedQuery(name = "CharacterGifts.findByFlags", query = "SELECT c FROM CharacterGifts c WHERE c.flags = :flags")})
public class CharacterGifts implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Id
    @Basic(optional = false)
    @Column(name = "item_guid")
    private Integer itemGuid;
    @Basic(optional = false)
    @Column(name = "entry")
    private int entry;
    @Basic(optional = false)
    @Column(name = "flags")
    private int flags;

    public CharacterGifts() {
    }

    public CharacterGifts(Integer itemGuid) {
        this.itemGuid = itemGuid;
    }

    public CharacterGifts(Integer itemGuid, int guid, int entry, int flags) {
        this.itemGuid = itemGuid;
        this.guid = guid;
        this.entry = entry;
        this.flags = flags;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public Integer getItemGuid() {
        return itemGuid;
    }

    public void setItemGuid(Integer itemGuid) {
        this.itemGuid = itemGuid;
    }

    public int getEntry() {
        return entry;
    }

    public void setEntry(int entry) {
        this.entry = entry;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemGuid != null ? itemGuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterGifts)) {
            return false;
        }
        CharacterGifts other = (CharacterGifts) object;
        if ((this.itemGuid == null && other.itemGuid != null) || (this.itemGuid != null && !this.itemGuid.equals(other.itemGuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterGifts[ itemGuid=" + itemGuid + " ]";
    }
    
}
