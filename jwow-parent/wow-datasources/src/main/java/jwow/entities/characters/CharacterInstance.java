/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_instance")
@NamedQueries({
    @NamedQuery(name = "CharacterInstance.findAll", query = "SELECT c FROM CharacterInstance c"),
    @NamedQuery(name = "CharacterInstance.findByGuid", query = "SELECT c FROM CharacterInstance c WHERE c.characterInstancePK.guid = :guid"),
    @NamedQuery(name = "CharacterInstance.findByInstance", query = "SELECT c FROM CharacterInstance c WHERE c.characterInstancePK.instance = :instance"),
    @NamedQuery(name = "CharacterInstance.findByPermanent", query = "SELECT c FROM CharacterInstance c WHERE c.permanent = :permanent")})
public class CharacterInstance implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterInstancePK characterInstancePK;
    @Basic(optional = false)
    @Column(name = "permanent")
    private short permanent;

    public CharacterInstance() {
    }

    public CharacterInstance(CharacterInstancePK characterInstancePK) {
        this.characterInstancePK = characterInstancePK;
    }

    public CharacterInstance(CharacterInstancePK characterInstancePK, short permanent) {
        this.characterInstancePK = characterInstancePK;
        this.permanent = permanent;
    }

    public CharacterInstance(int guid, int instance) {
        this.characterInstancePK = new CharacterInstancePK(guid, instance);
    }

    public CharacterInstancePK getCharacterInstancePK() {
        return characterInstancePK;
    }

    public void setCharacterInstancePK(CharacterInstancePK characterInstancePK) {
        this.characterInstancePK = characterInstancePK;
    }

    public short getPermanent() {
        return permanent;
    }

    public void setPermanent(short permanent) {
        this.permanent = permanent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterInstancePK != null ? characterInstancePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterInstance)) {
            return false;
        }
        CharacterInstance other = (CharacterInstance) object;
        if ((this.characterInstancePK == null && other.characterInstancePK != null) || (this.characterInstancePK != null && !this.characterInstancePK.equals(other.characterInstancePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterInstance[ characterInstancePK=" + characterInstancePK + " ]";
    }
    
}
