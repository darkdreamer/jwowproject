/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterSocialPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "friend")
    private int friend;
    @Basic(optional = false)
    @Column(name = "flags")
    private short flags;

    public CharacterSocialPK() {
    }

    public CharacterSocialPK(int guid, int friend, short flags) {
        this.guid = guid;
        this.friend = friend;
        this.flags = flags;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public int getFriend() {
        return friend;
    }

    public void setFriend(int friend) {
        this.friend = friend;
    }

    public short getFlags() {
        return flags;
    }

    public void setFlags(short flags) {
        this.flags = flags;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) friend;
        hash += (int) flags;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterSocialPK)) {
            return false;
        }
        CharacterSocialPK other = (CharacterSocialPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.friend != other.friend) {
            return false;
        }
        if (this.flags != other.flags) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterSocialPK[ guid=" + guid + ", friend=" + friend + ", flags=" + flags + " ]";
    }
    
}
