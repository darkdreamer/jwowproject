/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_spell_cooldown")
@NamedQueries({
    @NamedQuery(name = "CharacterSpellCooldown.findAll", query = "SELECT c FROM CharacterSpellCooldown c"),
    @NamedQuery(name = "CharacterSpellCooldown.findByGuid", query = "SELECT c FROM CharacterSpellCooldown c WHERE c.characterSpellCooldownPK.guid = :guid"),
    @NamedQuery(name = "CharacterSpellCooldown.findBySpell", query = "SELECT c FROM CharacterSpellCooldown c WHERE c.characterSpellCooldownPK.spell = :spell"),
    @NamedQuery(name = "CharacterSpellCooldown.findByItem", query = "SELECT c FROM CharacterSpellCooldown c WHERE c.item = :item"),
    @NamedQuery(name = "CharacterSpellCooldown.findByTime", query = "SELECT c FROM CharacterSpellCooldown c WHERE c.time = :time")})
public class CharacterSpellCooldown implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterSpellCooldownPK characterSpellCooldownPK;
    @Basic(optional = false)
    @Column(name = "item")
    private int item;
    @Basic(optional = false)
    @Column(name = "time")
    private int time;

    public CharacterSpellCooldown() {
    }

    public CharacterSpellCooldown(CharacterSpellCooldownPK characterSpellCooldownPK) {
        this.characterSpellCooldownPK = characterSpellCooldownPK;
    }

    public CharacterSpellCooldown(CharacterSpellCooldownPK characterSpellCooldownPK, int item, int time) {
        this.characterSpellCooldownPK = characterSpellCooldownPK;
        this.item = item;
        this.time = time;
    }

    public CharacterSpellCooldown(int guid, int spell) {
        this.characterSpellCooldownPK = new CharacterSpellCooldownPK(guid, spell);
    }

    public CharacterSpellCooldownPK getCharacterSpellCooldownPK() {
        return characterSpellCooldownPK;
    }

    public void setCharacterSpellCooldownPK(CharacterSpellCooldownPK characterSpellCooldownPK) {
        this.characterSpellCooldownPK = characterSpellCooldownPK;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterSpellCooldownPK != null ? characterSpellCooldownPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterSpellCooldown)) {
            return false;
        }
        CharacterSpellCooldown other = (CharacterSpellCooldown) object;
        if ((this.characterSpellCooldownPK == null && other.characterSpellCooldownPK != null) || (this.characterSpellCooldownPK != null && !this.characterSpellCooldownPK.equals(other.characterSpellCooldownPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterSpellCooldown[ characterSpellCooldownPK=" + characterSpellCooldownPK + " ]";
    }
    
}
