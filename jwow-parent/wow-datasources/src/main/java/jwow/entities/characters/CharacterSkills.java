/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_skills")
@NamedQueries({
    @NamedQuery(name = "CharacterSkills.findAll", query = "SELECT c FROM CharacterSkills c"),
    @NamedQuery(name = "CharacterSkills.findByGuid", query = "SELECT c FROM CharacterSkills c WHERE c.characterSkillsPK.guid = :guid"),
    @NamedQuery(name = "CharacterSkills.findBySkill", query = "SELECT c FROM CharacterSkills c WHERE c.characterSkillsPK.skill = :skill"),
    @NamedQuery(name = "CharacterSkills.findByValue", query = "SELECT c FROM CharacterSkills c WHERE c.value = :value"),
    @NamedQuery(name = "CharacterSkills.findByMax", query = "SELECT c FROM CharacterSkills c WHERE c.max = :max")})
public class CharacterSkills implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterSkillsPK characterSkillsPK;
    @Basic(optional = false)
    @Column(name = "value")
    private short value;
    @Basic(optional = false)
    @Column(name = "max")
    private short max;

    public CharacterSkills() {
    }

    public CharacterSkills(CharacterSkillsPK characterSkillsPK) {
        this.characterSkillsPK = characterSkillsPK;
    }

    public CharacterSkills(CharacterSkillsPK characterSkillsPK, short value, short max) {
        this.characterSkillsPK = characterSkillsPK;
        this.value = value;
        this.max = max;
    }

    public CharacterSkills(int guid, short skill) {
        this.characterSkillsPK = new CharacterSkillsPK(guid, skill);
    }

    public CharacterSkillsPK getCharacterSkillsPK() {
        return characterSkillsPK;
    }

    public void setCharacterSkillsPK(CharacterSkillsPK characterSkillsPK) {
        this.characterSkillsPK = characterSkillsPK;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public short getMax() {
        return max;
    }

    public void setMax(short max) {
        this.max = max;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterSkillsPK != null ? characterSkillsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterSkills)) {
            return false;
        }
        CharacterSkills other = (CharacterSkills) object;
        if ((this.characterSkillsPK == null && other.characterSkillsPK != null) || (this.characterSkillsPK != null && !this.characterSkillsPK.equals(other.characterSkillsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterSkills[ characterSkillsPK=" + characterSkillsPK + " ]";
    }
    
}
