/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class CharacterActionPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "guid")
    private int guid;
    @Basic(optional = false)
    @Column(name = "spec")
    private short spec;
    @Basic(optional = false)
    @Column(name = "button")
    private short button;

    public CharacterActionPK() {
    }

    public CharacterActionPK(int guid, short spec, short button) {
        this.guid = guid;
        this.spec = spec;
        this.button = button;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public short getSpec() {
        return spec;
    }

    public void setSpec(short spec) {
        this.spec = spec;
    }

    public short getButton() {
        return button;
    }

    public void setButton(short button) {
        this.button = button;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) guid;
        hash += (int) spec;
        hash += (int) button;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterActionPK)) {
            return false;
        }
        CharacterActionPK other = (CharacterActionPK) object;
        if (this.guid != other.guid) {
            return false;
        }
        if (this.spec != other.spec) {
            return false;
        }
        if (this.button != other.button) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterActionPK[ guid=" + guid + ", spec=" + spec + ", button=" + button + " ]";
    }
    
}
