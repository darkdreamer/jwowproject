/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "character_account_data")
@NamedQueries({
    @NamedQuery(name = "CharacterAccountData.findAll", query = "SELECT c FROM CharacterAccountData c"),
    @NamedQuery(name = "CharacterAccountData.findByGuid", query = "SELECT c FROM CharacterAccountData c WHERE c.characterAccountDataPK.guid = :guid"),
    @NamedQuery(name = "CharacterAccountData.findByType", query = "SELECT c FROM CharacterAccountData c WHERE c.characterAccountDataPK.type = :type"),
    @NamedQuery(name = "CharacterAccountData.findByTime", query = "SELECT c FROM CharacterAccountData c WHERE c.time = :time")})
public class CharacterAccountData implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CharacterAccountDataPK characterAccountDataPK;
    @Basic(optional = false)
    @Column(name = "time")
    private int time;
    @Basic(optional = false)
    @Lob
    @Column(name = "data")
    private byte[] data;

    public CharacterAccountData() {
    }

    public CharacterAccountData(CharacterAccountDataPK characterAccountDataPK) {
        this.characterAccountDataPK = characterAccountDataPK;
    }

    public CharacterAccountData(CharacterAccountDataPK characterAccountDataPK, int time, byte[] data) {
        this.characterAccountDataPK = characterAccountDataPK;
        this.time = time;
        this.data = data;
    }

    public CharacterAccountData(int guid, short type) {
        this.characterAccountDataPK = new CharacterAccountDataPK(guid, type);
    }

    public CharacterAccountDataPK getCharacterAccountDataPK() {
        return characterAccountDataPK;
    }

    public void setCharacterAccountDataPK(CharacterAccountDataPK characterAccountDataPK) {
        this.characterAccountDataPK = characterAccountDataPK;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (characterAccountDataPK != null ? characterAccountDataPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CharacterAccountData)) {
            return false;
        }
        CharacterAccountData other = (CharacterAccountData) object;
        if ((this.characterAccountDataPK == null && other.characterAccountDataPK != null) || (this.characterAccountDataPK != null && !this.characterAccountDataPK.equals(other.characterAccountDataPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.CharacterAccountData[ characterAccountDataPK=" + characterAccountDataPK + " ]";
    }
    
}
