/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.characters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "account_instance_times")
@NamedQueries({
    @NamedQuery(name = "AccountInstanceTimes.findAll", query = "SELECT a FROM AccountInstanceTimes a"),
    @NamedQuery(name = "AccountInstanceTimes.findByAccountId", query = "SELECT a FROM AccountInstanceTimes a WHERE a.accountInstanceTimesPK.accountId = :accountId"),
    @NamedQuery(name = "AccountInstanceTimes.findByInstanceId", query = "SELECT a FROM AccountInstanceTimes a WHERE a.accountInstanceTimesPK.instanceId = :instanceId"),
    @NamedQuery(name = "AccountInstanceTimes.findByReleaseTime", query = "SELECT a FROM AccountInstanceTimes a WHERE a.releaseTime = :releaseTime")})
public class AccountInstanceTimes implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AccountInstanceTimesPK accountInstanceTimesPK;
    @Basic(optional = false)
    @Column(name = "releaseTime")
    private long releaseTime;

    public AccountInstanceTimes() {
    }

    public AccountInstanceTimes(AccountInstanceTimesPK accountInstanceTimesPK) {
        this.accountInstanceTimesPK = accountInstanceTimesPK;
    }

    public AccountInstanceTimes(AccountInstanceTimesPK accountInstanceTimesPK, long releaseTime) {
        this.accountInstanceTimesPK = accountInstanceTimesPK;
        this.releaseTime = releaseTime;
    }

    public AccountInstanceTimes(int accountId, int instanceId) {
        this.accountInstanceTimesPK = new AccountInstanceTimesPK(accountId, instanceId);
    }

    public AccountInstanceTimesPK getAccountInstanceTimesPK() {
        return accountInstanceTimesPK;
    }

    public void setAccountInstanceTimesPK(AccountInstanceTimesPK accountInstanceTimesPK) {
        this.accountInstanceTimesPK = accountInstanceTimesPK;
    }

    public long getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(long releaseTime) {
        this.releaseTime = releaseTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountInstanceTimesPK != null ? accountInstanceTimesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountInstanceTimes)) {
            return false;
        }
        AccountInstanceTimes other = (AccountInstanceTimes) object;
        if ((this.accountInstanceTimesPK == null && other.accountInstanceTimesPK != null) || (this.accountInstanceTimesPK != null && !this.accountInstanceTimesPK.equals(other.accountInstanceTimesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.characters.AccountInstanceTimes[ accountInstanceTimesPK=" + accountInstanceTimesPK + " ]";
    }
    
}
