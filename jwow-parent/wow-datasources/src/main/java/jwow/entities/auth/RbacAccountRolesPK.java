/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class RbacAccountRolesPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "accountId")
    private int accountId;
    @Basic(optional = false)
    @Column(name = "roleId")
    private int roleId;
    @Basic(optional = false)
    @Column(name = "realmId")
    private int realmId;

    public RbacAccountRolesPK() {
    }

    public RbacAccountRolesPK(int accountId, int roleId, int realmId) {
        this.accountId = accountId;
        this.roleId = roleId;
        this.realmId = realmId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getRealmId() {
        return realmId;
    }

    public void setRealmId(int realmId) {
        this.realmId = realmId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) accountId;
        hash += (int) roleId;
        hash += (int) realmId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbacAccountRolesPK)) {
            return false;
        }
        RbacAccountRolesPK other = (RbacAccountRolesPK) object;
        if (this.accountId != other.accountId) {
            return false;
        }
        if (this.roleId != other.roleId) {
            return false;
        }
        if (this.realmId != other.realmId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.RbacAccountRolesPK[ accountId=" + accountId + ", roleId=" + roleId + ", realmId=" + realmId + " ]";
    }
    
}
