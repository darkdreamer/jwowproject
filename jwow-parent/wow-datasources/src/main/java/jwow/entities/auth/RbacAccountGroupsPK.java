/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class RbacAccountGroupsPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "accountId")
    private int accountId;
    @Basic(optional = false)
    @Column(name = "groupId")
    private int groupId;
    @Basic(optional = false)
    @Column(name = "realmId")
    private int realmId;

    public RbacAccountGroupsPK() {
    }

    public RbacAccountGroupsPK(int accountId, int groupId, int realmId) {
        this.accountId = accountId;
        this.groupId = groupId;
        this.realmId = realmId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getRealmId() {
        return realmId;
    }

    public void setRealmId(int realmId) {
        this.realmId = realmId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) accountId;
        hash += (int) groupId;
        hash += (int) realmId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbacAccountGroupsPK)) {
            return false;
        }
        RbacAccountGroupsPK other = (RbacAccountGroupsPK) object;
        if (this.accountId != other.accountId) {
            return false;
        }
        if (this.groupId != other.groupId) {
            return false;
        }
        if (this.realmId != other.realmId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.RbacAccountGroupsPK[ accountId=" + accountId + ", groupId=" + groupId + ", realmId=" + realmId + " ]";
    }
    
}
