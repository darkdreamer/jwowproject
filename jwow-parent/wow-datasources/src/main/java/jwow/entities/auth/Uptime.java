/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "uptime")
@NamedQueries({
    @NamedQuery(name = "Uptime.findAll", query = "SELECT u FROM Uptime u"),
    @NamedQuery(name = "Uptime.findByRealmid", query = "SELECT u FROM Uptime u WHERE u.uptimePK.realmid = :realmid"),
    @NamedQuery(name = "Uptime.findByStarttime", query = "SELECT u FROM Uptime u WHERE u.uptimePK.starttime = :starttime"),
    @NamedQuery(name = "Uptime.findByUptime", query = "SELECT u FROM Uptime u WHERE u.uptime = :uptime"),
    @NamedQuery(name = "Uptime.findByMaxplayers", query = "SELECT u FROM Uptime u WHERE u.maxplayers = :maxplayers"),
    @NamedQuery(name = "Uptime.findByRevision", query = "SELECT u FROM Uptime u WHERE u.revision = :revision")})
public class Uptime implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UptimePK uptimePK;
    @Basic(optional = false)
    @Column(name = "uptime")
    private int uptime;
    @Basic(optional = false)
    @Column(name = "maxplayers")
    private short maxplayers;
    @Basic(optional = false)
    @Column(name = "revision")
    private String revision;

    public Uptime() {
    }

    public Uptime(UptimePK uptimePK) {
        this.uptimePK = uptimePK;
    }

    public Uptime(UptimePK uptimePK, int uptime, short maxplayers, String revision) {
        this.uptimePK = uptimePK;
        this.uptime = uptime;
        this.maxplayers = maxplayers;
        this.revision = revision;
    }

    public Uptime(int realmid, int starttime) {
        this.uptimePK = new UptimePK(realmid, starttime);
    }

    public UptimePK getUptimePK() {
        return uptimePK;
    }

    public void setUptimePK(UptimePK uptimePK) {
        this.uptimePK = uptimePK;
    }

    public int getUptime() {
        return uptime;
    }

    public void setUptime(int uptime) {
        this.uptime = uptime;
    }

    public short getMaxplayers() {
        return maxplayers;
    }

    public void setMaxplayers(short maxplayers) {
        this.maxplayers = maxplayers;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (uptimePK != null ? uptimePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Uptime)) {
            return false;
        }
        Uptime other = (Uptime) object;
        if ((this.uptimePK == null && other.uptimePK != null) || (this.uptimePK != null && !this.uptimePK.equals(other.uptimePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.Uptime[ uptimePK=" + uptimePK + " ]";
    }
    
}
