/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class UptimePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "realmid")
    private int realmid;
    @Basic(optional = false)
    @Column(name = "starttime")
    private int starttime;

    public UptimePK() {
    }

    public UptimePK(int realmid, int starttime) {
        this.realmid = realmid;
        this.starttime = starttime;
    }

    public int getRealmid() {
        return realmid;
    }

    public void setRealmid(int realmid) {
        this.realmid = realmid;
    }

    public int getStarttime() {
        return starttime;
    }

    public void setStarttime(int starttime) {
        this.starttime = starttime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) realmid;
        hash += (int) starttime;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UptimePK)) {
            return false;
        }
        UptimePK other = (UptimePK) object;
        if (this.realmid != other.realmid) {
            return false;
        }
        if (this.starttime != other.starttime) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.UptimePK[ realmid=" + realmid + ", starttime=" + starttime + " ]";
    }
    
}
