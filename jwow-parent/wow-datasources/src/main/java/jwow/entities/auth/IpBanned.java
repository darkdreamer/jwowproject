/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "ip_banned")
@NamedQueries({
    @NamedQuery(name = "IpBanned.findAll", query = "SELECT i FROM IpBanned i"),
    @NamedQuery(name = "IpBanned.findByIp", query = "SELECT i FROM IpBanned i WHERE i.ipBannedPK.ip = :ip"),
    @NamedQuery(name = "IpBanned.findByBandate", query = "SELECT i FROM IpBanned i WHERE i.ipBannedPK.bandate = :bandate"),
    @NamedQuery(name = "IpBanned.findByUnbandate", query = "SELECT i FROM IpBanned i WHERE i.unbandate = :unbandate"),
    @NamedQuery(name = "IpBanned.findByBannedby", query = "SELECT i FROM IpBanned i WHERE i.bannedby = :bannedby"),
    @NamedQuery(name = "IpBanned.findByBanreason", query = "SELECT i FROM IpBanned i WHERE i.banreason = :banreason")})
public class IpBanned implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected IpBannedPK ipBannedPK;
    @Basic(optional = false)
    @Column(name = "unbandate")
    private int unbandate;
    @Basic(optional = false)
    @Column(name = "bannedby")
    private String bannedby;
    @Basic(optional = false)
    @Column(name = "banreason")
    private String banreason;

    public IpBanned() {
    }

    public IpBanned(IpBannedPK ipBannedPK) {
        this.ipBannedPK = ipBannedPK;
    }

    public IpBanned(IpBannedPK ipBannedPK, int unbandate, String bannedby, String banreason) {
        this.ipBannedPK = ipBannedPK;
        this.unbandate = unbandate;
        this.bannedby = bannedby;
        this.banreason = banreason;
    }

    public IpBanned(String ip, int bandate) {
        this.ipBannedPK = new IpBannedPK(ip, bandate);
    }

    public IpBannedPK getIpBannedPK() {
        return ipBannedPK;
    }

    public void setIpBannedPK(IpBannedPK ipBannedPK) {
        this.ipBannedPK = ipBannedPK;
    }

    public int getUnbandate() {
        return unbandate;
    }

    public void setUnbandate(int unbandate) {
        this.unbandate = unbandate;
    }

    public String getBannedby() {
        return bannedby;
    }

    public void setBannedby(String bannedby) {
        this.bannedby = bannedby;
    }

    public String getBanreason() {
        return banreason;
    }

    public void setBanreason(String banreason) {
        this.banreason = banreason;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ipBannedPK != null ? ipBannedPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IpBanned)) {
            return false;
        }
        IpBanned other = (IpBanned) object;
        if ((this.ipBannedPK == null && other.ipBannedPK != null) || (this.ipBannedPK != null && !this.ipBannedPK.equals(other.ipBannedPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.IpBanned[ ipBannedPK=" + ipBannedPK + " ]";
    }
    
}
