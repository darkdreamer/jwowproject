/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "account")
@NamedQueries({
    @NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a"),
    @NamedQuery(name = "Account.findById", query = "SELECT a FROM Account a WHERE a.id = :id"),
    @NamedQuery(name = "Account.findByUsername", query = "SELECT a FROM Account a WHERE a.username = :username"),
    @NamedQuery(name = "Account.findByShaPassHash", query = "SELECT a FROM Account a WHERE a.shaPassHash = :shaPassHash"),
    @NamedQuery(name = "Account.findBySessionkey", query = "SELECT a FROM Account a WHERE a.sessionkey = :sessionkey"),
    @NamedQuery(name = "Account.findByV", query = "SELECT a FROM Account a WHERE a.v = :v"),
    @NamedQuery(name = "Account.findByS", query = "SELECT a FROM Account a WHERE a.s = :s"),
    @NamedQuery(name = "Account.findByEmail", query = "SELECT a FROM Account a WHERE a.email = :email"),
    @NamedQuery(name = "Account.findByJoindate", query = "SELECT a FROM Account a WHERE a.joindate = :joindate"),
    @NamedQuery(name = "Account.findByLastIp", query = "SELECT a FROM Account a WHERE a.lastIp = :lastIp"),
    @NamedQuery(name = "Account.findByFailedLogins", query = "SELECT a FROM Account a WHERE a.failedLogins = :failedLogins"),
    @NamedQuery(name = "Account.findByLocked", query = "SELECT a FROM Account a WHERE a.locked = :locked"),
    @NamedQuery(name = "Account.findByLockCountry", query = "SELECT a FROM Account a WHERE a.lockCountry = :lockCountry"),
    @NamedQuery(name = "Account.findByLastLogin", query = "SELECT a FROM Account a WHERE a.lastLogin = :lastLogin"),
    @NamedQuery(name = "Account.findByOnline", query = "SELECT a FROM Account a WHERE a.online = :online"),
    @NamedQuery(name = "Account.findByExpansion", query = "SELECT a FROM Account a WHERE a.expansion = :expansion"),
    @NamedQuery(name = "Account.findByMutetime", query = "SELECT a FROM Account a WHERE a.mutetime = :mutetime"),
    @NamedQuery(name = "Account.findByMutereason", query = "SELECT a FROM Account a WHERE a.mutereason = :mutereason"),
    @NamedQuery(name = "Account.findByMuteby", query = "SELECT a FROM Account a WHERE a.muteby = :muteby"),
    @NamedQuery(name = "Account.findByLocale", query = "SELECT a FROM Account a WHERE a.locale = :locale"),
    @NamedQuery(name = "Account.findByOs", query = "SELECT a FROM Account a WHERE a.os = :os"),
    @NamedQuery(name = "Account.findByRecruiter", query = "SELECT a FROM Account a WHERE a.recruiter = :recruiter")})
public class Account implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @Column(name = "sha_pass_hash")
    private String shaPassHash;
    @Basic(optional = false)
    @Column(name = "sessionkey")
    private String sessionkey;
    @Basic(optional = false)
    @Column(name = "v")
    private String v;
    @Basic(optional = false)
    @Column(name = "s")
    private String s;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "joindate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date joindate;
    @Basic(optional = false)
    @Column(name = "last_ip")
    private String lastIp;
    @Basic(optional = false)
    @Column(name = "failed_logins")
    private int failedLogins;
    @Basic(optional = false)
    @Column(name = "locked")
    private short locked;
    @Basic(optional = false)
    @Column(name = "lock_country")
    private String lockCountry;
    @Basic(optional = false)
    @Column(name = "last_login")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;
    @Basic(optional = false)
    @Column(name = "online")
    private short online;
    @Basic(optional = false)
    @Column(name = "expansion")
    private short expansion;
    @Basic(optional = false)
    @Column(name = "mutetime")
    private long mutetime;
    @Basic(optional = false)
    @Column(name = "mutereason")
    private String mutereason;
    @Basic(optional = false)
    @Column(name = "muteby")
    private String muteby;
    @Basic(optional = false)
    @Column(name = "locale")
    private short locale;
    @Basic(optional = false)
    @Column(name = "os")
    private String os;
    @Basic(optional = false)
    @Column(name = "recruiter")
    private int recruiter;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account",fetch = FetchType.EAGER)
//    private List<RbacAccountGroups> rbacAccountGroupsList;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
//    private List<RbacAccountRoles> rbacAccountRolesList;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
//    private List<RbacAccountPermissions> rbacAccountPermissionsList;

    public Account() {
    }

    public Account(Integer id) {
        this.id = id;
    }

    public Account(Integer id, String username, String shaPassHash, String sessionkey, String v, String s, String email, Date joindate, String lastIp, int failedLogins, short locked, String lockCountry, Date lastLogin, short online, short expansion, long mutetime, String mutereason, String muteby, short locale, String os, int recruiter) {
        this.id = id;
        this.username = username;
        this.shaPassHash = shaPassHash;
        this.sessionkey = sessionkey;
        this.v = v;
        this.s = s;
        this.email = email;
        this.joindate = joindate;
        this.lastIp = lastIp;
        this.failedLogins = failedLogins;
        this.locked = locked;
        this.lockCountry = lockCountry;
        this.lastLogin = lastLogin;
        this.online = online;
        this.expansion = expansion;
        this.mutetime = mutetime;
        this.mutereason = mutereason;
        this.muteby = muteby;
        this.locale = locale;
        this.os = os;
        this.recruiter = recruiter;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getShaPassHash() {
        return shaPassHash;
    }

    public void setShaPassHash(String shaPassHash) {
        this.shaPassHash = shaPassHash;
    }

    public String getSessionkey() {
        return sessionkey;
    }

    public void setSessionkey(String sessionkey) {
        this.sessionkey = sessionkey;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getJoindate() {
        return joindate;
    }

    public void setJoindate(Date joindate) {
        this.joindate = joindate;
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp;
    }

    public int getFailedLogins() {
        return failedLogins;
    }

    public void setFailedLogins(int failedLogins) {
        this.failedLogins = failedLogins;
    }

    public short getLocked() {
        return locked;
    }

    public void setLocked(short locked) {
        this.locked = locked;
    }

    public String getLockCountry() {
        return lockCountry;
    }

    public void setLockCountry(String lockCountry) {
        this.lockCountry = lockCountry;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public short getOnline() {
        return online;
    }

    public void setOnline(short online) {
        this.online = online;
    }

    public short getExpansion() {
        return expansion;
    }

    public void setExpansion(short expansion) {
        this.expansion = expansion;
    }

    public long getMutetime() {
        return mutetime;
    }

    public void setMutetime(long mutetime) {
        this.mutetime = mutetime;
    }

    public String getMutereason() {
        return mutereason;
    }

    public void setMutereason(String mutereason) {
        this.mutereason = mutereason;
    }

    public String getMuteby() {
        return muteby;
    }

    public void setMuteby(String muteby) {
        this.muteby = muteby;
    }

    public short getLocale() {
        return locale;
    }

    public void setLocale(short locale) {
        this.locale = locale;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public int getRecruiter() {
        return recruiter;
    }

    public void setRecruiter(int recruiter) {
        this.recruiter = recruiter;
    }

//    public List<RbacAccountGroups> getRbacAccountGroupsList() {
//        return rbacAccountGroupsList;
//    }
//
//    public void setRbacAccountGroupsList(List<RbacAccountGroups> rbacAccountGroupsList) {
//        this.rbacAccountGroupsList = rbacAccountGroupsList;
//    }
//
//    public List<RbacAccountRoles> getRbacAccountRolesList() {
//        return rbacAccountRolesList;
//    }
//
//    public void setRbacAccountRolesList(List<RbacAccountRoles> rbacAccountRolesList) {
//        this.rbacAccountRolesList = rbacAccountRolesList;
//    }
//
//    public List<RbacAccountPermissions> getRbacAccountPermissionsList() {
//        return rbacAccountPermissionsList;
//    }
//
//    public void setRbacAccountPermissionsList(List<RbacAccountPermissions> rbacAccountPermissionsList) {
//        this.rbacAccountPermissionsList = rbacAccountPermissionsList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.Account[ id=" + id + " ]";
    }
    
}
