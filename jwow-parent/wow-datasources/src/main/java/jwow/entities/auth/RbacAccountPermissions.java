/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "rbac_account_permissions")
@NamedQueries({
    @NamedQuery(name = "RbacAccountPermissions.findAll", query = "SELECT r FROM RbacAccountPermissions r"),
    @NamedQuery(name = "RbacAccountPermissions.findByAccountId", query = "SELECT r FROM RbacAccountPermissions r WHERE r.rbacAccountPermissionsPK.accountId = :accountId"),
    @NamedQuery(name = "RbacAccountPermissions.findByPermissionId", query = "SELECT r FROM RbacAccountPermissions r WHERE r.rbacAccountPermissionsPK.permissionId = :permissionId"),
    @NamedQuery(name = "RbacAccountPermissions.findByGranted", query = "SELECT r FROM RbacAccountPermissions r WHERE r.granted = :granted"),
    @NamedQuery(name = "RbacAccountPermissions.findByRealmId", query = "SELECT r FROM RbacAccountPermissions r WHERE r.rbacAccountPermissionsPK.realmId = :realmId")})
public class RbacAccountPermissions implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RbacAccountPermissionsPK rbacAccountPermissionsPK;
    @Basic(optional = false)
    @Column(name = "granted")
    private boolean granted;
    @JoinColumn(name = "permissionId", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private RbacPermissions rbacPermissions;
    @JoinColumn(name = "accountId", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Account account;

    public RbacAccountPermissions() {
    }

    public RbacAccountPermissions(RbacAccountPermissionsPK rbacAccountPermissionsPK) {
        this.rbacAccountPermissionsPK = rbacAccountPermissionsPK;
    }

    public RbacAccountPermissions(RbacAccountPermissionsPK rbacAccountPermissionsPK, boolean granted) {
        this.rbacAccountPermissionsPK = rbacAccountPermissionsPK;
        this.granted = granted;
    }

    public RbacAccountPermissions(int accountId, int permissionId, int realmId) {
        this.rbacAccountPermissionsPK = new RbacAccountPermissionsPK(accountId, permissionId, realmId);
    }

    public RbacAccountPermissionsPK getRbacAccountPermissionsPK() {
        return rbacAccountPermissionsPK;
    }

    public void setRbacAccountPermissionsPK(RbacAccountPermissionsPK rbacAccountPermissionsPK) {
        this.rbacAccountPermissionsPK = rbacAccountPermissionsPK;
    }

    public boolean getGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }

    public RbacPermissions getRbacPermissions() {
        return rbacPermissions;
    }

    public void setRbacPermissions(RbacPermissions rbacPermissions) {
        this.rbacPermissions = rbacPermissions;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rbacAccountPermissionsPK != null ? rbacAccountPermissionsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbacAccountPermissions)) {
            return false;
        }
        RbacAccountPermissions other = (RbacAccountPermissions) object;
        if ((this.rbacAccountPermissionsPK == null && other.rbacAccountPermissionsPK != null) || (this.rbacAccountPermissionsPK != null && !this.rbacAccountPermissionsPK.equals(other.rbacAccountPermissionsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.RbacAccountPermissions[ rbacAccountPermissionsPK=" + rbacAccountPermissionsPK + " ]";
    }
    
}
