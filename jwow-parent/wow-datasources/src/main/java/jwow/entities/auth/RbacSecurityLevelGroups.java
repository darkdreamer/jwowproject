/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "rbac_security_level_groups")
@NamedQueries({
    @NamedQuery(name = "RbacSecurityLevelGroups.findAll", query = "SELECT r FROM RbacSecurityLevelGroups r"),
    @NamedQuery(name = "RbacSecurityLevelGroups.findBySecId", query = "SELECT r FROM RbacSecurityLevelGroups r WHERE r.rbacSecurityLevelGroupsPK.secId = :secId"),
    @NamedQuery(name = "RbacSecurityLevelGroups.findByGroupId", query = "SELECT r FROM RbacSecurityLevelGroups r WHERE r.rbacSecurityLevelGroupsPK.groupId = :groupId")})
public class RbacSecurityLevelGroups implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RbacSecurityLevelGroupsPK rbacSecurityLevelGroupsPK;
    @JoinColumn(name = "groupId", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private RbacGroups rbacGroups;

    public RbacSecurityLevelGroups() {
    }

    public RbacSecurityLevelGroups(RbacSecurityLevelGroupsPK rbacSecurityLevelGroupsPK) {
        this.rbacSecurityLevelGroupsPK = rbacSecurityLevelGroupsPK;
    }

    public RbacSecurityLevelGroups(int secId, int groupId) {
        this.rbacSecurityLevelGroupsPK = new RbacSecurityLevelGroupsPK(secId, groupId);
    }

    public RbacSecurityLevelGroupsPK getRbacSecurityLevelGroupsPK() {
        return rbacSecurityLevelGroupsPK;
    }

    public void setRbacSecurityLevelGroupsPK(RbacSecurityLevelGroupsPK rbacSecurityLevelGroupsPK) {
        this.rbacSecurityLevelGroupsPK = rbacSecurityLevelGroupsPK;
    }

    public RbacGroups getRbacGroups() {
        return rbacGroups;
    }

    public void setRbacGroups(RbacGroups rbacGroups) {
        this.rbacGroups = rbacGroups;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rbacSecurityLevelGroupsPK != null ? rbacSecurityLevelGroupsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbacSecurityLevelGroups)) {
            return false;
        }
        RbacSecurityLevelGroups other = (RbacSecurityLevelGroups) object;
        if ((this.rbacSecurityLevelGroupsPK == null && other.rbacSecurityLevelGroupsPK != null) || (this.rbacSecurityLevelGroupsPK != null && !this.rbacSecurityLevelGroupsPK.equals(other.rbacSecurityLevelGroupsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.RbacSecurityLevelGroups[ rbacSecurityLevelGroupsPK=" + rbacSecurityLevelGroupsPK + " ]";
    }
    
}
