/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "rbac_roles")
@NamedQueries({
    @NamedQuery(name = "RbacRoles.findAll", query = "SELECT r FROM RbacRoles r"),
    @NamedQuery(name = "RbacRoles.findById", query = "SELECT r FROM RbacRoles r WHERE r.id = :id"),
    @NamedQuery(name = "RbacRoles.findByName", query = "SELECT r FROM RbacRoles r WHERE r.name = :name")})
public class RbacRoles implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @ManyToMany(mappedBy = "rbacRolesList")
    private List<RbacGroups> rbacGroupsList;
    @ManyToMany(mappedBy = "rbacRolesList")
    private List<RbacPermissions> rbacPermissionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rbacRoles")
    private List<RbacAccountRoles> rbacAccountRolesList;

    public RbacRoles() {
    }

    public RbacRoles(Integer id) {
        this.id = id;
    }

    public RbacRoles(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RbacGroups> getRbacGroupsList() {
        return rbacGroupsList;
    }

    public void setRbacGroupsList(List<RbacGroups> rbacGroupsList) {
        this.rbacGroupsList = rbacGroupsList;
    }

    public List<RbacPermissions> getRbacPermissionsList() {
        return rbacPermissionsList;
    }

    public void setRbacPermissionsList(List<RbacPermissions> rbacPermissionsList) {
        this.rbacPermissionsList = rbacPermissionsList;
    }

    public List<RbacAccountRoles> getRbacAccountRolesList() {
        return rbacAccountRolesList;
    }

    public void setRbacAccountRolesList(List<RbacAccountRoles> rbacAccountRolesList) {
        this.rbacAccountRolesList = rbacAccountRolesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbacRoles)) {
            return false;
        }
        RbacRoles other = (RbacRoles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.RbacRoles[ id=" + id + " ]";
    }
    
}
