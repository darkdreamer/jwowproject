/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class AccountBannedPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @Column(name = "bandate")
    private int bandate;

    public AccountBannedPK() {
    }

    public AccountBannedPK(int id, int bandate) {
        this.id = id;
        this.bandate = bandate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBandate() {
        return bandate;
    }

    public void setBandate(int bandate) {
        this.bandate = bandate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) bandate;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountBannedPK)) {
            return false;
        }
        AccountBannedPK other = (AccountBannedPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.bandate != other.bandate) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.AccountBannedPK[ id=" + id + ", bandate=" + bandate + " ]";
    }
    
}
