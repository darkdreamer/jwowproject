/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "realmcharacters")
@NamedQueries({
    @NamedQuery(name = "Realmcharacters.findAll", query = "SELECT r FROM Realmcharacters r"),
    @NamedQuery(name = "Realmcharacters.findByRealmid", query = "SELECT r FROM Realmcharacters r WHERE r.realmcharactersPK.realmid = :realmid"),
    @NamedQuery(name = "Realmcharacters.findByAcctid", query = "SELECT r FROM Realmcharacters r WHERE r.realmcharactersPK.acctid = :acctid"),
    @NamedQuery(name = "Realmcharacters.findByNumchars", query = "SELECT r FROM Realmcharacters r WHERE r.numchars = :numchars")})
public class Realmcharacters implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RealmcharactersPK realmcharactersPK;
    @Basic(optional = false)
    @Column(name = "numchars")
    private short numchars;

    public Realmcharacters() {
    }

    public Realmcharacters(RealmcharactersPK realmcharactersPK) {
        this.realmcharactersPK = realmcharactersPK;
    }

    public Realmcharacters(RealmcharactersPK realmcharactersPK, short numchars) {
        this.realmcharactersPK = realmcharactersPK;
        this.numchars = numchars;
    }

    public Realmcharacters(int realmid, int acctid) {
        this.realmcharactersPK = new RealmcharactersPK(realmid, acctid);
    }

    public RealmcharactersPK getRealmcharactersPK() {
        return realmcharactersPK;
    }

    public void setRealmcharactersPK(RealmcharactersPK realmcharactersPK) {
        this.realmcharactersPK = realmcharactersPK;
    }

    public short getNumchars() {
        return numchars;
    }

    public void setNumchars(short numchars) {
        this.numchars = numchars;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (realmcharactersPK != null ? realmcharactersPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Realmcharacters)) {
            return false;
        }
        Realmcharacters other = (Realmcharacters) object;
        if ((this.realmcharactersPK == null && other.realmcharactersPK != null) || (this.realmcharactersPK != null && !this.realmcharactersPK.equals(other.realmcharactersPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.Realmcharacters[ realmcharactersPK=" + realmcharactersPK + " ]";
    }
    
}
