/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class IpBannedPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ip")
    private String ip;
    @Basic(optional = false)
    @Column(name = "bandate")
    private int bandate;

    public IpBannedPK() {
    }

    public IpBannedPK(String ip, int bandate) {
        this.ip = ip;
        this.bandate = bandate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getBandate() {
        return bandate;
    }

    public void setBandate(int bandate) {
        this.bandate = bandate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ip != null ? ip.hashCode() : 0);
        hash += (int) bandate;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IpBannedPK)) {
            return false;
        }
        IpBannedPK other = (IpBannedPK) object;
        if ((this.ip == null && other.ip != null) || (this.ip != null && !this.ip.equals(other.ip))) {
            return false;
        }
        if (this.bandate != other.bandate) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.IpBannedPK[ ip=" + ip + ", bandate=" + bandate + " ]";
    }
    
}
