/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "account_banned")
@NamedQueries({
    @NamedQuery(name = "AccountBanned.findAll", query = "SELECT a FROM AccountBanned a"),
    @NamedQuery(name = "AccountBanned.findById", query = "SELECT a FROM AccountBanned a WHERE a.accountBannedPK.id = :id"),
    @NamedQuery(name = "AccountBanned.findByBandate", query = "SELECT a FROM AccountBanned a WHERE a.accountBannedPK.bandate = :bandate"),
    @NamedQuery(name = "AccountBanned.findByUnbandate", query = "SELECT a FROM AccountBanned a WHERE a.unbandate = :unbandate"),
    @NamedQuery(name = "AccountBanned.findByBannedby", query = "SELECT a FROM AccountBanned a WHERE a.bannedby = :bannedby"),
    @NamedQuery(name = "AccountBanned.findByBanreason", query = "SELECT a FROM AccountBanned a WHERE a.banreason = :banreason"),
    @NamedQuery(name = "AccountBanned.findByActive", query = "SELECT a FROM AccountBanned a WHERE a.active = :active")})
public class AccountBanned implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AccountBannedPK accountBannedPK;
    @Basic(optional = false)
    @Column(name = "unbandate")
    private int unbandate;
    @Basic(optional = false)
    @Column(name = "bannedby")
    private String bannedby;
    @Basic(optional = false)
    @Column(name = "banreason")
    private String banreason;
    @Basic(optional = false)
    @Column(name = "active")
    private short active;

    public AccountBanned() {
    }

    public AccountBanned(AccountBannedPK accountBannedPK) {
        this.accountBannedPK = accountBannedPK;
    }

    public AccountBanned(AccountBannedPK accountBannedPK, int unbandate, String bannedby, String banreason, short active) {
        this.accountBannedPK = accountBannedPK;
        this.unbandate = unbandate;
        this.bannedby = bannedby;
        this.banreason = banreason;
        this.active = active;
    }

    public AccountBanned(int id, int bandate) {
        this.accountBannedPK = new AccountBannedPK(id, bandate);
    }

    public AccountBannedPK getAccountBannedPK() {
        return accountBannedPK;
    }

    public void setAccountBannedPK(AccountBannedPK accountBannedPK) {
        this.accountBannedPK = accountBannedPK;
    }

    public int getUnbandate() {
        return unbandate;
    }

    public void setUnbandate(int unbandate) {
        this.unbandate = unbandate;
    }

    public String getBannedby() {
        return bannedby;
    }

    public void setBannedby(String bannedby) {
        this.bannedby = bannedby;
    }

    public String getBanreason() {
        return banreason;
    }

    public void setBanreason(String banreason) {
        this.banreason = banreason;
    }

    public short getActive() {
        return active;
    }

    public void setActive(short active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountBannedPK != null ? accountBannedPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountBanned)) {
            return false;
        }
        AccountBanned other = (AccountBanned) object;
        if ((this.accountBannedPK == null && other.accountBannedPK != null) || (this.accountBannedPK != null && !this.accountBannedPK.equals(other.accountBannedPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.AccountBanned[ accountBannedPK=" + accountBannedPK + " ]";
    }
    
}
