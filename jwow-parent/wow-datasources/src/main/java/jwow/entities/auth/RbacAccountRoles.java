/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "rbac_account_roles")
@NamedQueries({
    @NamedQuery(name = "RbacAccountRoles.findAll", query = "SELECT r FROM RbacAccountRoles r"),
    @NamedQuery(name = "RbacAccountRoles.findByAccountId", query = "SELECT r FROM RbacAccountRoles r WHERE r.rbacAccountRolesPK.accountId = :accountId"),
    @NamedQuery(name = "RbacAccountRoles.findByRoleId", query = "SELECT r FROM RbacAccountRoles r WHERE r.rbacAccountRolesPK.roleId = :roleId"),
    @NamedQuery(name = "RbacAccountRoles.findByGranted", query = "SELECT r FROM RbacAccountRoles r WHERE r.granted = :granted"),
    @NamedQuery(name = "RbacAccountRoles.findByRealmId", query = "SELECT r FROM RbacAccountRoles r WHERE r.rbacAccountRolesPK.realmId = :realmId")})
public class RbacAccountRoles implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RbacAccountRolesPK rbacAccountRolesPK;
    @Basic(optional = false)
    @Column(name = "granted")
    private boolean granted;
    @JoinColumn(name = "roleId", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private RbacRoles rbacRoles;
    @JoinColumn(name = "accountId", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Account account;

    public RbacAccountRoles() {
    }

    public RbacAccountRoles(RbacAccountRolesPK rbacAccountRolesPK) {
        this.rbacAccountRolesPK = rbacAccountRolesPK;
    }

    public RbacAccountRoles(RbacAccountRolesPK rbacAccountRolesPK, boolean granted) {
        this.rbacAccountRolesPK = rbacAccountRolesPK;
        this.granted = granted;
    }

    public RbacAccountRoles(int accountId, int roleId, int realmId) {
        this.rbacAccountRolesPK = new RbacAccountRolesPK(accountId, roleId, realmId);
    }

    public RbacAccountRolesPK getRbacAccountRolesPK() {
        return rbacAccountRolesPK;
    }

    public void setRbacAccountRolesPK(RbacAccountRolesPK rbacAccountRolesPK) {
        this.rbacAccountRolesPK = rbacAccountRolesPK;
    }

    public boolean getGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }

    public RbacRoles getRbacRoles() {
        return rbacRoles;
    }

    public void setRbacRoles(RbacRoles rbacRoles) {
        this.rbacRoles = rbacRoles;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rbacAccountRolesPK != null ? rbacAccountRolesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbacAccountRoles)) {
            return false;
        }
        RbacAccountRoles other = (RbacAccountRoles) object;
        if ((this.rbacAccountRolesPK == null && other.rbacAccountRolesPK != null) || (this.rbacAccountRolesPK != null && !this.rbacAccountRolesPK.equals(other.rbacAccountRolesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.RbacAccountRoles[ rbacAccountRolesPK=" + rbacAccountRolesPK + " ]";
    }
    
}
