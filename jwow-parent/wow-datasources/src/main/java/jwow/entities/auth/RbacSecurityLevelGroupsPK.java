/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class RbacSecurityLevelGroupsPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "secId")
    private int secId;
    @Basic(optional = false)
    @Column(name = "groupId")
    private int groupId;

    public RbacSecurityLevelGroupsPK() {
    }

    public RbacSecurityLevelGroupsPK(int secId, int groupId) {
        this.secId = secId;
        this.groupId = groupId;
    }

    public int getSecId() {
        return secId;
    }

    public void setSecId(int secId) {
        this.secId = secId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) secId;
        hash += (int) groupId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbacSecurityLevelGroupsPK)) {
            return false;
        }
        RbacSecurityLevelGroupsPK other = (RbacSecurityLevelGroupsPK) object;
        if (this.secId != other.secId) {
            return false;
        }
        if (this.groupId != other.groupId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.RbacSecurityLevelGroupsPK[ secId=" + secId + ", groupId=" + groupId + " ]";
    }
    
}
