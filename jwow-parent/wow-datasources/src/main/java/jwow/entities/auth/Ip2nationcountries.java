/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "ip2nationcountries")
@NamedQueries({
    @NamedQuery(name = "Ip2nationcountries.findAll", query = "SELECT i FROM Ip2nationcountries i"),
    @NamedQuery(name = "Ip2nationcountries.findByCode", query = "SELECT i FROM Ip2nationcountries i WHERE i.code = :code"),
    @NamedQuery(name = "Ip2nationcountries.findByIsoCode2", query = "SELECT i FROM Ip2nationcountries i WHERE i.isoCode2 = :isoCode2"),
    @NamedQuery(name = "Ip2nationcountries.findByIsoCode3", query = "SELECT i FROM Ip2nationcountries i WHERE i.isoCode3 = :isoCode3"),
    @NamedQuery(name = "Ip2nationcountries.findByIsoCountry", query = "SELECT i FROM Ip2nationcountries i WHERE i.isoCountry = :isoCountry"),
    @NamedQuery(name = "Ip2nationcountries.findByCountry", query = "SELECT i FROM Ip2nationcountries i WHERE i.country = :country"),
    @NamedQuery(name = "Ip2nationcountries.findByLat", query = "SELECT i FROM Ip2nationcountries i WHERE i.lat = :lat"),
    @NamedQuery(name = "Ip2nationcountries.findByLon", query = "SELECT i FROM Ip2nationcountries i WHERE i.lon = :lon")})
public class Ip2nationcountries implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @Column(name = "iso_code_2")
    private String isoCode2;
    @Column(name = "iso_code_3")
    private String isoCode3;
    @Basic(optional = false)
    @Column(name = "iso_country")
    private String isoCountry;
    @Basic(optional = false)
    @Column(name = "country")
    private String country;
    @Basic(optional = false)
    @Column(name = "lat")
    private float lat;
    @Basic(optional = false)
    @Column(name = "lon")
    private float lon;

    public Ip2nationcountries() {
    }

    public Ip2nationcountries(String code) {
        this.code = code;
    }

    public Ip2nationcountries(String code, String isoCode2, String isoCountry, String country, float lat, float lon) {
        this.code = code;
        this.isoCode2 = isoCode2;
        this.isoCountry = isoCountry;
        this.country = country;
        this.lat = lat;
        this.lon = lon;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIsoCode2() {
        return isoCode2;
    }

    public void setIsoCode2(String isoCode2) {
        this.isoCode2 = isoCode2;
    }

    public String getIsoCode3() {
        return isoCode3;
    }

    public void setIsoCode3(String isoCode3) {
        this.isoCode3 = isoCode3;
    }

    public String getIsoCountry() {
        return isoCountry;
    }

    public void setIsoCountry(String isoCountry) {
        this.isoCountry = isoCountry;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ip2nationcountries)) {
            return false;
        }
        Ip2nationcountries other = (Ip2nationcountries) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.Ip2nationcountries[ code=" + code + " ]";
    }
    
}
