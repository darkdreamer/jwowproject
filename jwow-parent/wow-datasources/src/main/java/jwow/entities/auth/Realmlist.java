/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "realmlist")
@NamedQueries({
    @NamedQuery(name = "Realmlist.findAll", query = "SELECT r FROM Realmlist r"),
    @NamedQuery(name = "Realmlist.findById", query = "SELECT r FROM Realmlist r WHERE r.id = :id"),
    @NamedQuery(name = "Realmlist.findByName", query = "SELECT r FROM Realmlist r WHERE r.name = :name"),
    @NamedQuery(name = "Realmlist.findByAddress", query = "SELECT r FROM Realmlist r WHERE r.address = :address"),
    @NamedQuery(name = "Realmlist.findByLocalAddress", query = "SELECT r FROM Realmlist r WHERE r.localAddress = :localAddress"),
    @NamedQuery(name = "Realmlist.findByLocalSubnetMask", query = "SELECT r FROM Realmlist r WHERE r.localSubnetMask = :localSubnetMask"),
    @NamedQuery(name = "Realmlist.findByPort", query = "SELECT r FROM Realmlist r WHERE r.port = :port"),
    @NamedQuery(name = "Realmlist.findByIcon", query = "SELECT r FROM Realmlist r WHERE r.icon = :icon"),
    @NamedQuery(name = "Realmlist.findByFlag", query = "SELECT r FROM Realmlist r WHERE r.flag = :flag"),
    @NamedQuery(name = "Realmlist.findByTimezone", query = "SELECT r FROM Realmlist r WHERE r.timezone = :timezone"),
    @NamedQuery(name = "Realmlist.findByAllowedSecurityLevel", query = "SELECT r FROM Realmlist r WHERE r.allowedSecurityLevel = :allowedSecurityLevel"),
    @NamedQuery(name = "Realmlist.findByPopulation", query = "SELECT r FROM Realmlist r WHERE r.population = :population"),
    @NamedQuery(name = "Realmlist.findByGamebuild", query = "SELECT r FROM Realmlist r WHERE r.gamebuild = :gamebuild")})
public class Realmlist implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @Column(name = "localAddress")
    private String localAddress;
    @Basic(optional = false)
    @Column(name = "localSubnetMask")
    private String localSubnetMask;
    @Basic(optional = false)
    @Column(name = "port")
    private short port;
    @Basic(optional = false)
    @Column(name = "icon")
    private short icon;
    @Basic(optional = false)
    @Column(name = "flag")
    private short flag;
    @Basic(optional = false)
    @Column(name = "timezone")
    private short timezone;
    @Basic(optional = false)
    @Column(name = "allowedSecurityLevel")
    private short allowedSecurityLevel;
    @Basic(optional = false)
    @Column(name = "population")
    private float population;
    @Basic(optional = false)
    @Column(name = "gamebuild")
    private int gamebuild;

    public Realmlist() {
    }

    public Realmlist(Integer id) {
        this.id = id;
    }

    public Realmlist(Integer id, String name, String address, String localAddress, String localSubnetMask, short port, short icon, short flag, short timezone, short allowedSecurityLevel, float population, int gamebuild) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.localAddress = localAddress;
        this.localSubnetMask = localSubnetMask;
        this.port = port;
        this.icon = icon;
        this.flag = flag;
        this.timezone = timezone;
        this.allowedSecurityLevel = allowedSecurityLevel;
        this.population = population;
        this.gamebuild = gamebuild;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocalAddress() {
        return localAddress;
    }

    public void setLocalAddress(String localAddress) {
        this.localAddress = localAddress;
    }

    public String getLocalSubnetMask() {
        return localSubnetMask;
    }

    public void setLocalSubnetMask(String localSubnetMask) {
        this.localSubnetMask = localSubnetMask;
    }

    public short getPort() {
        return port;
    }

    public void setPort(short port) {
        this.port = port;
    }

    public short getIcon() {
        return icon;
    }

    public void setIcon(short icon) {
        this.icon = icon;
    }

    public short getFlag() {
        return flag;
    }

    public void setFlag(short flag) {
        this.flag = flag;
    }

    public short getTimezone() {
        return timezone;
    }

    public void setTimezone(short timezone) {
        this.timezone = timezone;
    }

    public short getAllowedSecurityLevel() {
        return allowedSecurityLevel;
    }

    public void setAllowedSecurityLevel(short allowedSecurityLevel) {
        this.allowedSecurityLevel = allowedSecurityLevel;
    }

    public float getPopulation() {
        return population;
    }

    public void setPopulation(float population) {
        this.population = population;
    }

    public int getGamebuild() {
        return gamebuild;
    }

    public void setGamebuild(int gamebuild) {
        this.gamebuild = gamebuild;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Realmlist)) {
            return false;
        }
        Realmlist other = (Realmlist) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.Realmlist[ id=" + id + " ]";
    }
    
}
