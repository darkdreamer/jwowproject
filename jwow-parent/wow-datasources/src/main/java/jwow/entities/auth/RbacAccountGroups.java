/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "rbac_account_groups")
@NamedQueries({
    @NamedQuery(name = "RbacAccountGroups.findAll", query = "SELECT r FROM RbacAccountGroups r"),
    @NamedQuery(name = "RbacAccountGroups.findByAccountId", query = "SELECT r FROM RbacAccountGroups r WHERE r.rbacAccountGroupsPK.accountId = :accountId"),
    @NamedQuery(name = "RbacAccountGroups.findByGroupId", query = "SELECT r FROM RbacAccountGroups r WHERE r.rbacAccountGroupsPK.groupId = :groupId"),
    @NamedQuery(name = "RbacAccountGroups.findByRealmId", query = "SELECT r FROM RbacAccountGroups r WHERE r.rbacAccountGroupsPK.realmId = :realmId")})
public class RbacAccountGroups implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RbacAccountGroupsPK rbacAccountGroupsPK;
    @JoinColumn(name = "groupId", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private RbacGroups rbacGroups;
    @JoinColumn(name = "accountId", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Account account;

    public RbacAccountGroups() {
    }

    public RbacAccountGroups(RbacAccountGroupsPK rbacAccountGroupsPK) {
        this.rbacAccountGroupsPK = rbacAccountGroupsPK;
    }

    public RbacAccountGroups(int accountId, int groupId, int realmId) {
        this.rbacAccountGroupsPK = new RbacAccountGroupsPK(accountId, groupId, realmId);
    }

    public RbacAccountGroupsPK getRbacAccountGroupsPK() {
        return rbacAccountGroupsPK;
    }

    public void setRbacAccountGroupsPK(RbacAccountGroupsPK rbacAccountGroupsPK) {
        this.rbacAccountGroupsPK = rbacAccountGroupsPK;
    }

    public RbacGroups getRbacGroups() {
        return rbacGroups;
    }

    public void setRbacGroups(RbacGroups rbacGroups) {
        this.rbacGroups = rbacGroups;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rbacAccountGroupsPK != null ? rbacAccountGroupsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbacAccountGroups)) {
            return false;
        }
        RbacAccountGroups other = (RbacAccountGroups) object;
        if ((this.rbacAccountGroupsPK == null && other.rbacAccountGroupsPK != null) || (this.rbacAccountGroupsPK != null && !this.rbacAccountGroupsPK.equals(other.rbacAccountGroupsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.RbacAccountGroups[ rbacAccountGroupsPK=" + rbacAccountGroupsPK + " ]";
    }
    
}
