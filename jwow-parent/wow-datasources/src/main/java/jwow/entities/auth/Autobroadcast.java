/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "autobroadcast")
@NamedQueries({
    @NamedQuery(name = "Autobroadcast.findAll", query = "SELECT a FROM Autobroadcast a"),
    @NamedQuery(name = "Autobroadcast.findByRealmid", query = "SELECT a FROM Autobroadcast a WHERE a.autobroadcastPK.realmid = :realmid"),
    @NamedQuery(name = "Autobroadcast.findById", query = "SELECT a FROM Autobroadcast a WHERE a.autobroadcastPK.id = :id"),
    @NamedQuery(name = "Autobroadcast.findByWeight", query = "SELECT a FROM Autobroadcast a WHERE a.weight = :weight")})
public class Autobroadcast implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AutobroadcastPK autobroadcastPK;
    @Column(name = "weight")
    private Short weight;
    @Basic(optional = false)
    @Lob
    @Column(name = "text")
    private String text;

    public Autobroadcast() {
    }

    public Autobroadcast(AutobroadcastPK autobroadcastPK) {
        this.autobroadcastPK = autobroadcastPK;
    }

    public Autobroadcast(AutobroadcastPK autobroadcastPK, String text) {
        this.autobroadcastPK = autobroadcastPK;
        this.text = text;
    }

    public Autobroadcast(int realmid, short id) {
        this.autobroadcastPK = new AutobroadcastPK(realmid, id);
    }

    public AutobroadcastPK getAutobroadcastPK() {
        return autobroadcastPK;
    }

    public void setAutobroadcastPK(AutobroadcastPK autobroadcastPK) {
        this.autobroadcastPK = autobroadcastPK;
    }

    public Short getWeight() {
        return weight;
    }

    public void setWeight(Short weight) {
        this.weight = weight;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (autobroadcastPK != null ? autobroadcastPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Autobroadcast)) {
            return false;
        }
        Autobroadcast other = (Autobroadcast) object;
        if ((this.autobroadcastPK == null && other.autobroadcastPK != null) || (this.autobroadcastPK != null && !this.autobroadcastPK.equals(other.autobroadcastPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.Autobroadcast[ autobroadcastPK=" + autobroadcastPK + " ]";
    }
    
}
