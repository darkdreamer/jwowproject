/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "rbac_permissions")
@NamedQueries({
    @NamedQuery(name = "RbacPermissions.findAll", query = "SELECT r FROM RbacPermissions r"),
    @NamedQuery(name = "RbacPermissions.findById", query = "SELECT r FROM RbacPermissions r WHERE r.id = :id"),
    @NamedQuery(name = "RbacPermissions.findByName", query = "SELECT r FROM RbacPermissions r WHERE r.name = :name")})
public class RbacPermissions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @JoinTable(name = "rbac_role_permissions", joinColumns = {
        @JoinColumn(name = "permissionId", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "roleId", referencedColumnName = "id")})
    @ManyToMany
    private List<RbacRoles> rbacRolesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rbacPermissions")
    private List<RbacAccountPermissions> rbacAccountPermissionsList;

    public RbacPermissions() {
    }

    public RbacPermissions(Integer id) {
        this.id = id;
    }

    public RbacPermissions(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RbacRoles> getRbacRolesList() {
        return rbacRolesList;
    }

    public void setRbacRolesList(List<RbacRoles> rbacRolesList) {
        this.rbacRolesList = rbacRolesList;
    }

    public List<RbacAccountPermissions> getRbacAccountPermissionsList() {
        return rbacAccountPermissionsList;
    }

    public void setRbacAccountPermissionsList(List<RbacAccountPermissions> rbacAccountPermissionsList) {
        this.rbacAccountPermissionsList = rbacAccountPermissionsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbacPermissions)) {
            return false;
        }
        RbacPermissions other = (RbacPermissions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.RbacPermissions[ id=" + id + " ]";
    }
    
}
