/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class AutobroadcastPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "realmid")
    private int realmid;
    @Basic(optional = false)
    @Column(name = "id")
    private short id;

    public AutobroadcastPK() {
    }

    public AutobroadcastPK(int realmid, short id) {
        this.realmid = realmid;
        this.id = id;
    }

    public int getRealmid() {
        return realmid;
    }

    public void setRealmid(int realmid) {
        this.realmid = realmid;
    }

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) realmid;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutobroadcastPK)) {
            return false;
        }
        AutobroadcastPK other = (AutobroadcastPK) object;
        if (this.realmid != other.realmid) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.AutobroadcastPK[ realmid=" + realmid + ", id=" + id + " ]";
    }
    
}
