/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author dreamer
 */
@Entity
@Table(name = "account_access")
@NamedQueries({
    @NamedQuery(name = "AccountAccess.findAll", query = "SELECT a FROM AccountAccess a"),
    @NamedQuery(name = "AccountAccess.findById", query = "SELECT a FROM AccountAccess a WHERE a.accountAccessPK.id = :id"),
    @NamedQuery(name = "AccountAccess.findByGmlevel", query = "SELECT a FROM AccountAccess a WHERE a.gmlevel = :gmlevel"),
    @NamedQuery(name = "AccountAccess.findByRealmID", query = "SELECT a FROM AccountAccess a WHERE a.accountAccessPK.realmID = :realmID")})
public class AccountAccess implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AccountAccessPK accountAccessPK;
    @Basic(optional = false)
    @Column(name = "gmlevel")
    private short gmlevel;

    public AccountAccess() {
    }

    public AccountAccess(AccountAccessPK accountAccessPK) {
        this.accountAccessPK = accountAccessPK;
    }

    public AccountAccess(AccountAccessPK accountAccessPK, short gmlevel) {
        this.accountAccessPK = accountAccessPK;
        this.gmlevel = gmlevel;
    }

    public AccountAccess(int id, int realmID) {
        this.accountAccessPK = new AccountAccessPK(id, realmID);
    }

    public AccountAccessPK getAccountAccessPK() {
        return accountAccessPK;
    }

    public void setAccountAccessPK(AccountAccessPK accountAccessPK) {
        this.accountAccessPK = accountAccessPK;
    }

    public short getGmlevel() {
        return gmlevel;
    }

    public void setGmlevel(short gmlevel) {
        this.gmlevel = gmlevel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountAccessPK != null ? accountAccessPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountAccess)) {
            return false;
        }
        AccountAccess other = (AccountAccess) object;
        if ((this.accountAccessPK == null && other.accountAccessPK != null) || (this.accountAccessPK != null && !this.accountAccessPK.equals(other.accountAccessPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.AccountAccess[ accountAccessPK=" + accountAccessPK + " ]";
    }
    
}
