/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.entities.auth;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dreamer
 */
@Embeddable
public class RealmcharactersPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "realmid")
    private int realmid;
    @Basic(optional = false)
    @Column(name = "acctid")
    private int acctid;

    public RealmcharactersPK() {
    }

    public RealmcharactersPK(int realmid, int acctid) {
        this.realmid = realmid;
        this.acctid = acctid;
    }

    public int getRealmid() {
        return realmid;
    }

    public void setRealmid(int realmid) {
        this.realmid = realmid;
    }

    public int getAcctid() {
        return acctid;
    }

    public void setAcctid(int acctid) {
        this.acctid = acctid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) realmid;
        hash += (int) acctid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RealmcharactersPK)) {
            return false;
        }
        RealmcharactersPK other = (RealmcharactersPK) object;
        if (this.realmid != other.realmid) {
            return false;
        }
        if (this.acctid != other.acctid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jwow.entities.auth.RealmcharactersPK[ realmid=" + realmid + ", acctid=" + acctid + " ]";
    }
    
}
