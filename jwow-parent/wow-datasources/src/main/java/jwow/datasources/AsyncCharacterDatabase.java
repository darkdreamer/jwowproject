/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.datasources;

import jwow.datasources.async.AsyncCallback;
import jwow.entities.aggregate.LoginPlayerInfo;

/**
 *
 * @author dreamer
 */
public interface AsyncCharacterDatabase {
    void loadPlayerData(int guid,AsyncCallback<LoginPlayerInfo> callback);
}
