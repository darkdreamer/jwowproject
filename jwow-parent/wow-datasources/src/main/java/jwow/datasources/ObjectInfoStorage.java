/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.datasources;

import jwow.entities.aggregate.PlayerInfo;
import jwow.entities.dbc.CharacterRace;
import jwow.entities.dbc.ClassEntry;

/**
 *
 * @author dreamer
 */
public interface ObjectInfoStorage {
    PlayerInfo getPlayerInfo(int race,int playerClass);
    CharacterRace getCharacterRace(int race);
    ClassEntry getClassEntry(int clasz);
}
