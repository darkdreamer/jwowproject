/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.datasources.async;

import java.rmi.Remote;

/**
 *
 * @author dreamer
 */
public interface AsyncCallback<T> extends Remote{
    void handleResult(T result);
}
