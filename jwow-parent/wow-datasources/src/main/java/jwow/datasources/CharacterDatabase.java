/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.datasources;

import java.io.Serializable;
import java.util.List;
import jwow.entities.characters.AccountData;

/**
 *
 * @author dreamer
 */
public interface CharacterDatabase {
    public static class CharacterEnumInformation implements Serializable{
        public int guid;
        public String name;
        public short race;
        public short characterClass;
        public short gender;
        public int playerBytes1;
        public int playerBytes2;
        public short level;
        
        public int zone;
        public short map;
        public float posX;
        public float posY;
        public float posZ;
        
        public Integer guildid;
        public int playerFlags;
    
        public int characterAtLogin;
        public Integer petEntry;
        public Integer petModelId;
        public Integer petLevel;
        public String equipmentCache;
        public Integer bannedGuid;
        public String declinedName;
    
    }
    
    List<AccountData> getGlobalAccountData(int accountId);
    
    List<CharacterEnumInformation> charSelEnum(int account,int slot);
}
