/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.dbc.support.impl;

import java.io.File;
import java.lang.reflect.ParameterizedType;
import jwow.dbc.support.DbcRecord;

/**
 *
 * @author dreamer
 */
public class DbcStorage<RecordType> {

    String storagePath;
    
    RecordType[] data;
    
    public RecordType lookupEntry(int idx){
        return data[idx];
    }

    public DbcStorage(String storagePath) throws Exception {
        this.storagePath = storagePath;

        if (getClass() == DbcStorage.class) {
            throw new RuntimeException("Due to implementation of JVM you need to extend storage class to use this API, cause record type is indetectable in runtime");
        }

        ParameterizedType supertype = (ParameterizedType) getClass().getGenericSuperclass();
        Class recordClass = (Class) supertype.getActualTypeArguments()[0];

        if (!recordClass.isAnnotationPresent(DbcRecord.class)) {
            throw new RuntimeException("DbcRecord annotation is required for record class");
        }

        DbcFileLoader dbcf_ldr = new DbcFileLoader(recordClass);
        data = (RecordType[])dbcf_ldr.loadFile(new File(storagePath));
        
    }
    
    public int size() {
        return data.length;
    }

    
}
