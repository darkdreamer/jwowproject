/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.dbc.support.impl;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.HashMap;
import jwow.dbc.support.DbcField;
import jwow.dbc.support.DbcRecord;

/**
 *
 * @author dreamer
 */
public class DbcFileLoader {

    Class recordClass;
    int indexFieldNum;

    public DbcFileLoader(Class recordClass) {
        this.recordClass = recordClass;
    }

    Object getCorrectValue(DBC dbc, int i, int index, Class c) throws Exception {
        if (c.equals(String.class)) {
            return dbc.getString(i, index);
        } else if (c.equals(Integer.class) || c.equals(Integer.TYPE)) {
            return dbc.getInt(i, index);
        } else if (c.equals(Float.class) || c.equals(Float.TYPE)) {
            return dbc.getFloat(i, index);
        } else {
            throw new RuntimeException("Unsupported field class. String or integer expected");
        }
    }

    private int[] createParserIdxOffsets(String format) {
        int[] offsets = new int[format.length()];


        for (int i = 0; i < format.length(); i++) {
            char c = format.charAt(i);
            DbcFieldTypes ft = DbcFieldTypes.valueOf(c);

            if (i == 0) {
                offsets[0] = 0;
            } else {
                offsets[i] = offsets[i - 1] + ft.getFieldSize();
            }
        }

        return offsets;
    }

    private int checkRecordClass(DbcRecord annotation) {
        String format = annotation.value();
        int sz = 0;
        for (int i = 0; i < format.length(); i++) {
            char c = format.charAt(i);
            DbcFieldTypes ft = DbcFieldTypes.valueOf(c);

            sz += ft.getFieldSize();

            if (DbcFieldTypes.IND.equals(ft)) {
                indexFieldNum = i;
            }
        }

        return sz;
    }

    private Object parseDbc(DBC dbc, Class recordClass) throws Exception {

        HashMap<Integer, Field> parserMapping = createParserMapping(recordClass);

        HashMap<Integer, Object> resultDb = new HashMap<Integer, Object>();

        int maxIndex= -1;

        for (int i = 0; i < dbc.size(); i++) {
            Object obj = recordClass.newInstance();
            int index = loadRecord(parserMapping, obj, dbc, i);
            
            maxIndex = Math.max(maxIndex, index);
            
            resultDb.put(index, obj);
        }
        
        //final transform
        Object dataArray = Array.newInstance(recordClass, maxIndex+1);

        for(int i = 0;i<maxIndex;i++){
            Array.set(dataArray, i, resultDb.get(i));
        }
//        data = (RecordType[])dataArray;
        return dataArray;
    }

    private HashMap<Integer, Field> createParserMapping(Class recordClass) {
        HashMap<Integer, Field> hashMap = new HashMap<Integer, Field>();

        Field[] fields = recordClass.getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {
            Field f = fields[i];

            if (f.isAnnotationPresent(DbcField.class)) {
                DbcField aField = f.getAnnotation(DbcField.class);

                if (aField.count() > 1 && !f.getType().isArray()) {
                    throw new RuntimeException("Field " + f.getName() + " must be array cause it's count expected be more then 1");
                }

                hashMap.put(aField.index(), f);
            }
        }

        return hashMap;
    }

    private int loadRecord(HashMap<Integer, Field> parserMapping, Object obj, DBC dbc, int i) throws Exception {
        int recordIndex = -1;
        for (Integer index : parserMapping.keySet()) {



            Field fld = parserMapping.get(index);

            int count = fld.getAnnotation(DbcField.class).count();

            if (count == 1) {
                Object v = getCorrectValue(dbc, i, index, fld.getType());
                boolean oldState = fld.isAccessible();
                fld.setAccessible(true);
                fld.set(obj, v);
                fld.setAccessible(oldState);

                if (index.equals(indexFieldNum)) {
                    recordIndex = (Integer) v;
                }
            } else {
                Class arrayClass = fld.getType();

                Class fieldClass = arrayClass.getComponentType();

                Object targetArray = Array.newInstance(fieldClass, count);

                for (int j = 0; j < count; j++) {
                    Object v = getCorrectValue(dbc, i, index + j, fieldClass);
                    Array.set(targetArray, j, v);
                }

                boolean chacc = fld.isAccessible();

                fld.setAccessible(true);
                fld.set(obj, targetArray);
                fld.setAccessible(chacc);
            }

        }
        
        return recordIndex;
    }

    Object loadFile(File file) throws Exception {
        DBC dbc = DBC.read(file);

        DbcRecord clz = (DbcRecord) recordClass.getAnnotation(DbcRecord.class);


        Integer value = checkRecordClass((DbcRecord) recordClass.getAnnotation(DbcRecord.class));
        int[] indexOffset = createParserIdxOffsets(clz.value());

        dbc.setIndexOffset(indexOffset);

        int sz = dbc.size();



        return parseDbc(dbc, recordClass);
    }
}
