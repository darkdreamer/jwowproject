/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.dbc.support;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author dreamer
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DbcField {
    int index();
    int count() default 1;
}
