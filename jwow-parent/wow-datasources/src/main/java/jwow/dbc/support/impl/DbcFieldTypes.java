/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.dbc.support.impl;

/**
 *
 * @author dreamer
 */
public enum DbcFieldTypes {
//        FT_NA='x',                                              //not used or unknown, 4 byte size
//    FT_NA_BYTE='X',                                         //not used or unknown, byte
//    FT_STRING='s',                                          //char*
//    FT_FLOAT='f',                                           //float
//    FT_INT='i',                                             //uint32
//    FT_BYTE='b',                                            //uint8
//    FT_SORT='d',                                            //sorted by this field, field is not included
//    FT_IND='n',                                             //the same, but parsed to data
//    FT_LOGIC='l',                                            //Logical (boolean)
//    FT_SQL_PRESENT='p',                                      //Used in sql format to mark column present in sql dbc
//    FT_SQL_ABSENT='a'                                       //Used in sql format to mark column absent in sql dbc
    
    NA('x',4),
    NA_BYTE('X',1),
    STRING('s',4),
    FLOAT('f',4),
    INT('i',4),
    BYTE('b',1),
    SORT('d',4),
    IND('n',4),
    LOGIC('l',1),
    SQL_PRESENT('p',4),
    SQL_ABSENT('a',4);

    
    private int fieldSize;
    private char fieldCode;

    private DbcFieldTypes(char fieldCode,int fieldSize) {
        this.fieldSize = fieldSize;
        this.fieldCode = fieldCode;
    }

    public char getFieldCode() {
        return fieldCode;
    }

    public int getFieldSize() {
        return fieldSize;
    }
    
    public static DbcFieldTypes valueOf(char c){
        for(DbcFieldTypes ft : values())
            if(ft.getFieldCode()==c)
                return ft;
        
        return null;
    }
}
