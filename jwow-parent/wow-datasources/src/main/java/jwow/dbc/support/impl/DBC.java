/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.dbc.support.impl;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


/**
Reference: http://madx.dk/wowdev/wiki/index.php?title=DBC
перепилено под свои нужды
@author Dan Watling <dan@synaptik.com>
 */
public class DBC {

        byte[] signature = new byte[4];
        int nRecords;
        int nFields;
        int lRecord;
        int lStringBlock;
        byte[] data;
        byte[] stringData;
        
        private int[] indexOffset;
        
        public static DBC read(File f) throws Exception {
                FileInputStream fis = null;
                DBC result = null;
                try {
                        ByteBuffer bb = ByteBuffer.allocate((int)f.length());
                        fis = new FileInputStream(f);
                        fis.getChannel().read(bb);
                        bb.rewind();
                        
                        result = read(bb);
                } finally {
                        if (fis != null) {
                                fis.close();
                        }
                }
                return result;
        }
        
        public static DBC read(ByteBuffer bb) {
                bb.order(ByteOrder.LITTLE_ENDIAN);
                DBC result = new DBC();
                
                bb.get(result.signature);
                result.nRecords = bb.getInt();
                result.nFields = bb.getInt();
                result.lRecord = bb.getInt();
                result.lStringBlock = bb.getInt();
                
                result.data = new byte[result.nRecords * result.lRecord];
                bb.get(result.data);
                
                result.stringData = new byte[result.lStringBlock];
                bb.get(result.stringData);
                
                return result;
        }
        
        public int size() {
                return nRecords;
        }
        
        public byte[] getRecord(int index) {
                byte[] result = new byte[lRecord];
                
                System.arraycopy(data, index*lRecord, result, 0, lRecord);
                
                return result;
        }

    public void setIndexOffset(int[] indexOffset) {
        this.indexOffset = indexOffset;
    }
        
        
        
        
        
        public String getString(int record, int column) {
                int offset = getInt(record,column);
                
//                int[] data = getRecordAsIntArray(record);
                
//                int offset = data[column];
                
                String result = "";
                boolean done = false;
                while (!done) {
                        if (stringData[offset] != 0) {
                                byte b = stringData[offset++];
                                result += (char)b;
                        } else {
                                done = true;
                        }
                }
                
                return result;
        }
        
        public float getFloat(int record,int column){
             byte[] recordData = getRecord(record);
                int columnIdx = indexOffset[column];
                                
                return ByteBuffer.wrap(recordData).order(ByteOrder.LITTLE_ENDIAN).getFloat(columnIdx);
        }
        
        public int getInt(int record, int column) {               
                byte[] recordData = getRecord(record);
                int columnIdx = indexOffset[column];
                                
                return ByteBuffer.wrap(recordData).order(ByteOrder.LITTLE_ENDIAN).getInt(columnIdx);
        }
        
        public String toString() {
                return "signature: " + new String(signature) + 
                        "; nRecords: " + nRecords +
                        "; nFields: " + nFields + 
                        "; lRecord: " + lRecord +
                        "; lStringBlock: " + lStringBlock;
        }
}