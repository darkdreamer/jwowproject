/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.shared;

/**
 *
 * @author dreamer
 */
public enum LocaleConstant {

    LOCALE_enUS(0),
    LOCALE_koKR(1),
    LOCALE_frFR(2),
    LOCALE_deDE(3),
    LOCALE_zhCN(4),
    LOCALE_zhTW(5),
    LOCALE_esES(6),
    LOCALE_esMX(7),
    LOCALE_ruRU(8);
    private int intValue;

    LocaleConstant(int v) {
        intValue = v;
    }

    public int getInt() {
        return intValue;
    }
    private static final String localeNames[] = {
        "enUS",
        "koKR",
        "frFR",
        "deDE",
        "zhCN",
        "zhTW",
        "esES",
        "esMX",
        "ruRU"
    };

    public static LocaleConstant GetLocaleByName(String name) {
        for (int i = 0; i < localeNames.length; ++i) {
            if (name.equalsIgnoreCase(localeNames[i])) {
                return findLocaleById(i);
            }
        }

        return LOCALE_enUS;                                     // including enGB case
    }

    public static LocaleConstant findLocaleById(int id) {
        for (LocaleConstant lc : values()) {
            if (lc.getInt() == id) {
                return lc;
            }
        }
        return LOCALE_enUS;
    }
}
