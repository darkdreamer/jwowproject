/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.security;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;


/**
 *
 * @author dreamer
 */
public class AuthEncryption {

    final private int SEED_KEY_SIZE = 16;

    public AuthEncryption(BigInteger K) throws Exception {
//        K = new BigInteger("1821eb3440696f183ea782cb9aa70b2b57043ffb3c1506976c5cb6389ecadad70abe09cc9a543681", 16);
        
        int[] serverEncryptionKey = {0xCC, 0x98, 0xAE, 0x04, 0xE8, 0x97, 0xEA, 0xCA, 0x12, 0xDD, 0xC0, 0x93, 0x42, 0x91, 0x53, 0x57};//{0x08, 0xF1, 0x95, 0x9F, 0x47, 0xE5, 0xD2, 0xDB, 0xA1, 0x3D, 0x77, 0x8F, 0x3F, 0x3E, 0xE7, 0x00 };
        int serverDecryptionKey[] = {0xC2, 0xB3, 0x72, 0x3C, 0xC6, 0xAE, 0xD9, 0xB5, 0x34, 0x3C, 0x53, 0xEE, 0x2F, 0x43, 0x67, 0xCE};//{0x40, 0xAA, 0xD3, 0x92, 0x26, 0x71, 0x43, 0x47, 0x3A, 0x31, 0x08, 0xA6, 0xE7, 0xDC, 0x98, 0x2A };

        byte[] encryptionHash = computeHash(K, serverEncryptionKey);
        byte[] decryptionHash = computeHash(K, serverDecryptionKey);



//        _serverEncrypt = Cipher.getInstance("ARC4", "BC");
//        _serverEncrypt.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(encryptionHash, "ARC4"));

        serverEncrypt = new ARC4Engine(encryptionHash);
        
//        _clientDecrypt = Cipher.getInstance("ARC4", "BC");
//        _clientDecrypt.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(decryptionHash, "ARC4"));
        
        clientDecrypt = new ARC4Engine(decryptionHash);

        /*
        uint8 syncBuf[1024];
        memset(syncBuf, 0, 1024);
        
        _serverEncrypt.UpdateData(1024, syncBuf);
        //_clientEncrypt.UpdateData(1024, syncBuf);
        
        memset(syncBuf, 0, 1024);
        
        //_serverDecrypt.UpdateData(1024, syncBuf);
        _clientDecrypt.UpdateData(1024, syncBuf);
         */

        byte[] syncBuf = new byte[1024];
        Arrays.fill(syncBuf, (byte) 0);        
//        syncBuf = _serverEncrypt.doFinal(syncBuf);
        serverEncrypt.crypt(syncBuf);
        
        Arrays.fill(syncBuf, (byte) 0);  
//        syncBuf = _clientDecrypt.doFinal(syncBuf);
        clientDecrypt.crypt(syncBuf);
    }

    private byte[] computeHash(BigInteger K, int[] key) throws InvalidKeyException, IllegalStateException, NoSuchAlgorithmException {


        byte[] ServerEncryptionKey = new byte[SEED_KEY_SIZE];
        for (int i = 0; i < SEED_KEY_SIZE; i++) {
            ServerEncryptionKey[i] = (byte) key[i];
        }

        Mac mac = Mac.getInstance("HmacSha1");
        mac.init(new SecretKeySpec(ServerEncryptionKey, "HmacSha1"));
//        
//        byte[] k = K.toByteArray();
//        ArrayUtils.reverse(k);
        //mac.update(SRP6Token.bigIntToBA(K, 40));
        mac.update(SRP6Token.rev(K, 40));



        byte[] hash = mac.doFinal();

        BigInteger bigInteger = new BigInteger(1, hash);
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Hash [{0}] {1}", new Object[]{ K.toString(16), bigInteger.toString(16)});

        return hash;
    }

//            void Init(BigNumber* K);
//        void DecryptRecv(uint8 *, size_t);
//        void EncryptSend(uint8 *, size_t);
    boolean IsInitialized() {
        return _initialized;
    }
//    Cipher _clientDecrypt;
//    Cipher _serverEncrypt;
    ARC4Engine clientDecrypt;
    ARC4Engine serverEncrypt;
    boolean _initialized;

    void initializeCrypt(BigInteger k) {
    }

    public void encrypt(byte[] cb) {
//        try {
//            byte[] bb = _serverEncrypt.doFinal(cb);
//            bb = cb;
//        }catch (IllegalBlockSizeException ex) {
//            Logger.getLogger(AuthEncryption.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (BadPaddingException ex) {
//            Logger.getLogger(AuthEncryption.class.getName()).log(Level.SEVERE, null, ex);
//        }
        serverEncrypt.crypt(cb);
    }

    public void decrypt(byte[] cb) {
//        try {
//            _clientDecrypt.doFinal(ByteBuffer.wrap(cb), ByteBuffer.wrap(cb));
//        } catch (ShortBufferException ex) {
//            Logger.getLogger(AuthEncryption.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IllegalBlockSizeException ex) {
//            Logger.getLogger(AuthEncryption.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (BadPaddingException ex) {
//            Logger.getLogger(AuthEncryption.class.getName()).log(Level.SEVERE, null, ex);
//        }
        clientDecrypt.crypt(cb);
    }
}
