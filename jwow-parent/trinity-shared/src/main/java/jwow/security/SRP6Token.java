/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.security;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.logging.Logger;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author dreamer
 */
public class SRP6Token {

    private static final Logger logger = Logger.getLogger(SRP6Token.class.getName());
    final static int s_BYTE_SIZE = 32;
    public BigInteger N, s, g, v;
    public BigInteger b, B;
    public BigInteger K;
    public BigInteger A;

    public SRP6Token(BigInteger N, BigInteger g) {
        this.N = N;
        this.g = g;
    }

    public String getHexV() {
        return v.toString(16);
    }

    public String getHexS() {
        return s.toString(16);
    }

    public void setHexS(String hs) {
        s = new BigInteger(hs, 16);
    }

    public void setHexV(String hv) {
        v = new BigInteger(hv, 16);
    }

    public byte[] getB_32Bit() {
        return rev(B, 32);
    }

    public byte[] getN_32Bit() {
        return rev(N, 32);
    }

    public byte[] getS_32Bit() {
        return rev(s, 32);
    }

    public byte get_g_byte() {
        return bigIntToBA(g, 1)[0];
    }

    public static byte[] rev(BigInteger bi, int size) {
        return rev(bigIntToBA(bi, size));
    }

    static byte[] rev(byte[] b) {
        ArrayUtils.reverse(b);
        return b;
    }

    public static byte[] bigIntToBA(BigInteger bi, int size) {
        byte[] ba = bi.toByteArray();
        int mod = size - ba.length;
        if (mod > 0) {
            ba = ArrayUtils.addAll(new byte[mod], ba);
        } else if (mod < 0) {
            ba = Arrays.copyOfRange(ba, Math.abs(mod), ba.length);

        }
        return ba;
    }

    public void generateSV(String passwordSha) throws NoSuchAlgorithmException {
        s = BigInteger.probablePrime(s_BYTE_SIZE * 8, new SecureRandom());

        BigInteger I = new BigInteger(passwordSha, 16);

        MessageDigest sha1 = MessageDigest.getInstance("SHA1");
        int SHA_DIGEST_LENGTH = sha1.getDigestLength();

        byte[] mDigest = new byte[SHA_DIGEST_LENGTH];

        Arrays.fill(mDigest, (byte) 0);

        byte[] ba = I.toByteArray();
        if (I.bitLength() <= SHA_DIGEST_LENGTH) {
            for (int i = 0; i < ba.length; i++) {
                mDigest[i] = ba[i];
            }
        }

        ArrayUtils.reverse(mDigest);


        sha1.update(s.toByteArray());
        sha1.update(mDigest);

        BigInteger x = new BigInteger(sha1.digest());

        v = g.modPow(x, N);
    }

    public static byte[] getRandomUnk16() {
        BigInteger unk3 = BigInteger.probablePrime(16 * 9, new SecureRandom());
        return rev(unk3, 16);
    }

    public boolean calculateK(byte[] lpA) throws NoSuchAlgorithmException {


        logger.severe(Arrays.toString(lpA));

        A = new BigInteger(1, rev(lpA));

        if (A.equals(BigInteger.ZERO)) {
            return false;
        }
//
//    SHA1Hash sha;
        MessageDigest md = MessageDigest.getInstance("SHA1");
//    sha.UpdateBigNumbers(&A, &B, NULL);
        byte[] ba = bigIntToBA(A, 32);


        ArrayUtils.reverse(ba);
     //   logger.severe("aa=" + Arrays.toString(ba));

        md.update(ba);

        ba = bigIntToBA(B, 32);
        ArrayUtils.reverse(ba);
//        logger.severe("ab=" + Arrays.toString(ba));
        md.update(ba);
        ba = md.digest();
        ArrayUtils.reverse(ba);
        BigInteger u = new BigInteger(1, ba);
//        logger.severe("u=" + u.toString(16));

        BigInteger S = A.multiply(v.modPow(u, N)).modPow(b, N);
//        logger.severe("S=" + S.toString(16));

        byte[] t = bigIntToBA(S, 32);
        ArrayUtils.reverse(t);
        byte[] t1 = new byte[16];

        for (int i = 0; i < 16; ++i) {
            t1[i] = t[i * 2];
        }

//        logger.severe(Arrays.toString(t1));

        md.reset();
        md.update(t1);

        byte[] vK = new byte[40];
        byte[] tmpD = md.digest();
        for (int i = 0; i < 20; ++i) {
            vK[i * 2] = tmpD[i];
        }
//
        for (int i = 0; i < 16; ++i) {
            t1[i] = t[i * 2 + 1];
        }

        md.reset();
        md.update(t1);
        tmpD = md.digest();
//
        for (int i = 0; i < 20; ++i) {
            vK[i * 2 + 1] = tmpD[i];
        }

//        logger.severe("VK " + Arrays.toString(vK));
        ArrayUtils.reverse(vK);
        K = new BigInteger(1, vK);
//        logger.severe(K.toString(16));

        return true;
    }

    public byte[] calculateM(String _login) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA1");
        md.reset();
//    sha.UpdateBigNumbers(&N, NULL);
        byte[] babbb = bigIntToBA(N, 32);
        babbb = rev(babbb);
        md.update(babbb);
//    sha.Finalize();
//    memcpy(hash, sha.GetDigest(), 20);
        byte[] hash = Arrays.copyOf(md.digest(), 20);
//        logger.severe("H: " + Arrays.toString(hash));
//    sha.Initialize();
        md.reset();
//    sha.UpdateBigNumbers(&g, NULL);

        md.update(rev(g.toByteArray()));
//    sha.Finalize();
//
        byte[] tmpD = md.digest();
        for (int i = 0; i < 20; ++i) {
            hash[i] ^= tmpD[i];
        }

//        logger.severe("H: " + Arrays.toString(hash));

        byte[] t3 = hash;

        md.reset();

        md.update(_login.getBytes());

        byte[] t4 = md.digest();

//        logger.severe("t4: " + Arrays.toString(t4));

        md.reset();

        byte[] ba = t3;//.toByteArray();

        md.update(ba);


        md.reset();
        md.update(ba);
        md.update(t4);

        md.update(rev(s,32));


        md.update(rev(A,32));

        md.update(rev(B,32));


        md.update(rev(K,40));

        return md.digest();
    }

    public byte[] calculateM2(BigInteger M) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA1");

        md.reset();
        byte[] ba = bigIntToBA(A, 32);
        ArrayUtils.reverse(ba);
        md.update(ba);
        ba = bigIntToBA(M, 20);
        md.update(ba);
        ba = bigIntToBA(K, 40);
        ArrayUtils.reverse(ba);
        md.update(ba);        
        return md.digest();
    }

    public BigInteger calculateReconnectR(byte[] R1, String _login, BigInteger _reconnectProof) throws NoSuchAlgorithmException {
        BigInteger t1 = new BigInteger(rev(R1));
        
        MessageDigest sha = MessageDigest.getInstance("SHA1");

        sha.update(_login.getBytes());
        sha.update(rev(t1,16));
        sha.update(rev(_reconnectProof,16));
        sha.update(rev(K,40));
        
        return new BigInteger(1,sha.digest());
    }

    public void initializeBB() {
    
        //b = BigInteger.probablePrime(19 * 8, new SecureRandom());
        b = new BigInteger("f7c8be0f10a93599b4623de83a4bbbf8c4fcd5", 16);
//        logger.severe("N=" + N.toString(16));
//        logger.severe("b=" + b.toString(16));
        BigInteger gmod = g.modPow(b, N);
//        logger.severe("gmod=" + gmod.toString(16));
        B = v.multiply(new BigInteger("3")).add(gmod).mod(N);
//        logger.severe("B=" + B.toString(16));

        assert gmod.bitLength() <= 32;
    
    }
}
