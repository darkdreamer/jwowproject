/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.cached;

import jwow.cached.hibernate.NewHibernateUtil;
import jwow.datasources.LoginDatabase;
import jwow.entities.auth.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.PersistenceContext;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author dreamer
 */
@Transactional("auth")
public class LoginDatabaseImpl extends AbstractProxiedDatasource implements LoginDatabase{
   
    
    @Autowired
    @Qualifier("authSessionFactory")
    protected SessionFactory sessionFactory;

    protected SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    
    @Override
    public Account LOGIN_SEL_ACCOUNT_INFO_BY_NAME(String accountName) {
        Account res = (Account)sessionFactory.getCurrentSession().createQuery("select a from Account a where a.username=:uname ").setString("uname", accountName).uniqueResult();
        return unproxy(res);
    }

    
    
}
