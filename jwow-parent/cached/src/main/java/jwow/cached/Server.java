/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.cached;

import java.util.logging.Level;
import java.util.logging.Logger;
import jwow.datasources.CharacterDatabase;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author dreamer
 */
public class Server {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("jwow/cached/config/context.xml");
        context.start();
        System.out.println("Server started");
        while(true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
