/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.cached;

import java.util.List;
import jwow.datasources.CharacterDatabase;
import jwow.entities.characters.AccountData;
import org.hibernate.SessionFactory;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author dreamer
 */
@Transactional("character")
public class CharacterDatabaseImpl extends AbstractProxiedDatasource implements CharacterDatabase{

    @Autowired
    @Qualifier("characterSessionFactory")
    protected SessionFactory sessionFactory;

    protected SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    
    
    @Override
    public List<AccountData> getGlobalAccountData(int accountId) {
        return unproxy(sessionFactory.getCurrentSession().getNamedQuery("AccountData.findByAccountId").setInteger("accountId", accountId).list());
    }

    @Override
    public List<CharacterEnumInformation> charSelEnum(int account, int slot) {
        String sql =  "SELECT c.guid as guid,"
                    + "c.name as name, "
                    + "c.race as race, c.class as characterClass,"
                    + " c.gender as gender,"
                    + " c.playerBytes as playerBytes1, c.playerBytes2 as playerBytes2,"
                    + " c.level as level, c.zone as zone, c.map as map,"
                    + " c.position_x as posX, c.position_y as posY, c.position_z as posZ,"
                    + "gm.guildid as guildid, c.playerFlags as playerFlags,"
                    + "c.at_login as characterAtLogin, "
                    + "cp.entry as petEntry, cp.modelid as petModelId, cp.level as petLevel, "
                    + "c.equipmentCache as equipmentCache, cb.guid as bannedGuid,"
                    + "cd.genitive as declinedName "+
                     "FROM characters AS c LEFT JOIN character_pet AS cp ON c.guid = cp.owner AND cp.slot = ? LEFT JOIN guild_member AS gm ON c.guid = gm.guid "
                    + "LEFT JOIN character_declinedname AS cd ON c.guid = cd.guid "+
                     "LEFT JOIN character_banned AS cb ON c.guid = cb.guid AND cb.active = 1 WHERE c.account = ? AND c.deleteInfos_Name IS NULL ORDER BY c.guid";
        return sessionFactory.getCurrentSession().createSQLQuery(sql)
                .setInteger(0, slot).setInteger(1, account)
                .setResultTransformer(new AliasToBeanResultTransformer(CharacterEnumInformation.class))
                .list();
    }
    
    
    
}
