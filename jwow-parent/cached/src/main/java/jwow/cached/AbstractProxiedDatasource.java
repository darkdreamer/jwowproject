/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.cached;

import jwow.entities.auth.Account;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.PersistenceContext;
import org.hibernate.engine.spi.SessionImplementor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author dreamer
 */
public abstract class AbstractProxiedDatasource {
    
    protected abstract SessionFactory getSessionFactory();
    
    protected <T> T unproxy(T res) {
        if(res==null){
            return res;
        }
        
        
        PersistenceContext ctx= ((SessionImplementor)getSessionFactory().getCurrentSession()).getPersistenceContext();
        return (T)ctx.unproxy(res);        
    }
}
