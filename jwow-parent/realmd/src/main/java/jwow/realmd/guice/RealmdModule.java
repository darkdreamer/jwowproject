/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.realmd.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.multibindings.Multibinder;
import jwow.datasources.CharacterDatabase;
import jwow.datasources.LoginDatabase;
import jwow.game.core.AuthQueueManager;
import jwow.game.core.World;
import jwow.game.core.WorldImpl;
import jwow.realmd.engine.NioServer;
import jwow.realmd.engine.PacketHandler;
import jwow.realmd.engine.impl.NioServerImpl;
import jwow.realmd.engine.world.AuthQueueManagerImpl;
import jwow.realmd.handlers.AuthHandler;
import jwow.realmd.handlers.CharacterEnumerationHandler;
import jwow.realmd.handlers.NullHandler;
import jwow.realmd.handlers.PreGamingHandler;
import jwow.realmd.handlers.VoiceChatHandler;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

/**
 *
 * @author dreamer
 */
public class RealmdModule extends AbstractModule{

    @Override
    protected void configure() {
        Multibinder binder = Multibinder.newSetBinder(binder(), PacketHandler.class);
        binder.addBinding().to(AuthHandler.class);
        binder.addBinding().to(NullHandler.class);
        binder.addBinding().to(PreGamingHandler.class);
        binder.addBinding().to(CharacterEnumerationHandler.class);
        binder.addBinding().to(VoiceChatHandler.class);
        
        bind(World.class).to(WorldImpl.class).asEagerSingleton();
        
        bind(AuthQueueManager.class).to(AuthQueueManagerImpl.class).asEagerSingleton();
        
        bind(NioServer.class).to(NioServerImpl.class);
    }
    
    @Provides
    public LoginDatabase loginDatabase(){
        RmiProxyFactoryBean factory = new RmiProxyFactoryBean();
        factory.setServiceInterface(LoginDatabase.class);
        factory.setServiceUrl("rmi://127.0.0.1:1199/LoginDatabase");
        factory.setRefreshStubOnConnectFailure(true);
        factory.afterPropertiesSet();
        return (LoginDatabase)factory.getObject();
    }
    
    @Provides
    public CharacterDatabase characterDatabase(){
        RmiProxyFactoryBean factory = new RmiProxyFactoryBean();
        factory.setServiceInterface(CharacterDatabase.class);
        factory.setServiceUrl("rmi://127.0.0.1:1199/CharacterDatabase");
        factory.setRefreshStubOnConnectFailure(true);
        factory.afterPropertiesSet();
        return (CharacterDatabase)factory.getObject();
    }
    
}
