/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd;

import com.google.inject.Guice;
import com.google.inject.Injector;
import jwow.datasources.LoginDatabase;
import jwow.game.core.World;
import jwow.realmd.engine.NioServer;
import jwow.realmd.guice.RealmdModule;

/**
 *
 * @author dreamer
 */
public class Realmd {

    public static void main(String[] args) throws Exception {

        Injector injector = Guice.createInjector(new RealmdModule());

        LoginDatabase db = injector.getInstance(LoginDatabase.class);

        World w = injector.getInstance(World.class);

        startUpdateThread(w);

        NioServer server = injector.getInstance(NioServer.class);

        server.start();
    }

    private static void startUpdateThread(final World world) {
        class WorldRunnable implements Runnable {

            public void run() {
                long lastDate = System.currentTimeMillis();

                while (!world.isClosed()) {

                    long currentDate = System.currentTimeMillis();
                    long diff = currentDate - lastDate;
                    lastDate = currentDate;

                    world.update(diff);
                }
            }

        }
        
        new Thread(new WorldRunnable()).start();
    }

}
