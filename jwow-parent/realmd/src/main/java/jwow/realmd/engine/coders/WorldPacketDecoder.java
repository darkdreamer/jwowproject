/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine.coders;

import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;
import jwow.realmd.engine.Opcodes;
import jwow.realmd.engine.Packet;
import jwow.realmd.engine.SocketBoundWorldSession;
import jwow.realmd.engine.impl.DecalrativePacketHandler;
import jwow.security.AuthEncryption;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

/**
 *
 * @author dreamer
 */
public class WorldPacketDecoder extends FrameDecoder {

    private static final Logger logger = Logger.getLogger(WorldPacketDecoder.class.getName());
    
    private DecalrativePacketHandler decalrativePacketHandler;
    

    public WorldPacketDecoder(DecalrativePacketHandler decalrativePacketHandler) {
        this.decalrativePacketHandler=decalrativePacketHandler;
    }

    @Override
    protected Object decode(ChannelHandlerContext chc, Channel chnl, ChannelBuffer cb) throws Exception {
        if (cb.readableBytes() < 6) {
            return null;    //Если не можем прочесть длинну пакета, то и говорить не о чем
        }
        logger.severe("recieving packet");
        
        ChannelBuffer hdr = cb.readBytes(6);
        
        decodeHdr(hdr);
        
        short length = readBigEndianShort(hdr);
        length-=4;
        
        logger.log(Level.SEVERE, "Recieved header. Length: {0}", length);
        
        
        
        if(cb.readableBytes()<length){
            cb.resetReaderIndex();
            //Если пакет еще не получен полностью - сбрасываем инедкс и ждем новых данных
            return null;
        }
        //length-=4;
        int command;
        
        //byte[] revertedCmd = cb.readBytes(4).array();
        
        command = hdr.readInt();//revertAndDecodeCommand(revertedCmd);
        
        logger.severe("Recieved "+Opcodes.getOpcodeName((short)command));
        //length -= 4;    //Вычитаем длинну пакета
        
        if(length<0){
            logger.log(Level.SEVERE, "Bad packet recieved. Kick.");
            decalrativePacketHandler.getSession().kick();
            return null;
        }
        

        Packet pk = new Packet((short)command, cb.readBytes(length));
        logger.log(Level.SEVERE, "{0} bytes left.", cb.readableBytes());
        return pk;
    }

    private int revertAndDecodeCommand(byte[] revertedCmd) {        
        ChannelBuffer cb = ChannelBuffers.directBuffer(ByteOrder.LITTLE_ENDIAN, 4);
        cb.writeBytes(revertedCmd);
        
        int rv = cb.readInt();
        
        return rv;
    }
    
    private short readBigEndianShort(ChannelBuffer cb){
        ChannelBuffer bcb = ChannelBuffers.directBuffer(ByteOrder.BIG_ENDIAN, 2);
        
        bcb.writeBytes(cb.readBytes(2));
        
        return bcb.readShort();
    }

    private void decodeHdr(ChannelBuffer hdr) {
        
        SocketBoundWorldSession session = decalrativePacketHandler.getSession();
        if(session!=null){
            AuthEncryption ae = session.getEncryption();
            if(ae!=null){
                ae.decrypt(hdr.array());
            }
        }
        
    }
}
