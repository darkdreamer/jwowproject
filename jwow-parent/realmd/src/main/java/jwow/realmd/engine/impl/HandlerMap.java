/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine.impl;

import java.lang.reflect.Method;
import jwow.realmd.engine.PacketHandler;
import jwow.realmd.engine.annotations.PacketProcessing;
import jwow.realmd.engine.annotations.SessionStatus;

/**
 *
 * @author dreamer
 */
public class HandlerMap {
    public PacketHandler handlerObject;
    public Method handler;
    public PacketProcessing packetProcessing;
    public SessionStatus sessionStatus;
}
