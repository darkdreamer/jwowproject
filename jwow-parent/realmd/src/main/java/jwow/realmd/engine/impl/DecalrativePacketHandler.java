/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine.impl;

import java.lang.reflect.InvocationTargetException;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;
import jwow.game.session.ScheduledOpcodeTask;
import jwow.game.session.ScheduledOperationScope;
import jwow.security.SRP6Token;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import jwow.realmd.engine.Opcodes;
import jwow.realmd.engine.Packet;
import jwow.realmd.engine.SocketBoundWorldSession;
import jwow.realmd.engine.annotations.PacketProcessing;

/**
 *
 * @author dreamer
 */
public class DecalrativePacketHandler extends SimpleChannelUpstreamHandler {

    private static final Logger logger = Logger.getLogger(DecalrativePacketHandler.class.getName());
    HandlerMap[] handlers;
    SocketBoundWorldSession session;

    public DecalrativePacketHandler(HandlerMap[] handlers) {
        this.handlers = handlers;

    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelOpen(ctx, e);

        session = new SocketBoundWorldSession(ctx.getChannel());

        ChannelBuffer cb = ChannelBuffers.dynamicBuffer(ByteOrder.LITTLE_ENDIAN, 24);
        Packet pk = new Packet(Opcodes.SMSG_AUTH_CHALLENGE, cb);
        pk.session = session;
        //cb.writeByte(Opcodes.SMSG_AUTH_CHALLENGE);
        cb.writeInt(1);
        cb.writeInt(session.getSeed());

        cb.writeBytes(SRP6Token.getRandomUnk16());
        cb.writeBytes(SRP6Token.getRandomUnk16());

        ctx.getChannel().write(pk);
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        super.messageReceived(ctx, e);
        handlePacket((Packet) e.getMessage());
    }

    public void handlePacket(final Packet pk) throws Exception {
        final HandlerMap handler = handlers[pk.opcode];
        pk.session = session;
        if (handler != null) {
            if (PacketProcessing.PROCESS_THREADSAFE.equals(handler.packetProcessing)) {
                ScheduledOpcodeTask task = new ScheduledOpcodeTask(ScheduledOperationScope.MapUpdate, new OpcodeHandlerRunnable(handler, pk),session);
                session.scheduleCommand(task);
            } else if (PacketProcessing.PROCESS_THREADUNSAFE.equals(handler.packetProcessing)) {
                ScheduledOpcodeTask task = new ScheduledOpcodeTask(ScheduledOperationScope.SessionUpdate, new OpcodeHandlerRunnable(handler, pk),session);
                session.scheduleCommand(task);
            }else{
                ScheduledOpcodeTask task = new ScheduledOpcodeTask(ScheduledOperationScope.Inplace, new OpcodeHandlerRunnable(handler, pk),session);
                //session.scheduleCommand(task);
                task.getRunnable().run();
            }
        } else {
            logger.severe("Opcode: " + Opcodes.getOpcodeName(pk.opcode) + " unhandled");
        }
    }

    public class OpcodeHandlerRunnable implements Runnable {

        private HandlerMap handler;
        private Packet pk;

        public OpcodeHandlerRunnable(HandlerMap handler, Packet pk) {
            this.handler = handler;
            this.pk = pk;
        }
        
        

        public void run() {
            try {
                handler.handler.invoke(handler.handlerObject, pk);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(DecalrativePacketHandler.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(DecalrativePacketHandler.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(DecalrativePacketHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    

    public SocketBoundWorldSession getSession() {
        return session;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        logger.log(Level.SEVERE, null, e.getCause());
        e.getChannel().close();
    }
}
