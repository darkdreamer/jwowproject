/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;

/**
 *
 * @author dreamer
 */
public class Packet {
    private static final Logger logger = Logger.getLogger(Packet.class.getName());
    
    public  short opcode;
    public ChannelBuffer body;
    public SocketBoundWorldSession session;

    public Packet(short opcode, ChannelBuffer body) {
        this.opcode = opcode;
        this.body = body;
        
         ArrayList<Byte> ba = new ArrayList<Byte>();
            while (body.readableBytes() > 0) {
                ba.add(body.readByte());
            }
            StringBuilder bldr = new StringBuilder();
            for(int i =0;i<ba.size();i++){
                if(i!=0 && i%16==0){
                    bldr.append('\n');
                }
                bldr.append(String.format("%02X ", ba.get(i)) );
            }
        body.resetReaderIndex();
        logger.log(Level.SEVERE, "new packet got {0}\n{1}\n{2}", new Object[]{Opcodes.getOpcodeName(opcode),Arrays.toString(ba.toArray(new Byte[0])),bldr.toString()
        });
    }

    public Packet(SocketBoundWorldSession session) {
        this.session = session;
    }
    
    public void initialize(short opcode,int size){
        this.opcode = opcode;
        body = ChannelBuffers.dynamicBuffer(ByteOrder.LITTLE_ENDIAN, size);
    }
    
}
