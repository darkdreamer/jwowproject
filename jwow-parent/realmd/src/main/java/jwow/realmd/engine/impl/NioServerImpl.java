/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine.impl;

import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.nio.ByteOrder;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import javax.inject.Inject;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.buffer.HeapChannelBufferFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import jwow.realmd.engine.NioServer;
import jwow.realmd.engine.Opcodes;
import jwow.realmd.engine.PacketHandler;
import jwow.realmd.engine.annotations.HandlerMapping;

/**
 *
 * @author dreamer
 */
public class NioServerImpl implements NioServer {

    private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(NioServerImpl.class.getName());

    HandlerMap[] handlers = new HandlerMap[Opcodes.NUM_MSG_TYPES];

    @Inject
    public NioServerImpl(Set<PacketHandler> handlerMappings) {
        for (PacketHandler handler : handlerMappings) {
            for (Method m : handler.getClass().getMethods()) {
                if (m.isAnnotationPresent(HandlerMapping.class)) {
                    processHandler(handler, m);
                }
            }
        }
    }

    private void processHandler(PacketHandler handler, Method m) {
        HandlerMapping mapping = m.getAnnotation(HandlerMapping.class);

        for (short opcode : mapping.opcodes()) {
            if (handlers[opcode] == null) {
                HandlerMap handlerMap = new HandlerMap();
                handlerMap.handler = m;
                handlerMap.handlerObject = handler;
                handlerMap.packetProcessing = mapping.processing();
                handlerMap.sessionStatus = mapping.sessionStatus();
                handlers[opcode] = handlerMap;

                logger.log(Level.INFO, "Installed {0} opcode handler to {1}", new Object[]{opcode, handler.getClass().getName()});
            } else {
                logger.log(Level.SEVERE, "Declarative handler creation error. Opcode handler for {0} already set to {1}. Duplicate Handler {2}",
                        new Object[]{opcode, handlers[opcode].handlerObject.getClass().getName(), handler.getClass().getName()});
                throw new RuntimeException("Duplicate handlers not supported");
            }
        }
    }

    public void start() {

        ServerBootstrap server = new ServerBootstrap(new NioServerSocketChannelFactory(Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
        server.setOption("child.bufferFactory", new HeapChannelBufferFactory(ByteOrder.LITTLE_ENDIAN));
        server.setPipelineFactory(new WorldPipelineFactory(handlers));

        logger.severe("Started...");

        server.bind(new InetSocketAddress(8085));
    }


}
