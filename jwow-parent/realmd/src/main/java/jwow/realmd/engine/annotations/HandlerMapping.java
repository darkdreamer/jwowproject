/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author dreamer
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface HandlerMapping {
    short[] opcodes();
    PacketProcessing processing();
    SessionStatus sessionStatus();
}
