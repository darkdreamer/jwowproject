/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine.annotations;

/**
 *
 * @author dreamer
 */
public enum PacketProcessing {
    PROCESS_INPLACE,                                    //process packet whenever we receive it - mostly for non-handled or non-implemented packets
    PROCESS_THREADUNSAFE,                                   //packet is not thread-safe - process it in World::UpdateSessions()
    PROCESS_THREADSAFE                                      //packet is thread-safe - process it in Map::Update()
}
