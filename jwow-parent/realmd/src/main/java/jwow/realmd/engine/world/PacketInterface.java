/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine.world;

import java.util.List;
import jwow.entities.characters.AccountData;
import jwow.game.session.WorldSession;
import jwow.realmd.engine.Opcodes;
import jwow.realmd.engine.Packet;
import jwow.realmd.engine.ResponseCodes;
import jwow.realmd.engine.SocketBoundWorldSession;
import jwow.realmd.handlers.AddonInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dreamer
 */
public class PacketInterface {

    private static final Logger logger = LoggerFactory.getLogger(PacketInterface.class);

    public void sendAuthWaitQue(WorldSession _worldSession, int position) {
        SocketBoundWorldSession worldSession = (SocketBoundWorldSession) _worldSession;
        if (position == 0) {
            Packet pk = worldSession.createPacket();
            pk.initialize(Opcodes.SMSG_AUTH_RESPONSE, 1);
            pk.body.writeByte(ResponseCodes.AUTH_OK);
            worldSession.send(pk);
        } else {
            Packet pk = worldSession.createPacket();
            pk.initialize(Opcodes.SMSG_AUTH_RESPONSE, 6);
            pk.body.writeByte(ResponseCodes.AUTH_WAIT_QUEUE);
            pk.body.writeByte(position);
            pk.body.writeByte(0);
            worldSession.send(pk);
        }
    }

    public void sendAddonsInfo(WorldSession _worldSession) {
        SocketBoundWorldSession worldSession = (SocketBoundWorldSession) _worldSession;
        final int[] addonPublicKey = new int[]{
            0xC3, 0x5B, 0x50, 0x84, 0xB9, 0x3E, 0x32, 0x42, 0x8C, 0xD0, 0xC7, 0x48, 0xFA, 0x0E, 0x5D, 0x54,
            0x5A, 0xA3, 0x0E, 0x14, 0xBA, 0x9E, 0x0D, 0xB9, 0x5D, 0x8B, 0xEE, 0xB6, 0x84, 0x93, 0x45, 0x75,
            0xFF, 0x31, 0xFE, 0x2F, 0x64, 0x3F, 0x3D, 0x6D, 0x07, 0xD9, 0x44, 0x9B, 0x40, 0x85, 0x59, 0x34,
            0x4E, 0x10, 0xE1, 0xE7, 0x43, 0x69, 0xEF, 0x7C, 0x16, 0xFC, 0xB4, 0xED, 0x1B, 0x95, 0x28, 0xA8,
            0x23, 0x76, 0x51, 0x31, 0x57, 0x30, 0x2B, 0x79, 0x08, 0x50, 0x10, 0x1C, 0x4A, 0x1A, 0x2C, 0xC8,
            0x8B, 0x8F, 0x05, 0x2D, 0x22, 0x3D, 0xDB, 0x5A, 0x24, 0x7A, 0x0F, 0x13, 0x50, 0x37, 0x8F, 0x5A,
            0xCC, 0x9E, 0x04, 0x44, 0x0E, 0x87, 0x01, 0xD4, 0xA3, 0x15, 0x94, 0x16, 0x34, 0xC6, 0xC2, 0xC3,
            0xFB, 0x49, 0xFE, 0xE1, 0xF9, 0xDA, 0x8C, 0x50, 0x3C, 0xBE, 0x2C, 0xBB, 0x57, 0xED, 0x46, 0xB9,
            0xAD, 0x8B, 0xC6, 0xDF, 0x0E, 0xD6, 0x0F, 0xBE, 0x80, 0xB3, 0x8B, 0x1E, 0x77, 0xCF, 0xAD, 0x22,
            0xCF, 0xB7, 0x4B, 0xCF, 0xFB, 0xF0, 0x6B, 0x11, 0x45, 0x2D, 0x7A, 0x81, 0x18, 0xF2, 0x92, 0x7E,
            0x98, 0x56, 0x5D, 0x5E, 0x69, 0x72, 0x0A, 0x0D, 0x03, 0x0A, 0x85, 0xA2, 0x85, 0x9C, 0xCB, 0xFB,
            0x56, 0x6E, 0x8F, 0x44, 0xBB, 0x8F, 0x02, 0x22, 0x68, 0x63, 0x97, 0xBC, 0x85, 0xBA, 0xA8, 0xF7,
            0xB5, 0x40, 0x68, 0x3C, 0x77, 0x86, 0x6F, 0x4B, 0xD7, 0x88, 0xCA, 0x8A, 0xD7, 0xCE, 0x36, 0xF0,
            0x45, 0x6E, 0xD5, 0x64, 0x79, 0x0F, 0x17, 0xFC, 0x64, 0xDD, 0x10, 0x6F, 0xF3, 0xF5, 0xE0, 0xA6,
            0xC3, 0xFB, 0x1B, 0x8C, 0x29, 0xEF, 0x8E, 0xE5, 0x34, 0xCB, 0xD1, 0x2A, 0xCE, 0x79, 0xC3, 0x9A,
            0x0D, 0x36, 0xEA, 0x01, 0xE0, 0xAA, 0x91, 0x20, 0x54, 0xF0, 0x72, 0xD8, 0x1E, 0xC7, 0x89, 0xD2
        };

//        
//        WorldPacket data(SMSG_ADDON_INFO, 4);
        Packet pk = worldSession.createPacket();
        pk.initialize(Opcodes.SMSG_ADDON_INFO, 4);
//
//    for (AddonsList::iterator itr = m_addonsList.begin(); itr != m_addonsList.end(); ++itr)
//    {
        for (AddonInfo add : worldSession.getAddonList()) {
//        data << uint8(itr->State);
            pk.body.writeByte(add.getState());
//
//        uint8 crcpub = itr->UsePublicKeyOrCRC;
//        data << uint8(crcpub);
            pk.body.writeByte(add.isHasPKorCRC() ? 1 : 0);
//        if (crcpub)
            if (add.isHasPKorCRC()) {
//            uint8 usepk = (itr->CRC != STANDARD_ADDON_CRC); // If addon is Standard addon CRC
                int usepk = add.getCrc() != 0x4c1c776d ? 1 : 0;
//            data << uint8(usepk);
                pk.body.writeByte(usepk);
//            if (usepk)                                      // if CRC is wrong, add public key (client need it)
                if (usepk != 0) {
//                sLog->outDetail("ADDON: CRC (0x%x) for addon %s is wrong (does not match expected 0x%x), sending pubkey",
//                    itr->CRC, itr->Name.c_str(), STANDARD_ADDON_CRC);
//
//                data.append(addonPublicKey, sizeof(addonPublicKey));
                    for (int i = 0; i < addonPublicKey.length; i++) {
                        pk.body.writeByte(addonPublicKey[i]);
                    }
                }
//
//            data << uint32(0);                              // TODO: Find out the meaning of this.
                pk.body.writeInt(0);
            }
//
//        uint8 unk3 = 0;                                     // 0 is sent here
//        data << uint8(unk3);
            pk.body.writeByte(0);
//        if (unk3)
//        {
//            // String, length 256 (null terminated)
//            data << uint8(0);
//        }
        }
//
//    m_addonsList.clear();
        worldSession.getAddonList().clear();
//
//    data << uint32(0); // count for an unknown for loop
//
        pk.body.writeInt(0);
//    SendPacket(&data);
        worldSession.send(pk);
    }

    public void sendAccountDataTimes(WorldSession _worldSession, CacheMask cacheMask) {
        //        WorldPacket data(SMSG_ACCOUNT_DATA_TIMES, 4 + 1 + 4 + 8 * 4); // changed in WotLK
        SocketBoundWorldSession worldSession = (SocketBoundWorldSession) _worldSession;
        Packet pk = worldSession.createPacket();
        pk.initialize(Opcodes.SMSG_ACCOUNT_DATA_TIMES, 4 + 1 + 4 + 8 * 4);
//    data << uint32(time(NULL));                             // unix time of something
        pk.body.writeInt((int) System.currentTimeMillis());
//    data << uint8(1);
        pk.body.writeByte(1);
//    data << uint32(mask);                                   // type mask
        pk.body.writeInt(cacheMask.getId());
        List<AccountData> ads = worldSession.getAccountData();
        for (int i = 0; i < ads.size(); i++) {
            if ((cacheMask.getId() & (1 << i)) != 0) {
                pk.body.writeInt(ads.get(i).getTime());
            }
        }
        worldSession.send(pk);

//    for (uint32 i = 0; i < NUM_ACCOUNT_DATA_TYPES; ++i)
//        if (mask & (1 << i))
//            data << uint32(GetAccountData(AccountDataType(i))->Time);// also unix time
//    SendPacket(&data);
    }

    public void sendClientCacheVersion(WorldSession _worldSession, Integer clientCacheVersion) {
        SocketBoundWorldSession worldSession = (SocketBoundWorldSession) _worldSession;
        Packet pk = worldSession.createPacket();

        pk.initialize(Opcodes.SMSG_CLIENTCACHE_VERSION, 4);
        pk.body.writeInt(clientCacheVersion);

        worldSession.send(pk);
    }

    public void sendTutorialsData(WorldSession poped_sess) {
        logger.error("Not supported yet");

    }

    public void sendAuthResponse(WorldSession _worldSession, int AUTH_OK) {
        SocketBoundWorldSession worldSession = (SocketBoundWorldSession) _worldSession;
        Packet pk = worldSession.createPacket();
        pk.initialize(Opcodes.SMSG_AUTH_RESPONSE, 1 + 4 + 1 + 4 + 1);

        pk.body.writeByte(AUTH_OK);
        pk.body.writeInt(0);  // BillingTimeRemaining
        pk.body.writeByte(0);// BillingPlanFlags
        pk.body.writeInt(0);// BillingTimeRested
        pk.body.writeByte(worldSession.getAccount().getExpansion()); // 0 - normal, 1 - TBC, 2 - WOTLK, must be set in database manually for each account

        worldSession.send(pk);

    }
}
