/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine.annotations;

/**
 *
 * @author dreamer
 */
public enum SessionStatus {
    STATUS_AUTHED,                                      // Player authenticated (_player == NULL, m_playerRecentlyLogout = false or will be reset before handler call, m_GUID have garbage)
    STATUS_LOGGEDIN,                                        // Player in game (_player != NULL, m_GUID == _player->GetGUID(), inWorld())
    STATUS_TRANSFER,                                        // Player transferring to another map (_player != NULL, m_GUID == _player->GetGUID(), !inWorld())
    STATUS_LOGGEDIN_OR_RECENTLY_LOGGOUT,                    // _player!= NULL or _player == NULL && m_playerRecentlyLogout, m_GUID store last _player guid)
    STATUS_NEVER                                           // Opcode not accepted from client (deprecated or server side only)
}
