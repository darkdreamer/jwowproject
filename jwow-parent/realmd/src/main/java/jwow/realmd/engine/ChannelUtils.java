/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.realmd.engine;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;

/**
 *
 * @author dreamer
 */
public class ChannelUtils {
      public static String readString(ChannelBuffer cb) {
        ChannelBuffer sb = ChannelBuffers.dynamicBuffer();

        while (cb.readerIndex() < cb.readableBytes()) {
            byte c = cb.readByte();
            if (c == 0) {
                break;
            } else {
                sb.writeByte(c);
            }
        }

        return new String(sb.readBytes(sb.writerIndex()).array());
    }
}
