/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine.world;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Queue;
import javax.inject.Inject;
import jwow.game.core.AuthQueueManager;
import jwow.game.session.WorldSession;
import jwow.realmd.engine.ResponseCodes;

/**
 *
 * @author dreamer
 */
public class AuthQueueManagerImpl implements AuthQueueManager {

    
    Integer m_playerLimit=0;//TODO: PLAYER LIMIT
//    @Inject
//    WorldConfig worldConfig;
    @Inject
    PacketInterface packetInterface;
    private Queue<WorldSession> playerQueue;

    public AuthQueueManagerImpl() {
        playerQueue = new ArrayDeque<WorldSession>();
    }

    public int playersInQueue() {
        return playerQueue.size();
    }

    public boolean removeSessionFromQueue(WorldSession ws, int sessions) {
        //           uint32 sessions = GetActiveSessionCount();
//
//    uint32 position = 1;
        int position = 1;
//    Queue::iterator iter = m_QueuedPlayer.begin();
        Iterator<WorldSession> iter = playerQueue.iterator();
//
//    // search to remove and count skipped positions
//    bool found = false;
        boolean found = false;
//
        WorldSession iteratedSession;

        while (iter.hasNext()) //    for (; iter.hasNext(); iter.next(), ++position)
        {
            WorldSession iSession = iter.next();
            if (iSession.equals(ws)) {
//            sess->SetInQueue(false);
                ws.setInQueue(false);
//            sess->ResetTimeOutTime();
                ws.resetTimeOutTime();
//            iter = m_QueuedPlayer.erase(iter);
                iter.remove();
//            found = true;                                   // removing queued session
                found = true;
//            break;
                break;
            }
            position++;
        }
//
//    // iter point to next socked after removed or end()
//    // position store position of removed socket and then new position next socket after removed
//
//    // if session not queued then we need decrease sessions count
//    if (!found && sessions)
//        --sessions;

        if (!found && sessions != 0) {
            --sessions;
        }
//      //    // accept first in queue
//    if ((!m_playerLimit || sessions < m_playerLimit) && !m_QueuedPlayer.empty())

        if ((m_playerLimit == 0 || sessions < m_playerLimit) && !playerQueue.isEmpty()) {
//        WorldSession* pop_sess = m_QueuedPlayer.front();
            WorldSession poped_sess = playerQueue.remove();
//        pop_sess->SetInQueue(false);
            poped_sess.setInQueue(false);

            passSessionToRealm(poped_sess, true);

//
//        // update iter to point first queued socket or end() if queue is empty now
//        iter = m_QueuedPlayer.begin();
            iter = playerQueue.iterator();
//        position = 1;
            position = 1;
        }

        while (iter.hasNext()) {
            iteratedSession = iter.next();
            packetInterface.sendAuthWaitQue(iteratedSession, position);
            position++;
        }
//

        return found;
    }

    private void passSessionToRealm(WorldSession poped_sess, boolean cameFromQueue) {
        //        pop_sess->ResetTimeOutTime();
        poped_sess.resetTimeOutTime();
        //        pop_sess->SendAuthWaitQue(0);
        
        if (cameFromQueue) {
            packetInterface.sendAuthWaitQue(poped_sess, 0);
        } else {
            packetInterface.sendAuthResponse(poped_sess, ResponseCodes.AUTH_OK);
        }
        packetInterface.sendAddonsInfo(poped_sess);
        packetInterface.sendClientCacheVersion(poped_sess, 52);//worldConfig.getClientCacheVersion());
        
        packetInterface.sendTutorialsData(poped_sess);

    }

    public void updateQueue(int count) {
    }

    public void addSessionToQueue(WorldSession session, int worldSessionCount) {
        if ((m_playerLimit == 0 || worldSessionCount < m_playerLimit)) {
            passSessionToRealm(session, false);
        } else {
            playerQueue.add(session);
            session.setInQueue(true);
            packetInterface.sendAuthWaitQue(session, playerQueue.size());
        }

    }
}
