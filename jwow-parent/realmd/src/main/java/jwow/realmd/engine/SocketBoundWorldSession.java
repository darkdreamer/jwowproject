/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine;

import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import jwow.entities.auth.Account;
import jwow.entities.characters.AccountData;
import jwow.game.player.Player;
import jwow.game.session.PacketFilter;
import jwow.game.session.ScheduledOpcodeTask;
import jwow.game.session.WorldSession;
import jwow.realmd.handlers.AddonInfo;
import jwow.security.AuthEncryption;
import org.jboss.netty.channel.Channel;


/**
 *
 * @author dreamer
 */
public class SocketBoundWorldSession implements WorldSession {

    private static final SecureRandom rand = new SecureRandom();
    AuthEncryption encryption;
    Channel underlyingChannel;
    List<AccountData> accountData;
    Set<Integer> allowedCharacters;
    boolean m_playerLoading = false;
    boolean m_inQueue = false;
    int seed;
    long m_timeOutTime;
    
    private Account account;
    private Player player;
    
    long _logoutTime = 0;

    public SocketBoundWorldSession(Channel underlyingChannel) {
        this.underlyingChannel = underlyingChannel;
        seed = rand.nextInt(999999);
    }

    public AuthEncryption getEncryption() {
        return encryption;
    }

    public void initializeAccount(Account account){
        this.account=account;
    }
    
    public void initializePlayer(Player player){
        this.player=player;
    }

    @Override
    public Account getAccount() {
        return account;
    }

    @Override
    public Player getPlayer() {
        return player;
    }
    
    
    
    public SocketBoundWorldSession() {
    }

    public void initializeCrypto(BigInteger k) throws Exception {
        encryption = new AuthEncryption(k);
    }

    public Packet createPacket() {
        return new Packet(this);
    }

    public void send(Packet answerPk) {
        underlyingChannel.write(answerPk);
    }

    public void terminate() {
        underlyingChannel.close();
    }

    public String getAddress() {
        return ((InetSocketAddress) underlyingChannel.getRemoteAddress()).getAddress().getHostAddress();
    }

    public int getSeed() {
        return seed;
    }

    public void kick() {
        terminate();
        underlyingChannel = null;
    }

    public boolean isPlayerLoading() {
        return m_playerLoading;
    }

    public boolean hasChannel() {
        return underlyingChannel != null;
    }

    public void setInQueue(boolean b) {
        m_inQueue = b;
    }

    public boolean isInQueue() {
        return m_inQueue;
    }

    public void resetTimeOutTime() {
        m_timeOutTime = 900000;
    }

    public Set<Integer> getAllowedCharacters() {
        return allowedCharacters;
    }

    public void setAllowedCharacters(Set<Integer> allowedCharacters) {
        this.allowedCharacters = allowedCharacters;
    }
    private Queue<ScheduledOpcodeTask> scheduledOpcodes = new ConcurrentLinkedQueue<ScheduledOpcodeTask>();

    public boolean update(long delta, PacketFilter filter) {
        Iterator<ScheduledOpcodeTask> queueIterator = scheduledOpcodes.iterator();


        while (queueIterator.hasNext()) {
            ScheduledOpcodeTask task = queueIterator.next();
            if (filter.Process(task)) {
                task.getRunnable().run();
                queueIterator.remove();
            }
        }

        if (hasChannel() && !underlyingChannel.isOpen()) {
            underlyingChannel = null;
        }

        if (filter.ProcessLogout()) {
            ///- If necessary, log the player out
            long currTime = System.currentTimeMillis()/1000;
            if (!hasChannel() || (ShouldLogOut(currTime) && !m_playerLoading)) {
                LogoutPlayer(true);
            }

            if (!hasChannel()) {
                return false;                                   // Will remove this session from the world session map
            }
        }
        
        return true;

    }

    public void scheduleCommand(ScheduledOpcodeTask task) {
        scheduledOpcodes.add(task);
    }

    private boolean ShouldLogOut(long currTime) {
        return _logoutTime>0 && (_logoutTime >= currTime+20);
    }

    private void LogoutPlayer(boolean b) {
        throw new UnsupportedOperationException();
    }

    public void setAccountData(List<AccountData> ad) {
        accountData=ad;
    }

    public List<AccountData> getAccountData() {
        return accountData;
    }
    private List<AddonInfo> addonList;

    public List<AddonInfo> getAddonList() {
        return addonList;
    }

    public void setAddonList(List<AddonInfo> addonList) {
        this.addonList = addonList;
    }
    
    public void beginPlayerLoading(){
        m_playerLoading=true;
    }
    
    
    
    
}
