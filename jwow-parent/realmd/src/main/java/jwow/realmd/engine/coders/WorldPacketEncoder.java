/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine.coders;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.logging.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import jwow.realmd.engine.Opcodes;
import jwow.realmd.engine.Packet;
import jwow.realmd.engine.impl.DecalrativePacketHandler;

/**
 *
 * @author dreamer
 */
public class WorldPacketEncoder extends OneToOneEncoder {

    private static final Logger logger = Logger.getLogger(WorldPacketEncoder.class.getName());

    public WorldPacketEncoder(DecalrativePacketHandler decalrativePacketHandler) {
        
    }
   
    
    @Override
    protected Object encode(ChannelHandlerContext chc, Channel chnl, Object o) throws Exception {
        if (o instanceof Packet) {
            logger.severe("pure answer");
            
            Packet pk = (Packet)o;

            
            ChannelBuffer serverPk = createServerPacket(pk);
            
            

            ArrayList<Byte> ba = new ArrayList<Byte>();
            while (serverPk.readableBytes() > 0) {
                ba.add(serverPk.readByte());
            }
            StringBuilder bldr = new StringBuilder();
            for(int i =0;i<ba.size()-4;i++){
                if(i!=0 && i%16==0){
                    bldr.append('\n');
                }
                bldr.append(String.format("%02X ", ba.get(i+4)) );
            }
 
            logger.severe("Sent: " + Opcodes.getOpcodeName(pk.opcode)+"Size: "+ba.size()+"Content:\n"+bldr);
            serverPk.resetReaderIndex();


            return serverPk;
        }
        else
            logger.severe("error..");
        return null;
    }

    private ChannelBuffer createServerPacket(Packet pk) {
        ChannelBuffer cb = ChannelBuffers.dynamicBuffer(ByteOrder.LITTLE_ENDIAN, 25);
        int size = pk.body.writerIndex()+2;
        int cmd = pk.opcode;
        
        //cb.writeInt(size);
        
        if(size > 0x7FFF){
             cb.writeByte(0x80|(0xFF &(size>>16)));
        }
        
        cb.writeByte( 0xFF &(size>>8));
        cb.writeByte( 0xFF &(0xFF &size));
        cb.writeByte( 0xFF &(0xFF & cmd));
        cb.writeByte( 0xFF &(0xFF & (cmd>>8)));
        byte[] data = cb.readBytes(cb.writerIndex()).array();
        
        if(pk.session.getEncryption()!=null){
            
            pk.session.getEncryption().encrypt(data);
        }
        
        
        
        ChannelBuffer cb2 = ChannelBuffers.dynamicBuffer();
        int hdrLen =  2+( (size > 0x7FFF)?3:2);
        
        cb2.writeBytes(data, 0, hdrLen);
        
        cb2.writeBytes(pk.body);
        return cb2;
    }
    
    /*
         ServerPktHeader(uint32 size, uint16 cmd) : size(size)
    {
        uint8 headerIndex=0;
        if (isLargePacket())
        {
            sLog->outDebug(LOG_FILTER_NETWORKIO, "initializing large server to client packet. Size: %u, cmd: %u", size, cmd);
            header[headerIndex++] = 0x80|(0xFF &(size>>16));
        }
        header[headerIndex++] = 0xFF &(size>>8);
        header[headerIndex++] = 0xFF &size;

        header[headerIndex++] = 0xFF & cmd;
        header[headerIndex++] = 0xFF & (cmd>>8);
    }

    uint8 getHeaderLength()
    {
        // cmd = 2 bytes, size= 2||3bytes
        return 2+(isLargePacket()?3:2);
    }

    bool isLargePacket()
    {
        return size > 0x7FFF;
    }

    const uint32 size;
    uint8 header[5];
     */
}
