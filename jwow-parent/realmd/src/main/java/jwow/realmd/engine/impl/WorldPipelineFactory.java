/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.engine.impl;

import jwow.realmd.engine.coders.WorldPacketDecoder;
import jwow.realmd.engine.coders.WorldPacketEncoder;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;

/**
 *
 * @author dreamer
 */
public class WorldPipelineFactory implements ChannelPipelineFactory {

    HandlerMap[] handlers;

    public WorldPipelineFactory(HandlerMap[] handlers) {
        this.handlers = handlers;
    }
    
    
    
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline pipe = Channels.pipeline();
        
        DecalrativePacketHandler decalrativePacketHandler = new DecalrativePacketHandler(handlers);
        
        pipe.addLast("decoder", new WorldPacketDecoder(decalrativePacketHandler));
        pipe.addLast("encoder", new WorldPacketEncoder(decalrativePacketHandler));
        
        pipe.addLast("handler",decalrativePacketHandler );
        
        return pipe;
    }
    
}
