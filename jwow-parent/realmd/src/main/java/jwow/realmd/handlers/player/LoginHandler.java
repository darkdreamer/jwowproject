/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.handlers.player;

import jwow.realmd.handlers.*;
import java.awt.Component;
import javax.inject.Inject;
import jwow.datasources.AsyncCharacterDatabase;
import jwow.datasources.async.AsyncCallback;
import jwow.entities.aggregate.LoginPlayerInfo;
import jwow.game.guid.GuidUtil;
import jwow.realmd.engine.Opcodes;
import jwow.realmd.engine.Packet;
import jwow.realmd.engine.PacketHandler;
import jwow.realmd.engine.SocketBoundWorldSession;
import jwow.realmd.engine.annotations.HandlerMapping;
import jwow.realmd.engine.annotations.PacketProcessing;
import jwow.realmd.engine.annotations.SessionStatus;
import jwow.realmd.engine.world.CacheMask;
import jwow.realmd.engine.world.PacketInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dreamer
 */
public class LoginHandler implements PacketHandler  {
    private Logger logger = LoggerFactory.getLogger(getClass());
    
    @Inject
    PacketInterface packetInterface;
    @Inject
    private AsyncCharacterDatabase asyncCharacterDatabase;
    
    
    @HandlerMapping(processing = PacketProcessing.PROCESS_THREADUNSAFE, sessionStatus = SessionStatus.STATUS_AUTHED,
            opcodes={Opcodes.CMSG_PLAYER_LOGIN})
    public void handleLogin(Packet pk) {
        if(pk.session.isPlayerLoading() || pk.session.getPlayer()!=null){
            logger.error("Player at acc {} is already loading",pk.session.getAccount().getId());
            return;
        }
        
        pk.session.beginPlayerLoading();
        
        long playerGuid = pk.body.readLong();
        
        int character = GuidUtil.getGuidLowPart(playerGuid);
        
        if(!isLegitCharacter(pk.session,character)){
            logger.error("Account {} can't login with that character {}.",pk.session.getAccount().getId(),character);
            pk.session.kick();
            return;
        }
        
        loadCharacter(pk.session,character);
    }

    private boolean isLegitCharacter(SocketBoundWorldSession session, int character) {
        return session.getAllowedCharacters().contains(character);
    }

    private void loadCharacter(SocketBoundWorldSession session, int character) {
        asyncCharacterDatabase.loadPlayerData(character, new PlayerLoadingCallback());
    }

    private class PlayerLoadingCallback implements AsyncCallback<LoginPlayerInfo> {

        @Override
        public void handleResult(LoginPlayerInfo result) {
            
        }
                
    }
    

    
}
