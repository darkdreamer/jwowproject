/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.handlers;


import java.awt.Component;
import jwow.realmd.engine.Opcodes;
import jwow.realmd.engine.Packet;
import jwow.realmd.engine.PacketHandler;
import jwow.realmd.engine.annotations.HandlerMapping;
import jwow.realmd.engine.annotations.PacketProcessing;
import jwow.realmd.engine.annotations.SessionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dreamer
 */
public class NullHandler implements PacketHandler {
    private static final Logger logger = LoggerFactory.getLogger(NullHandler.class.getName());
    
    @HandlerMapping(processing = PacketProcessing.PROCESS_INPLACE, sessionStatus = SessionStatus.STATUS_NEVER,
    opcodes = {
        Opcodes.MSG_NULL_ACTION, Opcodes.CMSG_BOOTME, Opcodes.CMSG_DBLOOKUP,
        Opcodes.MSG_MOVE_TOGGLE_COLLISION_CHEAT, Opcodes.MSG_MOVE_SET_TURN_RATE,
        Opcodes.MSG_MOVE_SET_TURN_RATE_CHEAT, Opcodes.MSG_MOVE_SET_ALL_SPEED_CHEAT, Opcodes.MSG_MOVE_SET_SWIM_BACK_SPEED,
        Opcodes.MSG_MOVE_SET_SWIM_BACK_SPEED_CHEAT, Opcodes.MSG_MOVE_SET_SWIM_SPEED, Opcodes.MSG_MOVE_SET_SWIM_SPEED_CHEAT,
        Opcodes.MSG_MOVE_SET_WALK_SPEED, Opcodes.MSG_MOVE_SET_WALK_SPEED_CHEAT, Opcodes.MSG_MOVE_SET_RUN_BACK_SPEED,
        Opcodes.MSG_MOVE_SET_RUN_BACK_SPEED_CHEAT, Opcodes.MSG_MOVE_SET_RUN_SPEED, Opcodes.MSG_MOVE_SET_RUN_SPEED_CHEAT,
        Opcodes.MSG_MOVE_TELEPORT_CHEAT, Opcodes.MSG_MOVE_TELEPORT, Opcodes.MSG_MOVE_TOGGLE_LOGGING,
        Opcodes.CMSG_DESTROY_ITEMS, Opcodes.UMSG_UPDATE_GROUP_MEMBERS, Opcodes.CMSG_CHANNEL_MODERATE,
        Opcodes.MSG_MOVE_TOGGLE_FALL_LOGGING, Opcodes.CMSG_ITEM_QUERY_MULTIPLE, Opcodes.CMSG_GROUP_CANCEL,
        Opcodes.CMSG_SERVERTIME, Opcodes.CMSG_GAMESPEED_SET, Opcodes.CMSG_GAMETIME_SET,
        Opcodes.CMSG_AUTH_SRP6_RECODE, Opcodes.CMSG_AUTH_SRP6_PROOF, Opcodes.CMSG_AUTH_SRP6_BEGIN,
        Opcodes.CMSG_ADVANCE_SPAWN_TIME, Opcodes.CMSG_DISABLE_PVP_CHEAT,
        Opcodes.CMSG_DEBUG_AISTATE, Opcodes.CMSG_SEND_EVENT, Opcodes.CMSG_CLEAR_QUEST,
        Opcodes.CMSG_FLAG_QUEST_FINISH, Opcodes.CMSG_FLAG_QUEST, Opcodes.CMSG_USE_SKILL_CHEAT,
        Opcodes.CMSG_COOLDOWN_CHEAT, Opcodes.CMSG_SET_WORLDSTATE, Opcodes.CMSG_PET_LEVEL_CHEAT,
        Opcodes.CMSG_LEVEL_CHEAT, Opcodes.CMSG_CHEAT_SETMONEY, Opcodes.CMSG_GODMODE,
        Opcodes.CMSG_BEASTMASTER, Opcodes.CMSG_UNDRESSPLAYER, Opcodes.CMSG_WEATHER_SPEED_CHEAT,
        Opcodes.CMSG_PETGODMODE, Opcodes.SMSG_GODMODE, Opcodes.CMSG_FORCEACTIONSHOW,
        Opcodes.CMSG_FORCEACTIONONOTHER, Opcodes.CMSG_FORCEACTION, Opcodes.CMSG_BOT_DETECTED2,
        Opcodes.CMSG_MAKEMONSTERATTACKGUID, Opcodes.CMSG_CREATEGAMEOBJECT, Opcodes.CMSG_CREATEITEM,
        Opcodes.CMSG_DESTROYMONSTER, Opcodes.CMSG_CREATEMONSTER, Opcodes.CMSG_LEARN_SPELL,
        Opcodes.CMSG_RECHARGE, Opcodes.SMSG_MOVE_CHARACTER_CHEAT, Opcodes.CMSG_MOVE_CHARACTER_CHEAT,
        Opcodes.CMSG_DEBUG_CHANGECELLZONE, Opcodes.CMSG_ZONE_MAP, Opcodes.CMSG_QUERY_OBJECT_ROTATION,
        Opcodes.CMSG_QUERY_OBJECT_POSITION, Opcodes.UMSG_UPDATE_GUILD,
        Opcodes.CMSG_MOVE_CHARM_PORT_CHEAT, Opcodes.CMSG_MOVE_SET_RAW_POSITION, Opcodes.MSG_MOVE_ROOT,
        Opcodes.SMSG_MOVE_UNSET_HOVER, Opcodes.CMSG_TRIGGER_CINEMATIC_CHEAT, Opcodes.SMSG_MOVE_SET_HOVER,
        Opcodes.CMSG_OPENING_CINEMATIC,})
    
    public void handleNull(Packet pk) {
        //sLog->outError("SESSION: received unhandled opcode %s (0x%.4X)", LookupOpcodeName(recvPacket.GetOpcode()), recvPacket.GetOpcode());
        logger.error("Recieved unhandled opcode {0} ({1})", Opcodes.getOpcodeName(pk.opcode),Integer.toHexString(pk.opcode));
    }
}
