/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.realmd.handlers;

/**
 *
 * @author dreamer
 */
public class AddonInfo {
    
    private int crc;
    private byte state;
    private boolean hasPKorCRC;

    AddonInfo(String addonName, int crc, byte enabled, int i, boolean b) {
        this.crc=crc;
        this.state=enabled;
        this.hasPKorCRC=b;
    }

    public byte getState() {
        return state;
    }

    public void setState(byte state) {
        this.state = state;
    }

    public int getCrc() {
        return crc;
    }

    public void setCrc(int crc) {
        this.crc = crc;
    }
    
    

    public boolean isHasPKorCRC() {
        return hasPKorCRC;
    }

    public void setHasPKorCRC(boolean hasPKorCRC) {
        this.hasPKorCRC = hasPKorCRC;
    }
    
    
    
}
