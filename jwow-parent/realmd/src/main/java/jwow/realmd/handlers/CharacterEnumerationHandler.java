/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.handlers;

import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.List;
import javax.inject.Inject;
import jwow.datasources.CharacterDatabase;
import jwow.game.guid.HighGuid;
import jwow.game.player.AtLoginFlags;
import jwow.game.player.CharacterFlag;
import jwow.game.player.PlayerFlag;
import jwow.game.primitive.FlagSet;
import jwow.realmd.engine.Opcodes;
import jwow.realmd.engine.Packet;
import jwow.realmd.engine.PacketHandler;
import jwow.realmd.engine.annotations.HandlerMapping;
import jwow.realmd.engine.annotations.PacketProcessing;
import jwow.realmd.engine.annotations.SessionStatus;

/**
 *
 * @author dreamer
 */
public class CharacterEnumerationHandler implements PacketHandler {

    @Inject
    CharacterDatabase cdb;

    @HandlerMapping(opcodes = Opcodes.CMSG_CHAR_ENUM, processing = PacketProcessing.PROCESS_THREADUNSAFE, sessionStatus = SessionStatus.STATUS_AUTHED)
    public void onCharacterEnum(Packet pk) throws UnsupportedEncodingException{
        int accid = pk.session.getAccount().getId();
        List<CharacterDatabase.CharacterEnumInformation> resultset = cdb.charSelEnum(accid, 1);

        Packet answer = pk.session.createPacket();
        answer.initialize(Opcodes.SMSG_CHAR_ENUM, 100);

        answer.body.writeByte(resultset.size());
        answer.session.setAllowedCharacters(new HashSet<Integer>());
        for (int i = 0; i < resultset.size(); i++) {
            //TODO: validation
            handleCharacter(answer, resultset.get(i));
        }
        
        answer.session.send(answer);
    }

    private void handleCharacter(Packet answer, CharacterDatabase.CharacterEnumInformation character) throws UnsupportedEncodingException{
        answer.session.getAllowedCharacters().add(character.guid);
        //TODO: check (class,race) tuple validness
        //TODO: check gender
        
//        *data << uint64(MAKE_NEW_GUID(guid, 0, HIGHGUID_PLAYER));
        answer.body.writeLong(MAKE_NEW_GUID(
                character.guid, 0, HighGuid.HIGHGUID_PLAYER.getValue()));
//    *data << fields[1].GetString();                         // name
        answer.body.writeBytes(character.name.getBytes("UTF-8"));
        answer.body.writeByte(0);
//    *data << uint8(plrRace);                                // race
        answer.body.writeByte(character.race & 0xFF);
//    *data << uint8(plrClass);                               // class
        answer.body.writeByte(character.characterClass & 0xFF);
//    *data << uint8(gender);                                 // gender
        answer.body.writeByte(character.gender & 0xFF);

        int playerBytes = character.playerBytes1;
//    *data << uint8(playerBytes);                            // skin
        answer.body.writeByte(playerBytes);
//    *data << uint8(playerBytes >> 8);                       // face
        answer.body.writeByte(playerBytes >>> 8);
//    *data << uint8(playerBytes >> 16);                      // hair style
        answer.body.writeByte(playerBytes >>> 16);
//    *data << uint8(playerBytes >> 24);                      // hair color
        answer.body.writeByte(playerBytes >>> 24);

        int playerBytes2 = character.playerBytes2;
        answer.body.writeByte(playerBytes2 & 0xFF);                    // facial hair

//    *data << uint8(fields[7].GetUInt8());                   // level
        answer.body.writeByte(character.level);
//    *data << uint32(fields[8].GetUInt16());                 // zone
        answer.body.writeInt(character.zone);
//    *data << uint32(fields[9].GetUInt16());                 // map
        answer.body.writeInt(character.map);

//    *data << fields[10].GetFloat();                         // x
        answer.body.writeFloat(character.posX);
//    *data << fields[11].GetFloat();                         // y
        answer.body.writeFloat(character.posY);
//    *data << fields[12].GetFloat();                         // z
        answer.body.writeFloat(character.posZ);

//        *data << uint32(fields[13].GetUInt32());                // guild id
        if (character.guildid != null) {
            answer.body.writeInt(character.guildid);
        } else {
            answer.body.writeInt(0);
        }
        
        FlagSet<PlayerFlag> playerFlags = new FlagSet<>(PlayerFlag.class);
        playerFlags.initFromBitmask(character.playerFlags);

        FlagSet<CharacterFlag> characterFlags = new FlagSet<>(CharacterFlag.class);
        if (playerFlags.getEnumSet().contains(PlayerFlag.PLAYER_FLAGS_HIDE_HELM)) {
            characterFlags.getEnumSet().add(CharacterFlag.CHARACTER_FLAG_HIDE_HELM);
        }
        if (playerFlags.getEnumSet().contains(PlayerFlag.PLAYER_FLAGS_HIDE_CLOAK)) {
            characterFlags.getEnumSet().add(CharacterFlag.CHARACTER_FLAG_HIDE_CLOAK);
        }

        if (playerFlags.getEnumSet().contains(PlayerFlag.PLAYER_FLAGS_GHOST)) {
            characterFlags.getEnumSet().add(CharacterFlag.CHARACTER_FLAG_GHOST);
        }
        FlagSet<AtLoginFlags> atLoginFlags = FlagSet.create(AtLoginFlags.class, character.characterAtLogin);
        if (atLoginFlags.getEnumSet().contains(AtLoginFlags.AT_LOGIN_RENAME)) {
            characterFlags.getEnumSet().add(CharacterFlag.CHARACTER_FLAG_RENAME);
        }

        if (character.bannedGuid != null) {
            characterFlags.getEnumSet().add(CharacterFlag.CHARACTER_FLAG_LOCKED_BY_BILLING);
        }

        characterFlags.getEnumSet().add(CharacterFlag.CHARACTER_FLAG_DECLINED);

        answer.body.writeInt(characterFlags.getBitMask());

        //TODO: character cusomize
        answer.body.writeInt(0);
        
        if(atLoginFlags.getEnumSet().contains(AtLoginFlags.AT_LOGIN_FIRST)){
            answer.body.writeByte(1);
        }else{
            answer.body.writeByte(0);
        }

        //TODO: PET DISPLAY
        answer.body.writeInt(0);
        answer.body.writeInt(0);
        answer.body.writeInt(0);

        //TODO: inventory preview support
        for (int i = 0; i < 23; i++) { //23 is INVENTORY_SLOT_BAG_END
            answer.body.writeInt(0);
            answer.body.writeByte(0);
            answer.body.writeInt(0);
        }
    }

    long MAKE_NEW_GUID(long l, long e, long h) {
        return (l | (e << 24) | (h << 48));
    }
}
