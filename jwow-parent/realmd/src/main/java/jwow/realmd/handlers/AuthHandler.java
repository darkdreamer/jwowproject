/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.handlers;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import javax.inject.Inject;
import jwow.datasources.CharacterDatabase;
import jwow.datasources.LoginDatabase;
import jwow.entities.auth.Account;
import jwow.entities.characters.AccountData;
import jwow.game.core.World;
import jwow.realmd.engine.ChannelUtils;
import jwow.realmd.engine.Opcodes;
import jwow.realmd.engine.Packet;
import jwow.realmd.engine.PacketHandler;
import jwow.realmd.engine.ResponseCodes;
import jwow.realmd.engine.SocketBoundWorldSession;
import jwow.realmd.engine.annotations.HandlerMapping;
import jwow.realmd.engine.annotations.PacketProcessing;
import jwow.realmd.engine.annotations.SessionStatus;
import jwow.security.SRP6Token;
import jwow.shared.LocaleConstant;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;

/**
 *
 * @author dreamer
 */
public class AuthHandler implements PacketHandler {

    private static final Logger logger = Logger.getLogger(AuthHandler.class.getName());

    @Inject
    World world;
    @Inject
    LoginDatabase loginDatabase;
    @Inject
    CharacterDatabase characterDatabase;

    @HandlerMapping(opcodes = Opcodes.CMSG_AUTH_SESSION,
            processing = PacketProcessing.PROCESS_INPLACE,
            sessionStatus = SessionStatus.STATUS_NEVER)
    public void handleAuthSession(Packet pk) throws Exception {
        byte[] digest;
        int clientSeed;
        int unk2, unk3, unk5, unk6, unk7;
        long unk4;
        int BuiltNumberClient;

        LocaleConstant locale = LocaleConstant.LOCALE_ruRU;
        String account;

        MessageDigest sha = MessageDigest.getInstance("SHA1");
        BigInteger v, s, g, N, k;
        SocketBoundWorldSession session = pk.session;
        Packet answerPk = session.createPacket();

        if (world.isClosed()) {

            answerPk.initialize(Opcodes.SMSG_AUTH_RESPONSE, 1);

            answerPk.body.writeByte(ResponseCodes.AUTH_REJECT);
            session.send(answerPk);

            logger.log(Level.SEVERE, "WorldSocket::HandleAuthSession: World closed, denying client ({0}).", pk.session.getAddress());
            session.terminate();
            return;
        }

        ChannelBuffer recvPacket = pk.body;
//
//    // Read the content of the packet
        BuiltNumberClient = recvPacket.readInt();
        unk2 = recvPacket.readInt();
        account = ChannelUtils.readString(recvPacket);
        unk3 = recvPacket.readInt();
        clientSeed = recvPacket.readInt();
        unk5 = recvPacket.readInt();
        unk6 = recvPacket.readInt();
        unk7 = recvPacket.readInt();
        unk4 = recvPacket.readLong();
        digest = recvPacket.readBytes(20).array();

        logger.log(Level.SEVERE, "WorldSocket::HandleAuthSession: client {0}, unk2 {1}, account {2}, unk3 {3}, clientseed {4}", new Object[]{
            BuiltNumberClient,
            unk2,
            account,
            unk3,
            clientSeed});

        logger.log(Level.SEVERE, "Bytes left: {0}", recvPacket.readableBytes());

        Account acc = loginDatabase.LOGIN_SEL_ACCOUNT_INFO_BY_NAME(account);

        if (acc == null) {
            answerPk.initialize(Opcodes.SMSG_AUTH_RESPONSE, 1);
            answerPk.body.writeByte(ResponseCodes.AUTH_UNKNOWN_ACCOUNT);
            session.send(answerPk);
            session.terminate();
            logger.severe("WorldSocket::HandleAuthSession: Sent Auth Response (unknown account).");
            return;
        }

        //check expansion
        //check ip
        //check mute
        //check ban
        //check population overflow
        //load gm level
        int security = 0;

        k = new BigInteger(acc.getSessionkey(), 16);

        int t = 0;

        int seed = session.getSeed();
        sha.reset();
        sha.update(account.getBytes());

        byte[] d = sha.digest();
        BigInteger bigInteger = new BigInteger(1, d);
        String hex = bigInteger.toString(16);
//
        sha.reset();
        sha.update(account.getBytes());
        sha.update(ByteBuffer.allocate(4).putInt(t).array());

        d = sha.digest();
        bigInteger = new BigInteger(1, d);
        hex = bigInteger.toString(16);

        sha.reset();
        sha.update(account.getBytes());
        sha.update(ByteBuffer.allocate(4).putInt(t).array());
        ChannelBuffer cba = ChannelBuffers.buffer(ByteOrder.LITTLE_ENDIAN, 4);
        cba.writeInt(clientSeed);
        sha.update(cba.array());

        d = sha.digest();
        bigInteger = new BigInteger(1, d);
        hex = bigInteger.toString(16);

        sha.reset();
        sha.update(account.getBytes());
        sha.update(ByteBuffer.allocate(4).putInt(t).array());
        sha.update(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(clientSeed).array());
        sha.update(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(seed).array());

        d = sha.digest();
        bigInteger = new BigInteger(1, d);
        hex = bigInteger.toString(16);

        sha.reset();
//    sha.UpdateData (account);
        sha.update(account.getBytes());
//    sha.UpdateData ((uint8 *) & t, 4);
        sha.update(ByteBuffer.allocate(4).putInt(t).array());
//    sha.UpdateData ((uint8 *) & clientSeed, 4);
        sha.update(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(clientSeed).array());
//    sha.UpdateData ((uint8 *) & seed, 4);
        sha.update(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(seed).array());
//    sha.UpdateBigNumbers (&k, NULL);
        sha.update(SRP6Token.rev(k, 40));
//    sha.Finalize();
//
        d = sha.digest();
        bigInteger = new BigInteger(1, d);
        hex = bigInteger.toString(16);
//    if (memcmp (sha.GetDigest(), digest, 20))
        if (!Arrays.equals(d, digest)) {
//        packet.Initialize (SMSG_AUTH_RESPONSE, 1);
            answerPk.initialize(Opcodes.SMSG_AUTH_RESPONSE, 1);
//        packet << uint8 (AUTH_FAILED);
            answerPk.body.writeByte(ResponseCodes.AUTH_FAILED);
//
//        SendPacket(packet);
            session.send(answerPk);
            session.terminate();
//
            logger.severe("WorldSocket::HandleAuthSession: Sent Auth Response (authentification failed).");
//        return -1;
            return;
        }
        session.initializeAccount(acc);
        //recrutier support
        //update last ip
        session.initializeCrypto(k);

        List<AccountData> ad = characterDatabase.getGlobalAccountData(session.getAccount().getId());
        session.setAccountData(ad);

        readAddonsInfo(session, recvPacket);

        world.addSession(session);
    }

    void readAddonsInfo(SocketBoundWorldSession worldSession, ChannelBuffer cb) {
//        if (data.rpos() + 4 > data.size())
//        return;

        if (cb.readableBytes() <= 4) {
            return;
        }
//
//    uint32 size;
//    data >> size;
        int size = cb.readInt();
//
//    if (!size)
//        return;
        if (size == 0) {
            return;
        }
//
//    if (size > 0xFFFFF)
//    {
//        sLog->outError("WorldSession::ReadAddonsInfo addon info too big, size %u", size);
//        return;
//    }

        if (size > 0xFFFFF) {
            logger.log(Level.SEVERE, "WorldSession::ReadAddonsInfo addon info too big, size {0}", size);
            return;
        }
//
//    uLongf uSize = size;
//
//    uint32 pos = data.rpos();
        logger.log(Level.SEVERE, "Reading addon info {0}", size);
        Inflater inflater = new Inflater();
        inflater.setInput(cb.readBytes(cb.readableBytes()).array());
//
//    ByteBuffer addonInfo;
//    addonInfo.resize(size);
//
        int uSize;
        byte[] buf = new byte[size];
        ChannelBuffer addonInfo = ChannelBuffers.dynamicBuffer(ByteOrder.LITTLE_ENDIAN, size);
        try {
            uSize = inflater.inflate(addonInfo.array());
            addonInfo.writerIndex(uSize);

        } catch (DataFormatException ex) {
            Logger.getLogger(AuthHandler.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }


        if (uSize != 0) {
            logger.log(Level.SEVERE, "Uncompressed {0} addon info bytes. Channel readable bytes {0}", new Object[]{uSize, addonInfo.readableBytes()});
            List<AddonInfo> addonInfos = new ArrayList<AddonInfo>();

            int addonCount = addonInfo.readInt();

            logger.log(Level.SEVERE, "Expected addon count {0}", addonCount);
//

            for (int i = 0; i < addonCount; i++) {
                if (addonInfo.readableBytes() < 2) {
                    return;
                }
//

                String addonName = ChannelUtils.readString(addonInfo);

                byte enabled = addonInfo.readByte();
                int crc = addonInfo.readInt();
                int unk1 = addonInfo.readInt();

                logger.log(Level.FINER, "ADDON: Name {0}, Enabled: {1},CRC: {2}, Unk2: {3}",
                        new Object[]{addonName, enabled, crc, unk1});

//
                AddonInfo addon = new AddonInfo(addonName, crc, enabled, 2, true);

                //ADDON VERIFICATION

                addonInfos.add(addon);
            }
//
//        uint32 currentTime;
//        addonInfo >> currentTime;
//        sLog->outDebug(LOG_FILTER_NETWORKIO, "ADDON: CurrentTime: %u", currentTime);
//
//        if (addonInfo.rpos() != addonInfo.size())
//            sLog->outDebug(LOG_FILTER_NETWORKIO, "packet under-read!");
            worldSession.setAddonList(addonInfos);
            
            int currentTime = addonInfo.readInt();

            if (addonInfo.readableBytes() != 0) {
                logger.log(Level.SEVERE, "addon info. left unread {0} bytes", addonInfo.readableBytes());
            }
        } else {
            logger.severe("Addon packet uncompress error!");
        }
    }
}
