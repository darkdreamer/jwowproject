/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.realmd.handlers;

import java.awt.Component;
import javax.inject.Inject;
import jwow.realmd.engine.Opcodes;
import jwow.realmd.engine.Packet;
import jwow.realmd.engine.PacketHandler;
import jwow.realmd.engine.annotations.HandlerMapping;
import jwow.realmd.engine.annotations.PacketProcessing;
import jwow.realmd.engine.annotations.SessionStatus;
import jwow.realmd.engine.world.CacheMask;
import jwow.realmd.engine.world.PacketInterface;

/**
 *
 * @author dreamer
 */
public class PreGamingHandler implements PacketHandler  {

    @Inject
    PacketInterface packetInterface;
    
    //    /*0x38C*/ { "CMSG_REALM_SPLIT",                             STATUS_AUTHED,   PROCESS_THREADUNSAFE, &WorldSession::HandleRealmSplitOpcode          },
    @HandlerMapping(processing = PacketProcessing.PROCESS_INPLACE, sessionStatus = SessionStatus.STATUS_AUTHED,
            opcodes={Opcodes.CMSG_REALM_SPLIT})
    public void handlRealmSplit(Packet pk) {
//        uint32 unk;
//    std::string split_date = "01/01/01";
//    recv_data >> unk;
//
//    WorldPacket data(SMSG_REALM_SPLIT, 4+4+split_date.size()+1);
//    data << unk;
//    data << uint32(0x00000000);                             // realm split state
//    // split states:
//    // 0x0 realm normal
//    // 0x1 realm split
//    // 0x2 realm split pending
//    data << split_date;
//    SendPacket(&data);
        int unk = pk.body.readInt();
        String split_date="01/01/01";
        Packet resp = pk.session.createPacket();
        
        resp.initialize(Opcodes.SMSG_REALM_SPLIT, 4+4+split_date.length()+1);
        
        resp.body.writeInt(unk);
        resp.body.writeInt(0);
        resp.body.writeBytes(split_date.getBytes());
        resp.body.writeByte(0);
        
        pk.session.send(resp);
    }
    
   
    @HandlerMapping(opcodes = Opcodes.CMSG_READY_FOR_ACCOUNT_DATA_TIMES,processing = PacketProcessing.PROCESS_INPLACE,sessionStatus = SessionStatus.STATUS_AUTHED)
    public void handleReadyForAccountDataTimes(Packet pk){
        
        packetInterface.sendAccountDataTimes(pk.session, CacheMask.Global);
    }
    
}
