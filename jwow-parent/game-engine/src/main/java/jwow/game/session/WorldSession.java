/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.session;

import jwow.entities.auth.Account;
import jwow.game.player.Player;

/**
 *
 * @author prog_lf
 */
public interface WorldSession {

    Account getAccount();

    void kick();

    boolean isPlayerLoading();

    void resetTimeOutTime();

    void setInQueue(boolean b);
    
    boolean update(long  delta,PacketFilter scope);

    Player getPlayer();
}
