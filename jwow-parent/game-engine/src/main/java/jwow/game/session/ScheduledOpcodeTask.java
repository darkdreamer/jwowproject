/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.session;

/**
 *
 * @author prog_lf
 */
public class ScheduledOpcodeTask {
    
    private final WorldSession session;
    private final ScheduledOperationScope scope;
    private final Runnable runnable;

    public ScheduledOpcodeTask(ScheduledOperationScope scope, Runnable runnable,WorldSession session) {
        this.scope = scope;
        this.runnable = runnable;
        this.session=session;
    }

    

    public Runnable getRunnable() {
        return runnable;
    }

    public ScheduledOperationScope getScope() {
        return scope;
    }

    public WorldSession getSession() {
        return session;
    }
   
    
    
}
