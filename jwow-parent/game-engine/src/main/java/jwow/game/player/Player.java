/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.player;

import java.util.logging.Logger;
import jwow.game.common.TimeConstants;
import jwow.game.object.fields.EPlayerFields;
import jwow.game.object.layout.ByteField;
import jwow.game.object.layout.GOField;
import jwow.game.object.layout.Int16Field;
import jwow.game.object.layout.Int32Field;
import jwow.game.object.layout.Int64Field;
import jwow.game.units.Unit;

/**
 *
 * @author dreamer
 */
public class Player extends Unit {

    private static final Logger logger = Logger.getLogger(Player.class.getName());
    public static final int MAX_STATS = 5;
    public static final int MAX_RUNES = 6;
    public static final int RUNE_COOLDOWN = (2 * 5 * TimeConstants.IN_MILLISECONDS); // msec
    public static final int MAX_DRUNKEN = 4;
    public static final int MAX_TIMERS = 3;
    public static final int DISABLED_MIRROR_TIMER = -1;
    // Player summoning auto-decline time (in secs)
    public static final int MAX_PLAYER_SUMMON_DELAY = (2 * TimeConstants.MINUTE);
    public static final long MAX_MONEY_AMOUNT = (999999999);    // from wowpedia

    @GOField(offset = EPlayerFields.PLAYER_FIELD_WATCHED_FACTION_INDEX)
    public final Int32Field watchedFactionIndex = new Int32Field();
    @GOField(offset = EPlayerFields.PLAYER_BYTES, byteOffset = 0)
    public final Int32Field playerBytes1 = new Int32Field();
    @GOField(offset = EPlayerFields.PLAYER_BYTES, byteOffset = 0)
    public final ByteField skin = new ByteField();
    @GOField(offset = EPlayerFields.PLAYER_BYTES, byteOffset = 1)
    public final ByteField face = new ByteField();
    @GOField(offset = EPlayerFields.PLAYER_BYTES, byteOffset = 2)
    public final ByteField hairStyle = new ByteField();
    @GOField(offset = EPlayerFields.PLAYER_BYTES, byteOffset = 3)
    public final ByteField hairColor = new ByteField();
    @GOField(offset = EPlayerFields.PLAYER_BYTES_2, byteOffset = 0)
    public final Int32Field playerBytes2 = new Int32Field();
    @GOField(offset = EPlayerFields.PLAYER_BYTES_2, byteOffset = 0)
    public final ByteField facialHair = new ByteField();
    @GOField(offset = EPlayerFields.PLAYER_BYTES_2, byteOffset = 3)
    public final ByteField restState = new ByteField();
    @GOField(offset = EPlayerFields.PLAYER_BYTES_3, byteOffset = 0) //
    public final Int16Field playerBytes3_gender = new Int16Field(); // only GENDER_MALE/GENDER_FEMALE (1 bit) allowed, drunk state = 0

    @GOField(offset = EPlayerFields.PLAYER_BYTES_3, byteOffset = 3)
    public final ByteField battlefieldArenaFaction = new ByteField();
    @GOField(offset = EPlayerFields.OBJECT_FIELD_TYPE, byteOffset = 1)
    public final Int16Field hasGuild = new Int16Field();

    @GOField(offset = EPlayerFields.PLAYER_GUILDRANK)
    public final Int32Field guildRank = new Int32Field();
    @GOField(offset = EPlayerFields.PLAYER_GUILD_TIMESTAMP)
    public final Int32Field guildTimestamp = new Int32Field();

    @GOField(offset = EPlayerFields.PLAYER__FIELD_KNOWN_TITLES)
    public final Int64Field knownTitles1 = new Int64Field();
    @GOField(offset = EPlayerFields.PLAYER__FIELD_KNOWN_TITLES1)
    public final Int64Field knownTitles2 = new Int64Field();
    @GOField(offset = EPlayerFields.PLAYER__FIELD_KNOWN_TITLES2)
    public final Int64Field knownTitles3 = new Int64Field();

    @GOField(offset = EPlayerFields.PLAYER_CHOSEN_TITLE)
    public final Int32Field chosenTitle = new Int32Field();

    @GOField(offset = EPlayerFields.PLAYER_FIELD_KILLS)
    public final Int32Field kills = new Int32Field();

    @GOField(offset = EPlayerFields.PLAYER_FIELD_COINAGE)
    public final Int32Field money = new Int32Field();

    @GOField(offset = EPlayerFields.PLAYER_FIELD_MAX_LEVEL)
    public final Int32Field maxLevel = new Int32Field();
    @GOField(offset = EPlayerFields.PLAYER_XP)
    public final Int32Field xp = new Int32Field();
    @GOField(offset = EPlayerFields.PLAYER_NEXT_LEVEL_XP)
    public final Int32Field nextLevelXp = new Int32Field();

    @GOField(offset = EPlayerFields.PLAYER_FIELD_KNOWN_CURRENCIES)
    public final Int64Field knownCurrencies = new Int64Field();
    @GOField(offset = EPlayerFields.PLAYER_AMMO_ID)
    public final Int32Field playerAmmoID = new Int32Field();
    @GOField(offset = EPlayerFields.PLAYER_FIELD_BYTES,byteOffset = 2)
    public final ByteField playerFieldBytes = new ByteField();
    

}
