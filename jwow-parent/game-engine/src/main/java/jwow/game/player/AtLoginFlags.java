/*
 * To change this license header), choose License Headers in Project Properties.
 * To change this template file), choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.game.player;

import jwow.game.primitive.Flag;

/**
 *
 * @author dreamer
 */
public enum AtLoginFlags implements Flag{
     AT_LOGIN_NONE              ( 0x00),
    AT_LOGIN_RENAME            ( 0x01),
    AT_LOGIN_RESET_SPELLS      ( 0x02),
    AT_LOGIN_RESET_TALENTS     ( 0x04),
    AT_LOGIN_CUSTOMIZE         ( 0x08),
    AT_LOGIN_RESET_PET_TALENTS ( 0x10),
    AT_LOGIN_FIRST             ( 0x20),
    AT_LOGIN_CHANGE_FACTION    ( 0x40),
    AT_LOGIN_CHANGE_RACE       ( 0x80);
    
    private int value;

    private AtLoginFlags(int value) {
        this.value = value;
    }

    @Override
    public int getMask() {
        return value;
    }
    
}
