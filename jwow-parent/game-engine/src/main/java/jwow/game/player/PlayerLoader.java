/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.game.player;

import javax.inject.Inject;
import jwow.datasources.ObjectInfoStorage;
import jwow.entities.aggregate.LoginPlayerInfo;
import jwow.entities.aggregate.PlayerInfo;
import jwow.entities.characters.Characters;
import jwow.game.guid.HighGuid;
import jwow.game.guid.ObjectGuid;


/**
 *
 * @author dreamer
 */
public class PlayerLoader {
    
    @Inject
    private ObjectInfoStorage objectInfoStorage;
    
    public Player loadPlayer(LoginPlayerInfo loginPlayerInfo){
        Player player = new Player();
        Characters character = loginPlayerInfo.character;
        
        player.guid.set(new ObjectGuid(HighGuid.HIGHGUID_PLAYER, 0,character.getGuid()));
        
        
        player.gender.set(character.getGender()&0xFF);
        player.race.set(character.getRace());
        player.gender.set(character.getGender());
        
        player.level.set(character.getLevel());
        player.xp.set(character.getXp());
        
        
        //TODO: _LoadIntoDataField(fields[61].GetCString(), PLAYER_EXPLORED_ZONES_1, PLAYER_EXPLORED_ZONES_SIZE);
        //TODO: _LoadIntoDataField(fields[64].GetCString(), PLAYER__FIELD_KNOWN_TITLES, KNOWN_TITLES_SIZE*2);
        
        player.setObjectScale(1.0f);
        player.hoverHeight.set(1.0f);
        
        //TODO: Load achievements
        
        player.money.set(character.getMoney());
        player.playerBytes1.set(character.getPlayerBytes());
        player.playerBytes2.set(character.getPlayerBytes2());
        
        player.playerBytes3_gender.set(character.getGender());
        player.battlefieldArenaFaction.set(character.getWatchedFaction());
        
        player.flags.set(character.getPlayerFlags());
        player.watchedFactionIndex.set(character.getWatchedFaction());
        player.knownCurrencies.set(character.getKnownCurrencies());
        player.playerAmmoID.set(character.getAmmoId());
        player.playerFieldBytes.set(character.getActionBars());
        
        //https://github.com/FrickX/Arctium/blob/master/WorldServer/Game/WorldEntities/Character.cs
        
        PlayerInfo info = objectInfoStorage.getPlayerInfo(character.getRace(), character.getClass1());
        if(character.getGender()==0){
            player.displayId.set(info.displayId_m);
            player.nativeDisplayId.set(info.displayId_m);
        }else{
            player.displayId.set(info.displayId_f);
            player.nativeDisplayId.set(info.displayId_f);
        }
        
        
    }
}
