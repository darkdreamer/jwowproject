/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.units;

import jwow.game.object.WorldObject;
import jwow.game.object.fields.EPlayerFields;
import jwow.game.object.fields.EUnitFields;
import jwow.game.object.layout.ByteField;
import jwow.game.object.layout.FlagField;
import jwow.game.object.layout.FloatField;
import jwow.game.object.layout.GOField;
import jwow.game.object.layout.Int32Field;

/**
 *
 * @author prog_lf
 */
public class Unit extends WorldObject {

    @GOField(offset = EUnitFields.UNIT_FIELD_BYTES_0, byteOffset = 0)
    public final ByteField race = new ByteField();
    @GOField(offset = EUnitFields.UNIT_FIELD_BYTES_0, byteOffset = 1)
    public final ByteField class_ = new ByteField();
    @GOField(offset = EUnitFields.UNIT_FIELD_BYTES_0, byteOffset = 2)
    public final ByteField gender = new ByteField();
    @GOField(offset = EUnitFields.UNIT_FIELD_BYTES_0, byteOffset = 3)
    public final ByteField powertype = new ByteField();
    @GOField(offset = EUnitFields.UNIT_FIELD_BYTES_2, byteOffset = 1)
    public final FlagField pvpStateFlags = new FlagField();
    @GOField(offset = EUnitFields.UNIT_FIELD_FLAGS)
    public final FlagField flags = new FlagField();
    @GOField(offset = EUnitFields.UNIT_FIELD_FLAGS_2)
    public final FlagField flags2 = new FlagField();
    @GOField(offset = EUnitFields.UNIT_MOD_CAST_SPEED)
    public final FloatField modCastSpeed = new FloatField();
    @GOField(offset = EUnitFields.UNIT_FIELD_HOVERHEIGHT)
    public final FloatField hoverHeight = new FloatField();
    @GOField(offset = EUnitFields.UNIT_FIELD_LEVEL)
    public final Int32Field level = new Int32Field();
    @GOField(offset = EUnitFields.UNIT_FIELD_MAXHEALTH)
    public final Int32Field maxHealth = new Int32Field();
        
    @GOField(offset=EPlayerFields.UNIT_FIELD_AURASTATE)
    public final Int32Field auraState = new Int32Field();
    @GOField(offset=EPlayerFields.UNIT_FIELD_DISPLAYID)
    public final Int32Field displayId = new Int32Field();
    @GOField(offset=EPlayerFields.UNIT_FIELD_NATIVEDISPLAYID)
    public final Int32Field nativeDisplayId = new Int32Field();
    
    @GOField(offset=EPlayerFields.UNIT_FIELD_FACTIONTEMPLATE)
    public final Int32Field factionTemplate = new Int32Field();

    public void relocate(float positionX, float positionY, float positionZ, float orientation) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void setHealth(Integer get) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    
}
