/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.game.primitive;

import java.util.EnumSet;
import java.util.Set;
import jwow.game.player.AtLoginFlags;

/**
 *
 * @author dreamer
 */
public class FlagSet<E extends Enum<E>>{

    public static <T extends Enum<T>> FlagSet<T> create(Class<T> aClass, int characterAtLogin) {
        FlagSet<T> flags = new FlagSet<>(aClass);
        flags.initFromBitmask(characterAtLogin);
        return flags;
    }
    
    private Class<E> flagClass;
    private EnumSet<E> enumSet;
    
    public FlagSet(Class<E> flagClass) {
        this.flagClass = flagClass;
        if(!Flag.class.isAssignableFrom(flagClass)){
            throw new RuntimeException("Class requires 'Flag' class inheritance");
        }
        enumSet = EnumSet.noneOf(flagClass);
    }
    
    

    public EnumSet<E> getEnumSet() {
        return enumSet;
    }
    
    public void initFromBitmask( int value){
        enumSet.clear();
        
        Object[] enums =flagClass.getEnumConstants();
        
        for(int i =0;i<enums.length;i++){
            Flag f = (Flag)enums[i];
            
            if((value&f.getMask())!=0){
                enumSet.add((E)f);
            }
        }
    }

    public int getBitMask() {
        int mask = 0;
        
        for(E flag : enumSet){
            mask |= ((Flag)flag).getMask();
        }
        return mask;
    }
    
    
}
