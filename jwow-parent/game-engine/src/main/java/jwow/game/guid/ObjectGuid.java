/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.guid;


/**
 *
 * @author prog_lf
 */
public class ObjectGuid {

    public ObjectGuid() {
        m_guid = 0;
    }

    public ObjectGuid(long m_guid) {
        this.m_guid = m_guid;
        fillBytes();
    }

    public ObjectGuid(HighGuid hi, int entry, int counter) {

        if (counter != 0) {
            long shiftEntry = (long) (entry) << 32;
            long shiftHi = ((long) (hi.getValue()) << (IsLargeHigh(hi) ? 48 : 52));
            m_guid = ((long) (counter) | shiftEntry | shiftHi);
        } else {
            m_guid = 0;
        }

    }

    public ObjectGuid(HighGuid hi, int counter) {
        if (counter != 0) {
            m_guid = (long) counter | ((long) (hi.getValue()) << (IsLargeHigh(hi) ? 48 : 52));
        } else {
            m_guid = 0;
        }

    }

  
    void Set(long guid) {
        m_guid = guid;
        fillBytes();
    }

    void Clear() {
        m_guid = 0;
        fillBytes();
    }

    public long GetRawValue() {
        return m_guid;
    }

    public HighGuid GetHigh() {
        HighGuid high = HighGuid.fromCode((int) (m_guid >> 48) & 0xFFFF);

        if (IsLargeHigh(high)) {
            return high;
        } else {
            return HighGuid.fromCode((int) ((m_guid >> 52) & 0xFFF));
        }

    }

    int GetEntry() {
        return HasEntry() ? (int) ((m_guid >> 32) & (0xFFFF)) : 0;
    }

    public int GetCounter() {
        return HasEntry()
                ? (int) (m_guid & (0x00000000FFFFFFFF))
                : (int) (m_guid & (0x00000000FFFFFFFF));  // TODO: switch to 40 bits, but this needs rewrite code to use uint64 instead uint32
    }
//

    public static int GetMaxCounter(HighGuid high) {
        return HasEntry(high)
                ? (0xFFFFFFFF)
                : (0xFFFFFFFF);    // TODO: switch to 40 bits
    }
//

    int GetMaxCounter() {
        return GetMaxCounter(GetHigh());
    }
//

    boolean IsEmpty() {
        return m_guid == 0;
    }

    boolean IsCreature() {
        return GetHigh() == HighGuid.HIGHGUID_UNIT;
    }

    boolean IsPet() {
        return GetHigh() == HighGuid.HIGHGUID_PET;
    }

    boolean IsVehicle() {
        return GetHigh() == HighGuid.HIGHGUID_VEHICLE;
    }

    boolean IsCreatureOrPet() {
        return IsCreature() || IsPet();
    }

    boolean IsCreatureOrVehicle() {
        return IsCreature() || IsVehicle();
    }

    boolean IsAnyTypeCreature() {
        return IsCreature() || IsPet() || IsVehicle();
    }

    boolean IsPlayer() {
        return !IsEmpty() && GetHigh() == HighGuid.HIGHGUID_PLAYER;
    }

    boolean IsUnit() {
        return IsAnyTypeCreature() || IsPlayer();
    }

    boolean IsItem() {
        return GetHigh() == HighGuid.HIGHGUID_ITEM;
    }

    boolean IsGameObject() {
        return GetHigh() == HighGuid.HIGHGUID_GAMEOBJECT;
    }

    boolean IsDynamicObject() {
        return GetHigh() == HighGuid.HIGHGUID_DYNAMICOBJECT;
    }

    boolean IsCorpse() {
        return GetHigh() == HighGuid.HIGHGUID_CORPSE;
    }

    boolean IsTransport() {
        return GetHigh() == HighGuid.HIGHGUID_TRANSPORT;
    }

    boolean IsMOTransport() {
        return GetHigh() == HighGuid.HIGHGUID_MO_TRANSPORT;
    }

   
    boolean IsGroup() {
        return GetHigh() == HighGuid.HIGHGUID_GROUP;
    }

   

    static TypeID GetTypeId(HighGuid high) {
        switch (high) {
            case HIGHGUID_ITEM:
                return TypeID.TYPEID_ITEM;
            // case HIGHGUID_CONTAINER:    return TYPEID_CONTAINER; HIGHGUID_CONTAINER==HIGHGUID_ITEM currently
            case HIGHGUID_UNIT:
                return TypeID.TYPEID_UNIT;
            case HIGHGUID_PET:
                return TypeID.TYPEID_UNIT;
            case HIGHGUID_PLAYER:
                return TypeID.TYPEID_PLAYER;
            case HIGHGUID_GAMEOBJECT:
                return TypeID.TYPEID_GAMEOBJECT;
            case HIGHGUID_DYNAMICOBJECT:
                return TypeID.TYPEID_DYNAMICOBJECT;
            case HIGHGUID_CORPSE:
                return TypeID.TYPEID_CORPSE;
            case HIGHGUID_MO_TRANSPORT:
                return TypeID.TYPEID_GAMEOBJECT;
            case HIGHGUID_VEHICLE:
                return TypeID.TYPEID_UNIT;
 
            case HIGHGUID_GROUP:
            default:
                return TypeID.TYPEID_OBJECT;
        }
    }
//

    public TypeID GetTypeId() {
        return GetTypeId(GetHigh());
    }


    static String GetTypeName(HighGuid high) {
        switch (high) {
            case HIGHGUID_ITEM:
                return "Item";
            case HIGHGUID_PLAYER:
                return "Player";
            case HIGHGUID_GAMEOBJECT:
                return "Gameobject";
            case HIGHGUID_TRANSPORT:
                return "Transport";
            case HIGHGUID_UNIT:
                return "Creature";
            case HIGHGUID_PET:
                return "Pet";
            case HIGHGUID_VEHICLE:
                return "Vehicle";
            case HIGHGUID_DYNAMICOBJECT:
                return "DynObject";
            case HIGHGUID_CORPSE:
                return "Corpse";
            case HIGHGUID_MO_TRANSPORT:
                return "MoTransport";
            case HIGHGUID_GROUP:
                return "Group";
            default:
                return "<unknown>";
        }
    }

    String GetTypeName() {
        return !IsEmpty() ? GetTypeName(GetHigh()) : "None";
    }

    public String GetString() {
        StringBuilder str = new StringBuilder();
        str.append(GetTypeName());

        if (IsPlayer()) {

            String name = "HIGUID.UNIMPL";//Managers.getObjectManager().GetPlayerNameByGUID(this);

            if (name != null) {
                str.append(" ").append(name);
            }
        }

        str.append(" (");
        if (HasEntry()) {
            str.append(IsPet() ? "Petnumber: " : "Entry: ").append(GetEntry()).append(" ");
        }
        str.append("Guid: ").append(GetCounter()).append(")");
        return str.toString();
    }


    public static boolean HasEntry(HighGuid high) {
        switch (high) {
            case HIGHGUID_ITEM:
            case HIGHGUID_PLAYER:
            case HIGHGUID_DYNAMICOBJECT:
            case HIGHGUID_CORPSE:
            case HIGHGUID_MO_TRANSPORT:
            case HIGHGUID_GROUP:
                return false;
            case HIGHGUID_GAMEOBJECT:
            case HIGHGUID_TRANSPORT:
            case HIGHGUID_UNIT:
            case HIGHGUID_PET:
            case HIGHGUID_VEHICLE:
            default:
                return true;
        }
    }
//

    public boolean HasEntry() {
        return HasEntry(GetHigh());
    }
//

    static boolean IsLargeHigh(HighGuid high) {
        return false;
    }

    boolean IsLargeHigh() {
        return IsLargeHigh(GetHigh());
    }
    long m_guid;
    short m_guidBytes[] = new short[8];

    private void fillBytes() {
        m_guidBytes[0] = (short) (m_guid >> (64 - 8));
        m_guidBytes[1] = (short) (m_guid >> (64 - 16));
        m_guidBytes[2] = (short) (m_guid >> (64 - 16 - 8));
        m_guidBytes[3] = (short) (m_guid >> (64 - 32));
        m_guidBytes[5] = (short) (m_guid >> (64 - 32 - 8));
        m_guidBytes[6] = (short) (m_guid >> (64 - 32 - 16));
        m_guidBytes[7] = (short) (m_guid >> (64 - 32 - 16 - 8));
    }

    public int getGetByte(int idx) {
        return m_guidBytes[idx];
    }
}
