/*
 * To change this template), choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.guid;

/**
 *
 * @author prog_lf
 */
public enum TypeID {

    TYPEID_OBJECT(0),
    TYPEID_ITEM(1),
    TYPEID_CONTAINER(2),
    TYPEID_UNIT(3),
    TYPEID_PLAYER(4),
    TYPEID_GAMEOBJECT(5),
    TYPEID_DYNAMICOBJECT(6),
    TYPEID_CORPSE(7);
    private int id;

    private TypeID(int id) {
        this.id = id;
    }
}
