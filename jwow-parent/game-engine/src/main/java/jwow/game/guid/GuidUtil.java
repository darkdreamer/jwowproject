/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.guid;

/**
 *
 * @author dreamer
 */
public class GuidUtil {

    public static int getGuidLowPart(long x) {
        return IsGuidHaveEnPart(x)
            ? (int)(x & (0x0000000000FFFFFF))
            : (int)(x & (0x00000000FFFFFFFF));
    }

    public static int guidHigh(long guid) {
        return (int) ((guid >>> 48) & 0x0000FFFF);
    }

    public static boolean IsGuidHaveEnPart(long guid) {

        switch (HighGuid.fromCode(guidHigh(guid))) {
            case HIGHGUID_ITEM:
            case HIGHGUID_PLAYER:
            case HIGHGUID_DYNAMICOBJECT:
            case HIGHGUID_CORPSE:
            case HIGHGUID_GROUP:
                return false;
            case HIGHGUID_GAMEOBJECT:
            case HIGHGUID_TRANSPORT:
            case HIGHGUID_UNIT:
            case HIGHGUID_PET:
            case HIGHGUID_VEHICLE:
            case HIGHGUID_MO_TRANSPORT:
            default:
                return true;
        }

    }
}
