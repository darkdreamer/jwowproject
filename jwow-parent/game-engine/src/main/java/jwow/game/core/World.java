/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jwow.game.core;

import jwow.game.session.WorldSession;

/**
 *
 * @author dreamer
 */
public interface World {
    boolean isClosed();

    void addSession(WorldSession session);

    void update(long diff);
}
