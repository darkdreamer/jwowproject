/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.core;

import jwow.game.session.WorldSession;

/**
 *
 * @author dreamer
 */
public interface AuthQueueManager {
    boolean removeSessionFromQueue(WorldSession session,int sessionCount);
    int playersInQueue();

    void updateQueue(int size);

    public void addSessionToQueue(WorldSession session, int worldSessionCount);
}
