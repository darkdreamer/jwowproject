/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;
import javax.inject.Inject;
import jwow.game.player.Player;
import jwow.game.session.PacketFilter;
import jwow.game.session.ScheduledOpcodeTask;
import jwow.game.session.ScheduledOperationScope;
import jwow.game.session.WorldSession;

/**
 *
 * @author dreamer
 */
public class WorldImpl implements World {

    @Override
    public boolean isClosed() {
        return false;
    }
    @Inject
    AuthQueueManager authQueueManager;
    private static final Logger logger = Logger.getLogger(WorldImpl.class.getName());
    private Queue<WorldSession> newSessionsQueue;

    private Map<Integer, WorldSession> sessions = new HashMap<Integer, WorldSession>();

    public WorldImpl() {
        newSessionsQueue = new ConcurrentLinkedQueue<WorldSession>();
    }

    public void addSession(WorldSession session) {
        newSessionsQueue.add(session);
    }

    private void updateSessions(long msDiff) {
        while (!newSessionsQueue.isEmpty()) {
            processNewSession(newSessionsQueue.remove());
        }

        Iterator<WorldSession> itr = sessions.values().iterator();

        while (itr.hasNext()) {
//    {
//        next = itr;
//        ++next;
//
//        ///- and remove not active sessions from the list
//        WorldSession* pSession = itr->second;
            WorldSession psSession = itr.next();
//        WorldSessionFilter updater(pSession);
//
            if (psSession.update(msDiff, new WorldSessionFilter())) {
//        if (!pSession->Update(diff, updater))    // As interval = 0
//        {
//            if (!RemoveQueuedPlayer(itr->second) && itr->second && getIntConfig(CONFIG_INTERVAL_DISCONNECT_TOLERANCE))
//                m_disconnects[itr->second->GetAccountId()] = time(NULL);
//            RemoveQueuedPlayer(pSession);
//            m_sessions.erase(itr);
//            delete pSession;
//
            }
//    }
        }
    }

    private class WorldSessionFilter implements PacketFilter {

        @Override
        public boolean ProcessLogout() {
            return true;
        }

        @Override
        public boolean Process(ScheduledOpcodeTask task) {
            
      
            
            if(task.getScope()==ScheduledOperationScope.Inplace){
                return true;
            }
            
            if(task.getScope()==ScheduledOperationScope.SessionUpdate){
                return true;
            }

            Player player = task.getSession().getPlayer();

            //no player attached? -> our client! ^^
            if(player==null){
                return true;
            }

            //lets process all packets for non-in-the-world player
            return (player.isInWorld() == false);
        }

    }

    public void update(long msDiff) {
        updateTimers(msDiff);
        updateGameTime(msDiff);
        updateQuests(msDiff);
        updateRandomBGs(msDiff);
        updateAuctions(msDiff);
        updateSessions(msDiff);
        updateWeather(msDiff);

        //TODO: other updates
    }

    private void updateTimers(long diff) {
        ///- Update the different timers
//    for (int i = 0; i < WUPDATE_COUNT; ++i)
//    {
//        if (m_timers[i].GetCurrent() >= 0)
//            m_timers[i].Update(diff);
//        else
//            m_timers[i].SetCurrent(0);
//    }
    }

    private void updateGameTime(long msDiff) {
    }

    private void updateQuests(long msDiff) {
    }

    private void updateRandomBGs(long msDiff) {
    }

    private void updateAuctions(long msDiff) {
    }

    private void updateWeather(long msDiff) {
    }

    private void processNewSession(WorldSession session) {
//         //NOTE - Still there is race condition in WorldSession* being used in the Sockets
//
//    ///- kick already loaded player with same account (if any) and remove session
//    ///- if player is in loading and want to load again, return
        if (!RemoveSession(session.getAccount().getId())) {
            session.kick();
            return;
        }
//
//    // decrease session counts only at not reconnection case
        boolean decrease_session = true;
//
//    // if session already exist, prepare to it deleting at next world update
//    // NOTE - KickPlayer() should be called on "old" in RemoveSession()
        {
//        SessionMap::const_iterator old = m_sessions.find(s->GetAccountId());
            WorldSession ws = sessions.get(session.getAccount().getId());
//
            if (ws != null) {
//            // prevent decrease sessions count if session queued
//            if (RemoveQueuedPlayer(old->second))
//                decrease_session = false;
                authQueueManager.removeSessionFromQueue(ws, sessions.size());
//            // not remove replaced session form queue if listed
//            delete old->second;
            }
        }
//
//    m_sessions[s->GetAccountId()] = s;
        sessions.put(session.getAccount().getId(), session);
//
//    uint32 Sessions = GetActiveAndQueuedSessionCount();
//    uint32 pLimit = GetPlayerAmountLimit();
//    uint32 QueueSize = GetQueuedSessionCount();             //number of players in the queue
//
//    //so we don't count the user trying to
//    //login as a session and queue the socket that we are using
//    if (decrease_session)
//        --Sessions;
//
        authQueueManager.addSessionToQueue(session, sessions.size());

//    if (pLimit > 0 && Sessions >= pLimit && AccountMgr::IsPlayerAccount(s->GetSecurity()) && !HasRecentlyDisconnected(s))
//    {
//        AddQueuedPlayer (s);
//        UpdateMaxSessionCounters();
//        sLog->outDetail("PlayerQueue: Account id %u is in Queue Position (%u).", s->GetAccountId(), ++QueueSize);
//        return;
//    }
//
//    s->SendAuthResponse(AUTH_OK, true);
//    s->SendAddonsInfo();
//    s->SendClientCacheVersion(sWorld->getIntConfig(CONFIG_CLIENTCACHE_VERSION));
//    s->SendTutorialsData();
//
//    UpdateMaxSessionCounters();
//
//    // Updates the population
//    if (pLimit > 0)
//    {
//        float popu = (float)GetActiveSessionCount();              // updated number of users on the server
//        popu /= pLimit;
//        popu *= 2;
//        sLog->outDetail("Server Population (%f).", popu);
//    }
    }

    private boolean RemoveSession(int id) {
//         SessionMap::const_iterator itr = m_sessions.find(id);
//
//    if (itr != m_sessions.end() && itr->second)
//    {
//        if (itr->second->PlayerLoading())
//            return false;
//
//        itr->second->KickPlayer();
//    }
//
//    return true;
        WorldSession session = sessions.get(id);

        if (session != null) {
            if (session.isPlayerLoading()) {
                return false;
            }

            session.kick();
        }

        return true;
    }
}
