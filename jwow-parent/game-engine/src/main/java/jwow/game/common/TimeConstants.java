/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.common;

/**
 *
 * @author prog_lf
 */
public interface TimeConstants {

    public static final int MINUTE = 60,
            HOUR = MINUTE * 60,
            DAY = HOUR * 24,
            WEEK = DAY * 7,
            MONTH = DAY * 30,
            YEAR = MONTH * 12,
            IN_MILLISECONDS = 1000;
}
