/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object.layout;

/**
 *
 * @author prog_lf
 */
public class ByteField extends GoFieldAccessor<Byte> {

    public ByteField() {
        super(Byte.class);
    }

    public void set(int value) {
        set((byte)value);
    }
}
