/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object.layout;

/**
 *
 * @author prog_lf
 */
public class Int32Field extends GoFieldAccessor<Integer> {

    public Int32Field() {
        super(Integer.class);
    }
    
    public void set(int value){
        set(Integer.valueOf(value));
    }
}
