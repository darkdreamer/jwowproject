/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object.layout;

/**
 *
 * @author prog_lf
 */
public abstract class PropertyAccessor <T> {
    protected GoFieldAccessor<T> realAccessor;
    
    public abstract void set(T value);
    public abstract T get();
}
