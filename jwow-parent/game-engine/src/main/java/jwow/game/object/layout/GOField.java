/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object.layout;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author prog_lf
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface GOField {
    int offset();
    int byteOffset() default 0;
    
}
