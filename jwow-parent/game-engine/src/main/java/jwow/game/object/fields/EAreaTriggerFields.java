/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object.fields;

/**
 *
 * @author prog_lf
 */
public interface EAreaTriggerFields extends EObjectFields {

    public static final int AREATRIGGER_SPELLID = OBJECT_END + 0x0,
            AREATRIGGER_SPELLVISUALID = OBJECT_END + 0x1,
            AREATRIGGER_DURATION = OBJECT_END + 0x2,
            AREATRIGGER_FINAL_POS = OBJECT_END + 0x3,
            AREATRIGGER_END = OBJECT_END + 0x6;
}
