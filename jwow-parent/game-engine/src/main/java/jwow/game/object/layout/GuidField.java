/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object.layout;

import jwow.game.guid.ObjectGuid;

/**
 *
 * @author prog_lf
 */
public class GuidField extends PropertyGoFieldAccessor<ObjectGuid> {

    public GuidField() {
        super(ObjectGuid.class);
    }
    
    public GuidField(PropertyAccessor<ObjectGuid> acessor) {
        super(acessor,ObjectGuid.class);
    }
    
}
