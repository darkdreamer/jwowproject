/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object;

import jwow.game.guid.ObjectGuid;
import jwow.game.guid.TypeID;
import jwow.game.guid.TypeMask;
import jwow.game.object.fields.EObjectFields;
import jwow.game.object.layout.FloatField;
import jwow.game.object.layout.GODataLayout;
import jwow.game.object.layout.GOField;
import jwow.game.object.layout.GuidField;
import jwow.game.object.layout.Int32Field;

/**
 *
 * @author prog_lf
 */
public class GameObject {
    protected final GODataLayout dataLayout = new GODataLayout();
    
    boolean m_inWorld;
    
    
    short m_objectType;
    TypeID m_objectTypeId;
    
    @GOField(offset=EObjectFields.OBJECT_FIELD_GUID)
    public final GuidField guid = new GuidField();
    @GOField(offset=EObjectFields.OBJECT_FIELD_TYPE)
    public final Int32Field type = new Int32Field();
    @GOField(offset=EObjectFields.OBJECT_FIELD_SCALE_X)
    public final FloatField scaleX = new FloatField();
    @GOField(offset=EObjectFields.OBJECT_FIELD_ENTRY)
    public final Int32Field entry = new Int32Field();

    public GameObject() {
        m_inWorld=false;
        m_objectTypeId=TypeID.TYPEID_OBJECT;
        m_objectType=TypeMask.TYPEMASK_OBJECT;
    }
    
    protected final void initGameObjectLayout(){
        dataLayout.bind(this);
    }
    
    
    
    public boolean isInWorld() {
        return m_inWorld;
    }

    void AddToWorld() {
        if (m_inWorld) {
            return;
        }

        m_inWorld = true;

        // synchronize values mirror with values array (changes will send in updatecreate opcode any way
        dataLayout.clearUpdateMask();                         // false - we can't have update data in update queue before adding to world
    }

    public ObjectGuid getObjectGuid() {
        return guid.get();
    }

    public int getGUIDLow() {
        return getObjectGuid().GetCounter();
    }


    public String getGuidString() {
        return getObjectGuid().GetString();
    }

    public int getEntry() {
        return entry.get();
    }

    public void setEntry(int entry) {
        this.entry.set(entry);
    }

    public float getObjectScale() {
        float scale = scaleX.get();
        return scale != 0 ? scale : ObjectConstants.DEFAULT_OBJECT_SCALE;
    }

    public TypeID getTypeId() {
        return m_objectTypeId;
    }
    
    //TypeMask interface
    public boolean isType(int mask) {
        return (mask & m_objectType) == mask;
    }

    public void setObjectScale(float newScale){
        scaleX.set(newScale);
    }
}
