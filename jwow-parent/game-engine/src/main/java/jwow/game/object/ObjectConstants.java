/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object;

/**
 *
 * @author prog_lf
 */
public interface ObjectConstants {

    public static final float CONTACT_DISTANCE = 0.5f;
    public static final float INTERACTION_DISTANCE = 5.0f;
    public static final float ATTACK_DISTANCE = 5.0f;
    public static final float MAX_VISIBILITY_DISTANCE = 333.0f;      // max distance for visible object show, limited in 333 yards
    public static final float DEFAULT_VISIBILITY_DISTANCE = 90.0f;       // default visible distance, 90 yards on continents
    public static final float DEFAULT_VISIBILITY_INSTANCE = 120.0f;      // default visible distance in instances, 120 yards
    public static final float DEFAULT_VISIBILITY_BGARENAS = 180.0f;      // default visible distance in BG/Arenas, 180 yards
    public static final float DEFAULT_WORLD_OBJECT_SIZE = 0.388999998569489f;      // currently used (correctly?) for any non Unit world objects. This is actually the bounding_radius, like player/creature from creature_model_data
    public static final float DEFAULT_OBJECT_SCALE = 1.0f;                    // player/item scale as default, npc/go from database, pets from dbc
    public static final float MAX_STEALTH_DETECT_RANGE = 45.0f;
}
