/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object.layout;

import java.lang.reflect.Field;
import jwow.game.guid.ObjectGuid;
import jwow.game.object.GameObject;

/**
 *
 * @author prog_lf
 */
public class GODataLayout {

    public void clearUpdateMask() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public ObjectGuid getGuidValue(int OBJECT_FIELD_GUID) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    public void setGuidValue(int offset, ObjectGuid guid) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public int getInt32Value(int OBJECT_FIELD_ENTRY) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void setInt32Value(int OBJECT_FIELD_ENTRY, int entry) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public float getFloatValue(int OBJECT_FIELD_SCALE_X) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void setFloatValue(int OBJECT_FIELD_SCALE_X, float newScale) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void bind(GameObject aThis) {
        Class cls = aThis.getClass();

        while (cls != null) {
            for (Field field : cls.getDeclaredFields()) {
                GOField declaration = field.getAnnotation(GOField.class);

                if (declaration != null) {
                    if (!GoFieldAccessor.class.isAssignableFrom(field.getType())) {
                        throw new RuntimeException("Field " + field.getName() + " of class " + cls.getName() + " is marked as GOField, but is not GoFieldAccessor subtype!");
                    }

                    GoFieldAccessor accessor = reflexGetValueOrDie(aThis, field);
                    
                    accessor.initAccessor(declaration, this);
                }
            }
            
            cls = cls.getSuperclass();
        }
    }

    private GoFieldAccessor reflexGetValueOrDie(Object instance, Field f) {
        boolean acc = f.isAccessible();
        f.setAccessible(true);
        try {
            return GoFieldAccessor.class.cast(f.get(instance));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            f.setAccessible(acc);
        }
    }
}
