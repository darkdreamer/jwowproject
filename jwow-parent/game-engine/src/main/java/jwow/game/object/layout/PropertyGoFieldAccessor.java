/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object.layout;

/**
 *
 * @author prog_lf
 */
public class PropertyGoFieldAccessor<T> extends GoFieldAccessor<T> {

    final private GoFieldAccessor<T> realAccessor;
    private PropertyAccessor<T> accessor;

    public PropertyGoFieldAccessor(PropertyAccessor<T> accessor, Class<T> tClass) {
        super(tClass);
        this.accessor = accessor;
        this.realAccessor = new GoFieldAccessor<T>(tClass);
        this.accessor.realAccessor = this.realAccessor;
    }

    public PropertyGoFieldAccessor(Class<T> tClass) {
        super(tClass);
        realAccessor = new GoFieldAccessor<T>(tClass);
    }

    @Override
    public void initAccessor(GOField descriptor, GODataLayout dataLayout) {
        realAccessor.initAccessor(descriptor, dataLayout);
    }

    @Override
    public void set(T value) {
        if (accessor == null) {
            realAccessor.set(value);
        } else {
            accessor.set(value);
        }
    }

    @Override
    public T get() {
        if (accessor == null) {
            return realAccessor.get();
        } else {
            return accessor.get();
        }
    }
}
