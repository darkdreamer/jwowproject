/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object.fields;

/**
 *
 * @author prog_lf
 */
public interface EContainerFields extends EItemFields {
    public static final int CONTAINER_FIELD_NUM_SLOTS                 = ITEM_END + 0x0000, // Size: 1, Type: INT, Flags: PUBLIC
    CONTAINER_ALIGN_PAD                       = ITEM_END + 0x0001, // Size: 1, Type: BYTES, Flags: NONE
    CONTAINER_FIELD_SLOT_1                    = ITEM_END + 0x0002, // Size: 72, Type: LONG, Flags: PUBLIC
    CONTAINER_END                             = ITEM_END + 0x004A;
}
