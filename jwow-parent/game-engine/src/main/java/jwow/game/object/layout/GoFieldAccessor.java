/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jwow.game.object.layout;

import jwow.game.guid.ObjectGuid;

/**
 *
 * @author prog_lf
 */
public class GoFieldAccessor<T> {
    
    private Class _tClass;
    private GODataLayout dataLayout;
    private GOField goField;
    
    public GoFieldAccessor(Class<T> tClass) {
        _tClass=tClass;
    }
    
    public void initAccessor(GOField descriptor,GODataLayout dataLayout){
        this.dataLayout=dataLayout;
        this.goField=descriptor;
    }
    
    public void set(T value){
        if(_tClass.equals(ObjectGuid.class)){
            dataLayout.setGuidValue(goField.offset(),(ObjectGuid)value);
        }
    }
    
    public T get(){
        throw new RuntimeException();
    }
}
